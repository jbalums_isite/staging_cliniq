$(function(x){

		$(document).off('click', '.side-menu a').on('click', '.side-menu a', function(e){
			e.preventDefault();

			var ajaxURL = $(this).attr('href');


			loadPage(ajaxURL);


			var a_href = ajaxURL.split('/'); 
			var location_href = window.location.href.split('/');
			var str =  "/"+location_href[3]+"/"+a_href[(a_href.length-1)]; 

			if(!$('body').hasClass('nav-md')){
				$("#menu_toggle").click();
			}

			history.pushState({}, null,""+str);
		});

		$(document).off('click', '.dashboard_menu_ li').on('click', '.dashboard_menu_ li', function(e){
			e.preventDefault();

			var ajaxURL = $(this).attr('href'); 

			if(!$(this).hasClass('active')){
				loadPage(ajaxURL);			
			
				var a_href = ajaxURL.split('/'); 
				var location_href = window.location.href.split('/');
				var str =  "/"+location_href[3]+"/"+a_href[(a_href.length-1)]; 
				
				history.pushState({}, null,""+str);
			}

		});

		$(document).off('click', '.ajax_btn').on('click', '.ajax_btn', function(e){
			e.preventDefault();

			var ajaxURL = $(this).attr('href');
			
			if(!$(this).hasClass('active')){
				loadPage(ajaxURL);			
				
				var a_href = ajaxURL.split('/'); 
				var location_href = window.location.href.split('/');
				var str =  "/"+location_href[3]+"/"+a_href[(a_href.length-1)]; 
				
				history.pushState({}, null,""+str);
			}

		});

		
	
});
function loadPage(ajaxURL){
		if(ajaxURL != null){
				$.ajax({
				    // Adds a new body class, preps the content container, and adds a css animation
					beforeSend: function() {
					  $('#content_wrapper').addClass('animated animated-shortest fadeOut');
					  NProgress.start();
					},
					url: ajaxURL,
					cache:false, 
					success: function(data) {
					   $('.pg_content').html(data);					   
					},
					// On Complete reInit checkbox plugin and remove css animations
					complete: function() {
        				NProgress.done();
					}	  
				});
			}
	}	
function notiff(title, msg, type){
	new PNotify({
	    title: title,
	    text: msg,
	    type: type,
	    styling: 'bootstrap3'
	});
}