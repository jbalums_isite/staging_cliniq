<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMedicationIdMedDescriptionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('medication_descriptions', function (Blueprint $table) {

            $table->unsignedInteger('medication_id');
            $table->foreign('medication_id')->references('id')->on('medications');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('medication_descriptions', function (Blueprint $table) {
            //
        });
    }
}
