<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBillingMastersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('billing_masters', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('appointment_id'); 
            $table->timestamps();
            
            $table->foreign('appointment_id')->references('id')->on('appointments'); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('billing_masters');
    }
}
