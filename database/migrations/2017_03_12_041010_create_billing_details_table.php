<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBillingDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('billing_details', function (Blueprint $table) {
            $table->increments('id'); 
            $table->unsignedInteger('master_id');
            $table->string('record_type');
            $table->integer('record_id');
            $table->double('qty');
            $table->double('amount');
            $table->double('discount');
            $table->boolean('percentage'); 
            $table->timestamps();

            $table->foreign('master_id')->references('id')->on('billing_masters'); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('billing_details');
    }
}
