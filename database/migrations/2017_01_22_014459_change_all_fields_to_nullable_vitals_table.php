<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeAllFieldsToNullableVitalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vitals', function (Blueprint $table) {
            //
            $table->date('date')->nullable()->change();
            $table->string('weight', 30)->nullable()->change();
            $table->string('temperature', 30)->nullable()->change();
            $table->string('pulse', 30)->nullable()->change();
            $table->string('blood_pressure', 30)->nullable()->change();
            $table->string('respiration', 30)->nullable()->change();
            $table->string('pain', 30)->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vitals', function (Blueprint $table) {
            //
        });
    }
}
