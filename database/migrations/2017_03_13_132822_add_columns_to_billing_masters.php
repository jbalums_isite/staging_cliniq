<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToBillingMasters extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('billing_masters', function (Blueprint $table) {
            $table->double('change');
            $table->double('total_payable');
            $table->double('paid_amount'); 
            $table->double('discount');
            $table->boolean('percentage');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('billing_masters', function (Blueprint $table) {
            $table->dropColumn('change');
            $table->dropColumn('total_payable');
            $table->dropColumn('paid_amount');
        });
    }
}
