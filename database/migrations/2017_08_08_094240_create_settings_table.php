<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->boolean('dasboard')->nullable();
            $table->boolean('patient')->nullable(); 
            $table->boolean('medicine')->nullable();
            $table->boolean('services')->nullable();
            $table->boolean('view_products')->nullable();
            $table->boolean('stocks')->nullable();
            $table->boolean('manufacturers')->nullable();   
            $table->boolean('brands')->nullable();
            $table->boolean('schedules')->nullable();
            $table->boolean('referral_form')->nullable();
            $table->boolean('request_form')->nullable();
            $table->boolean('custom_form')->nullable();
            $table->boolean('dialy_reports')->nullable();
            $table->boolean('monthly_reports')->nullable();
            $table->boolean('quarterly_reports')->nullable();
            $table->boolean('annual_reports')->nullable();
            $table->boolean('users')->nullable();
            $table->boolean('allergies')->nullable();
            $table->timestamps();
            
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
