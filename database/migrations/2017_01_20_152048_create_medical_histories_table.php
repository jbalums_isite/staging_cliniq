<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMedicalHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('medical_histories', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date');
            $table->unsignedInteger('patient_id');
            $table->string('name');
            $table->text('description');
            $table->text('attached_file');
            $table->enum('history_type', ['medical_history', 'surgical_history', 'social_history', 'family_history']);
            $table->timestamps();

            $table->foreign('patient_id')->references('id')->on('patients');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('medical_histories');
    }
}
