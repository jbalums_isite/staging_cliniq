<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToBillingMaster extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('billing_masters', function (Blueprint $table) {
            
            $table->text('address')->nullable();
            $table->text('notes_medication')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('billing_masters', function (Blueprint $table) {
            $table->dropColumn('address');
            $table->dropColumn('address');
        });
    }
}
