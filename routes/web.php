<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
 /*
Route::get('/', function () {
    return view('pages.home');
});*/



Auth::routes();
//.\App\ClinicAccounts::find(1)->prefix
Route::group(['middleware' => ['auth']], function () {
      Route::get('/', 'HomeController@index');
      //Route::get('/johndoe',  'HomeController@index');  

      // Route::get('/login', function(){
      //             if (!Auth::check()) {
      //               return view('auth.login');
      //             }else{
      //                   return redirect('/home');
      //             }
      // });

      Route::get('get_logs', function(){
            return view('admin/logs');
      });
      Route::resource('/patients', 'PatientRecordsController');
      Route::get('/get-patients', 'PatientRecordsController@get_patients');
      /*Route::get('/get-allergies/{id}', 'PatientRecordsController@get_allergies');
      Route::get('/get-hist/{id}/{type}', 'PatientRecordsController@get_hist');
      Route::get('/get-result/{id}/{type}', 'PatientRecordsController@get_result');
      Route::get('/get-meds/{id}', 'PatientRecordsController@get_meds');
      Route::get('/get-vitals/{id}', 'PatientRecordsController@get_vitals');
      Route::get('/get-notes/{id}', 'PatientRecordsController@get_notes');*/
      Route::get('/get-last-patient-id', 'PatientRecordsController@get_last_patient_id');
      Route::post('/update_profile_data', 'PatientRecordsController@update_profile_data');
      Route::post('/delete_profile_data', 'PatientRecordsController@delete_profile_data');
      Route::get('/get-meds-desc', 'PatientRecordsController@get_meds_desc');

      Route::get('/getAllRecords/{patient_id}/{table}', 'PatientRecordsController@getAllRecords');
      Route::get('/getAllVitals/{patient_id}', 'PatientRecordsController@getAllVitals');
      Route::get('/getAllMeds/{patient_id}', 'PatientRecordsController@getAllMeds');
      Route::get('/getAllLabRes/{patient_id}', 'PatientRecordsController@getAllLabRes');
      Route::get('/getAllMedHists/{patient_id}', 'PatientRecordsController@getAllMedHists');

      Route::get('/upload-form', 'PatientRecordsController@upload_form');

      Route::get('image-upload','ImageController@imageUpload');
      Route::get('view-images', 'ImageController@view_images');
      Route::post('image-upload','ImageController@imageUploadPost');
      Route::post('/uploadPicture','ImageController@uploadPicture');

      Route::resource('/allergies', 'AllergiesController');
      Route::resource('/medicalhistories', 'MedicalHistoriesController');
      Route::resource('/results', 'ResultsController');
      Route::resource('/medications', 'MedicationsController');
      Route::resource('/vitals', 'VitalsController');
      Route::resource('/notes', 'NotesController');
      Route::get('/patient/send/{id}', 'PatientRecordsController@send_patient_record');
      Route::get('/patient/print/{id}', 'PatientRecordsController@print_patient_record');
      Route::get('/patient/download/{id}', 'PatientRecordsController@download_patient_record');


      Route::resource('/products', 'ProductsController');
      Route::get('/get-products', 'ProductsController@get_products');


      Route::resource('/manufacturers', 'ManufacturersController');
      Route::get('/get-manufacturers', 'ManufacturersController@get_manufacturers');

      Route::resource('/brands', 'BrandsController');
      Route::get('/get-brands', 'BrandsController@get_brands');

      Route::resource('/inventories', 'InventoriesController');
      Route::get('/get-inventories', 'InventoriesController@get_inventories'); 
      Route::get('/get-inventory-histories', 'InventoriesController@get_inventory_histories'); 

      Route::resource('/medicines', 'MedicinesController');
      Route::get('/get-medicines', 'MedicinesController@get_medicines');

      Route::resource('/services', 'ServicesController');
      Route::get('/get-services', 'ServicesController@get_services');

      Route::resource('/schedules', 'SchedulesController');
      Route::get('/check-schedule', 'SchedulesController@check_schedule');

      Route::resource('/users', 'UsersController');
      Route::get('/get-users', 'UsersController@get_users');
      Route::get('/check-password/{id}', 'UsersController@check_password');
      Route::patch('/changepassword/{id}', 'ChangePasswordController@changePassword'); 
      Route::resource('/settings', 'SettingController');

      Route::get('/get_all_appointments/{date}', 'SchedulesController@get_all_appointments');
      Route::get('/get_all_other_appointments/{date}', 'SchedulesController@get_all_other_appointments');
      Route::get('/get_apt_patient_name/{id}', 'SchedulesController@get_apt_patient_name');

      Route::resource('/brands', 'BrandsController');
      Route::get('/get-brands', 'BrandsController@get_brands');

      Route::resource('/transactions', 'TransactionController');
      Route::get('/get-transactions', 'TransactionController@get_transactions');
      Route::get('/get-appointment-details/{id}', 'TransactionController@get_appointment_details');
      Route::get('/get-record-details/{id}/{type}', 'TransactionController@get_record_details');
      Route::get('/print/{id}', 'TransactionController@print_invoice');

      Route::resource('/reports', 'ReportsController');
      Route::get('/daily_sales_reports', 'ReportsController@daily_sales_reports');
      Route::get('/get-sales-report/{start}/{end}', 'ReportsController@get_sales_report');

      Route::get('/monthly_sales_reports', 'ReportsController@monthly_sales_reports');
      Route::get('/get-monthly-sales-report/{yr}', 'ReportsController@get_sales_report_monthly');

      Route::get('/quarterly_sales_reports', 'ReportsController@quarterly_sales_reports');
      Route::get('/get-quarterly-sales-report/{yr}', 'ReportsController@get_sales_report_quarterly');

      Route::get('/annualy_sales_reports', 'ReportsController@annualy_sales_reports');
      Route::get('/get-annually-sales-report/{start}/{end}', 'ReportsController@get_sales_report_annualy' );

      Route::resource('/forms', 'FormsController');
      Route::get('/custom_form', 'FormsController@custom_form');
      Route::get('/request_form', 'FormsController@request_form');
      Route::get('/referral_form', 'FormsController@referral_form');

      Route::resource('/allergieslist', 'AllergiesListsController');
      Route::get('/get_allergieslist', 'AllergiesListsController@get_allergieslist');
      Route::get('/get_allergy_details/{id}', 'AllergiesListsController@get_allergy_details');


      Route::resource('/set-up', 'AccountSetUpController'); 

      Route::get('/new_page/{page}',function($page){
      	$clinic = \App\ClinicAccounts::where('id', Auth::user()->clinic_id)->get()->first();
      	$link_names = array("patients",
                  "medicines",
                  "services",
                  "products",
                  "inventories",
                  "manufacturers",
                  "brands",
                  "schedules",
                  "transactions",
                  "referral_form",
                  "request_form",
                  "custom_form",
                  "daily_sales_reports",
                  "monthly_sales_reports",
                  "quarterly_sales_reports",
                  "annualy_sales_reports",
                  "users",
                  "allergieslist",
                  "transactions");
      	if(in_array($page, $link_names)){
      		return redirect(url($clinic->prefix.'/'.$page))->with('non_ajax', true); 		
      	}else{
      		return redirect('/home');
      	}
      });

      Route::get('/error_404', function(){
            return view('errors.404');
      });

      Route::post('/error_404', function(){
            return view('errors.404');
      });

      Route::patch('/error_404', function(){
            return view('errors.404');
      });


});
/*Route::get('{name}', function($name){
      $link_names = array("patients",
            "medicines",
            "services",
            "products",
            "inventories",
            "manufacturers",
            "brands",
            "schedules",
            "transactions",
            "referral_form",
            "request_form",
            "custom_form",
            "daily_sales_reports",
            "monthly_sales_reports",
            "quarterly_sales_reports",
            "annualy_sales_reports",
            "users",
            "allergieslist",
            "transactions");
      if(in_array($name, $link_names)){
            return redirect('/');
      }
});*/
/*Route::get('/{name}/{page}',function($nane,$page){ 

		return redirect('/home');
	
	$link_names = array("patients",
            "medicines",
            "services",
            "products",
            "inventories",
            "manufacturers",
            "brands",
            "schedules",
            "transactions",
            "referral_form",
            "request_form",
            "custom_form",
            "daily_sales_reports",
            "monthly_sales_reports",
            "quarterly_sales_reports",
            "annualy_sales_reports",
            "users",
            "allergieslist",
            "transactions");
	if(in_array($page, $link_names)){
		return view('non_ajax')->with('url', $page)->with('non_ajax', true); 		
	}else{
		return redirect('/home');
	} 
});*/

// Route::get('/{name}', function($name){ 
// 		$clinic = \App\ClinicAccounts::where('prefix', $name)->get()->first();
// 		if(sizeof($clinic) > 0){
// 			if (!Auth::check()) {
// 		        $clinic = \App\ClinicAccounts::where('prefix', $name)->get()->first();
// 		        return view('auth.login')->with('clinic', $clinic);
// 			}else{
// 				return redirect('/home');
// 			}		
// 		}else{
// 			return redirect('/set-up');
// 		}
// }) ;