<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Appointments extends Model
{
    
    public function doctor(){
    	return $this->belongsTo('\App\Users', 'doctor_id');
    }
    
    public function patient(){
    	return $this->belongsTo('\App\Patients', 'patient_id');
    }
}
