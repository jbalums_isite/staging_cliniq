<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Users extends Model
{
    
    use SoftDeletes;

    protected $dates = ['deleted_at'];


    public function people(){
    	return $this->belongsTo('\App\People', 'people_id');
    }

    public function clinic(){
    	return $this->belongsTo('\App\ClinicAccounts', 'clinic_id');
    }
    public function setting(){
    	return $this->hasOne('\App\Settings', 'user_id', 'id');
    }

}
