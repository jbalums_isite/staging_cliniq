<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use Datatables;
use Auth;
class AllergiesListsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        return view('allergieslist.allergieslist');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('allergieslist.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $allergy = new \App\AllergiesList;

        $allergy->name          = $request->name;
        $allergy->description   = $request->description; 
        
        if($allergy->save())
        {
            activity() 
           ->withProperties(['id' => $allergy->id, 'name' => $allergy->name, 'table' => 'Allergy'])
           ->log('create');
            return response()->json(['success' => true, 'msg' => 'Record successfully Added']);
        }else{
            return response()->json(['success' => false, 'msg' => 'An error occured while adding a new record!']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $allergy = \App\AllergiesList::find($id);
        return view('allergieslist.edit')->with('allergy', $allergy);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $allergy = \App\AllergiesList::find($id);

        $allergy->name          = $request->name;
        $allergy->description   = $request->description; 
        
        if($allergy->save())
        {
            activity() 
           ->withProperties(['id' => $allergy->id, 'name' => $allergy->name, 'table' => 'Allergy'])
           ->log('update');
            return response()->json(['success' => true, 'msg' => 'Record successfully Updated']);
        }else{
            return response()->json(['success' => false, 'msg' => 'An error occured while updating a new record!']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $patient = \App\AllergiesList::find($id);
            activity() 
           ->withProperties(['id' => $patient->id, 'name' => $patient->name, 'table' => 'Allergy'])
           ->log('delete');
        
        if($patient->delete()){
            return response()->json(['success' => true, 'msg' => 'Record successfully removed!']);
        }else{
            return response()->json(['success' => true, 'msg' => 'An error occured while removing record!']);
        }
    }

    public function get_allergieslist(){

        DB::statement(DB::raw('set @row:=0'));
        $allergies = \App\AllergiesList::selectRaw('*, @row:=@row+1 as row');

         return Datatables::of($allergies)

            ->AddColumn('row', function($column){
               return $column->row;
            })
            ->AddColumn('name', function($column){
               return $column->name;
            })            
            ->AddColumn('description', function($column){
               return $column->description;
            })
            ->AddColumn('actions', function($column){
                return '<div class="btn-group table-dropdown">
                            <button data-toggle="dropdown" class="btn-xs btn-for-table dropdown-toggle">
                                
                                <span class="ace-icon fa fa-ellipsis-h icon-on-center"></span>
                            </button>

                            <ul class="dropdown-menu dropdown-caret dropdown-menu-right">
                                <li>
                                    <a href="#" class="edit-patient-btn edit-mf-btn" data-id="'.$column->id.'">Edit</a>
                                </li> 
                                <li>
                                    <a href="#" class="delete-patient-btn dele-mf-btn" data-id="'.$column->id.'">Delete</a>
                                </li> 
                            </ul>
                        </div>';
            })

            ->make(true); 
    }

    public function get_allergy_details($id){
        $allergy = \App\AllergiesList::where('id', $id)->get();

        return response()->json(['data' => $allergy]);
    }
}
