<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use Datatables;
use Auth;

class MedicinesController extends Controller
{
   /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        return view('medicines.medicines');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('medicines.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $medicine = new \App\Medicines;

        $medicine->name              = $request->name;
        $medicine->description       = $request->description;
        $medicine->price             = 0;//$request->price;
        $medicine->clinic_id         = Auth::user()->clinic_id;

        if($medicine->save())
        {
            activity() 
           ->withProperties(['id' => $medicine->id, 'name' => $medicine->name, 'table' => 'Medicine'])
           ->log('create');
            return response()->json(['success' => true, 'msg' => 'Record successfully Added']);
        }else{
            return response()->json(['success' => false, 'msg' => 'An error occured while adding a new record!']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    { 
        $medicine = \App\Medicines::find($id);
        return view('medicines.edit')->with('medicine', $medicine);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $medicine = \App\Medicines::find($id);

        $medicine->name              = $request->name;
        $medicine->description       = $request->description;
        $medicine->price             = $request->price; 

        if($medicine->save())
        {
            activity() 
           ->withProperties(['id' => $medicine->id, 'name' => $medicine->name, 'table' => 'Medicine'])
           ->log('update');
            return response()->json(['success' => true, 'msg' => 'Record successfully Updated']);
        }else{
            return response()->json(['success' => false, 'msg' => 'An error occured while updating a new record!']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $medicine = \App\Medicines::find($id);
        
            activity() 
           ->withProperties(['id' => $medicine->id, 'name' => $medicine->name, 'table' => 'Medicine'])
           ->log('delete');
        if($medicine->delete()){
            return response()->json(['success' => true, 'msg' => 'Record successfully removed!']);
        }else{
            return response()->json(['success' => true, 'msg' => 'An error occured while removing record!']);
        }
    }

    public function get_medicines(){
        

        DB::statement(DB::raw('set @row:=0'));
        $medicines = \App\Medicines::selectRaw('*, @row:=@row+1 as row');

         return Datatables::of($medicines)
            ->AddColumn('row', function($column){
               return $column->row;
            })
            ->AddColumn('name', function($column){
               return $column->name;
            })
            ->AddColumn('description', function($column){
               return $column->description;
            })
            ->AddColumn('price', function($column){
               return number_format($column->price, 2);
            })
            ->AddColumn('actions', function($column){
                return '<div class="btn-group table-dropdown">
                            <button data-toggle="dropdown" class="btn-xs btn-for-table dropdown-toggle">
                                
                                <span class="ace-icon fa fa-ellipsis-h icon-on-center"></span>
                            </button>

                            <ul class="dropdown-menu dropdown-caret dropdown-menu-right">
                                <li>
                                    <a href="#" class="edit-patient-btn edit-mf-btn" data-id="'.$column->id.'">Edit</a>
                                </li> 
                                <li>
                                    <a href="#" class="delete-patient-btn dele-mf-btn" data-id="'.$column->id.'">Delete</a>
                                </li> 
                            </ul>
                        </div>';
               //return '<a href="#" class="editBtn edit-mf-btn" data-id="'.$column->p_id.'"><i class="fa fa-edit"></i></a>';
            })

            ->make(true);    
    }
}

