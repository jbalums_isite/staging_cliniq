<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use Datatables;
use Auth;

class ManufacturersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        return view('manufacturers.manufacturers');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('manufacturers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $manufacturer = new \App\Manufacturers;
        $manufacturer->name         = $request->name;
        $manufacturer->clinic_id    = Auth::user()->clinic_id; 
        if($manufacturer->save())
        {
            activity() 
           ->withProperties(['id' => $manufacturer->id, 'name' => $manufacturer->name, 'table' => 'Manufacturer'])
           ->log('create');
            return response()->json(['success' => true, 'msg' => 'Record successfully Added']);
        }else{
            return response()->json(['success' => false, 'msg' => 'An error occured while adding a new record!']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $manufacturers = \App\Manufacturers::find($id);
        return view('manufacturers.edit')->with('manufacturers', $manufacturers);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $manufacturer = \App\Manufacturers::find($id);
        $manufacturer->name         = $request->name; 

        if($manufacturer->save())
        {
            activity() 
           ->withProperties(['id' => $manufacturer->id, 'name' => $manufacturer->name, 'table' => 'Manufacturer'])
           ->log('update');
            return response()->json(['success' => true, 'msg' => 'Record successfully updated']);
        }else{
            return response()->json(['success' => false, 'msg' => 'An error occured while updating a new record!']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $manufacturer = \App\Manufacturers::find($id);
            activity() 
           ->withProperties(['id' => $manufacturer->id, 'name' => $manufacturer->name, 'table' => 'Manufacturer'])
           ->log('update');
        
        if($manufacturer->delete()){
            return response()->json(['success' => true, 'msg' => 'Record successfully removed!']);
        }else{
            return response()->json(['success' => true, 'msg' => 'An error occured while removing record!']);
        }
    }

    public function get_manufacturers(){

        DB::statement(DB::raw('set @row:=0'));
        $manufacturers = \App\Manufacturers::selectRaw('*, @row:=@row+1 as row');

         return Datatables::of($manufacturers)
            ->AddColumn('row', function($column){
               return $column->row;
            })
            ->AddColumn('name', function($column){
               return $column->name;
            })
            ->AddColumn('actions', function($column){
                return '<div class="btn-group table-dropdown">
                            <button data-toggle="dropdown" class="btn-xs btn-for-table dropdown-toggle">
                                
                                <span class="ace-icon fa fa-ellipsis-h icon-on-center"></span>
                            </button>

                            <ul class="dropdown-menu dropdown-caret dropdown-menu-right">
                                <li>
                                    <a href="#" class="edit-patient-btn edit-mf-btn" data-id="'.$column->id.'">Edit</a>
                                </li> 
                                <li>
                                    <a href="#" class="delete-patient-btn dele-mf-btn" data-id="'.$column->id.'">Delete</a>
                                </li> 
                            </ul>
                        </div>';
            })
            ->make(true);    
    }
}
