<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use Validator;
use Redirect; 
use Session;

class ApplyController extends Controller {
	
	public function upload(Request $request) {

		var_dump($request->get('image'));
		return;
	  // getting all of the post data
	  $file = array('image' => Input::file('image'));
	  // setting up rules
	  $rules = array('image' => 'required',); //mimes:jpeg,bmp,png and for max size max:10000
	  // doing the validation, passing post data, rules and the messages
	  var_dump($file);
	  return;
	  $validator = Validator::make($file, $rules);
	  if ($validator->fails()) {
	    // send back to the page with the input data and errors
	    echo "false";
	    //return Redirect::to('upload')->withInput()->withErrors($validator);
	  }
	  else {
	    // checking file is valid.
	    if (Input::file('image')->isValid()) {
	      $destinationPath = 'uploads'; // upload path
	      $extension = Input::file('image')->getClientOriginalExtension(); // getting image extension
	      $fileName = rand(111111,999999).'.'.$extension; // renameing image



	      Input::file('image')->move($destinationPath, $fileName); // uploading file to given path
	      return response()->json(['success' => true, 'msg' => 'Image successfully Added']);
	      // sending back with message
	      // Session::flash('success', 'Upload successfully'); 
	      // return Redirect::to('upload');
	    }
	    else {

	      return response()->json(['success' => false, 'msg' => 'An Error Occured while adding image!']);
	      // sending back with error message.
	      // Session::flash('error', 'uploaded file is not valid');
	      // return Redirect::to('upload');
	    }
	  }
	}
}