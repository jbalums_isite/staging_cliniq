<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use Datatables;
use Auth;
class MedicationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $medicines = \App\Medicines::where('clinic_id',Auth::user()->clinic_id)->pluck('name', 'ID');
        return view('medications.create')->with('patient_id', $request->patient_id)->with('medicines', $medicines);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{

            DB::beginTransaction();

            $medication = new \App\Medications;
            $medication->patient_id     = $request->patient_id;
            $medication->date           = $request->date;
            $medication->save();

            for($i = 0 ; $i < sizeof($request->get('medicine_id')) ; $i++){
                $med_desc = new \App\MedicationDescriptions;

                $med_desc->medicine_id      = $request->get('medicine_id')[$i];
                $med_desc->description      = $request->get('description')[$i];
                $med_desc->medication_id    = $medication->id;
                $med_desc->save();

            }
            DB::commit();

            return response()->json(['success' => true, 'msg' => 'Patient successfully added!']);

        }catch(\Exception $e){
            DB::rollback();
            return response()->json(['success' => false, 'msg'  => 'An error occured while trying to add a new record!']);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {   
        $medication = \App\Medications::find($id);
        $medicines = \App\Medicines::where('clinic_id',Auth::user()->clinic_id)->pluck('name', 'ID');
        return view('medications.edit')->with('patient_id', $request->patient_id)->with('medicines', $medicines)->with('medication', $medication);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{

            DB::beginTransaction();

            $medication = \App\Medications::find($id);
            $medication->patient_id     = $request->patient_id;
            $medication->date           = $request->date;
            $medication->save();

            $old_med_desc = \App\MedicationDescriptions::where('medication_id', $id)->delete();
            
            for($i = 0 ; $i < sizeof($request->get('medicine_id')) ; $i++){
                $med_desc = new \App\MedicationDescriptions;

                $med_desc->medicine_id      = $request->get('medicine_id')[$i];
                $med_desc->description      = $request->get('description')[$i];
                $med_desc->medication_id    = $medication->id;
                $med_desc->save();

            }
            DB::commit();

            return response()->json(['success' => true, 'msg' => 'Patient successfully Updated!']);

        }catch(\Exception $e){
            DB::rollback();
            return response()->json(['success' => false, 'msg'  => 'An error occured while trying to update record!']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
