<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ResultsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        return view('results.create')->with('patient_id', $request->patient_id)->with('type', $request->type);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $res = new \App\Results;

        $res->date          = $request->date;
        $res->name          = $request->name;
        $res->description   = $request->description;
        $res->patient_id    = $request->patient_id;
        $res->result_type   = $request->type;
        $res->attached_file = 'n/a';
        
        if($res->save())
        {
            activity() 
           ->withProperties(['id' => $res->id, 'name' => $res->name, 'table' => 'Result'])
           ->log('create');
            return response()->json(['success' => true, 'msg' => 'Record successfully Added']);
        }else{
            return response()->json(['success' => false, 'msg' => 'An error occured while adding a new record!']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $res = \App\Results::find($id);
        return view('results.edit')->with('patient_id', $request->patient_id)->with('type', $request->type)->with('result', $res);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $res = \App\Results::find($id);

        $res->date          = $request->date;
        $res->name          = $request->name;
        $res->description   = $request->description; 
        $res->result_type   = $request->type; 
        
        if($res->save())
        {
            activity() 
           ->withProperties(['id' => $res->id, 'name' => $res->name, 'table' => 'Result'])
           ->log('update');
            return response()->json(['success' => true, 'msg' => 'Record successfully Updated']);
        }else{
            return response()->json(['success' => false, 'msg' => 'An error occured while updating a new record!']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
