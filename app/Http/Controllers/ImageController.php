<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use File;
use Storage;
class ImageController extends Controller
{

    /**
    * Create view file
    *
    * @return void
    */
    public function imageUpload()
    {
        return view('image-upload');
    }

    /**
    * Manage Post Request
    *
    * @return void
    */
    public function imageUploadPost(Request $request)
    {    
        $this->validate($request, [
            'file' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $imageName = $request->patient_id.date('mdy').'-'.microtime().'.'.$request->file->getClientOriginalExtension();
        
        if($request->record_type == 'users'){

            $old_img = \App\Uploads::where('record_type', 'users')->where('record_id', $request->record_id)->get()->first();
            
            if(sizeof($old_img) > 0){ 
                File::Delete('uploads/'.$old_img->image_name); 
                $o_img = \App\Uploads::where('record_type', 'users')->where('record_id', $request->record_id)->delete();
                $img = new \App\Uploads;
                $img->image_name    = $imageName; 
                $img->patient_id    = $request->patient_id;
                $img->record_type   = $request->record_type;
                $img->record_id     = $request->record_id;    

            }else{    
                $img = new \App\Uploads;
                $img->image_name    = $imageName; 
                $img->patient_id    = $request->patient_id;
                $img->record_type   = $request->record_type;
                $img->record_id     = $request->record_id; 
            }

        }else{

            $img = new \App\Uploads;

            $img->image_name    = $imageName; 
            $img->patient_id    = $request->patient_id;
            $img->record_type   = $request->record_type;
            $img->record_id     = $request->record_id; 
            
        }


        if($request->file->move(public_path('uploads'), $imageName)){
            if($img->save()){
                return response()->json(['success'=> true, 'msg'=> 'Image Uploaded successfully.', 'img'=>$imageName]);             
            }else{
                return response()->json(['success'=> false, 'msg'=> 'An error occured while uploading image.']);    
            }
        }else{
            return response()->json(['success'=> false, 'msg'=> 'An error occured while uploading image.']);  
        }
    }

    public function view_images(Request $request){
        $imgs = \App\Uploads::where('patient_id', $request->patient_id)
                                ->where('record_type', $request->record_type)
                                ->where('record_id', $request->record_id)->get();
        return view('medical_history.view_uploaded')->with('images', $imgs);
    }

    public function deleteUpload()
    {

        $filename = Input::get('id');

        if(!$filename)
        {
            return 0;
        }

        $response = $this->image->delete( $filename );

        return $response;
    }


    public function uploadPicture(Request $request)
    {    
        $this->validate($request, [
            'file' => 'required|image|max:4128',
        ]);

        $imageName = date('mdy').'-'.microtime().'.'.$request->file->getClientOriginalExtension();
        
        if($request->old_file){
            File::delete('uploads/'.$request->old_file);
        }
        $user =  \App\Users::find($request->get('user_id'));
        $user->icon  = $imageName;
        $user->save();

        $patient = \App\Patients::where('user_id',$user->id)->first();
        if($patient){
            $patient->icon = $imageName; 
            $patient->save();
        }

        if($request->file->move(public_path('uploads'), $imageName)){ 
            return response()->json(['success'=> true, 'msg'=> 'Image Uploaded successfully.', 'img' => $imageName]);              
        }else{
            return response()->json(['success'=> false, 'msg'=> 'An error occured while uploading image.']);  
        }
    }

}
