<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Validator;
use Auth;
use Hash;
class ChangePasswordController extends Controller
{
    public function changePassword(\App\Http\Requests\ChangePasswordRequest $request, $id){

            $user =  \App\Users::find($id);
            if(Hash::check($request->get('old_password'), $user->password)){
                $user->password = bcrypt($request->get('new_password'));                  
            }
            if($user->save()){
	            return response()->json(['success' => true, 'msg' => 'Password successfully changed!']);
            }else{
	            return response()->json(['success' => false, 'msg' => 'An error occured while changing password!']);
            }
    }
}
