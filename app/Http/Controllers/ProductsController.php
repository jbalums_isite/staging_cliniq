<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use Datatables;
use Auth;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        return view('products.products');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $manufacturers = \App\Manufacturers::where('clinic_id',Auth::user()->clinic_id)->pluck('name', 'ID');
        $brands = \App\Brands::where('clinic_id',Auth::user()->clinic_id)->pluck('brand_name', 'ID');
        return view('products.create')->with('manufacturers', $manufacturers)->with('brands', $brands);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user_name = \App\People::find(Auth::user()->people_id);
        try{

             DB::beginTransaction();

                $product = new \App\Products;

                $product->name              = $request->name;
                $product->description       = $request->description;
                $product->price             = $request->price;
                $product->manufacturer_id   = $request->manufacturer_id;
                $product->brand_id          = $request->brand_id;
                $product->clinic_id         = Auth::user()->clinic_id;
                $product->save();

                $inventory = new \App\Inventories;
                $inventory->beginning_qty   = 0;
                $inventory->end_qty         = 0;
                $inventory->date            = date('Y-m-d');
                $inventory->product_id      = $product->id;
                $inventory->save();

                
                $inventory_history = new \App\InventoryHistories;
                $inventory_history->product_id      = $product->id;
                $inventory_history->qty             = 0;
                $inventory_history->remarks         = 'created product added by '.$user_name->firstname.' '.$user_name->lastname;
                $inventory_history->date            = date('Y-m-d');
                $inventory_history->save();

                
        activity() 
       ->withProperties(['id' => $product->id, 'name' => $product->name, 'table' => 'Product'])
       ->log('create');
            DB::commit();

            return response()->json(['success' => true, 'msg' => 'Record successfully Added']);

         }catch(\Exception $e){
            DB::rollback();
            return response()->json(['success' => false, 'msg' => 'An error occured while adding product!']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        $manufacturers = \App\Manufacturers::where('clinic_id',Auth::user()->clinic_id)->pluck('name', 'ID');
        $brands = \App\Brands::where('clinic_id',Auth::user()->clinic_id)->pluck('brand_name', 'ID');
        $product = \App\Products::find($id);
        return view('products.edit')->with('product', $product)->with('manufacturers', $manufacturers)->with('brands', $brands);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user_name = \App\People::find(Auth::user()->people_id);
        try{

             DB::beginTransaction();

                $product = \App\Products::find($id);

                $product->name              = $request->name;
                $product->description       = $request->description;
                $product->price             = $request->price;
                $product->manufacturer_id   = $request->manufacturer_id;
                $product->brand_id          = $request->brand_id; 
                $product->save();
  
                
            activity() 
           ->withProperties(['id' => $product->id, 'name' => $product->name, 'table' => 'Product'])
           ->log('update');
            DB::commit();

            return response()->json(['success' => true, 'msg' => 'Record successfully Updated']);

         }catch(\Exception $e){
            DB::rollback();
            return response()->json(['success' => false, 'msg' => 'An error occured while updating product!']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = \App\Products::find($id);
        
            activity() 
           ->withProperties(['id' => $product->id, 'name' => $product->name, 'table' => 'Product'])
           ->log('delete');
        if($product->delete()){
            return response()->json(['success' => true, 'msg' => 'User successfully removed!']);
        }else{
            return response()->json(['success' => true, 'msg' => 'An error occured while removing user!']);
        }
    }

    public function get_products(){
        

        DB::statement(DB::raw('set @row:=0'));
        $inventories = \App\Products::selectRaw('*, @row:=@row+1 as row');

         return Datatables::of($inventories)
            ->AddColumn('row', function($column){
               return $column->row;
            })
            ->AddColumn('name', function($column){
               return $column->name;
            })
            ->AddColumn('description', function($column){
               return $column->description;
            })
            ->AddColumn('manufacturer', function($column){
               return $column->manufacturer->name;
            })
            ->AddColumn('brand', function($column){
               return $column->brand->brand_name;
            })
            ->AddColumn('price', function($column){
               return number_format($column->price, 2);
            })
            ->AddColumn('actions', function($column){
                return '<div class="btn-group table-dropdown">
                            <button data-toggle="dropdown" class="btn-xs btn-for-table dropdown-toggle">
                                
                                <span class="ace-icon fa fa-ellipsis-h icon-on-center"></span>
                            </button>

                            <ul class="dropdown-menu dropdown-caret dropdown-menu-right">
                                <li>
                                    <a href="#" class="edit-patient-btn edit-mf-btn" data-id="'.$column->id.'">Edit</a>
                                </li> 
                                <li>
                                    <a href="#" class="delete-patient-btn dele-mf-btn" data-id="'.$column->id.'">Delete</a>
                                </li> 
                            </ul>
                        </div>';
            })
            
            ->make(true);    
    }
}
