<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use Datatables;
use Auth;


class InventoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $products = \App\Products::all();
        return view('inventories.inventories');

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $products = \App\Products::where('clinic_id', Auth::user()->clinic_id)->pluck('name', 'ID');
        return view('inventories.create')->with('products', $products);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user_name = \App\People::find(Auth::user()->people_id);
        try{

            DB::beginTransaction();

            for($i = 0 ; $i < sizeof($request->get('product_id')) ; $i++){
                $inventory = \App\Inventories::where('date', date('Y-m-d'))->where('product_id', $request->get('product_id')[$i])->get()->first(); 
                if($inventory == null){
                    $old_inventory = \App\Inventories::where('product_id', $request->get('product_id')[$i])->orderBy('date', 'desc')->get()->first();  
                    if($old_inventory != null){
                        $new_inventory = new \App\Inventories;
                        $new_inventory->product_id          =   $request->get('product_id')[$i];
                        $new_inventory->beginning_qty       =   $old_inventory->end_qty;
                        $new_inventory->end_qty             =   (double) $old_inventory->end_qty + (double)$request->get('qty')[$i];
                        $new_inventory->date                =   date('Y-m-d');
                        $new_inventory->save();                         
                    }else{
                        $new_inventory = new \App\Inventories;
                        $new_inventory->product_id          =   $request->get('product_id')[$i];
                        $new_inventory->beginning_qty       =   $request->get('qty')[$i];
                        $new_inventory->end_qty             =   $request->get('qty')[$i];
                        $new_inventory->date                =   date('Y-m-d');
                        $new_inventory->save();                         
                    }
                }else{  
                    $inventory->end_qty             =  (double)$inventory->end_qty + (double) $request->get('qty')[$i];
                    $inventory->save();
                }
                
                $inventory_history = new \App\InventoryHistories;
                $inventory_history->product_id      = $request->get('product_id')[$i];
                $inventory_history->qty             = $request->get('qty')[$i];
                $inventory_history->remarks         = 'added stock by '.$user_name->firstname.' '.$user_name->lastname;
                $inventory_history->date            = date('Y-m-d');
                $inventory_history->save();
            }

            DB::commit();

            return response()->json(['success' => true, 'msg' => 'Record successfully added!']);

        }catch(\Exception $e){
            DB::rollback();
            return response()->json(['success' => false, 'msg'  => 'An error occured while trying to add a new record!']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function stocks()
    {
        return view('inventories.stocks');
    }

    public function get_inventories(){ 
        DB::statement(DB::raw('set @row:=0'));
        $inventories = \App\Inventories::get();
         return Datatables::of($inventories)
            
             
            ->AddColumn('row', function($column){
               return '#';
            })
            ->AddColumn('product', function($column){
               return $column->product->name;
            })
            ->AddColumn('beginning', function($column){
               return $column->beginning_qty;
            })
            ->AddColumn('ending', function($column){
               return $column->end_qty;
            })
            ->AddColumn('date', function($column){
                return date("F d, Y", strtotime($column->date));
            })
            ->make(true);             
    
    }
    public function get_inventory_histories(){

        DB::statement(DB::raw('set @row:=0'));
        $inventories = \App\InventoryHistories::selectRaw('*, @row:=@row+1 as row')
                                        ->join('products', 'products.id', '=', 'product_id')
                                        ->where('products.clinic_id', Auth::user()->clinic_id);
         return Datatables::of($inventories)
            
             
            ->AddColumn('row', function($column){
               return $column->row;
            })
            ->AddColumn('product', function($column){
               return $column->product->name;
            })
            ->AddColumn('qty', function($column){
               return $column->qty;
            })
            ->AddColumn('remarks', function($column){
               return $column->remarks;
            })
            ->AddColumn('date', function($column){
                return date("F d, Y", strtotime($column->date));
            })
            ->make(true);             
    
    }
}
