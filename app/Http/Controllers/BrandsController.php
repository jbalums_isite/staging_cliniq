<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use Datatables;
use Auth;

class BrandsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        return view('brands.brands');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('brands.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $brand = new \App\Brands;
        $brand->brand_name         = $request->name;
        $brand->clinic_id    = Auth::user()->clinic_id; 
        if($brand->save())
        {
            activity() 
           ->withProperties(['id' => $brand->id, 'name' => $brand->brand_name, 'table' => 'Brand'])
           ->log('create');
            return response()->json(['success' => true, 'msg' => 'Record successfully Added']);
        }else{
            return response()->json(['success' => false, 'msg' => 'An error occured while adding a new record!']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $brands = \App\Brands::find($id);
        return view('brands.edit')->with('brands', $brands);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $brand = \App\Brands::find($id);
        $brand->brand_name         = $request->name;

        if($brand->save())
        {
            activity() 
           ->withProperties(['id' => $brand->id, 'name' => $brand->brand_name, 'table' => 'Brand'])
           ->log('update');
            return response()->json(['success' => true, 'msg' => 'Record successfully Updated']);
        }else{
            return response()->json(['success' => false, 'msg' => 'An error occured while updating a new record!']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $patient = \App\Brands::find($id);
        activity() 
       ->withProperties(['id' => $patient->id, 'name' => $patient->brand_name, 'table' => 'Brand'])
       ->log('delete');
        
        if($patient->delete()){
            return response()->json(['success' => true, 'msg' => 'Record successfully removed!']);
        }else{
            return response()->json(['success' => true, 'msg' => 'An error occured while removing record!']);
        }
    }


    public function get_brands(){

        DB::statement(DB::raw('set @row:=0'));
        $brands = \App\Brands::selectRaw('*, @row:=@row+1 as row');

         return Datatables::of($brands)
            ->AddColumn('row', function($column){
               return $column->row;
            })
            ->AddColumn('brand_name', function($column){
               return $column->brand_name;
            })
            ->AddColumn('actions', function($column){
                return '<div class="btn-group table-dropdown">
                            <button data-toggle="dropdown" class="btn-xs btn-for-table dropdown-toggle">
                                
                                <span class="ace-icon fa fa-ellipsis-h icon-on-center"></span>
                            </button>

                            <ul class="dropdown-menu dropdown-caret dropdown-menu-right">
                                <li>
                                    <a href="#" class="edit-patient-btn edit-mf-btn" data-id="'.$column->id.'">Edit</a>
                                </li> 
                                <li>
                                    <a href="#" class="delete-patient-btn dele-mf-btn" data-id="'.$column->id.'">Delete</a>
                                </li> 
                            </ul>
                        </div>';
            })

            ->make(true);    
    }
}
