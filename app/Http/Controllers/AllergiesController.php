<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use Datatables;
use Auth;
class AllergiesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        return view('allergies.allergies');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $allergies = \App\AllergiesList::all();
        return view('allergies.create')->with('patient_id', $request->patient_id)->with('allergies', $allergies);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $allergy = new \App\Allergies;

        $allergy->name          = $request->name;
        $allergy->description   = $request->description;
        $allergy->patient_id    = $request->patient_id;
        
        if($allergy->save())
        {
            activity() 
           ->withProperties(['id' => $allergy->id, 'name' => $allergy->name, 'table' => 'Allergy'])
           ->log('create');
            return response()->json(['success' => true, 'msg' => 'Record successfully Added', 
                                    'id' =>  $allergy->id, 
                                    'name' =>  $request->name, 
                                    'description' => $request->description]);
        }else{
            return response()->json(['success' => false, 'msg' => 'An error occured while adding a new record!']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $allergy = \App\Allergies::find($id);
        return view('allergies.edit')->with('patient_id', $request->patient_id)->with('allergy', $allergy);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $allergy = \App\Allergies::find($id);

        $allergy->name          = $request->name;
        $allergy->description   = $request->description; 
        
        if($allergy->save())
        {
            activity() 
           ->withProperties(['id' => $allergy->id, 'name' => $allergy->name, 'table' => 'Allergy'])
           ->log('update');
            return response()->json(['success' => true, 'msg' => 'Record successfully Updated']);
        }else{
            return response()->json(['success' => false, 'msg' => 'An error occured while updating a new record!']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $success = false; 
        $success = \App\Clients::find($id);
        activity() 
       ->withProperties(['id' => $allergy->id, 'name' => $allergy->name, 'table' => 'Allergy'])
       ->log('delete');
        $success = \App\Clients::find($id)->delete();
        if($success){
            return response()->json(['success' => true, 'msg' => 'Record successfully deleted!']);

        }
        else{
            return response()->json(['success' => false, 'msg' => 'An error occur while trying to remove the record']);

        }
    }

    public function all_records($patient_id){
        $records = \App\Allergies::where('patient_id', $patient_id)->get();
        return response()->json(['data'=> $records]);
    }
}
