<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
class SettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {    
        if(sizeof(Auth::user()->setting) > 0){
            $ss = $request->get('success');
            if(isset($ss)){
                return view('settings.settings')->with('success', true);
            }else{
                return view('settings.settings');
            }

        }else{ 

            $setttings = new \App\Settings;
            $setttings->dasboard       = 1;
            $setttings->patient        = 1;
            $setttings->medicine       = 1;
            $setttings->services       = 1;
            $setttings->view_products  = 1;
            $setttings->stocks         = 1;
            $setttings->manufacturers  = 1;
            $setttings->brands         = 1;
            $setttings->schedules      = 1;
            $setttings->referral_form  = 1;
            $setttings->request_form   = 1;
            $setttings->custom_form        = 1;
            $setttings->dialy_reports        = 1;
            $setttings->monthly_reports      = 1;
            $setttings->quarterly_reports    = 1;
            $setttings->annual_reports       = 1;
            $setttings->users          = 1;
            $setttings->allergies      = 1;
            $setttings->user_id         = Auth::user()->id;
            $setttings->save();

            return view('settings.settings');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('settings.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    { 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {   
        $user = \App\Users::find($id); 
        if(sizeof($user->setting) > 0){
            $ss = $request->get('success');
            if(isset($ss)){
                return view('settings.settings')->with('success', true)->with('user', $user);
            }else{
                return view('settings.settings')->with('user', $user);
            }

        }else{ 

            $setttings = new \App\Settings;
            $setttings->dasboard       = 1;
            $setttings->patient        = 1;
            $setttings->medicine       = 1;
            $setttings->services       = 1;
            $setttings->view_products  = 1;
            $setttings->stocks         = 1;
            $setttings->manufacturers  = 1;
            $setttings->brands         = 1;
            $setttings->schedules      = 1;
            $setttings->referral_form  = 1;
            $setttings->request_form   = 1;
            $setttings->custom_form        = 1;
            $setttings->dialy_reports        = 1;
            $setttings->monthly_reports      = 1;
            $setttings->quarterly_reports    = 1;
            $setttings->annual_reports       = 1;
            $setttings->users          = 1;
            $setttings->allergies      = 1;
            $setttings->user_id         = $user->id;
            $setttings->save();

            return view('settings.settings')->with('user', $user);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $setttings = \App\Settings::find($id);
        $setttings->dasboard       = $request->dasboard == 'on' ? 1:0;
        $setttings->patient        =  $request->patient == 'on' ? 1:0;
        $setttings->medicine       =  $request->medicine == 'on' ? 1:0;
        $setttings->services       =  $request->services == 'on' ? 1:0;
        $setttings->view_products  =  $request->view_products == 'on' ? 1:0;
        $setttings->stocks         =  $request->stocks == 'on' ? 1:0;
        $setttings->manufacturers  =  $request->manufacturers == 'on' ? 1:0;
        $setttings->brands         =  $request->brands == 'on' ? 1:0;
        $setttings->schedules      =  $request->schedules == 'on' ? 1:0;
        $setttings->referral_form  =  $request->referral_form == 'on' ? 1:0;
        $setttings->request_form   =  $request->request_form == 'on' ? 1:0;
        $setttings->custom_form          = $request->custom_form == 'on' ? 1:0;
        $setttings->dialy_reports        = $request->dialy_reports == 'on' ? 1:0;
        $setttings->monthly_reports      = $request->monthly_reports == 'on' ? 1:0;
        $setttings->quarterly_reports    = $request->quarterly_reports == 'on' ? 1:0;
        $setttings->annual_reports       = $request->annual_reports == 'on' ? 1:0;
        $setttings->users          = $request->users == 'on' ? 1:0;
        $setttings->allergies      = $request->allergies == 'on' ? 1:0; 
        if($setttings->save()){
            activity() 
           ->withProperties(['id' => $setttings->id, 'table' => 'Setting'])
           ->log('update');
            return redirect('/settings/'.$setttings->user_id.'?success=true');            
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
