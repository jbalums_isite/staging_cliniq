<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use Datatables;
class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        return view('transactions.transactions');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $appointments = \App\Appointments::where('datetime', '>=' ,date('Y-m-d H:00:00'))->where('datetime', '<=' ,date('Y-m-d 23:59:59'))->get();/*all();*/
        $services = \App\Services::all();
        $products = \App\Products::all();
        return view('transactions.create')->with('appointments', $appointments)
                                          ->with('services', $services)
                                          ->with('products', $products);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

           try{

            DB::beginTransaction();
                $master = new \App\BillingMasters;

                $master->appointment_id = $request->get('appointment_id');
                $master->change         = $request->get('change');
                $master->total_payable  = $request->get('total_payable');
                $master->paid_amount    = $request->get('paid_amount');
                $master->discount       = str_replace('%', '', $request->get('discount'));
                $master->percentage     = $request->get('is_percentage');
                $master->customer_name  = $request->get('customer_name');
                $master->address        = '';
                $master->notes_medication =  '';
                $master->save();

                for($i = 0 ; $i < sizeof($request->get('record_id')) ; $i++){
                    $details = new \App\BillingDetails;
         
                    $details->master_id     = $master->id;
                    $details->record_type   = $request->get('record_type')[$i];
                    $details->record_id     = $request->get('record_id')[$i];
                    $details->qty           = $request->get('qty')[$i];
                    $details->amount        = $request->get('amount')[$i];
                    $details->discount      = str_replace('%', '', $request->get('discount'));
                    $details->percentage    = $request->get('is_percentage');

                    $details->save();

                    if($request->get('record_type')[$i] == 'product'){
                        $inventory = \App\Inventories::where('date', date('Y-m-d'))->where('product_id', $request->get('record_id')[$i])->get()->first(); 
                        if($inventory == null){
                            $old_inventory = \App\Inventories::where('product_id', $request->get('record_id')[$i])->orderBy('date', 'desc')->get()->first();  
                            if($old_inventory != null){
                                $new_inventory = new \App\Inventories;
                                $new_inventory->product_id          =   $request->get('record_id')[$i];
                                $new_inventory->beginning_qty       =   $old_inventory->beginning_qty;
                                $new_inventory->end_qty             =   (double) $old_inventory->end_qty - (double)$request->get('qty')[$i];
                                $new_inventory->date                =   date('Y-m-d');
                                $new_inventory->save();                         
                            }
                        }else{  
                            $inventory->end_qty             =  (double)$inventory->end_qty - (double) $request->get('qty')[$i];
                            $inventory->save();
                        }
                        
                        $user_name = \App\People::find(Auth::user()->people_id);
                        $inventory_history = new \App\InventoryHistories;
                        $inventory_history->product_id      = $request->get('record_id')[$i];
                        $inventory_history->qty             = $request->get('qty')[$i];
                        $inventory_history->remarks         = 'sold stocks by '.$user_name->firstname.' '.$user_name->lastname;
                        $inventory_history->date            = date('Y-m-d');
                        $inventory_history->save();
                    }
                }
            DB::commit();

             return response()->json(['success' => true, 'msg' => 'Patient successfully checked out!', 'master_id' => $master->id]);

         }catch(\Exception $e){
             DB::rollback();
             return response()->json(['success' => false, 'msg'  => 'An error occured while trying processing record!']);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $master = \App\BillingMasters::find($id);

        return view('transactions.show')->with('master', $master);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function get_transactions(){

        DB::statement(DB::raw('set @row:=0'));
        $inventories = \App\BillingMasters::selectRaw('*, @row:=@row+1 as row');

         return Datatables::of($inventories)
            ->AddColumn('row', function($column){
               return $column->row;
            })
            ->AddColumn('patient', function($column){
               return ucfirst($column->appointment->patient->user->people->firstname)." ".
                      ucfirst(substr($column->appointment->patient->user->people->middlename, 0, 1)).". ".
                      ucfirst($column->appointment->patient->user->people->lastname)." ";
            })
            ->AddColumn('date', function($column){
               return date('M. d, Y h:i A', strtotime($column->appointment->datetime));
            })
            ->AddColumn('doctor', function($column){
               return ucfirst($column->appointment->doctor->people->title)." ".
                      ucfirst($column->appointment->doctor->people->firstname)." ".
                      ucfirst(substr($column->appointment->doctor->people->middlename, 0, 1)).". ".
                      ucfirst($column->appointment->doctor->people->lastname)." ";
            })
            ->AddColumn('actions', function($column){
               return '<a href="#" class="editBtn edit-mf-btn" data-id="'.$column->id.'"><i class="fa fa-eye"></i></a>';
            })
            
            ->make(true); 
    }

    public function get_appointment_details($id){

        $appointment = \App\Appointments::find($id);

        return response()->json(['appointment'  =>$appointment, 
                                 'doctor'       =>  ucfirst($appointment->doctor->people->title)." ".
                                                    ucfirst($appointment->doctor->people->firstname)." ".
                                                    ucfirst(substr($appointment->doctor->people->middlename, 0, 1)).". ".
                                                    ucfirst($appointment->doctor->people->lastname),
                                 'date'         => date('M. d, Y h:i A', strtotime($appointment->datetime))
                                ]);
    }

    public function get_record_details($id, $type){
        if($type == 'product'){
            $record = \App\Products::find($id);
            return response()->json($record);
        }else{
            $record = \App\Services::find($id);
            return response()->json($record);
        }
    }

    public function print_invoice($id){
        $trans = \App\BillingDetails::where('master_id', $id)->get(); 
        $master = \App\BillingMasters::where('id', $id)->get(); 
        return view('transactions.print')->with('id', $id)->with('details', $trans)->with('master', $master);
    }
}
