<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

use DB;
use Datatables;
use Auth;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        return view('users.users');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(\App\Http\Requests\AddUserRequest $request)
    { 
          try{

            DB::beginTransaction();

            $people_contact = new \App\Contacts;
            
            $people_contact->mobile        = $request->get('mobile');
            $people_contact->telephone     = $request->get('tel');
            $people_contact->save();

            $people_address = new \App\Address;

            $people_address->street        = $request->get('street');
            $people_address->province      = $request->get('province');
            $people_address->city          = $request->get('city');
            $people_address->save();

            $people = new \App\People;

            $people->firstname          = $request->get('firstname');
            $people->middlename         = $request->get('middlename');
            $people->lastname           = $request->get('lastname');
            $people->birthplace         = $request->get('birthplace');
            $people->birthdate          = date('Y-m-d', strtotime($request->get('birthdate')));
            $people->gender             = $request->get('gender');
            $people->nationality        = $request->get('nationality');
            $people->civil_status       = $request->get('civil_status');
            $people->title              = $request->get('title');
            $people->contact_id         = $people_contact->id;
            $people->address_id         = $people_address->id;
            $people->save();
            

            $user = new \App\Users;

            $user->email                    = $request->get('email');
            $user->icon                    = $request->get('icon');
            $user->password                 = bcrypt($request->get('password'));
            $user->clinic_id                = Auth::user()->clinic_id;
            $user->people_id                = $people->id;
            $user->type                     = $request->get('type');
            $user->save();

            $setttings = new \App\Settings;
            $setttings->dasboard       = 1;
            $setttings->patient        = 1;
            $setttings->medicine       = 1;
            $setttings->services       = 1;
            $setttings->view_products  = 1;
            $setttings->stocks         = 1;
            $setttings->manufacturers  = 1;
            $setttings->brands         = 1;
            $setttings->schedules      = 1;
            $setttings->referral_form  = 1;
            $setttings->request_form   = 1;
            $setttings->custom_form        = 1;
            $setttings->dialy_reports        = 1;
            $setttings->monthly_reports      = 1;
            $setttings->quarterly_reports    = 1;
            $setttings->annual_reports       = 1;
            $setttings->allergies      = 1;
            $setttings->users          = 1;
            $setttings->user_id         = $user->id;
            $setttings->save();

            DB::commit();

            return response()->json(['success' => true, 'msg' => 'Account successfully added!']);

        }catch(\Exception $e){
            DB::rollback();
            return response()->json(['success' => false, 'msg'  => 'An error occured while trying to add a new record!']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {   
        $user = \App\User::find($id);
        if($request->get('success_profile')){
          return view('users.profile')->with('user', $user)->with('success_update_profile', true);
        }else{
          return view('users.profile')->with('user', $user);
          
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = \App\Users::find($id); 
        return view('users.edit')->with('user', $user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {    
        
        // $this->validate($request, [
        //     'email'                 => 'email|min:5|required|unique:users,email,'.$id,
        // ]);

       try{

            DB::beginTransaction();

            $people_contact = \App\Contacts::find($request->get('contact_id'));
            
            $people_contact->mobile        = $request->get('mobile');
            $people_contact->telephone     = $request->get('tel');
            $people_contact->save();

            $people_address =  \App\Address::find($request->get('address_id'));

            $people_address->street        = $request->get('street');
            $people_address->province      = $request->get('province');
            $people_address->city          = $request->get('city');
            $people_address->save();

            $people = \App\People::find($request->get('people_id'));

            $people->firstname          = $request->get('firstname');
            $people->middlename         = $request->get('middlename');
            $people->lastname           = $request->get('lastname');
            $people->birthplace         = $request->get('birthplace');
            $people->birthdate          = date('Y-m-d', strtotime($request->get('birthdate')));
            $people->gender             = $request->get('gender');
            $people->nationality        = $request->get('nationality');
            $people->civil_status       = $request->get('civil_status');
            $people->title              = $request->get('title');

            $people->save();
             
            $user =  \App\Users::find($id);

            $user->email                    = $request->get('email');

            if(strlen($request->get('password')) > 0){ 
                if(Hash::check($request->get('password'), $user->password)){
                    $user->password             = bcrypt($request->get('new_password'));                  
                }
            }

            $user->type                     = $request->get('type');
            $user->icon                    = $request->get('icon');
            $user->save();



            DB::commit();
            $f_p = $request->get('from_profile');
            if(isset($f_p)){
              return redirect('/users/'.Auth::user()->id.'/?success_profile=true');
            }else{
              return response()->json(['success' => true, 'msg' => 'Account successfully updated!']);
              
            }

        }catch(\Exception $e){
            DB::rollback();
            return response()->json(['success' => false, 'msg'  => 'An error occured while trying to updated  record!', 'error'=>$e]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $patient = \App\Users::find($id);
        
        if($patient->delete()){
            return response()->json(['success' => true, 'msg' => 'User successfully removed!']);
        }else{
            return response()->json(['success' => true, 'msg' => 'An error occured while removing user!']);
        }
    }

    public function get_users(){

        DB::statement(DB::raw('set @row:=0'));
        $inventories = \App\Users::selectRaw('*, users.id as u_id , @row:=@row+1 as row')->where('users.type', '!=', 'patient');

         return Datatables::of($inventories)
            ->AddColumn('row', function($column){
               return $column->row;
            })
            ->AddColumn('name', function($column){
               return ucfirst($column->people->firstname)." ".
                      ucfirst(substr($column->people->middlename, 0, 1)).". ".
                      ucfirst($column->people->lastname)." ";
            })
            ->AddColumn('gender', function($column){
               return ucfirst($column->people->gender);
            })
            ->AddColumn('address', function($column){
               return $column->people->address->street.", ".
                      $column->people->address->city.", ".
                      $column->people->address->province;
            })
            ->AddColumn('contact_info', function($column){
               return $column->people->contact->mobile;
            })
            ->AddColumn('actions', function($column){
              
                return '<div class="btn-group table-dropdown">
                            <button data-toggle="dropdown" class="btn-xs btn-for-table dropdown-toggle">
                                
                                <span class="ace-icon fa fa-ellipsis-h icon-on-center"></span>
                            </button>

                            <ul class="dropdown-menu dropdown-caret dropdown-menu-right">
                                <li>
                                    <a href="'.url('settings/'.$column->id).'" class="setting-patient-btn edit-mf-btn" data-id="'.$column->u_id.'">Settings</a>
                                </li> 
                                <li>
                                    <a href="#" class="edit-patient-btn edit-mf-btn" data-id="'.$column->u_id.'">Edit</a>
                                </li> 
                                <li>
                                    <a href="#" class="delete-patient-btn dele-mf-btn" data-id="'.$column->u_id.'">Delete</a>
                                </li> 
                            </ul>
                        </div>';
            })
            
            ->make(true);    
    
    }

    public function check_password(Request $request, $id){
        $user = \App\Users::find($id);

        if(Hash::check($request->password, $user->password)){
            return response()->json(true);
        }else{
            return response()->json(false);
        }
    }
}
