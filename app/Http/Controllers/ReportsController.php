<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use Datatables;

class ReportsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        return view('reports.reports');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
    public function daily_sales_reports(){
        return view('reports.daily-sales-reports');
    }

    public function monthly_sales_reports(){
        return view('reports.monthly-sales-reports');
    }
    public function quarterly_sales_reports(){
        return view('reports.quarterly-sales-reports');
    }
    public function annualy_sales_reports(){
        return view('reports.annually-sales-report');
    }

    public function get_sales_report($start, $end){

        DB::statement(DB::raw('set @row:=0')); 
        $reports = \App\BillingDetails::selectRaw('*, billing_details.created_at as bcrat, @row:=@row+1 as row')
                            ->leftJoin('products', function ($join) {
                                $join->on('products.id', '=', 'billing_details.record_id')
                                     ->where('billing_details.record_type', '=', 'products');
                            }) 
                            ->leftJoin('services', function ($join) {
                                $join->on('services.id', '=', 'billing_details.record_id')
                                     ->where('billing_details.record_type', '=', 'services');
                            })  
                            ->whereBetween('billing_details.created_at', [date('Y-m-d 00:00:00', strtotime($start)), date('Y-m-d 23:59:59', strtotime($end))])
                            ->get();
         return Datatables::of($reports)
            ->AddColumn('row', function($column){
               return $column->row;
            })
            ->AddColumn('date', function($column){
               return date('M. d, Y', strtotime($column->bcrat));
            })
            ->AddColumn('patient', function($column){
                if($column->master->appointment_id == 0){
                    return ucfirst($column->master->customer_name);
                }else{
                   return  ucfirst($column->master->appointment->patient->user->people->firstname)." ".
                        ucfirst(substr($column->master->appointment->patient->user->people->middlename, 0, 1)).". ".
                        ucfirst($column->master->appointment->patient->user->people->lastname)." "; 
                }
               

            })
            ->AddColumn('item', function($column){
               return $column->record_type == 'product' ? $column->product->name : $column->service->name;
            }) 
            ->AddColumn('price', function($column){
                $p  = ( (double) $column->amount/ (double) $column->qty );
               return number_format($p,2);
            }) 
            ->AddColumn('qty', function($column){
               return number_format($column->qty,2);
            }) 
            ->AddColumn('discount', function($column){
               return $column->percentage == 1 ? $column->discount.'%' : $column->discount;//number_format($column->discount,2);
            }) 
            ->AddColumn('total', function($column){
                $prc  = ( (double) $column->amount / (double) $column->qty );
                $v = ($prc * $column->qty);

                $p = $column->percentage == 1 ? ($v - ($v * ($column->discount/100))) : ($v - $column->discount);
               return number_format($p,2);
            }) 
            ->AddColumn('actions', function($column){
               return '<a href="#" class="editBtn edit-mf-btn" data-id="'.$column->id.'"><i class="fa fa-edit"></i></a>';
            })

            ->make(true);    
    }

    public function get_sales_report_monthly($yr=null){ 
        
        if($yr == null){
          $yr = date('Y');  
        }
        $arr = array();
        $jan = DB::select("SELECT '1' as row, 'January' as month, IF(sum(total_payable) > 0, FORMAT(sum(total_payable),2), '0.00') as s FROM `billing_masters` where created_at like '%".$yr."-01%'" );
        $feb = DB::select("SELECT '2' as row, 'February'as month, IF(sum(total_payable) > 0, FORMAT(sum(total_payable),2), '0.00') as s FROM `billing_masters` where created_at like '%".$yr."-02%'" );
        $mar = DB::select("SELECT '3' as row, 'March'as month, IF(sum(total_payable) > 0, FORMAT(sum(total_payable),2), '0.00') as s FROM `billing_masters` where created_at like '%".$yr."-03%'" );
        $apr = DB::select("SELECT '4' as row, 'April'as month, IF(sum(total_payable) > 0, FORMAT(sum(total_payable),2), '0.00') as s FROM `billing_masters` where created_at like '%".$yr."-04%'" );
        $may = DB::select("SELECT '5' as row, 'May'as month, IF(sum(total_payable) > 0, FORMAT(sum(total_payable),2), '0.00') as s FROM `billing_masters` where created_at like '%".$yr."-05%'" );
        $jun = DB::select("SELECT '6' as row, 'June'as month, IF(sum(total_payable) > 0, FORMAT(sum(total_payable),2), '0.00') as s FROM `billing_masters` where created_at like '%".$yr."-06%'" );
        $jul = DB::select("SELECT '7' as row, 'July'as month, IF(sum(total_payable) > 0, FORMAT(sum(total_payable),2), '0.00') as s FROM `billing_masters` where created_at like '%".$yr."-07%'" );
        $aug = DB::select("SELECT '8' as row, 'August'as month, IF(sum(total_payable) > 0, FORMAT(sum(total_payable),2), '0.00') as s FROM `billing_masters` where created_at like '%".$yr."-08%'" );
        $sep = DB::select("SELECT '9' as row, 'September'as month, IF(sum(total_payable) > 0, FORMAT(sum(total_payable),2), '0.00') as s FROM `billing_masters` where created_at like '%".$yr."-09%'" );
        $oct = DB::select("SELECT '10' as row, 'October'as month, IF(sum(total_payable) > 0, FORMAT(sum(total_payable),2), '0.00') as s FROM `billing_masters` where created_at like '%".$yr."-10%'" );
        $nov = DB::select("SELECT '11' as row, 'November'as month, IF(sum(total_payable) > 0, FORMAT(sum(total_payable),2), '0.00') as s FROM `billing_masters` where created_at like '%".$yr."-11%'" );
        $dec = DB::select("SELECT '12' as row, 'December'as month, IF(sum(total_payable) > 0, FORMAT(sum(total_payable),2), '0.00') as s FROM `billing_masters` where created_at like '%".$yr."-12%'" ); 
        array_push($arr, $jan[0]);
        array_push($arr, $feb[0]);
        array_push($arr, $mar[0]);
        array_push($arr, $apr[0]);
        array_push($arr, $may[0]);
        array_push($arr, $jun[0]);
        array_push($arr, $jul[0]);
        array_push($arr, $aug[0]);
        array_push($arr, $sep[0]);
        array_push($arr, $oct[0]);
        array_push($arr, $nov[0]);
        array_push($arr, $dec[0]); 

        return response()->json(['data'=>$arr, 'draw'=>0]);  
    }

    public function get_sales_report_quarterly($yr=null){ 
        
        if($yr == null){
          $yr = date('Y');  
        }
        $arr = array();
        $jan = DB::select("SELECT '1' as row, 'January - March' as month, IF(sum(total_payable) > 0, FORMAT(sum(total_payable),2), '0.00') as s FROM `billing_masters` where created_at BETWEEN '".$yr."-01-01 00:00:00' AND '".$yr."-03-31 23:59:59'"  );
        $feb = DB::select("SELECT '2' as row, 'April - June'as month, IF(sum(total_payable) > 0, FORMAT(sum(total_payable),2), '0.00') as s FROM `billing_masters` where created_at BETWEEN '".$yr."-04-01 00:00:00' AND '".$yr."-06-31 23:59:59'"  );
        $mar = DB::select("SELECT '3' as row, 'July - September'as month, IF(sum(total_payable) > 0, FORMAT(sum(total_payable),2), '0.00') as s FROM `billing_masters` where created_at BETWEEN '".$yr."-07-01 00:00:00' AND '".$yr."-09-31 23:59:59'"  );
        $apr = DB::select("SELECT '4' as row, 'October - December'as month, IF(sum(total_payable) > 0, FORMAT(sum(total_payable),2), '0.00') as s FROM `billing_masters` where created_at BETWEEN '".$yr."-10-01 00:00:00' AND '".$yr."-12-31 23:59:59'"  );

        array_push($arr, $jan[0]);
        array_push($arr, $feb[0]);
        array_push($arr, $mar[0]);
        array_push($arr, $apr[0]); 

        return response()->json(['data'=>$arr, 'draw'=>0]);  
    }

    public function get_sales_report_annualy($start = null, $end = null){
        $arr = array();
        $diff = 0;

        if($start != null){
            $diff = $end - $start;
        }

        DB::statement(DB::raw('set @row:=0')); 
        for($i = 0 ; $i <= $diff; $i++ ){
            $jan = DB::select("SELECT @row:=@row+1  as row, '".($start+$i)."' as month, IF(sum(total_payable) > 0, FORMAT(sum(total_payable),2), '0.00') as s FROM `billing_masters` where created_at BETWEEN '".($start+$i)."-01-01 00:00:00' AND '".($start+$i)."-12-31 23:59:59'"  );
            array_push($arr, $jan[0]); 
        }

        return response()->json(['data'=>$arr, 'draw'=>0]);  
    }
}
