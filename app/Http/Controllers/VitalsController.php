<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use Datatables;
use Auth;

class VitalsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        return view('vitals.create')->with('patient_id', $request->patient_id);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $vitals = new \App\Vitals;

        $vitals->date               = $request->date;
        $vitals->weight             = $request->weight;
        $vitals->temperature        = $request->temperature;
        $vitals->pulse              = $request->pulse;
        $vitals->blood_pressure     = $request->blood_pressure;
        $vitals->respiration        = $request->respiration;
        $vitals->pain               = $request->pain;
        $vitals->patient_id         = $request->patient_id;

        if($vitals->save())
        {
            return response()->json(['success' => true, 'msg' => 'Record successfully Added']);
        }else{
            return response()->json(['success' => false, 'msg' => 'An error occured while adding a new record!']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {   
        $vitals = \App\Vitals::find($id);
        return view('vitals.edit')->with('patient_id', $request->patient_id)->with('vitals', $vitals);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $vitals = \App\Vitals::find($id);

        $vitals->date               = $request->date;
        $vitals->weight             = $request->weight;
        $vitals->temperature        = $request->temperature;
        $vitals->pulse              = $request->pulse;
        $vitals->blood_pressure     = $request->blood_pressure;
        $vitals->respiration        = $request->respiration;
        $vitals->pain               = $request->pain; 

        if($vitals->save())
        {
            return response()->json(['success' => true, 'msg' => 'Record successfully Updated']);
        }else{
            return response()->json(['success' => false, 'msg' => 'An error occured while updating a new record!']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
