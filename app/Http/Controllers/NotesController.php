<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
class NotesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        return view('notes.create')->with('patient_id', $request->patient_id);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $notes = new \App\Notes;

        $notes->date               = date('Y-m-d', strtotime($request->date));
        $notes->notes              = $request->notes; 
        $notes->patient_id         = $request->patient_id;
        $notes->clinic_id          = Auth::user()->clinic_id;
        if($notes->save())
        {
            return response()->json(['success' => true, 'msg' => 'Record successfully Added']);
        }else{
            return response()->json(['success' => false, 'msg' => 'An error occured while adding a new record!']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {   
        $notes = \App\Notes::find($id);
        return view('notes.edit')->with('patient_id', $request->patient_id)->with('notes', $notes);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $notes = \App\Notes::find($id);

        $notes->date               = date('Y-m-d', strtotime($request->date));
        $notes->notes              = $request->notes;  

        if($notes->save())
        {
            return response()->json(['success' => true, 'msg' => 'Record successfully Updated']);
        }else{
            return response()->json(['success' => false, 'msg' => 'An error occured while updating a new record!']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
