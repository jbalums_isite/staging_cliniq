<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use Datatables;
use Auth;

class PatientRecordsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        return view('patient.patient');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('patient.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(\App\Http\Requests\AddPatientRequest $request)
    {
         try{

            DB::beginTransaction();

            $people_contact = new \App\Contacts;
            
            $people_contact->mobile        = $request->get('mobile');
            $people_contact->telephone     = $request->get('tel');
            $people_contact->save();

            $people_address = new \App\Address;

            $people_address->street        = $request->get('street');
            $people_address->province      = $request->get('province');
            $people_address->city          = $request->get('city');
            $people_address->save();

            $people = new \App\People;

            $people->firstname              = $request->get('firstname');
            $people->middlename             = $request->get('middlename');
            $people->lastname               = $request->get('lastname');
            $people->birthplace             = $request->get('birthplace');
            $people->birthdate              = date_format(date_create($request->get('birthdate')),"Y-m-d");
            $people->gender                 = $request->get('gender');
            $people->nationality            = $request->get('nationality');
            $people->civil_status           = $request->get('civil_status');
            $people->title                  = $request->get('title');
            $people->contact_id             = $people_contact->id;
            $people->address_id             = $people_address->id;
            $people->save();
            

            $user = new \App\Users;

            $user->email                    = $request->get('email');
            $user->password                 = bcrypt('123Password');
            $user->clinic_id                = Auth::user()->clinic_id;
            $user->people_id                = $people->id;
            $user->type                     = $request->get('type');
            $user->save();

            $imageName = '';
            if($request->hasFile('icon')){
                $imageName = date('mdyHiss').'.'.$request->icon->getClientOriginalExtension();
                $request->icon->move(public_path('uploads'), $imageName); 
            }
            $patient = new \App\Patients;
            
            $patient->PatientID             = $request->get('patient_id'); //month day yr - number
            $patient->blood_type            = $request->get('blood_type');
            $patient->user_id               = $user->id;
            $patient->previous_doctor       = $request->get('previous_doctor');
            $patient->icon                  = $imageName;
            $patient->save();
 
            for ($i=0; $i < count($request->get('previous_docs')); $i++) {  
                $pd = new \App\PreviousDoctors;
                $pd->name       = $request->get('previous_docs')[$i];
                $pd->details    = $request->get('doc_hosp')[$i];
                $pd->patient_id   = $patient->id;
                $pd->save();
            }

            activity() 
           ->withProperties(['id' => $patient->id, 'name' => $patient->PatientID, 'table' => 'Patient'])
           ->log('create');
            DB::commit();

            return response()->json(['success' => true, 'msg' => 'Patient successfully added!']);

        }catch(\Exception $e){
            DB::rollback();
            return response()->json(['success' => false, 'msg'  => 'An error occured while trying to add a new record!']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {   
        $patient = \App\Patients::where('patientID', $id)->get()->first();
        return view('patient.profile')->with('patient', $patient);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        $patient = \App\Patients::find($id);
        $prev_docs = \App\PreviousDoctors::where('patient_id', $id)->get();
        return view('patient.edit')->with('patient', $patient)->with('prev_docs', $prev_docs);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   
        $p = \App\Patients::where('id', $id)->first();
        try{

            DB::beginTransaction();

            $people_contact = \App\Contacts::find($p->user->people->contact_id);
            
            $people_contact->mobile        = $request->get('mobile');
            $people_contact->telephone     = $request->get('tel');
            $people_contact->save();

            $people_address = \App\Address::find($p->user->people->address_id);

            $people_address->street        = $request->get('street');
            $people_address->province      = $request->get('province');
            $people_address->city          = $request->get('city');
            $people_address->save();

            $people = \App\People::find($p->user->people->id);

            $people->firstname              = $request->get('firstname');
            $people->middlename             = $request->get('middlename');
            $people->lastname               = $request->get('lastname');
            $people->birthplace             = $request->get('birthplace');
            $people->birthdate              = date('Y-m-d', strtotime($request->get('birthdate')));
            $people->gender                 = $request->get('gender');
            $people->nationality            = $request->get('nationality');
            $people->civil_status           = $request->get('civil_status');
            $people->title                  = $request->get('title'); 
            $people->save();
            

            /*$user = new \App\Users;

            $user->email                    = $request->get('email');
            $user->password                 = bcrypt($request->get('password'));
            $user->clinic_id                = Auth::user()->clinic_id;
            $user->people_id                = $people->id;
            $user->type                     = $request->get('type');
            $user->save();*/


            $patient = \App\Patients::find($id);
             
            $patient->blood_type            = $request->get('blood_type'); 
            $patient->previous_doctor       = $request->get('previous_doctor');
            $patient->icon                  = $request->get('icon');
            $patient->save();


            $pd1 = \App\PreviousDoctors::where('patient_id', $patient->id)->delete();
            
            for ($i=0; $i < count($request->get('previous_docs')); $i++) {  
                $pd = new \App\PreviousDoctors;
                $pd->name       = $request->get('previous_docs')[$i];
                $pd->details    = $request->get('doc_hosp')[$i];
                $pd->patient_id   = $patient->id;
                $pd->save();
            }

            activity() 
           ->withProperties(['id' => $patient->id, 'name' => $patient->PatientID, 'table' => 'Patient'])
           ->log('update');
            DB::commit();

            return response()->json(['success' => true, 'msg' => 'Patient successfully updated!']);

        }catch(\Exception $e){
            DB::rollback();
            return response()->json(['success' => false, 'msg'  => 'An error occured while trying to update record!']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $patient = \App\Patients::find($id);
        
            activity() 
           ->withProperties(['id' => $patient->id, 'name' => $patient->PatientID, 'table' => 'Patient'])
           ->log('delete');
        if($patient->delete()){
            return response()->json(['success' => true, 'msg' => 'Patient successfully removed!']);
        }else{
            return response()->json(['success' => true, 'msg' => 'An error occured while removing patient!']);
        }
    }

    public function upload_form(Request $request){ 
        return view('medical_history.upload')->with('patient_id', $request->patient_id)
                                             ->with('record_type', $request->record_type)
                                             ->with('record_id', $request->record_id);
    }

    public function get_last_patient_id(){
        //month day yr - number
        $date = date('mdy');
        $patients = \App\Patients::join('users', 'users.id', '=', 'patients.user_id')->where('patientID', 'like', '%'.$date.'%')->get();
        if(sizeof($patients) <= 0){
            $patients = array(array('PatientID'=>$date.'-0'));
        }
        return response()->json(['patients' => $patients]);
    }

    public function get_meds_desc(Request $request){
        $meds = \App\Medicines::find($request->id);
        return response()->json(['data' => $meds]);
    }

    public function get_patients(){
        DB::statement(DB::raw('set @row:=0'));
        $users = \App\Patients::selectRaw('*, patients.id as p_id, @row:=@row+1 as row')
                                ->join('users', 'users.id', '=', 'patients.user_id')
                                ->where('users.clinic_id', Auth::user()->clinic_id);
         return Datatables::of($users)
            ->AddColumn('id', function($column){
               return $column->PatientID;
            })
            ->AddColumn('patientid', function($column){
               return $column->PatientID;
            })
            ->AddColumn('name', function($column){
               return ucfirst($column->user->people->firstname)." ".
                      ucfirst(substr($column->user->people->middlename, 0, 1)).". ".
                      ucfirst($column->user->people->lastname)." ";
            })
            ->AddColumn('gender', function($column){
               return ucfirst($column->user->people->gender);
            })
            ->AddColumn('age', function($column){
               return date_diff(date_create($column->user->people->birthdate), date_create('today'))->y;
            })
            ->AddColumn('address', function($column){
               return $column->user->people->address->street.", ".
                      $column->user->people->address->city.", ".
                      $column->user->people->address->province;
            })
            ->AddColumn('contact', function($column){
               return $column->user->people->contact->mobile;
            })
            ->AddColumn('status', function($column){
               return 'active';
            })
            ->AddColumn('actions', function($column){
                return '<div class="btn-group table-dropdown">
                            <button data-toggle="dropdown" class="btn-xs btn-for-table dropdown-toggle">
                                
                                <span class="ace-icon fa fa-ellipsis-h icon-on-center"></span>
                            </button>

                            <ul class="dropdown-menu dropdown-caret dropdown-menu-right">
                                <li>
                                    <a href="#" class="edit-patient-btn edit-mf-btn" data-id="'.$column->p_id.'">Edit</a>
                                </li> 
                                <li>
                                    <a href="#" class="delete-patient-btn dele-mf-btn" data-id="'.$column->p_id.'">Delete</a>
                                </li> 
                                <li>
                                    <a href="/patient/print/'.$column->p_id.'" class="" data-id="'.$column->p_id.'">Print</a>
                                </li> 
                                <li>
                                    <a href="/patient/send/'.$column->p_id.'" class="" data-id="'.$column->p_id.'">Send</a>
                                </li> 
                                <li>
                                    <a href="/patient/download/'.$column->p_id.'" class="" data-id="'.$column->p_id.'">Download</a>
                                </li> 
                            </ul>
                        </div>';
               //return '<a href="#" class="editBtn edit-mf-btn" data-id="'.$column->p_id.'"><i class="fa fa-edit"></i></a>';
            })
            
            
            ->make(true);    
    }
    
    public function get_allergies($id){

        DB::statement(DB::raw('set @row:=0'));
        $allergies = \App\Allergies::selectRaw('*, @row:=@row+1 as row')->where('patient_id', $id);

         return Datatables::of($allergies)

            ->AddColumn('row', function($column){
               return $column->row;
            })
            ->AddColumn('name', function($column){
               return $column->name;
            })            
            ->AddColumn('description', function($column){
               return $column->description;
            })
            ->AddColumn('actions', function($column){
               return '<a href="#" class="editAllergyBtn edit-mf-btn" data-id="'.$column->id.'"><i class="fa fa-edit"></i></a>';
            })

            ->make(true); 
    }
    

    public function get_notes($id){
        DB::statement(DB::raw('set @row:=0'));
        $allergies = \App\Notes::selectRaw('*, @row:=@row+1 as row')->where('patient_id', $id);

         return Datatables::of($allergies)

            ->AddColumn('row', function($column){
               return $column->row;
            })
            ->AddColumn('date', function($column){
               return $column->date;
            })            
            ->AddColumn('notes', function($column){
               return $column->notes;
            })
            ->AddColumn('actions', function($column){

               if(Auth::user()->type == 'doctor'){
                   return '<a href="#" class="editNotesBtn edit-mf-btn" data-id="'.$column->id.'"><i class="fa fa-edit"></i></a>';
                }else{
                    return '';
                }
            })

            ->make(true); 
    }

    public function get_hist($patient_id, $type){

        DB::statement(DB::raw('set @row:=0'));
        $hist = \App\MedicalHistories::selectRaw('*, @row:=@row+1 as row')
                                        ->where('patient_id', $patient_id)
                                        ->where('history_type', $type);

         return Datatables::of($hist)

            ->AddColumn('row', function($column){
               return $column->row;
            })
            ->AddColumn('date', function($column){
               return date('M. d, Y', strtotime($column->date));
            })            
            ->AddColumn('name', function($column){
               return $column->name;
            })            
            ->AddColumn('description', function($column){
               return $column->description;
            })
            ->AddColumn('actions', function($column){
                $str_ = '';
                $img_ctr_ = \App\Uploads::where('patient_id', $column->patient_id)
                                ->where('record_type', $column->history_type)
                                ->where('record_id', $column->id)->count();
                if($img_ctr_ > 0 ){
                    $str_ = ' <a href="#" class="viewUploadedImage edit-mf-btn" data-type="'.$column->history_type.'" data-id="'.$column->id.'"><i class="fa fa-image"></i></a> ';
                }
                    
                return ' <a href="#" class="uploadImage edit-mf-btn" data-type="'.$column->history_type.'" data-id="'.$column->id.'"><i class="fa fa-upload"></i></a>'.$str_.'
                        <a href="#" class="editMedHistBtn edit-mf-btn"  data-type="'.$column->history_type.'" data-id="'.$column->id.'"><i class="fa fa-edit"></i></a>';
            })

            ->make(true); 
    }
    public function get_result($patient_id, $type){


        DB::statement(DB::raw('set @row:=0'));
        $res = \App\Results::selectRaw('*, @row:=@row+1 as row')
                                        ->where('patient_id', $patient_id)
                                        ->where('result_type', $type);

         return Datatables::of($res)

            ->AddColumn('row', function($column){
               return $column->row;
            })
            ->AddColumn('date', function($column){
               return date('M. d, Y', strtotime($column->date));
            })            
            ->AddColumn('name', function($column){
               return $column->name;
            })            
            ->AddColumn('description', function($column){
               return $column->description;
            })
            ->AddColumn('actions', function($column){
                $str_ = '';
                $img_ctr_ = \App\Uploads::where('patient_id', $column->patient_id)
                                ->where('record_type', $column->result_type)
                                ->where('record_id', $column->id)->count();
                if($img_ctr_ > 0 ){
                    $str_ = ' <a href="#" class="viewUploadedImage edit-mf-btn" data-type="'.$column->result_type.'" data-id="'.$column->id.'"><i class="fa fa-image"></i></a> ';
                }
               return '<a href="#" class="uploadImage edit-mf-btn" data-type="'.$column->result_type.'" data-id="'.$column->id.'"><i class="fa fa-upload"></i></a>'.$str_.'
                        <a href="#" class="ediResBtn edit-mf-btn" data-type="'.$column->result_type.'"  data-id="'.$column->id.'"><i class="fa fa-edit"></i></a>';
            })

            ->make(true); 
    }
    public function get_meds($patient_id){


        DB::statement(DB::raw('set @row:=0'));
        // $res = \App\MedicationDescriptions::selectRaw('*, @row:=@row+1 as row') 
        //                                 ->join('medications', 'medications.id', '=', 'medication_id')
        //                                 ->where('patient_id', $patient_id)
        //                                 ->groupBy('date');;
        $res = \App\Medications::selectRaw('*, @row:=@row+1 as row')->where('patient_id', $patient_id);

         return Datatables::of($res)

            ->AddColumn('row', function($column){
               return $column->row;
            })
            ->AddColumn('date', function($column){
               return date('M. d, Y', strtotime($column->date));
            })             
            ->AddColumn('description', function($column){
                $str = '';//'<thead><tr><th>Medicine</th><th>Description</th></tr></thead>';
                $descs  = \App\MedicationDescriptions::where('medication_id', $column->id)->get();
                foreach ($descs as $desc) {
                    $str .= '<tr>
                                <td class="col-md-4">'
                                    .$desc->medicine->name.
                                '</td>
                                <td class="col-md-8">'
                                    .$desc->description.
                                '</td>
                            </tr>';

                }
               return '<table class="table table-custom">'.$str.'</table>';


               //return $column->description;
            })
            ->AddColumn('actions', function($column){
               if(Auth::user()->type == 'doctor'){
                return '<a href="#" class="editmedsBtn edit-mf-btn" data-id="'.$column->id.'"><i class="fa fa-edit"></i></a>';
               }else{
                return '';
               }
            })

            ->make(true); 
    }


    public function get_vitals($id){

        DB::statement(DB::raw('set @row:=0'));
        $allergies = \App\Vitals::selectRaw('*, @row:=@row+1 as row')->where('patient_id', $id);

         return Datatables::of($allergies)

            ->AddColumn('row', function($column){
               return $column->row;
            })
            ->AddColumn('date', function($column){
               return date('M. d, Y', strtotime($column->date));
            })            
            ->AddColumn('weight', function($column){
               return $column->weight;
            })       
            ->AddColumn('temperature', function($column){
               return $column->temperature;
            })       
            ->AddColumn('pulse', function($column){
               return $column->pulse;
            })       
            ->AddColumn('blood_pressure', function($column){
               return $column->blood_pressure;
            })
            ->AddColumn('respiration', function($column){
               return $column->respiration;
            })
            ->AddColumn('pain', function($column){
               return $column->pain;
            }) 
            ->AddColumn('actions', function($column){
               return '<a href="#" class="editVitalsBtn edit-mf-btn" data-id="'.$column->id.'"><i class="fa fa-edit"></i></a>';
            })

            ->make(true); 
    }

    public function update_profile_data(Request $request){ 

        $res = DB::table($request->table) 
                    ->where('id', $request->id)
                    ->update([$request->field => $request->value]); 

        return $res;
    }

    public function delete_profile_data(Request $request)
    {
       
        $res =  DB::table($request->table) 
                    ->where('id', $request->id)
                    ->delete();
        
        if($res){
            return response()->json(['success' => true, 'msg' => 'Record successfully removed!']);
        }else{
            return response()->json(['success' => true, 'msg' => 'An error occured while removing record!']);
        }
    }


    public function getAllRecords($patient_id, $table){
        $records = DB::table($table)->where('patient_id', $patient_id)->get();
        return response()->json(['records'=> $records, 'table'=> $table]);
    }


    public function getAllVitals($patient_id){
        $records = DB::table('vitals')->where('patient_id', $patient_id)->get();
        return response()->json(['records'=> $records]);
    }


    public function getAllMeds($patient_id){
        $meds = DB::table('medications')->where('patient_id', $patient_id)->get();
        
        $records = array();
        foreach ($meds as $med) {
          $desc = \App\MedicationDescriptions::where('medication_id', $med->id)
                                              ->join('medicines', 'medicines.id', '=', 'medicine_id')->get();
          array_push($records, ['med'=>[$med, 'desc'=> $desc]]);
        }

        return response()->json(['records'=> $records]);
    }
    public function getAllLabRes($patient_id){
        $recs = DB::table('results')->where('patient_id', $patient_id)->get();
        
        $records = array();
        foreach ($recs as $rec) { 
          $imgs_ = \App\Uploads::where('patient_id', $rec->patient_id)
                          ->where('record_type', $rec->result_type)
                          ->where('record_id', $rec->id)->get();
          array_push($records, ['rec'=>[$rec, 'desc'=> $desc]]);
        }

        return response()->json(['records'=> $records]);
    }
    public function getAllTestRes($patient_id){
        $recs = DB::table('results')->where('patient_id', $patient_id)->get();
        
        $records = array();
        foreach ($recs as $rec) { 
          $imgs_ = \App\Uploads::where('patient_id', $rec->patient_id)
                          ->where('record_type', $rec->result_type)
                          ->where('record_id', $rec->id)->get();
          array_push($records, ['rec'=>[$rec, 'desc'=> $desc]]);
        }

        return response()->json(['records'=> $records]);
    }

    public function send_patient_record($id){
      $patient = \App\Patients::find($id);
      return view('patient.send')->with('patient', $patient);
    }

    public function print_patient_record($id){
      $patient = \App\Patients::find($id);
      return view('patient.print')->with('patient', $patient);
    }

    public function download_patient_record($id){
      $patient = \App\Patients::find($id);
      return view('patient.download')->with('patient', $patient);
    }
}
