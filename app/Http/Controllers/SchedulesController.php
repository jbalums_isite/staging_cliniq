<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Datatbles;
use DB;
class SchedulesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $doctors = \App\Users::where('type', 'doctor')->where('clinic_id', Auth::user()->clinic_id)->get();
        $patients = \App\Patients::selectRaw('*, patients.id as pid')
                                    ->join('users', 'users.id', '=', 'user_id')->where('clinic_id', Auth::user()->clinic_id)->get();
        return view('schedules.schedules')->with('doctors', $doctors)->with('patients', $patients);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $doctors = \App\Users::where('type', 'doctor')->where('clinic_id', Auth::user()->clinic_id)->get();
        $patients = \App\Patients::selectRaw('*, patients.id as pid')
                                    ->join('users', 'users.id', '=', 'user_id')->where('clinic_id', Auth::user()->clinic_id)->get();
        return view('schedules.create')->with('doctors', $doctors)->with('patients', $patients);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $apt = new \App\Appointments;

        $apt->doctor_id = $request->get('doctor');
        $apt->patient_id = $request->get('patient');
        $apt->notes = $request->get('notes');
        $apt->datetime = $request->get('datetime')/*date('Y-m-d H:i:s', strtotime($request->get('date')))*/; 

        if($apt->save())
        {
            return response()->json(['success' => true, 'msg' => 'Appointment successfully Added']);
        }else{
            return response()->json(['success' => false, 'msg' => 'An error occured while adding a new appointment!']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $doctors = \App\Users::where('type', 'doctor')->where('clinic_id', Auth::user()->clinic_id)->get();
        $patients = \App\Patients::selectRaw('*, patients.id as pid')
                                    ->join('users', 'users.id', '=', 'user_id')->where('clinic_id', Auth::user()->clinic_id)->get();
        $apt = \App\Appointments::find($id);

        return view('schedules.edit')->with('doctors', $doctors)->with('patients', $patients)->with('apt', $apt);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $apt = \App\Appointments::find($id);

        $apt->doctor_id = $request->get('doctor');
        $apt->patient_id = $request->get('patient');
        $apt->notes = $request->get('notes');
        $apt->datetime = $request->get('datetime'); 

        if($apt->save())
        {
            return response()->json(['success' => true, 'msg' => 'Appointment successfully Updated']);
        }else{
            return response()->json(['success' => false, 'msg' => 'An error occured while updating a new appointment!']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = \App\Appointments::find($id)->destroy($id);
        if($data){
            return response()->json(['success' => true,  'msg' => 'Record successfully removed']);
        }else{
             return response()->json(['success' => false, 'msg' => 'An error occured while trying to remove the record']);
        }
    }

    public function get_all_appointments($date){
        $app = \App\Appointments::selectRaw('appointments.*, appointments.id as apt_id')
                                ->join('patients', 'patients.id', '=', 'patient_id')
                                ->where('appointments.datetime', '>=', $date.' 00:00:00')    
                                ->orderBy('datetime', 'ASC')->get();

        return response()->json(['appmnts'=> $app]);
    }

    public function get_all_other_appointments($date){
        $app = \App\Appointments::selectRaw('*, appointments.id as apt_id')
                                ->join('patients', 'patients.id', '=', 'patient_id')
                                ->where('datetime', '>=', $date.' 00:00:00')
                                ->where('datetime', '<=', $date.' 23:59:59')
                                ->orderBy('datetime', 'ASC')->get();

        return response()->json(['appmnts'=> $app]);
    }

    public function get_apt_patient_name($id){
        $column = \App\Patients::find($id);
        $name = ucfirst($column->user->people->firstname)." ".
                      ucfirst(substr($column->user->people->middlename, 0, 1)).". ".
                      ucfirst($column->user->people->lastname)." ";
        return response()->json(['name'=> $name]);
    }

    public function check_schedule(Request $request){
        $dt = date('Y-m-d H:i', strtotime($request->date));
        if(strlen($request->id) > 0){            
            $data = \App\Appointments::where('id', '!=', $request->id)->where('datetime', 'like',  '%'.$dt.'%')->count();
        }else{
            $data = \App\Appointments::where('datetime', 'like',  '%'.$dt.'%')->count();            
        }
        return response()->json($data);
    }
}
