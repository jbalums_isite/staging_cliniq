<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class AccountSetUpController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('set_up.set_up');  
        
        /*$clinic = \App\ClinicAccounts::all();

        if(sizeof($clinic) > 0){ 
            return redirect('/home');
        }else{
            return view('set_up.set_up');           
        }*/
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{

            DB::beginTransaction();

            $clinic = new \App\ClinicAccounts;
            $clinic->clinic_name = $request->get('clinic_name');
            $clinic->clinic_type = $request->get('clinic_type');
            $clinic->prefix      = $request->get('prefix');
            $clinic->save();

            $people_contact = new \App\Contacts;
            
            $people_contact->mobile        = $request->get('mobile');
            $people_contact->telephone     = $request->get('tel');
            $people_contact->save();

            $people_address = new \App\Address;

            $people_address->street        = $request->get('street');
            $people_address->province      = $request->get('province');
            $people_address->city          = $request->get('city');
            $people_address->save();

            $people = new \App\People;

            $people->firstname          = $request->get('firstname');
            $people->middlename         = $request->get('middlename');
            $people->lastname           = $request->get('lastname');
            $people->birthplace         = $request->get('birthplace');
            $people->birthdate          = date_format(date_create($request->get('birthdate')),"Y-m-d");
            $people->gender             = $request->get('gender');
            $people->nationality        = $request->get('nationality');
            $people->civil_status       = $request->get('civil_status');
            $people->title              = $request->get('title');
            $people->contact_id         = $people_contact->id;
            $people->address_id         = $people_address->id;
            $people->save();
            

            $user = new \App\Users;

            $user->email                    = $request->get('email');
            $user->password                 = bcrypt($request->get('password'));
            $user->clinic_id                = $clinic->id;
            $user->people_id                = $people->id;
            $user->type                     = $request->get('type');
            $user->save();
            DB::commit();

            return response()->json(['success' => true, 'msg' => 'Account successfully added!']);

        }catch(\Exception $e){
            DB::rollback();
            return response()->json(['success' => false, 'msg'  => 'An error occured while trying to add a new record!']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
