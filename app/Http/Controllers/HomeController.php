<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    { 
            if (!Auth::check()) {
                return view('auth.login');
            }else{
                if(sizeof(Auth::user()->setting) == 0){
                    $setttings = new \App\Settings;
                    $setttings->dasboard       = 1;
                    $setttings->patient        = 1;
                    $setttings->medicine       = 1;
                    $setttings->services       = 1;
                    $setttings->view_products  = 1;
                    $setttings->stocks         = 1;
                    $setttings->manufacturers  = 1;
                    $setttings->brands         = 1;
                    $setttings->schedules      = 1;
                    $setttings->referral_form  = 1;
                    $setttings->request_form   = 1;
                    $setttings->custom_form        = 1;
                    $setttings->dialy_reports        = 1;
                    $setttings->monthly_reports      = 1;
                    $setttings->quarterly_reports    = 1;
                    $setttings->annual_reports       = 1;
                    $setttings->users          = 1;
                    $setttings->allergies      = 1;
                    $setttings->user_id         = Auth::user()->id;
                    $setttings->save();
                }

                return view('admin.new_dashboard');
            }
    }
}
