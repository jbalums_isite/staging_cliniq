<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EditUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        
        return [
            'firstname'             => 'required',
            'lastname'              => 'required',
            'birthdate'             => 'required',
            'birthplace'            => 'required',
            'nationality'           => 'required',
            'mobile'                => 'required',
            'tel'                   => 'required',
            'province'              => 'required',
            'city'                  => 'required',
            'street'                => 'required',
            'gender'                => 'required',
            'civil_status'          => 'required',
            'type'                  => 'required',
            'password'              => 'min:5',
            'new_password'          => 'min:5',
            'confirm_new_password'   => 'min:5|same:new_password',
        ];
    }

    public function messages(){
        return[
            '*.required' => 'This field is required!'
        ];
    }
}
