<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Validator;
use Auth;
use Hash;

class ChangePasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
       Validator::extend('pwdvalidation', function($field, $value, $parameters)
        {
            return Hash::check($value, Auth::user()->password);
        });
        return [  
                'old_password' => 'required|pwdvalidation:'.Auth::user()->password,
                'new_password'       => 'required|min:6',
                'confirm_password'   => 'required|min:6|same:new_password', 
        ];
    }
    public function messages(){
        return [
            'old_password.pwdvalidation' => 'Old password does not match'
        ];
    }
}
