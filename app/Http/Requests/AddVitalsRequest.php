<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddVitalsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'date'              => 'required',
            'weight'            => 'required',
            'temperature'       => 'required',
            'pulse'             => 'required',
            'blood_pressure'    => 'required',
            'respiration'       => 'required',
            'pain'              => 'required',

        ];
    }
}
