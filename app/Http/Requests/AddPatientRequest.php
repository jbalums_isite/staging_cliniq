<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddPatientRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'firstname'          => 'required',
            'lastname'           => 'required',
            'birthdate'          => 'required',
            'birthplace'         => 'required',
            'nationality'        => 'required',
            'mobile'             => 'required',
            'tel'                => 'required',
            'province'           => 'required',
            'city'               => 'required',
            'street'             => 'required',
            'gender'             => 'required',
            'civil_status'       => 'required',
            'type'               => 'required',
            'email'              => 'email|min:5|required|unique:users,email',
            /*'password'           => 'required|min:5',
            'confirm_password'   => 'required|min:5|same:password', */
            'patient_id'         => 'required|unique:patients,PatientID',
            'blood_type'         => 'required',
        ];
    }
}
