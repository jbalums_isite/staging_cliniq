<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BillingDetails extends Model
{
    
    public function master(){
    	return $this->belongsTo('\App\BillingMasters', 'master_id');
    }
    
    public function service(){
    	return $this->belongsTo('\App\Services', 'record_id');
    }
    
    public function product(){
    	return $this->belongsTo('\App\Products', 'record_id');
    }
}
