<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Medications extends Model
{
    public function descriptions(){
        return $this->hasMany('\App\MedicationDescriptions', 'medication_id');
    } 
    
    public function patient(){
    	return $this->belongsTo('\App\Patients', 'patient_id');
    }
}
