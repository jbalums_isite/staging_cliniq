<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notes extends Model
{
    
    public function patient(){
    	return $this->belongsTo('\App\Patients', 'patient_id');
    }
}
