<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Products extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    
    public function manufacturer(){
    	return $this->belongsTo('\App\Manufacturers', 'manufacturer_id');
    }
    public function brand(){
    	return $this->belongsTo('\App\Brands', 'brand_id');
    }
}
