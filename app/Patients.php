<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Patients extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    
    public function user(){
    	return $this->belongsTo('\App\Users', 'user_id');
    }

    public function medical_histories(){
    	return $this->hasMany('\App\MedicalHistories', 'patient_id');
    }
	
	public function allergies(){
    	return $this->hasMany('\App\Allergies', 'patient_id');
    }
	
	public function  medications(){
    	return $this->hasMany('\App\Medications', 'patient_id');
    }
	
	public function notes(){
    	return $this->hasMany('\App\Notes', 'patient_id');
    }
	
	public function vitals(){
    	return $this->hasMany('\App\Vitals', 'patient_id');
    }
	
	public function results(){
    	return $this->hasMany('\App\Results', 'patient_id');
    } 

}
