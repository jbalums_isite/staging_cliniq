<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    
    public function people(){
        return $this->belongsTo('\App\People', 'people_id');
    }

    public function clinic(){
        return $this->belongsTo('\App\ClinicAccounts', 'clinic_id');
    }
    public function setting(){
        return $this->hasMany('\App\Settings', 'user_id');
    }
}
