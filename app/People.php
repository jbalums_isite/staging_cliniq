<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class People extends Model
{
    //
    public function contact(){
    	return $this->belongsTo('\App\Contacts', 'contact_id');
    }
    public function address(){
    	return $this->belongsTo('\App\Address', 'address_id');
    }
}
