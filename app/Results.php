<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Results extends Model
{
    public function patient(){
    	return $this->belongsTo('\App\Patients', 'patient_id');
    }
}
