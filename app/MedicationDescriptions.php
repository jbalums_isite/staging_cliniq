<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MedicationDescriptions extends Model
{
    // 
    public function medicine(){
    	return $this->belongsTo('\App\Medicines', 'medicine_id');
    }
    public function medication(){
        return $this->belongsTo('\App\Medications', 'medication_id');
    }
}
