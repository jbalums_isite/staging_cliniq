<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Allergies extends Model
{
    
    public function patient(){
    	return $this->belongsTo('\App\Patients', 'patient_id');
    }
}
