<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inventories extends Model
{
    
    public function product(){
    	return $this->belongsTo('\App\Products', 'product_id');
    }
}
