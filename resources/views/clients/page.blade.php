@extends('includes.app')

@section('content')

<div class="full-page">
	
	<div class="login-grids col-md-4 col-md-offset-4">
			
			<div class="login-right">
				<div class="col-md-12  text-center">
					<img src="/tmpl/images/cliniq-logo.png">
				</div>
				<h3 class="c-gray  text-center">DR. MEJIA CLINIQ</h3>
				<form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
                        {{ csrf_field() }}


	                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">    
						<div class="inner-addon left-addon">
						    <i class="glyphicon glyphicon-user"></i>
						    <input type="email" class="form-control" name="email" value="Email" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Email';}" required="" />
						</div>
						@if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
					</div>

					<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">	
						<div class="inner-addon left-addon">
						    <i class="glyphicon glyphicon-lock"></i>
						    <input type="password" class="form-control" name="password" value="Password" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Password';}" required="" />
						</div>
						@if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
					</div>

					<button type="submit" class="btn btn-block btn-sky-blue">
					SIGN IN
					</button>
					
					<br>
					<h4 class="sky-blue text-center p5"><a href="#">Forgot password?</a></h4>
				</form>
			</div>
			
	</div>
</div>

@endsection
