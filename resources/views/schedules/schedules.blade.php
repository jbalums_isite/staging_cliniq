@extends('new_template.app')
 
@section('content')
<link rel="stylesheet" type="text/css" href="{{asset('css/cal_theme.css')}}">
<link href="{{asset('tmpl/sweetalert.css')}}" rel="stylesheet">
<script type="text/javascript" src="{{asset('tmpl/sweetalert.min.js')}}"></script>
<div class="breadcrumbs ace-save-state" id="breadcrumbs">
	<ul class="breadcrumb">
		<li> 
			<a href="/">Home</a>
		</li> 
		<li> 
			Schedules
		</li> 
	</ul><!-- /.breadcrumb -->

	
	<ul class="breadcrumb pull-right action_buttons"> 
		<li> 
			<a href="javascript:void(0)" class="back_btn">
				<i class="fa fa-angle-left"></i>
				Back
			</a> 
		</li>  
	</ul><!-- /.breadcrumb -->

</div>


<div class="page-content"> 
	<div class="row">
		<div class="col-md-9" style="min-height: 550px;">
		    <div id="caleandar">

		    </div>
		    <div class="col-md-12">&nbsp;</div>
		</div>
		<div class="col-md-3 cal_top_head__" style="padding-left: 0px;">
			<div class="cal_top_head">
				<img src="{{asset('img/cal.png')}}" alt="img">
				<div>
					<small>YOUR SCHEDULE TODAY!</small>
					<h2 id="todaysDate">{{date('F')}} <b>{{date('d')}}</b>, {{date('Y')}}</h2>
				</div>
			</div>
			<div class="cal_content">
				<ul>
					<!-- <li>
						<i class="fa fa-clock-o fa-2x"></i>
						<div class="appointments_content">
							<b>9:30 AM</b>
							<p>Sample content here...</p>
						</div>
					</li> -->
				</ul>
				<a href="#" class="addappointment_btn"><i class="fa fa-plus"></i> ADD APPOINTMENT</a>
			</div>
		</div>
	</div><!-- /.row -->
</div><!-- /.page-content -->
  <!-- /page content -->

<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;" id="addmodal"></div>
<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;" id="editmodal"></div>
 
    <div id="fc_create" data-toggle="modal" data-target="#CalenderModalNew"></div>
    <div id="fc_edit" data-toggle="modal" data-target="#CalenderModalEdit"></div>
@endsection 

@section('script_content')
<script type="text/javascript" src="{{asset('tmpl/js/calendar.js')}}"></script>
<script type="text/javascript">
	$(function(){

		var d = new Date(); 
		var y = d.getFullYear(), m = d.getMonth(), d = d.getDate();

		refreshCalendar(y,m+1,d);


		$(".addappointment_btn").click(function(x){  
			x.preventDefault();
			var that = this;
			$("#addmodal").modal();
			$.ajax({
				url: '/schedules/create',					
				success: function(data) {
					$("#addmodal").html(data);
		
				}
			});	
		});

		setTimeout(resize_action_buttons_height, 100);
        function resize_action_buttons_height(){  
			$('.action_buttons').css('height',$('#breadcrumbs').height()+"px");
			$('.action_buttons a').css('height',$('#breadcrumbs').height()+"px");

			var style = '<style type="text/css">';
			style+= '.eventday{width:'+($('.cld-day').width())+'px !important;}';
			style+= '.eventday{height:'+($('.cld-day').height())+'px !important;}';
			style+= '.cld-title{width:'+($('.cld-number').width()+30)+'px !important;}';
			style+= '.cld-title{height:'+($('.eventday').height())+'px !important;}';
			style+= '.cld-title{padding-top:'+($('.eventday').height()/($(window).width() < 768 ? 2:8))+'px !important;}';
			style+= '</style>';
			$(style).appendTo("head");
/*		      $('.eventday').css('width', ($('.cld-day').width()));
		      $('.eventday').css('height', ($('.cld-day').height()));
		      $('.cld-title').css('width', ($('.eventday').width()+10));
		      $('.cld-title').css('height', ($('.eventday').height()));
		      $('.cld-title').css('padding-top',($('.eventday').height()/8)+'px');*/
        }
/*        $('.eventday').click(function(a){
        	console.log();
        });	*/
        $(document).off('click', '.cld-day').on('click', '.cld-day', function(e){
        	e.preventDefault();
        	$("#todaysDate").fadeOut();
        	$(".cal_content").slideUp();
        	var d = $(this).find('b')[0].dataset.date.split('-');
        	//var d = ($(this).children()[0].dataset.date).split('-');
        	//refreshNewCalendar()
        	refreshNewCalendar(d[0], d[1], d[2]);
        	$("#todaysDate").fadeIn();
        	$(".cal_content").slideDown();
        });	
     /*   $('.cld-title a').on('click', function(e){
        	e.preventDefault();
        	console.log($(this));
        })*/

        $(document).off('click', '.edit-sched-profile-btn').on('click', '.edit-sched-profile-btn', function(e){
			e.preventDefault();
			var that = this;
			$("#editmodal").modal();
			$("#editmodal").html('');
            $.ajax({
              url: '/schedules/'+that.dataset.id+'/edit',         
              success: function(data) {
					$("#editmodal").html(data);
              }
            });
        });

        $(document).off('click', '.delete-sched-profile-btn').on('click', '.delete-sched-profile-btn', function(e){
			e.preventDefault();
            var that = this;
            swal({
              title: "Are you sure?",
              text: "This will delete the selected appointment",
              type: "warning",
              showCancelButton: true,
              confirmButtonColor: "#DD6B55",
              confirmButtonText: "Yes, delete it!",
              closeOnConfirm: false
            },
            function(){
              $.ajax({
                  url:'/schedules/'+that.dataset.id, 
                  data: $("#edit-appointment-form").serialize(),
                  type:'DELETE',
                  success: function(data) { 
                     if(data.success){                         
                       swal("Deleted!",  data.msg, "success"); 
                       refreshCalendar(y,m,d);
                     // notiff('Success!', data.msg,'success'); //title, msg, type
                      $('.antoclose').click();
                    }else{
                      notiff('Error!', data.msg,'error'); //title, msg, type
                     // $('#addallergymodal').modal('toggle');
                    }
                  }
                });
             
            });
          });
	});
	
		function refreshCalendar(year, month, date){
			var dt = year+"-"+month+"-"+date;
			$.ajax({
				type: 'GET',
				async: false,
				url: '/get_all_appointments/'+dt,					
				success: function(data) {
					var appointments_ctr = 0;
					var str = '';

					var events = [
					  //{'Date': new Date(year, month ,date), 'Title': '<b data-date="'+d_+'">'+appointments_ctr+'</b><br>Appointments'},
					];
/*					console.log(data.appmnts);*/
					var temp_dt = null;
					for(var i = 0 ; i < data.appmnts.length ; i++){
						 

						var tt = new Date(data.appmnts[i].datetime);
						var hours = (tt.getHours() >= 12) ? tt.getHours()-12 : tt.getHours(); 
						var mins = tt.getMinutes();
						var ampm = (tt.getHours() >= 12) ? "PM" : "AM"; 
						var y = tt.getFullYear();
						var m = tt.getMonth();
						var d = tt.getDate();
						var cur_date = tt.getFullYear()+'-'+(tt.getMonth()+1)+'-'+tt.getDate();
						var today__ = new Date();
						var today_date = today__.getFullYear()+'-'+(today__.getMonth()+1)+'-'+today__.getDate();
						var pname = '';
 
 						var nxtt = null;
 						var next_date = null; 
 						if(i < data.appmnts.length-1){
							nxtt = new Date(data.appmnts[i+1].datetime);
							next_date = nxtt.getFullYear()+'-'+(nxtt.getMonth()+1)+'-'+nxtt.getDate();
 						}
 						//console.log(next_date == null);
 						/*console.log(cur_date != next_date);
 						 */
						appointments_ctr++;
						var appt__ = $(window).width() < 768 ? 'Appt':'Appointments';
 						if(cur_date != next_date){ 
							events.push({'Date': new Date(y, m ,d), 'Title': '<b data-date="'+cur_date+'">'+appointments_ctr+'</b><br>'+appt__});
							appointments_ctr = 0;
 						}else if(next_date == null){
							events.push({'Date': new Date(y, m ,d), 'Title': '<b data-date="'+cur_date+'">'+appointments_ctr+'</b><br>'+appt__});
 						}


						$.ajax({
							async: false,
							type: 'GET',
							url: '/get_apt_patient_name/'+data.appmnts[i].patient_id,					
							success: function(data) { 
								pname = data.name;
							}
						});	

						if(today_date == cur_date){ 
							str+= 	'<li>'+
										'<div class="btn-group table-dropdown" style="float: right;top:0px;margin-bottom: -25px;right: 10px;"> '+
											'<button data-toggle="dropdown" class="btn btn-white btn-xs no-border setting_btn dropdown-toggle" style="top: -5px;right: -1px;">'+
												'<i class="ace-icon fa fa-cog bigger-120 blue"></i>'+
											'</button>	'+
					                        '<ul class="dropdown-menu dropdown-caret dropdown-menu-right">'+
					                            '<li>'+
					                                '<a href="#" class="edit-sched-profile-btn edit-ptnt-btn " data-id="'+data.appmnts[i].apt_id+'">Edit</a>'+
					                            '</li>  '+
					                            '<li>'+
					                                '<a href="#" class="delete-sched-profile-btn edit-mf-btn" data-id="'+data.appmnts[i].apt_id+'">Cancel</a>'+
					                            '</li>'+  
					                        '</ul>'+
					                    '</div>'+
										'<i class="fa fa-clock-o fa-2x"></i>'+
										'<div class="appointments_content">'+
											'<b>'+hours+':'+mins+' '+ampm+'</b>'+
											'<i><br/>'+pname+'</i>	'+
											'<p>'+data.appmnts[i].notes+'</p>	'+
										'</div>'+

									'</li>'; 
						}

					}
					$('.cal_content ul').html(str);
					var d_ = year+'-'+month+'-'+date;
					var settings = {};
					$('#caleandar').html('');
					var element = document.getElementById('caleandar');
					caleandar(element, events, settings);
				}
			});	

		}


	
		function refreshNewCalendar(year, month, date){
			var dt = year+"-"+month+"-"+date;
			var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
			$.ajax({
				type: 'GET',
				async: false,
				url: '/get_all_other_appointments/'+dt,					
				success: function(data) {
					var appointments_ctr = 0;
					var str = ''; 

					for(var i = 0 ; i < data.appmnts.length ; i++){
						
						var tt = new Date(data.appmnts[i].datetime);
						var hours = (tt.getHours() >= 12) ? tt.getHours()-12 : tt.getHours(); 
						var mins = tt.getMinutes();
						var ampm = (tt.getHours() >= 12) ? "PM" : "AM"; 
						var pname = '';
						$.ajax({
							async: false,
							type: 'GET',
							url: '/get_apt_patient_name/'+data.appmnts[i].patient_id,					
							success: function(data) { 
								pname = data.name;
							}
						});	

						var cur_date = tt.getFullYear()+'-'+(tt.getMonth()+1)+'-'+tt.getDate();	
							str+= 	'<li>'+
										'<i class="fa fa-clock-o fa-2x"></i>'+
										'<div class="appointments_content">'+
											'<b>'+hours+':'+mins+' '+ampm+'</b>'+
											'<i><br/>'+pname+'</i>	'+
											'<p>'+data.appmnts[i].notes+'</p>	'+
										'</div>'+
									'</li>'; 
					}
					$('.cal_content ul').html(str);
					$('#todaysDate').html(months[month-1]+' <b>'+date+'</b>, '+year)
				}
			});
		}
</script>
@endsection