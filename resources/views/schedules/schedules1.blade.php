@include('not_supported')
 
<!-- FullCalendar --> 
{!! Html::style(url('dst/vendors/fullcalendar/fullcalendar.min.css')) !!} 
<link href="dst/vendors/fullcalendar/dist/fullcalendar.min.css" rel="stylesheet" media="print">
<link href="tmpl/sweetalert.css" rel="stylesheet">
<!-- <script type="text/javascript" src="tmpl/sweetalert.min.js"></script> -->

<div class="top_nav" style="min-height: 37px;margin-bottom: 0px;background: #ededed;    border-bottom: 0px;">
  <div class="nav_menu"  style="min-height: 37px;margin-bottom: 10px;background: #ededed;margin-top: -10px;    border-bottom: 0px;">
    <nav> 


 <!--      <div class="col-md-6">
        
        <ul class="nav navbar-nav navbar-left">
          <li class="">
            <h4>&nbsp;&nbsp;<i class="fa fa-calendar"></i> Schedules</h4>
            <input type="text" name="search" placeholder="Search..." class="hidden">
            <i class="fa fa-search hidden"></i>
          </li>
        </ul>
      </div>
      <div class="col-md-6 col-xs-12 text-right"> 
        <div class="pull-right">
          <a href="#" class="pull-right action_btns addSchedBtn"><h5>Add Schedule <i class=" c-orange glyphicon glyphicon-remove-circle trans-rt-45"></i></h5></a>
        <a href="#" class="pull-right action_btns addTransBtn"><h5>Add Transaction <i class=" c-orange glyphicon glyphicon-remove-circle trans-rt-45"></i></h5></a>
        </div>
      </div> -->


 

      <div class="col-md-6 search_bar_">

        <ul class="nav navbar-nav navbar-left s_bar_">
          <li class="">
            <h4>&nbsp;&nbsp;<i class="fa fa-calendar"></i> Schedules</h4>
          </li>
        </ul>

          <div class="btn-group action_group pull-right"> 
            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
              <i class="fa fa-bars"></i>
              <span class="sr-only">Toggle Dropdown</span>
            </button>
            <ul class="dropdown-menu dropdown-menu-left" role="menu">
              <li> 
              <a href="#" class="pull-right action_btns addSchedBtn"><h5>Add Schedule <i class=" c-orange glyphicon glyphicon-remove-circle trans-rt-45"></i></h5></a>
              </li>
              <li>
                <a href="#" class="pull-right action_btns addTransBtn"><h5>Add Transaction <i class=" c-orange glyphicon glyphicon-remove-circle trans-rt-45"></i></h5></a>
              </li>  
            </ul>
          </div>

      </div>

      <div class="col-md-6 pull-right"> 
        <a href="#" class="pull-right action_btns addSchedBtn hide_md"><h5>Add Schedule <i class=" c-orange glyphicon glyphicon-remove-circle trans-rt-45"></i></h5></a>
        <a href="#" class="pull-right action_btns addTransBtn hide_md"><h5>Add Transaction <i class=" c-orange glyphicon glyphicon-remove-circle trans-rt-45"></i></h5></a>
      </div>


    </nav>
  </div>
</div>
<!-- /top action nav -->
<div class="right_col p15 pt30" role="main">
  
  <div class="row   mt50  p15">
    <div id='calendar'></div>
  </div>

</div>

        <!-- FullCalendar --> 

    {!! Html::script(url('dst/vendors/moment/min/moment.min.js')) !!}
    {!! Html::script(url('dst/vendors/fullcalendar/fullcalendar.min.js')) !!}

   <!-- calendar modal -->
    <div id="CalenderModalNew" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      
    </div>
    <div id="CalenderModalEdit" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">

          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title" id="myModalLabel2">Edit Calendar Entry</h4>
          </div>  
          <div class="modal-body">

            <div id="testmodal2" style="padding: 5px 20px;">
              <form id="antoform2" class="form-horizontal calender" role="form">
                <div class="form-group">
                  <label class="col-sm-3 control-label">Title</label>
                  <div class="col-sm-9">
                    <input type="text" class="form-control" id="title2" name="title2">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3 control-label">Description</label>
                  <div class="col-sm-9">
                    <textarea class="form-control" style="height:55px;" id="descr2" name="descr"></textarea>
                  </div>
                </div>

              </form>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default antoclose2" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary antosubmit2">Set Appointment</button>
          </div>
        </div>
      </div>
    </div>

    <div id="fc_create" data-toggle="modal" data-target="#CalenderModalNew"></div>
    <div id="fc_edit" data-toggle="modal" data-target="#CalenderModalEdit"></div>
    <!-- FullCalendar -->
 <!-- Laravel Javascript Validation -->
 <div class="hidden">
    <button id="refresh_btn">&nbsp;</button>
                <div class="form-group">
                  <label class="col-sm-3 control-label">Doctor:</label>
                  <div class="col-sm-9">
                    <select class="form-control" name="doctor">
                      <option disabled selected>Select Doctor</option>
                      @foreach($doctors as $doctor)
                        <option value="{{$doctor->id}}">{{ ucfirst($doctor->people->firstname)." ".
                                      ucfirst(substr($doctor->people->middlename, 0, 1)).". ".
                                      ucfirst($doctor->people->lastname) }}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3 control-label">Patient:</label>
                  <div class="col-sm-9">
                    <select class="form-control" name="patient2">
                      <option disabled selected>Select Patient</option>
                      @foreach($patients as $patient)
                        <option value="{{$patient->pid}}" data-patient_id="{{$patient->PatientID}}">{{ ucfirst($patient->user->people->firstname)." ".
                                      ucfirst(substr($patient->user->people->middlename, 0, 1)).". ".
                                      ucfirst($patient->user->people->lastname) }}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
 </div>
 <style type="text/css">
   
  tbody > tr:hover{
    cursor: pointer;
  }
  .dataTables_filter{
    display: none;
  }
  .search_bar_{
    display: block;
  }
  .ppi_{
    display: none;
  }
  @media (max-width: 768px){
 
    .p15{
      padding: 5px !important;
      padding-left: 5px !important;
        margin-right: 0px;
        margin-left: 0px;
    }
    .s_bar_{
      float: left;
        margin-right: 5px;
        margin-top: 0px;
    }
    .search_bar_ {
      width: 100%;
    } 
    .ppi_{
      display: block;
    }
  }
  @media (max-width: 769px){
    .open>.dropdown-menu {
        display: block;
        margin-top: 0px;
    }
    .fc-center{
          margin-top: 10px;
    }
    .fc-scroller{
      height: 100% !important;
    }
  }
    
 </style>
 <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>

 {!! JsValidator::formRequest('App\Http\Requests\AddScheduleRequest', '#add-appointment-form') !!}
    <script>
      $(document).ready(function() {
/*
        $('#birthday').daterangepicker({
            singleDatePicker: true,
            calender_style: "picker_4"
          }, function(start, end, label) {
            console.log(start.toISOString(), end.toISOString(), label);
          }); */
        $('.addSchedBtn').click(function(){
          var d = new Date();
            $('#birthday').val((d.getMonth()+1)+'/'+d.getDate()+"/"+d.getFullYear());
            $('#fc_create').click();  
            $('#birthday').daterangepicker({ 
              singleDatePicker: true,
              calender_style: "picker_4",
              timePicker: true, 
              timePickerIncrement: 15,
              startDate:(d.getMonth()+1)+'/'+d.getDate()+"/"+d.getFullYear()+' 7:30 AM' ,
              endDate: (d.getMonth()+1)+'/'+d.getDate()+"/"+d.getFullYear() ,
              minDate: (d.getMonth()+1)+'/'+d.getDate()+"/"+d.getFullYear()+' 7:30 AM' ,  
              locale: {
                format: 'MM/DD/YYYY h:mm A'
              }
            },function(start, end, label) {
              var d1_ = new Date(start._d);
              var d2_ = new Date();
              if(d2_ > d1_){
                $('.antosubmit').attr('disabled', true);
             //   $("#birthday").parent().parent().addClass('has-error has-error');
              }else{                
                $('.antosubmit').attr('disabled', false);
           //     $("#birthday").parent().parent().addClass('has-error');
              }

            }); 
        });


        $("#fc_create").on('click', function(x){  

          var that = this;
          //$("#CalenderModalNew").modal();
          $.ajax({
            url: '/schedules/create',         
            success: function(data) {
              $("#CalenderModalNew").html(data);
            }
          }); 
        });
        
        var date = new Date(),
            d = date.getDate(),
            m = date.getMonth(),
            y = date.getFullYear(),
            started,
            categoryClass;

        var calendar = $('#calendar').fullCalendar({
          header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
          },
          selectable: false ,
          selectHelper: true,
          draggable: false,
          eventLimit: true,
          select: function(start, end, allDay) {
              /*      $('#fc_create').click(); 
                    $('#birthday').val();
                    started = start;
                    ended = end;

                    $(".antosubmit").on("click", function() {
                      var title = $("#title").val();
                      if (end) {
                        ended = end;
                      }

                      categoryClass = $("#event_type").val();

                      if (title) {
                        calendar.fullCalendar('renderEvent', {
                            title: title,
                            start: new Date(),  
                            end: new Date(),
                            allDay: false
                          },
                          true // make the event "stick"
                        );
                      }

                      $('#title').val('');

                      calendar.fullCalendar('unselect');

                      $('.antoclose').click();

                      return false;
                    });*/
          },
          eventStartEditable : false,
          eventClick: function(calEvent, jsEvent, view) {
            var new_d = new Date();
            var this_d = new Date(calEvent.start._d);
            if(new_d > this_d){
              return;
            }
            $('#fc_edit').click();
            $('#title2').val(calEvent.title); 
            
            $.ajax({
              url: '/schedules/'+calEvent._id+'/edit',         
              success: function(data) {
                $("#CalenderModalEdit").html(data);
              }
            });
            categoryClass = $("#event_type").val();

            // $(".antosubmit2").on("click", function() {
            //   calEvent.title = $("#title2").val();

            //   calendar.fullCalendar('updateEvent', calEvent);
            //   $('.antoclose2').click();
            // });

            calendar.fullCalendar('unselect');
          },
          dayClick: function(date, jsEvent, view) {  
              //console.log(jsEvent);
               /*   $('#fc_create').click();
                var d = new Date(date._d); 
                var today = new Date();
                var time_ = today.getHours();
                var t2 = 0;
                t2 = time_-12;
                var ft = ' ';
                if(t2 < 0){
                  ft += time_+':30 AM'; 
                }else{
                  ft += '5:30 PM'; 
                }


                if(d.getDate() >= today.getDate() && d.getMonth() == today.getMonth()){
                  $('#birthday').val((d.getMonth()+1)+'-'+d.getDate()+"-"+d.getFullYear());
                  $('#birthday').daterangepicker({ 
                    singleDatePicker: true,
                    calender_style: "picker_4",
                    timePicker: true,
                    timePickerIncrement: 15,
                    startDate:(d.getMonth()+1)+'/'+d.getDate()+"/"+d.getFullYear()+ft,
                    endDate: (d.getMonth()+1)+'/'+d.getDate()+"/"+d.getFullYear()+ft,
                    minDate: (d.getMonth()+1)+'/'+d.getDate()+"/"+d.getFullYear()+ft,  
                    maxDate: (d.getMonth()+1)+'/'+d.getDate()+"/"+d.getFullYear()+' 9:30 PM',
                    locale: {
                      format: 'MM/DD/YYYY h:mm A'
                    }
                  },function(start, end, label) {
                     var d1_ = new Date(start._d);
                        var d2_ = new Date();
                        if(d2_ > d1_){
                          $('.antosubmit').attr('disabled', true);
                          //$("#birthday").parent().parent().addClass('has-error has-error');
                        }else{                
                          $('.antosubmit').attr('disabled', false);
                          //$("#birthday").parent().parent().addClass('has-error');
                        }
                  });  
                }
                 */
          },
          editable: true,
                    timeFormat: 'h:mmA',
          events: []
        });
        refresh_appt()
        $("#refresh_btn").on('click',function(){
        }); 
        $('.fc-button').on('click', function(){
           refresh_appt();                  
        })
      });

          function refresh_appt(){
            $("#calendar").fullCalendar('removeEvents');
            $.ajax({
              url:'/get_all_appointments',
              success: function(data) {
                console.log(data);
                for(var i = 0 ; i < data.appmnts.length ; i++){
                  var d3 = new Date(data.appmnts[i].datetime); 
                  var d4 = new Date(data.appmnts[i].datetime);
                    d4.setMinutes(d4.getMinutes() + 15);
                   $("#calendar").fullCalendar( 'renderEvent', { 
                        title: ' '+$("[name='patient2'] option[value='"+data.appmnts[i].patient_id+"']").text(),
                        start: d3,
                        end: d4, 
                        allDay: false,
                        _id: data.appmnts[i].apt_id,
                        eventRender: function ( event, element ) {
                          $(element).attr("id",data.appmnts[i].apt_id);
                        }
                      }, 
                      false 
                    );
                }
              }
            });
        }
    </script>
    <!-- /FullCalendar -->