<div class="modal-dialog">
        <div class="modal-content">

              {!! Form::open(array('url' => url('/schedules'), 'method' => 'POST', 'class' => 'form-horizontal calender', 'id' => 'add-appointment-form')) !!}
  
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title" id="myModalLabel">Add Appointment</h4>
          </div>
          <div class="modal-body">
            <div id="testmodal" style="padding: 5px 20px;">
             <!--  <form id="antoform" class="form-horizontal calender" role="form">   -->
                <div class="form-group">
                  <label class="col-sm-3 control-label">Date &amp; Time: </label>
                  <div class="col-sm-5"> 
                    <input id="birthday" name="date" class="date-picker form-control" required="required" type="text" value="{{date('Y-m-d')}}">
                  </div>
                  <div class="col-sm-4"> 
                    <div class="form-group">
                        <div class='input-group date' id='datetimepicker3'>
                            <input type='text' class="form-control" name="time" />
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-time"></span>
                            </span>
                        </div>
                    </div>
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-sm-3 control-label">Doctor:</label>
                  <div class="col-sm-9">
                    <select class="form-control" name="doctor">
                      <option disabled selected>Select Doctor</option>
                      @foreach($doctors as $doctor)
                        <option value="{{$doctor->id}}">{{ ucfirst($doctor->people->firstname)." ".
                                      ucfirst(substr($doctor->people->middlename, 0, 1)).". ".
                                      ucfirst($doctor->people->lastname) }}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3 control-label">Patient:</label>
                  <div class="col-sm-9">
                    <select class="form-control" name="patient">
                      <option disabled selected>Select Patient</option>
                      @foreach($patients as $patient)
                        <option value="{{$patient->pid}}" data-patient_id="{{$patient->PatientID}}">{{ ucfirst($patient->user->people->firstname)." ".
                                      ucfirst(substr($patient->user->people->middlename, 0, 1)).". ".
                                      ucfirst($patient->user->people->lastname) }}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3 control-label">Notes:</label>
                  <div class="col-sm-9">
                    <textarea class="form-control" id="descr" name="notes" rows='3'></textarea>
                  </div>
                </div>

            </div>

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-danger antoclose pull-left" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary antosubmit">Set Appointment</button>
            {!! Form::close() !!}
          </div>
        </div>
      </div>
 <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>

 {!! JsValidator::formRequest('App\Http\Requests\AddScheduleRequest', '#add-appointment-form') !!}
      <script type="text/javascript">
        $(function(){
          $('#birthday').datepicker({
            format: "yyyy-mm-dd",
            autoclose: true, 
        }).on('changeDate', function(selected){
            startDate =  $("#from").val();
            $('#to').datepicker('setStartDate', startDate);
            $("#annually-report-table").DataTable().ajax.url( '{{url("/get-annually-sales-report")}}/'+$("#from").val()+'/'+$("#to").val() ).load();
        }); 
          $('#datetimepicker3').datetimepicker({
                format: 'LT'
            });

        /*  var d = new Date();
            $('#birthday').val((d.getMonth()+1)+'/'+d.getDate()+"/"+d.getFullYear()); 
            $('#birthday').daterangepicker({ 
              singleDatePicker: true,
              calender_style: "picker_4",
              timePicker: true, 
              timePickerIncrement: 15,
              startDate:(d.getMonth()+1)+'/'+d.getDate()+"/"+d.getFullYear()+' 7:30 AM' ,
              endDate: (d.getMonth()+1)+'/'+d.getDate()+"/"+d.getFullYear() ,
              minDate: (d.getMonth()+1)+'/'+d.getDate()+"/"+d.getFullYear()+' 7:30 AM' ,  
              locale: {
                format: 'MM/DD/YYYY h:mm A'
              }
            },function(start, end, label) {
              var d1_ = new Date(start._d);
              var d2_ = new Date();
              if(d2_ > d1_){
                $('.antosubmit').attr('disabled', true);
                $("#birthday").parent().parent().addClass('has-error has-error');
              }else{                
                $('.antosubmit').attr('disabled', false);
                $("#birthday").parent().parent().addClass('has-error');
              }

            }); */

          $("#add-appointment-form").on('submit', function(x){
            x.preventDefault();
            var date = $('[name="date"]').val();
            var time = $('[name="time"]').val().split(':'); 
            var hour = time[0]; 
            var min = time[1].split(' ')[0];
            var ampm =  time[1].split(' ')[1];
            if(ampm == 'PM'){
              hour = (parseInt(hour)+parseInt(12));
            } 
            var datetime = date+' '+hour+":"+min+':00'; 
           $.ajax({
            url:'/check-schedule',
            data: $("#add-appointment-form").serialize()+'&datetime='+datetime,
            type:'GET',
            success: function(dt2) { 
              var sel_date = new Date(datetime);
              var today_2 = new Date(); 

              if(sel_date >= today_2){
                if(dt2 == 0){
                  $('.antosubmit').attr('disabled', true);
                  $("#birthday").parent().parent().addClass('has-error has-error');
                  if($("#add-appointment-form").valid()){
                    $.ajax({
                      url:'/schedules',
                      data: $("#add-appointment-form").serialize()+'&datetime='+datetime,
                      type:'POST',
                      success: function(data) { 
                         if(data.success){    
                          notiff('Success!', data.msg,'success'); //title, msg, type
                          $('.antoclose').click();
                          
                          var d = new Date(); 
                          var y = d.getFullYear(), m = d.getMonth(), d = d.getDate();

                          refreshCalendar(y,m+1,d);
                          
                        }else{
                          notiff('Error!', data.msg,'error'); //title, msg, type 
                        }
                      }
                    });
                  }else{
                   $('.antosubmit').attr('disabled', false);
                  }
                  
                }else{
                  notiff('Error!', 'Conflict schedule!','error'); 
                  $('.antosubmit').attr('disabled', false);
                  $("#birthday").parent().parent().addClass('has-error');
                }
              }else{                
                notiff('Error!', 'You can\'t schedule before the current date and time!','error'); 
              }
              

                       
            }
          });
          
        })
            
        })
      </script>