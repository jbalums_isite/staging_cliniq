<div class="modal-dialog modal-md">
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button>
      <h4 class="modal-title">Edit Medicine Form</h4>
    </div>
    {!! Form::open(array('url' => url('/medicines/'.$medicine->id), 'method' => 'PATCH', 'id' => 'edit-medicines-form')) !!}
    <div class="modal-body">
      <div class="form-group">
        <label for="name">Medicine Name</label>
        <input id="name" name="name" type="text" class="form-control" placeholder="Name" value="{{$medicine->name}}" required>
      </div>
      <div class="form-group">
        <label for="description">Description/SIG(Prescription)</label>
        <textarea id="description" name="description" class="form-control" placeholder="Description" rows="3"  required>{{$medicine->description}}</textarea>
      </div>
      <div class="form-group hidden">
        <label for="price">Price</label>
        <input id="price" name="price" type="number" min="1" class="form-control" placeholder="price" value="{{$medicine->price}}" required>
      </div>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        {!! Form::submit('Submit', ['class' => 'btn submit-btn btn-primary btn-gradient pull-right']) !!}
      {!! Form::close() !!}
    </div>

  </div>
</div>



 <!-- Laravel Javascript Validation -->
 <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>

 {!! JsValidator::formRequest('App\Http\Requests\AddMedicinesRequest', '#edit-medicines-form') !!}

<script type="text/javascript">
  $(function(){
      $("#edit-medicines-form").on('submit', function(e){
          e.preventDefault();
          var that = this;
          if($("#edit-medicines-form").valid()){
            $(".submit-btn").addClass("disabled");
            $('button[type=submit], input[type=submit]').prop('disabled',true);
            $.ajax({
                type: 'PATCH',
                url: '/medicines/'+{{$medicine->id}},
                data: $('#edit-medicines-form').serialize(),
                dataType: 'json',
                success: function(data){
                  $('button[type=submit], input[type=submit]').prop('disabled',false);
                  $(".submit-btn").removeClass("disabled");
                }
            }).done(function(data) {
              if(data.success){
                $("#medicines-table").DataTable().ajax.url( '/get-medicines' ).load();
                notiff('Success!', data.msg,'success'); //title, msg, type
                $('#editmodal').modal('toggle');
              }else{
                notiff('Error!', data.msg,'error'); //title, msg, type
                $('#editmodal').modal('toggle');
              }
            }).error(function(data) {
                notiff('Error!', data.msg, 'warning'); //title, msg, type
                $('#editmodal').modal('toggle');
            });
          }
        });
    $("#price").focusout(function(){
      $(this).val(parseFloat($(this).val()).toFixed(2));
    })
  });  
 </script>