@extends('new_template.app')
 
@section('content') 
<div class="breadcrumbs ace-save-state" id="breadcrumbs">
	<ul class="breadcrumb">
		<li> 
			<a href="/">Home</a>
		</li> 
		<li> 
			<a href="/patients">Patients</a>
		</li>  
		<li> 
			Download
		</li> 
	</ul><!-- /.breadcrumb -->

	 

</div>


<div class="page-content"> 
	<div class="row">
		<div class="col-xs-12">
			<div class="btn btn-primary pull-right btn-green-gradient print-btn"> <i class="fa fa-print"></i> <b>Download Now</b></div>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12 text-center" >

			<div id="home" class="tab-pane in active printable" style="">
				<div class="row">
					<div class="  center" style="width: 30%;padding-left: 10px; float: left;">
						<span class="profile-picture">
							<img class="editable img-responsive" alt="Alex's Avatar" id="avatar2" src="{{asset('uploads/'.$patient->icon)}}" style="width: 1in; height: 1in;" />
						</span>
 
  						<h5 style="margin-top: 0px;"> 
							@php
								$mname = strlen($patient->user->people->middlename) > 0 ? substr($patient->user->people->middlename, 0,1).'. ':' ';
							@endphp
							<b>{{$patient->user->people->firstname.' '.
							$mname.
							$patient->user->people->lastname
							}} <br> [ {{$patient->PatientID}} ]</b>
						</h5>
						<div class="space space-4"></div>
  						<table class="table text-left">
  							<tr>
  								<th>Contact No.:</th> 
  								<th>Address:</th> 
  							</tr>
  							<tr> 
  								<td>{{$patient->user->people->contact->mobile}}</td>
  								<td>{{ucfirst($patient->user->people->address->street)}}, 
								{{ucfirst($patient->user->people->address->city)}},
								{{ucfirst($patient->user->people->address->province)}}</td>
  							</tr>
  							<tr>
  								<th>Gender:</th> 
  								<th>Date of Birth:</th> 
  							</tr>
  							<tr>
  								<td>{{ucfirst($patient->user->people->gender)}}</td>
  								<td>{{date('M d, Y', strtotime($patient->user->people->birthdate))}}</td>
  							</tr>
  							<tr>
  								<th>Nationality:</th> 
  								<th>Age:</th> 
  							</tr>
  							<tr>
  								<td>{{ucfirst($patient->user->people->nationality)}}</td>
  								<td>{{date_diff(date_create($patient->user->people->birthdate), date_create('now'))->y}} yrs. old  </td>
  							</tr>
  							<tr>
  								<th>Status:</th> 
  								<th>&nbsp;</th> 
  							</tr>
  							<tr>
  								<td>{{ucfirst($patient->user->people->civil_status)}}</td>
  								<td>&nbsp;</td>
  							</tr> 
  						
  						</table>
  						<table class="table text-left">
  							<tr>
  								<th colspan="2" style="width: 100%;border-bottom: 2px solid #eee;">Previous Doctor</th>
  							</tr>
								@php
									$prev_docs = \App\PreviousDoctors::where('patient_id', $patient->id)->get();
								@endphp
								@foreach($prev_docs as $pd) 
  							<tr>
									<td  style="width: 100%;">
										<span>
											<small><b>{{$pd->name}}</b><br>
											 {{$pd->details}}</small>
										</span>
									</td>
  							</tr>
								@endforeach
  						</table>
					</div><!-- /.col -->
					<div class="text-left" style="width: 69%;padding-left: 10px; float: left;">
						<h6 style="margin-bottom: 0px; background: #eee;padding: 3px; "><b>Allergies</b></h6>
						<table class="table recordsTable">
							<tr>
								<th>Name</th>
								<th>Description</th>
							@foreach($patient->allergies as $allergy)
							</tr>
								<td>{{$allergy->name}}</td>
								<td>{{$allergy->description}}</td>
							</tr>
							@endforeach
						</table>

							@php
								$mh = '';
								$sgh = '';
								$sch = '';
								$fh = '';
							@endphp
							@foreach($patient->medical_histories as $medical_history)
								@if($medical_history->history_type == 'medical_history')
									@php
										$mh.='</tr>'.
												'<td>'.date('m/d/Y', strtotime($medical_history->date)).'</td>'.
												'<td>'.$medical_history->name.'</td>'.
												'<td>'.$medical_history->description.'</td>'.
											 '</tr>';
									@endphp
								@elseif($medical_history->history_type == 'surgical_history')
									@php
										$sgh.='</tr>'.
												'<td>'.date('m/d/Y', strtotime($medical_history->date)).'</td>'.
												'<td>'.$medical_history->name.'</td>'.
												'<td>'.$medical_history->description.'</td>'.
											 '</tr>';
									@endphp  
								@elseif($medical_history->history_type == 'social_history') 
									@php
										$sch.='</tr>'.
												'<td>'.date('m/d/Y', strtotime($medical_history->date)).'</td>'.
												'<td>'.$medical_history->name.'</td>'.
												'<td>'.$medical_history->description.'</td>'.
											 '</tr>';
									@endphp
								@elseif($medical_history->history_type == 'family_history') 
									@php
										$fh.='</tr>'.
												'<td>'.date('m/d/Y', strtotime($medical_history->date)).'</td>'.
												'<td>'.$medical_history->name.'</td>'.
												'<td>'.$medical_history->description.'</td>'.
											 '</tr>';
									@endphp
								@endif
							@endforeach
						<h6 style="margin-bottom: 0px; background: #eee;padding: 3px; "><b>Medical Histories</b></h6>
						<table class="table history_table"> {!!$mh!!}
						</table>
						
						<h6 style="margin-bottom: 0px; background: #eee;padding: 3px; "><b>Surgical Histories</b></h6>
						<table class="table history_table"> {!!$sgh!!}
						</table>

						<h6 style="margin-bottom: 0px; background: #eee;padding: 3px; "><b>Social Histories</b></h6>
						<table class="table history_table"> {!!$sch!!}
						</table>
						
						<h6 style="margin-bottom: 0px; background: #eee;padding: 3px; "><b>Family Histories</b></h6>
						<table class="table history_table"> {!!$fh!!}
						</table>



							@php
								$lr = '';
								$tr = ''; 
							@endphp
							@foreach($patient->results as $result)
								@if($result->result_type == 'test_result')
									@php
										$tr.='</tr>'.
											'<td>'.date('m/d/Y', strtotime($result->date)).'</td>'.
											'<td>'.$result->name.'</td>'.
											'<td>'.$result->description.'</td>'.
										 '</tr>';
									@endphp
								@else
									@php

										$lr.='</tr>'.
												'<td>'.date('m/d/Y', strtotime($result->date)).'</td>'.
												'<td>'.$result->name.'</td>'.
												'<td>'.$result->description.'</td>'.
											 '</tr>';
									@endphp
								@endif
							@endforeach

						<h6 style="margin-bottom: 0px; background: #eee;padding: 3px; "><b>Test Results</b></h6>
						<table class="table history_table"> {!!$tr!!}
						</table>

						<h6 style="margin-bottom: 0px; background: #eee;padding: 3px; "><b>Lab Results</b></h6>
						<table class="table history_table"> {!!$lr!!}
						</table>

						<h6 style="margin-bottom: 0px; background: #eee;padding: 3px; "><b>Notes</b></h6>
						<table class="table recordsTable">
							<tr>
								<th>Date</th>
								<th>Description</th>
							@foreach($patient->notes as $note)
							</tr>
								<td>{{date('m/d/Y', strtotime($note->date))}}</td>
								<td>{{$note->notes}}</td>
							</tr>
							@endforeach
						</table>

						<h6 style="margin-bottom: 0px; background: #eee;padding: 3px; "><b>Vitals</b></h6>
						<table class="table vitals_table">
							<tr>
								<th>Date</th>
								<th>Weight</th>
								<th>Temperature</th>
								<th>Pulse</th>
								<th>BP</th>
								<th>RR</th>
								<th>Pain</th> 
							@foreach($patient->vitals as $vital)
							</tr>
								<td>{{date('m/d/Y', strtotime($vital->date))}}</td>
								<td>{{$vital->weight}}</td>
								<td>{{$vital->temperature}}</td>
								<td>{{$vital->pulse}}</td>
								<td>{{$vital->blood_pressure}}</td>
								<td>{{$vital->respiration}}</td>
								<td>{{$vital->pain}}</td>
							</tr>
							@endforeach
						</table>
					</div>
					 
				</div><!-- /.row -->

				<div class="space-20"></div>
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12"></div>
				</div>
 
			</div><!-- /#home -->
		</div>
	</div>
</div>




<style type="text/css">
	tbody > tr:hover{
		cursor: pointer;
	} 
	.search_bar_{
		display: block;
	}
	.ppi_{
		display: none;
	}
	@media (max-width: 768px){
 
		.p15{
			padding: 5px !important;
			padding-left: 5px !important;
		    margin-right: 0px;
		    margin-left: 0px;
		}
		.s_bar_{
			float: right;
		    margin-right: 5px;
		    margin-top: 0px;
		}
		.search_bar_ {
			width: 100%;
		} 
		.ppi_{
			display: block;
		}
	}
	.table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th{
    padding: 2px !important;
	}	
	.table{  
    	font-size: small;
	}
	td:first-child, th:first-child{
	    width: 50%;
	}
	.recordsTable td:first-child, .recordsTable th:first-child{
		width: 25%;
	} 
	.recordsTable td, .recordsTable th, .history_table td, .history_table th, .vitals_table td, .vitals_table th{
		text-align: left;
		border: 1px solid #eee !important;
	}
	.recordsTable th, .history_table th{
		border-bottom: 2px solid #eee !important;
	}
	
	.history_table td:first-child, .history_table th:first-child, 
	.vitals_table td:first-child, .vitals_table th:first-child{
		width: 20% !important;
		text-align: center;
	} 
	.history_table td:nth-child(2), .history_table th:nth-child(2){
		width: 20% !important;
	}
	.vitals_table td, .vitals_table th{
		width: 13.3333%;
		text-align: center;
	}
	.printable{
		    width: 8.5in;
		    float: none;
		    display: inline-block;
    padding: .5in;
		    box-shadow: 0px 0px 2px 1px #d8d8d8; 
		    min-height: 11.5in;
		    background: #fff;
	}
</style>
<style type="text/css" media="print">
	td, th{
		padding: 2px !important;
	}
	h5{
		margin-top: 0px;
	}
	.table{

    font-size: x-small;
	}
	td:first-child, th:first-child{
	    width: 50%;
	}
	.recordsTable td:first-child, .recordsTable th:first-child{
		width: 25%;
	} 
</style>

@endsection 

@section('script_content')
<script type="text/javascript" src="{{asset('js/jspdf.min.js')}}"></script>
<script type="text/javascript">
	$('.print-btn').on('click', function(){
		 var pdf = new jsPDF();
		 pdf.addHTML($('.printable')[0], function () {
		     pdf.save('{{$patient->PatientID}}_records.pdf');
		 });
	});
</script>

@endsection