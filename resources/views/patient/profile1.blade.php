<!-- top action nav -->
<div class="top_nav" style="height: 37px;margin-bottom: 0px;background: #ededed;    border-bottom: 0px;">
  <div class="nav_menu"  style="height: 37px;margin-bottom: 10px;background: #ededed;margin-top: -10px;    border-bottom: 0px;">
    <nav> 

      <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
        <h4>Patient Profile
            <a href="{{url('/patients')}}" class="btn btn-default btn-xs ajax_btn pull-right"><i class="fa fa-chevron-left"></i> Back</a>
        </h4>
      </div>
    </nav>
  </div>
</div>
<!-- /top action nav -->


<div class="right_col p15 pt30" role="main">  
  <div class="row p15 mt50">
  <div class="x_panel"> 
    <div class="x_content">
      <div class="col-md-12 col-sm-12 col-xs-12   profile_left p0">
        <div class="row mb30 mt15">
          <div class="profile_img profile_img__ text-center p10">
            <div id="crop-avatar">
              <!-- Current avatar -->

              <div class="thumbnail btn-flat">
                  <div class="caption">
                      <i class="fa fa-camera"></i><p>Update Profile Picture</p> 
                  </div>
                  @php 
                    $old_img = \App\Uploads::where('record_type', 'users')->where('patient_id', $patient->id)->get()->first(); 
                  @endphp 
                  @if($patient->user->people->gender == 'male' && sizeof($old_img) == 0)                
                    <img class=" avatar-view" src="/dst/images/user.png" alt="Avatar" title=" ">
                  @elseif($patient->user->people->gender == 'female' && sizeof($old_img) == 0)
                    <img class=" avatar-view" src="/dst/images/picture.jpg" alt="Avatar" title=" ">
                  @elseif(sizeof($old_img) > 0)
                    <img class=" avatar-view" src="/uploads/{{$old_img->image_name}}" alt="Avatar" title=" ">
                  @endif
              </div> 
              <form action="{{url('image-upload')}}"  class="dropzone dropzone2  hidden" id="upload-profile-picture-form"  >
                {{ csrf_field() }}  
                <input type="hidden" name="patient_id" value="{{ $patient->id }}">
                <input type="hidden" name="record_type" value="users">
                <input type="hidden" name="record_id" value="{{ $patient->user->id }}"> 
              </form> 
            </div> 
            <script type="text/javascript"> 
              var dpz1 = null;
              $('.thumbnail').hover( 
                    function(){ 
                          if($(".dz-clickable").length > 0){
                            Dropzone.forElement(".dropzone2").destroy();                                  
                          }
                         dpz1 = $(".dropzone2").dropzone({
                          clickable: true,
                          maxFilesize: 2, 
                          init: function () {
                            var that = this;
                            this.on("success", function (file, response) {  
                              if(response.success){ 
                                notiff('Success!', response.msg,'success'); //title, msg, type 
                                $(".avatar-view").attr('src', '/uploads/'+response.img);
                                        
                              }else{
                                notiff('Error!', response.msg,'error'); //title, msg, type  
                              }
                            });
                          }
                        });
                        $(this).find('.caption').slideDown(250); //.fadeIn(250)
                    },
                    function(){   
                        $(this).find('.caption').slideUp(250); //.fadeOut(205)
                    }
                ); 
              $(".caption").click(function(){
                $("#f1").click();
                $(".dropzone2").click();
              }); 
            </script>

          </div>
          <div class="col-md-9 col-lg-9 col-sm-9 col-xs-12 ">
            <div class="col-md-6 ">
              <input type="hidden" id="patient_id" value="{{ $patient->id }}">
              <div class="form-group col-md-12 ">
                <label class="control-label col-md-3 col-sm-3 col-xs-5  ">Patient ID: </label>
                <div class="col-md-9 col-sm-9 col-xs-7 ">
                  <b>{{ ucfirst($patient->PatientID) }}</b>
                </div>
              </div>
              <div class="form-group col-md-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-5  ">Blood Type: </label>
                <div class="col-md-9 col-sm-9 col-xs-7 ">
                  <b>{{ ucfirst($patient->blood_type) }}</b>
                </div>
              </div>

              <div class="form-group col-md-12 mt15">
                <label class="control-label col-md-3 col-sm-3 col-xs-5  ">Firstname: </label>
                <div class="col-md-9 col-sm-9 col-xs-7 ">
                  <b>{{ ucfirst($patient->user->people->firstname) }}</b>
                </div>
              </div>
              <div class="form-group col-md-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-5  ">Middlename: </label>
                <div class="col-md-9 col-sm-9 col-xs-7 ">
                  <b>{{ ucfirst($patient->user->people->middlename) }}</b>
                </div>
              </div>
              <div class="form-group col-md-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-5  ">Lastname: </label>
                <div class="col-md-9 col-sm-9 col-xs-7 ">
                  <b>{{ ucfirst($patient->user->people->lastname) }}</b>
                </div>
              </div>
              <div class="form-group col-md-12 mt15">
                <label class="control-label col-md-3 col-sm-3 col-xs-5  ">Gender: </label>
                <div class="col-md-9 col-sm-9 col-xs-7 ">
                  <b>{{ ucfirst($patient->user->people->gender) }}</b>
                </div>
              </div>
              <div class="form-group col-md-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-5  ">Birthdate: </label>
                <div class="col-md-9 col-sm-9 col-xs-7 ">
                  <b>{{ date('M. d, Y', strtotime($patient->user->people->birthdate))}}</b>
                </div>
              </div>
              <div class="form-group col-md-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-5  ">Age: </label>
                <div class="col-md-9 col-sm-9 col-xs-7 ">
                  <b>{{ date_diff(date_create($patient->user->people->birthdate), date_create('today'))->y." yrs. old" }}</b>
                </div>
              </div>
              
            </div>

            <div class="col-md-6">
              <div class="form-group col-md-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-5  ">Nationality: </label>
                <div class="col-md-9 col-sm-9 col-xs-7 ">
                  <b>{{ ucfirst($patient->user->people->nationality) }}</b>
                </div>
              </div>
              <div class="form-group col-md-12 mt15">
                <label class="control-label col-md-3 col-sm-3 col-xs-5  ">Street: </label>
                <div class="col-md-9 col-sm-9 col-xs-7 ">
                  <b>{{ ucfirst($patient->user->people->address->street) }}</b>
                </div>
              </div>
              <div class="form-group col-md-12 ">
                <label class="control-label col-md-3 col-sm-3 col-xs-5  ">City: </label>
                <div class="col-md-9 col-sm-9 col-xs-7 ">
                  <b>{{ ucfirst($patient->user->people->address->city) }}</b>
                </div>
              </div>
              <div class="form-group col-md-12 ">
                <label class="control-label col-md-3 col-sm-3 col-xs-5  ">Province: </label>
                <div class="col-md-9 col-sm-9 col-xs-7 ">
                  <b>{{ ucfirst($patient->user->people->address->province) }}</b>
                </div>
              </div>
            </div>

          </div>
        </div>  
             
      </div>

      <div class="col-md-12 col-sm-12 col-xs-12  ">
        <div class="row"> 
            <div class="x_panel">
              <div class="x_title">
                <h2>Allergies &nbsp;<a href="#" class="pull-right  addAllergyBtn"><i class=" c-orange glyphicon glyphicon-remove-circle trans-rt-45"></i></a></h2>
                 <ul class="nav navbar-right panel_toolbox ">
                    <li class="pull-right"><a class="collapse-link"><i class="fa fa-chevron-down tg_c" id="tg_c"></i></a></li>
                  </ul>
                <div class="clearfix"></div>
              </div>
              <div class="x_content">
          
                  <table width="100%" class="table jambo_table theme-table theme-table-edit  mt5 dt-responsive nowrap" id="allergies-table" style="width: 100% !important;">
                    <thead>
                      <th>#</th> 
                      <th>Name</th>
                      <th>Description</th>
                      <th>Edit</th>
                    </thead>
                  </table>
              </div>
            </div>
        </div>  
        <div class="row"> 
            <div class="x_panel">
              <div class="x_title">
                <h2>Medical Histories &nbsp;<a href="#" class="pull-right  addmedhistorybtn"><i class=" c-orange glyphicon glyphicon-remove-circle trans-rt-45"></i></a></h2>
                 <ul class="nav navbar-right panel_toolbox ">
                    <li class="pull-right"><a class="collapse-link"><i class="fa fa-chevron-down tg_c" id="tg_c"></i></a></li>
                  </ul>                 
                <div class="clearfix"></div>
              </div>
              <div class="x_content">
                <div class="" role="tabpanel" data-example-id="togglable-tabs">
                  <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Medical History <span class="badge badge1 bg-green"></span></a> 
                    </li>
                    <li role="presentation" class=""><a href="#tab_content2" role="tab" id="profile-tab1" data-toggle="tab" aria-expanded="false">Surgical History <span class="badge badge2 bg-green"></span></a>
                    </li>
                    <li role="presentation" class=""><a href="#tab_content3" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Social History <span class="badge badge3 bg-green"></span></a>
                    </li>
                    <li role="presentation" class=""><a href="#tab_content4" role="tab" id="profile-tab3" data-toggle="tab" aria-expanded="false">Family History <span class="badge badge4 bg-green"></span></a>
                    </li>
                  </ul>
                  <div id="myTabContent" class="tab-content">
                    <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
                      <table style="width:100%" class="table jambo_table theme-table theme-table-edit   mt5 dt-responsive nowrap" id="medical-history-table" style="width: 100% !important;"  width="100%">
                        <thead>
                          <th>#</th>
                          <th>Date</th>
                          <th>Name</th>
                          <th>Description</th>
                          <th>Actions</th>
                        </thead>
                      </table>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab"> 
                        <table style="width:100%" class="table jambo_table theme-table theme-table-edit  mt5 dt-responsive nowrap" id="surgical-history-table" style="width: 100% !important;" width="100%">
                          <thead>
                            <th>#</th>
                            <th>Date</th>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Actions</th>
                          </thead>
                        </table> 
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="tab_content3" aria-labelledby="profile-tab">
                      <table style="width:100%" class="table jambo_table theme-table theme-table-edit  mt5 dt-responsive nowrap" id="social-history-table" style="width: 100% !important;"  width="100%">
                        <thead>
                          <th>#</th>
                          <th>Date</th>
                          <th>Name</th>
                          <th>Description</th>
                          <th>Actions</th>
                        </thead>
                      </table>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="tab_content4" aria-labelledby="profile-tab">
                      <table style="width:100%" class="table jambo_table theme-table theme-table-edit  mt5 dt-responsive nowrap" id="family-history-table" style="width: 100% !important;"  width="100%">
                        <thead>
                          <th>#</th>
                          <th>Date</th>
                          <th>Name</th>
                          <th>Description</th>
                          <th>Actions</th>
                        </thead>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        </div>  
  
        <div class="row"> 
            <div class="x_panel">
              <div class="x_title">
                <h2>Laboratory Results &nbsp;<a href="#" class="pull-right  addresultbtn"  data-type="laboratory"><i class=" c-orange glyphicon glyphicon-remove-circle trans-rt-45"></i></a></h2>
                 
                 <ul class="nav navbar-right panel_toolbox ">
                    <li class="pull-right"><a class="collapse-link"><i class="fa fa-chevron-down tg_c" id="tg_c"></i></a></li>
                  </ul>
                <div class="clearfix"></div>
              </div>
              <div class="x_content">
                <table style="width:100%" class="table jambo_table theme-table theme-table-edit  mt5 dt-responsive nowrap" id="laboratory-result-table"  width="100%">
                  <thead>
                    <th>#</th>
                    <th>Date</th>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Edit</th>
                  </thead>
                </table>
              </div>
            </div>
        </div>  
  
        <div class="row"> 
            <div class="x_panel">
              <div class="x_title">
                <h2>Test Results &nbsp;<a href="#" class="pull-right  addresultbtn"  data-type="test"><i class=" c-orange glyphicon glyphicon-remove-circle trans-rt-45"></i></a></h2>
                 
                 <ul class="nav navbar-right panel_toolbox ">
                    <li class="pull-right"><a class="collapse-link"><i class="fa fa-chevron-down tg_c" id="tg_c"></i></a></li>
                  </ul>
                <div class="clearfix"></div>
              </div>
              <div class="x_content">
                <table style="width:100%" class="table jambo_table theme-table theme-table-edit  mt5 dt-responsive nowrap" id="test-result-table"  width="100%">
                  <thead>
                    <th>#</th>
                    <th>Date</th>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Edit</th>
                  </thead>
                </table>
              </div>
            </div>
        </div>  


        <div class="row"> 
            <div class="x_panel">
              <div class="x_title">
                <h2>Vitals &nbsp;<a href="#" class="pull-right  addvitalsbtn"  data-type="test"><i class=" c-orange glyphicon glyphicon-remove-circle trans-rt-45"></i></a></h2>
                 
                 <ul class="nav navbar-right panel_toolbox ">
                    <li class="pull-right"><a class="collapse-link"><i class="fa fa-chevron-down tg_c" id="tg_c"></i></a></li>
                  </ul>
                <div class="clearfix"></div>
              </div>
              <div class="x_content">
                <table style="width:100%" class="table jambo_table theme-table theme-table-edit  mt5 dt-responsive nowrap" id="vitals-table"  width="100%">
                  <thead>
                    <th>#</th>
                    <th>Date</th>
                    <th>Weight</th>
                    <th>Temperature</th>
                    <th>Pulse</th>
                    <th>Blood Pressure</th>
                    <th>Respiration</th>
                    <th>Pain</th>
                    <th>Edit</th>
                  </thead>
                </table>
              </div>
            </div>
        </div>  

        <div class="row"> 
            <div class="x_panel">
              <div class="x_title">
                <h2>Medications &nbsp;
                @if(Auth::user()->type == 'doctor')
                  <a href="#" class="pull-right  addmedsbtn"><i class=" c-orange glyphicon glyphicon-remove-circle trans-rt-45"></i></a>
                @endif
                </h2>
                 <ul class="nav navbar-right panel_toolbox ">
                    <li class="pull-right"><a class="collapse-link"><i class="fa fa-chevron-down tg_c" id="tg_c"></i></a></li>
                  </ul>
                <div class="clearfix"></div>
              </div>
              <div class="x_content">
                <table style="width:100%" class="table jambo_table theme-table theme-table-edit  mt5 dt-responsive nowrap" id="medications-table">
                  <thead>
                    <th>#</th>
                    <th>Date</th> 
                    <th>Prescriptions</th>
                    <th>Edit</th>
                  </thead>
                </table>
              </div>
            </div>
        </div> 
        <div class="row"> 
            <div class="x_panel">
              <div class="x_title">
                <h2>Notes &nbsp;

                @if(Auth::user()->type == 'doctor')
                  <a href="#" class="pull-right  addnotesbtn"><i class=" c-orange glyphicon glyphicon-remove-circle trans-rt-45"></i></a>
                  @endif
                </h2>
                 
                 <ul class="nav navbar-right panel_toolbox ">
                    <li class="pull-right"><a class="collapse-link"><i class="fa fa-chevron-down tg_c" id="tg_c"></i></a></li>
                  </ul>
                <div class="clearfix"></div>
              </div>
              <div class="x_content">
                <table style="width:100%" class="table jambo_table theme-table theme-table-edit  mt5 dt-responsive nowrap" id="notes-table">
                  <thead>
                    <th>#</th>
                    <th>Date</th>
                    <th>Notes</th> 
                    <th>Edit</th>
                  </thead>
                </table>
              </div>
            </div>
        </div> 

         
  
      </div>
    </div>
  </div>
  </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;" id="addallergymodal"></div>
<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;" id="addhistmodal"></div>
<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;" id="addresmodal"></div>
<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;" id="addmedsmodal"></div>
<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;" id="addvitalmodal"></div>
<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;" id="addnotesmodal"></div>


<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;" id="editallergymodal"></div>
<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;" id="edithistmodal"></div>
<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;" id="editresmodal"></div>
<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;" id="editmedsmodal"></div>
<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;" id="editvitalmodal"></div>
<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;" id="editnotesmodal"></div>


<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;" id="uploadFormModal"></div>
<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;" id="viewuploadFormModal"></div>

<script type="text/javascript">
  $(function(){
    var med_hist_ctr = 0;
    $('#allergies-table').DataTable({
        bProcessing: true,
        bServerSide: false,
        sServerMethod: "GET",
      'ajax': '/get-allergies/'+$("#patient_id").val(),
      searching: true, 
      paging: true, 
      filtering:true, 
      bInfo: false,
      responsive: true,
      language:{
        "paginate": {
              "next":       "<i class='fa fa-chevron-right'></i>",
              "previous":   "<i class='fa fa-chevron-left'></i>"
          }
      },
        footerCallback: function ( row, data, start, end, display ) {
          var $BOX_PANEL = $('#allergies-table').closest('.x_panel'),
              $ICON = $('#allergies-table').parent().parent().parent().find('#tg_c'),
              $BOX_CONTENT = $BOX_PANEL.find('.x_content'); 
          if(data.length > 0){
              $BOX_CONTENT.slideDown(0);  
              $ICON.removeClass('fa-chevron-down').addClass('fa-chevron-up');
          }else{
              $BOX_CONTENT.slideUp(0); 
              $ICON.removeClass('fa-chevron-up').addClass('fa-chevron-down');
          } 
        },
      "columns": [
              {data: 'row', name: 'row', className: 'text-center', searchable: false,  sortable: true},
              {data: 'name',  name: 'name', className: ' text-left',  searchable: true, sortable: true},
              {data: 'description',  name: 'description', className: '  text-left',  searchable: true, sortable: true},
              {data: 'actions',  name: 'actions', className: 'text-left',  searchable: false, sortable: false},
          ]
    });
    $('#medical-history-table').DataTable({
        bProcessing: true,
        bServerSide: false,
        sServerMethod: "GET",
        'ajax': '/get-hist/'+$("#patient_id").val()+'/medical_history',
        searching: true, 
        paging: true, 
        filtering:true, 
        bInfo: false,
        responsive: true,
        language:{
          "paginate": {
                "next":       "<i class='fa fa-chevron-right'></i>",
                "previous":   "<i class='fa fa-chevron-left'></i>"
            }
        },
        footerCallback: function ( row, data, start, end, display ) {
          var $BOX_PANEL = $('#medical-history-table').closest('.x_panel'),
              $ICON = $('#medical-history-table').parent().parent().parent().parent().parent().parent().find('#tg_c'),
              $BOX_CONTENT = $BOX_PANEL.find('.x_content'),
              $BADGE =  $('#medical-history-table').parent().parent().parent().parent().find('.badge1'); 
              
          if(data.length > 0){
              $BADGE.html(data.length);
              med_hist_ctr++; 
          } 
          if(med_hist_ctr > 0){
              $BOX_CONTENT.slideDown(0);  
              $ICON.removeClass('fa-chevron-down').addClass('fa-chevron-up');
          }else{

              $BOX_CONTENT.slideUp(0); 
              $ICON.removeClass('fa-chevron-up').addClass('fa-chevron-down');
          } 
        },
        "columns": [
              {data: 'row', name: 'row', className: 'text-center', searchable: false,  sortable: true},
              {data: 'date',  name: 'date', className: 'col-md-3 text-left',  searchable: true, sortable: true},
              {data: 'name',  name: 'name', className: 'col-md-3 text-left',  searchable: true, sortable: true},
              {data: 'description',  name: 'description', className: 'col-md-4 text-left',  searchable: true, sortable: true},
              {data: 'actions', name: 'actions', className: 'text-center', searchable: false,  sortable: false},
          ]
    });

    $('#surgical-history-table').DataTable({
        bProcessing: true,
        bServerSide: false,
        sServerMethod: "GET",
        'ajax': '/get-hist/'+$("#patient_id").val()+'/surgical_history',
        searching: true, 
        paging: true, 
        filtering:true, 
        bInfo: false,
        responsive: true,
        language:{
          "paginate": {
                "next":       "<i class='fa fa-chevron-right'></i>",
                "previous":   "<i class='fa fa-chevron-left'></i>"
            }
        }, 
        footerCallback: function ( row, data, start, end, display ) {
          var $BOX_PANEL = $('#surgical-history-table').closest('.x_panel'),
              $ICON = $('#surgical-history-table').parent().parent().parent().parent().parent().parent().find('#tg_c'),
              $BOX_CONTENT = $BOX_PANEL.find('.x_content'),
              $BADGE =  $('#surgical-history-table').parent().parent().parent().parent().parent().find('.badge2'); 
              
          if(data.length > 0){
              $BADGE.html(data.length);
              med_hist_ctr++; 
          } 
          if(med_hist_ctr > 0){
              $BOX_CONTENT.slideDown(0);  
              $ICON.removeClass('fa-chevron-down').addClass('fa-chevron-up');
          }else{

              $BOX_CONTENT.slideUp(0); 
              $ICON.removeClass('fa-chevron-up').addClass('fa-chevron-down');
          } 
        },
        "columns": [
              {data: 'row', name: 'row', className: 'text-center', searchable: false,  sortable: true},
              {data: 'date',  name: 'date', className: 'col-md-3 text-left',  searchable: true, sortable: true},
              {data: 'name',  name: 'name', className: 'col-md-3 text-left',  searchable: true, sortable: true},
              {data: 'description',  name: 'description', className: 'col-md-4 text-left',  searchable: true, sortable: true},
              {data: 'actions', name: 'actions', className: 'text-center', searchable: false,  sortable: false},
          ]
    });

    $('#social-history-table').DataTable({
        bProcessing: true,
        bServerSide: false,
        sServerMethod: "GET",
        'ajax': '/get-hist/'+$("#patient_id").val()+'/social_history',
        searching: true, 
        paging: true, 
        filtering:true, 
        bInfo: false,
        responsive: true,
        language:{
          "paginate": {
                "next":       "<i class='fa fa-chevron-right'></i>",
                "previous":   "<i class='fa fa-chevron-left'></i>"
            }
        }, 
        footerCallback: function ( row, data, start, end, display ) {
          var $BOX_PANEL = $('#social-history-table').closest('.x_panel'),
              $ICON = $('#social-history-table').parent().parent().parent().parent().parent().parent().find('#tg_c'),
              $BOX_CONTENT = $BOX_PANEL.find('.x_content'),
              $BADGE =  $('#social-history-table').parent().parent().parent().parent().find('.badge3'); 
              
          if(data.length > 0){
              $BADGE.html(data.length);
              med_hist_ctr++; 
          } 
          if(med_hist_ctr > 0){
              $BOX_CONTENT.slideDown(0);  
              $ICON.removeClass('fa-chevron-down').addClass('fa-chevron-up');
          }else{

              $BOX_CONTENT.slideUp(0); 
              $ICON.removeClass('fa-chevron-up').addClass('fa-chevron-down');
          } 
        },
        "columns": [
              {data: 'row', name: 'row', className: 'text-center', searchable: false,  sortable: true},
              {data: 'date',  name: 'date', className: 'col-md-3 text-left',  searchable: true, sortable: true},
              {data: 'name',  name: 'name', className: 'col-md-3 text-left',  searchable: true, sortable: true},
              {data: 'description',  name: 'description', className: 'col-md-4 text-left',  searchable: true, sortable: true},
              {data: 'actions', name: 'actions', className: 'text-center', searchable: false,  sortable: false},
          ]
    });
    $('#family-history-table').DataTable({
        bProcessing: true,
        bServerSide: false,
        sServerMethod: "GET",
        'ajax': '/get-hist/'+$("#patient_id").val()+'/family_history',
        searching: true, 
        paging: true, 
        filtering:true, 
        bInfo: false,
        responsive: true,
        language:{
          "paginate": {
                "next":       "<i class='fa fa-chevron-right'></i>",
                "previous":   "<i class='fa fa-chevron-left'></i>"
            }
        }, 
        language:{
          "paginate": {
                "next":       "<i class='fa fa-chevron-right'></i>",
                "previous":   "<i class='fa fa-chevron-left'></i>"
            }
        },
        footerCallback: function ( row, data, start, end, display ) {
          var $BOX_PANEL = $('#family-history-table').closest('.x_panel'),
              $ICON = $('#family-history-table').parent().parent().parent().parent().parent().parent().find('#tg_c'),
              $BOX_CONTENT = $BOX_PANEL.find('.x_content'),
              $BADGE =  $('#family-history-table').parent().parent().parent().parent().find('.badge4'); 
              
          if(data.length > 0){
              $BADGE.html(data.length);
              med_hist_ctr++; 
          } 
          if(med_hist_ctr > 0){
              $BOX_CONTENT.slideDown(0);  
              $ICON.removeClass('fa-chevron-down').addClass('fa-chevron-up');
          }else{

              $BOX_CONTENT.slideUp(0); 
              $ICON.removeClass('fa-chevron-up').addClass('fa-chevron-down');
          } 
        },
        "columns": [
              {data: 'row', name: 'row', className: 'text-center', searchable: false,  sortable: true},
              {data: 'date',  name: 'date', className: 'col-md-3 text-left',  searchable: true, sortable: true},
              {data: 'name',  name: 'name', className: 'col-md-3 text-left',  searchable: true, sortable: true},
              {data: 'description',  name: 'description', className: 'col-md-4 text-left',  searchable: true, sortable: true},
              {data: 'actions', name: 'actions', className: 'text-center', searchable: false,  sortable: false},
          ]
    });
    
    $('#laboratory-result-table').DataTable({
        bProcessing: true,
        bServerSide: false,
        sServerMethod: "GET",
        'ajax': '/get-result/'+$("#patient_id").val()+'/laboratory_result',
        searching: true, 
        paging: true, 
        filtering:true, 
        bInfo: false,
        responsive: true,
        language:{
          "paginate": {
                "next":       "<i class='fa fa-chevron-right'></i>",
                "previous":   "<i class='fa fa-chevron-left'></i>"
            }
        },
        footerCallback: function ( row, data, start, end, display ) {
          var $BOX_PANEL = $('#laboratory-result-table').closest('.x_panel'),
              $ICON = $('#laboratory-result-table').parent().parent().parent().find('#tg_c'),
              $BOX_CONTENT = $BOX_PANEL.find('.x_content'); 
          if(data.length > 0){
              $BOX_CONTENT.slideDown(0);  
              $ICON.removeClass('fa-chevron-down').addClass('fa-chevron-up');
          }else{
              $BOX_CONTENT.slideUp(0); 
              $ICON.removeClass('fa-chevron-up').addClass('fa-chevron-down');
          } 
        },
        language:{
          "paginate": {
                "next":       "<i class='fa fa-chevron-right'></i>",
                "previous":   "<i class='fa fa-chevron-left'></i>"
            }
        },
        "columns": [
              {data: 'row', name: 'row', className: 'text-center', searchable: false,  sortable: true},
              {data: 'date',  name: 'date', className: 'col-md-3 text-left',  searchable: true, sortable: true},
              {data: 'name',  name: 'name', className: 'col-md-3 text-left',  searchable: true, sortable: true},
              {data: 'description',  name: 'description', className: 'col-md-4 text-left',  searchable: true, sortable: true},
              {data: 'actions', name: 'actions', className: 'col-md-1 text-center', searchable: false,  sortable: false},
          ]
    });
    
    
    $('#test-result-table').DataTable({
        bProcessing: true,
        bServerSide: false,
        sServerMethod: "GET",
        'ajax': '/get-result/'+$("#patient_id").val()+'/test_result',
        searching: true, 
        paging: true, 
        filtering:true, 
        bInfo: false,
        responsive: true,
        language:{
          "paginate": {
                "next":       "<i class='fa fa-chevron-right'></i>",
                "previous":   "<i class='fa fa-chevron-left'></i>"
            }
        },
        footerCallback: function ( row, data, start, end, display ) {
          var $BOX_PANEL = $('#test-result-table').closest('.x_panel'),
              $ICON = $('#test-result-table').parent().parent().parent().find('#tg_c'),
              $BOX_CONTENT = $BOX_PANEL.find('.x_content'); 
          if(data.length > 0){
              $BOX_CONTENT.slideDown(0);  
              $ICON.removeClass('fa-chevron-down').addClass('fa-chevron-up');
          }else{
              $BOX_CONTENT.slideUp(0); 
              $ICON.removeClass('fa-chevron-up').addClass('fa-chevron-down');
          } 
        },
        language:{
          "paginate": {
                "next":       "<i class='fa fa-chevron-right'></i>",
                "previous":   "<i class='fa fa-chevron-left'></i>"
            }
        },
        "columns": [
              {data: 'row', name: 'row', className: ' text-center', searchable: false,  sortable: true},
              {data: 'date',  name: 'date', className: 'col-md-3 text-left',  searchable: true, sortable: true},
              {data: 'name',  name: 'name', className: 'col-md-3 text-left',  searchable: true, sortable: true},
              {data: 'description',  name: 'description', className: 'col-md-4 text-left',  searchable: true, sortable: true},
              {data: 'actions', name: 'actions', className: 'col-md-1 text-center', searchable: false,  sortable: false},
          ]
    });
    
    $('#medications-table').DataTable({
        bProcessing: true,
        bServerSide: false,
        sServerMethod: "GET",
        'ajax': '/get-meds/'+$("#patient_id").val(),
        searching: true, 
        paging: true, 
        filtering:true, 
        bInfo: false,
        responsive: true,
        language:{
          "paginate": {
                "next":       "<i class='fa fa-chevron-right'></i>",
                "previous":   "<i class='fa fa-chevron-left'></i>"
            }
        },
        footerCallback: function ( row, data, start, end, display ) {
          var $BOX_PANEL = $('#medications-table').closest('.x_panel'),
              $ICON = $('#medications-table').parent().parent().parent().find('#tg_c'),
              $BOX_CONTENT = $BOX_PANEL.find('.x_content'); 
          if(data.length > 0){
              $BOX_CONTENT.slideDown(0);  
              $ICON.removeClass('fa-chevron-down').addClass('fa-chevron-up');
          }else{
              $BOX_CONTENT.slideUp(0); 
              $ICON.removeClass('fa-chevron-up').addClass('fa-chevron-down');
          } 
        },
        language:{
          "paginate": {
                "next":       "<i class='fa fa-chevron-right'></i>",
                "previous":   "<i class='fa fa-chevron-left'></i>"
            }
        },
        "columns": [
              {data: 'row', name: 'row', className: 'text-center', searchable: false,  sortable: true},
              {data: 'date',  name: 'date', className: 'col-md-3 text-left',  searchable: true, sortable: true}, 
              {data: 'description',  name: 'description', className: 'col-md-9 text-left',  searchable: true, sortable: true},
              {data: 'actions', name: 'actions', className: 'text-center', searchable: false,  sortable: false},
          ]
    });
    
    $('#notes-table').DataTable({
        bProcessing: true,
        bServerSide: false,
        sServerMethod: "GET",
        'ajax': '/get-notes/'+$("#patient_id").val(),
        searching: true, 
        paging: true, 
        filtering:true, 
        bInfo: false,
        responsive: true,
        footerCallback: function ( row, data, start, end, display ) {
          var $BOX_PANEL = $('#notes-table').closest('.x_panel'),
              $ICON = $('#notes-table').parent().parent().parent().find('#tg_c'),
              $BOX_CONTENT = $BOX_PANEL.find('.x_content'); 
          if(data.length > 0){
              $BOX_CONTENT.slideDown(0);  
              $ICON.removeClass('fa-chevron-down').addClass('fa-chevron-up');
          }else{
              $BOX_CONTENT.slideUp(0); 
              $ICON.removeClass('fa-chevron-up').addClass('fa-chevron-down');
          } 
        },
        language:{
          "paginate": {
                "next":       "<i class='fa fa-chevron-right'></i>",
                "previous":   "<i class='fa fa-chevron-left'></i>"
            }
        },
        "columns": [
              {data: 'row', name: 'row', className: 'text-center', searchable: false,  sortable: true},
              {data: 'date',  name: 'date', className: 'col-md-4 text-left',  searchable: true, sortable: true},
              {data: 'notes',  name: 'notes', className: 'col-md-7 text-left',  searchable: true, sortable: true}, 
              {data: 'actions', name: 'actions', className: 'text-center', searchable: false,  sortable: false},
          ]
    });
    $('#vitals-table').DataTable({
        bProcessing: true,
        bServerSide: false,
        sServerMethod: "GET",
        'ajax': '/get-vitals/'+$("#patient_id").val(),
        searching: true, 
        paging: true, 
        filtering:true, 
        bInfo: false,
        responsive: true,
        language:{
          "paginate": {
                "next":       "<i class='fa fa-chevron-right'></i>",
                "previous":   "<i class='fa fa-chevron-left'></i>"
            }
        },
        footerCallback: function ( row, data, start, end, display ) {
          var $BOX_PANEL = $('#vitals-table').closest('.x_panel'),
              $ICON = $('#vitals-table').parent().parent().parent().find('#tg_c'),
              $BOX_CONTENT = $BOX_PANEL.find('.x_content'); 
          if(data.length > 0){
              $BOX_CONTENT.slideDown(0);  
              $ICON.removeClass('fa-chevron-down').addClass('fa-chevron-up');
          }else{
              $BOX_CONTENT.slideUp(0); 
              $ICON.removeClass('fa-chevron-up').addClass('fa-chevron-down');
          } 
        },
        "columns": [
              {data: 'row', name: 'row', className: 'text-center', searchable: false,  sortable: true},
              {data: 'date', name: 'date', className: 'text-center', searchable: true,  sortable: true},
              {data: 'weight', name: 'weight', className: 'text-center', searchable: true,  sortable: true},
              {data: 'temperature', name: 'temperature', className: 'text-center', searchable: true,  sortable: true},
              {data: 'pulse', name: 'pulse', className: 'text-center', searchable: true,  sortable: true},
              {data: 'blood_pressure', name: 'blood_pressure', className: 'text-center', searchable: true,  sortable: true},
              {data: 'respiration', name: 'respiration', className: 'text-center', searchable: true,  sortable: true},
              {data: 'pain', name: 'pain', className: 'text-center', searchable: true,  sortable: true},
              {data: 'actions', name: 'actions', className: 'text-center', searchable: false,  sortable: false},
          ]
    });
    

 

    
    $(document).off('click', ".editAllergyBtn").on('click', ".editAllergyBtn", function(x){ 
      x.preventDefault(); 
      var that = this;
      $("#editallergymodal").modal();
      $.ajax({
        url: '/allergies/'+that.dataset.id+'/edit',  
        data: 'patient_id='+$("#patient_id").val(),       
        success: function(data) {
          $("#editallergymodal").html(data);
        }
      }); 
    });
    
    $(document).off('click', ".editMedHistBtn").on('click', ".editMedHistBtn", function(x){ 
      x.preventDefault(); 
      var that = this;
      $("#edithistmodal").modal();
      $.ajax({
        url: '/medicalhistories/'+that.dataset.id+'/edit',  
        data: 'patient_id='+$("#patient_id").val(),       
        success: function(data) {
          $("#edithistmodal").html(data);
        }
      }); 
    });

    
    $(document).off('click', ".ediResBtn").on('click', ".ediResBtn", function(x){ 
      x.preventDefault(); 
      var that = this;
      $("#editresmodal").modal();
      $.ajax({
        url: '/results/'+that.dataset.id+'/edit',  
        data: 'patient_id='+$("#patient_id").val()+'&type='+that.dataset.type,       
        success: function(data) {
          $("#editresmodal").html(data);
        }
      }); 
    });

    $(document).off('click', ".editVitalsBtn").on('click', ".editVitalsBtn", function(x){ 
      x.preventDefault(); 
      var that = this;
      $("#editvitalmodal").modal();
      $.ajax({
        url: '/vitals/'+that.dataset.id+'/edit',  
        data: 'patient_id='+$("#patient_id").val(),       
        success: function(data) {
          $("#editvitalmodal").html(data);
        }
      }); 
    });

    $(document).off('click', ".editNotesBtn").on('click', ".editNotesBtn", function(x){ 
      x.preventDefault(); 
      var that = this;
      $("#editnotesmodal").modal();
      $.ajax({
        url: '/notes/'+that.dataset.id+'/edit',  
        data: 'patient_id='+$("#patient_id").val(),       
        success: function(data) {
          $("#editnotesmodal").html(data);
        }
      }); 
    });

    $(document).off('click', ".editmedsBtn").on('click', ".editmedsBtn", function(x){ 
      x.preventDefault(); 
      var that = this;
      $("#editmedsmodal").modal();
      $.ajax({
        url: '/medications/'+that.dataset.id+'/edit',  
        data: 'patient_id='+$("#patient_id").val(),       
        success: function(data) {
          $("#editmedsmodal").html(data);
        }
      }); 
    });

    $(document).off('click', '.uploadImage').on('click', '.uploadImage', function(x){
      if($(".dz-clickable").length > 0){
        Dropzone.forElement(".dropzone2").destroy();                                  
      } 
      x.preventDefault();  
      var that = this;
      $("#uploadFormModal").modal();
      $.ajax({
        url: '/upload-form',  
        data: 'patient_id='+$("#patient_id").val()+'&record_type='+that.dataset.type+'&record_id='+that.dataset.id,   
        success: function(data) {
          $("#uploadFormModal").html(data);
        }
      }); 
    });

    $(document).off('click', '.viewUploadedImage').on('click', '.viewUploadedImage', function(x){
      x.preventDefault();  
      var that = this;
      $("#viewuploadFormModal").modal();
      $.ajax({
        url: '/view-images',  
        data: 'patient_id='+$("#patient_id").val()+'&record_type='+that.dataset.type+'&record_id='+that.dataset.id,        
        success: function(data) {
          $("#viewuploadFormModal").html(data);
        }
      }); 
    });
    
  });
  
</script>


<style type="text/css">
  .form-group {
      margin-bottom: 0px;
  }  
  table{
    border-left: 2px solid #f7f7f7 !important;
  }
  .table-custom{
    border: 0px solid !important;
    margin: 0px;
  }

  .table-custom tr,
  .table-custom tr > td{
    border: 0px solid #ddd;
    border-top: 0px !important;
    border-bottom: 1px solid #f7f7f7;
  }
  .table-custom tr:last-child,
  .table-custom tr:last-child > td{
    border-bottom: 0px solid #ddd !important;
  }
  .form-group.col-md-12{
    width: 100% !important;
  }
  /*.x_content, .x_panel{
    min-width: 800px;
    overflow-x: auto; 
  }*/
  .x_content{
    padding-top: 5px;
  }

  .profile_img__{
    float: left;
    width: 180px;
    height: 180px;
  }
  @media (max-width: 768px){
 
    .p15{
      padding: 5px !important;
      padding-left: 5px !important;
        margin-right: 0px;
        margin-left: 0px;
    } 
    .x_panel{
      padding: 0px !important;
    }
    .profile_left .form-group{
      padding: 0px;
    }
    .profile_left  .col-md-6{
      padding: 0px;
    }
  }

  @media (max-width: 415px){
    .profile_img__{
      float: none;
      margin: 0 auto;
    }
  }
</style>