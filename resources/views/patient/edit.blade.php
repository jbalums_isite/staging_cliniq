<div class="modal-dialog modal-lg edit-pt-form">
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button>
      <h4 class="modal-title">Edit Patient Form</h4>
    </div>

    <form action="{{url('/image-upload')}}"  class="hidden" id="upload-profile-picture-form"  >
        {{ csrf_field() }} 
          <input type="file" name="file" id="file_img">
    </form> 

    {!! Form::open(array('url' => url('/patients/'.$patient->id), 'method' => 'PATCH', 'id' => 'edit-patients-form')) !!}
    <div class="modal-body">

      <h5>Profile Picture</h5>
        
      <div class="profile_img profile_img__ text-center p10"> 
        <div class="col-md-12 img_preview">

            @if($patient->user->people->gender == 'male' && sizeof($patient->icon) == 0)     
              <img class=" avatar-view" src="{{asset('/dst/images/user.png')}}" alt="Avatar" title=" ">
            @elseif($patient->user->people->gender == 'female' && sizeof($patient->icon) == 0) 
              <img class=" avatar-view" src="{{asset('/dst/images/picture.jpg')}}" alt="Avatar" title=" ">
            @elseif(sizeof($patient->icon) > 0) 
              <img class=" avatar-view" src="{{asset('uploads/'.$patient->icon)}}" alt="Avatar" title=" ">
            @endif
        </div> 
        <div class="btn btn-success upload-btn fileinput-new"> <i class="fa fa-upload"></i> Upload</div>
        <div class="btn btn-info upload-btn fileinput-exists" style="display: none;"> <i class="fa fa-refresh"></i> Change</div>
      </div>


      <h5>Personal Information</h5>
       

        <div class="row">
          <div class="form-group col-md-6 col-xs-12">
            <label for="firstname">Firstname</label>
            <input id="firstname" name="firstname" type="text" class="form-control" placeholder="Firstname" required value="{{$patient->user->people->firstname}}">
          </div>
          <div class="form-group col-md-6 col-xs-12">
            <label for="lastname">Lastname</label>
            <input id="lastname" name="lastname" type="text" class="form-control" placeholder="Lastname" required value="{{$patient->user->people->lastname}}">
          </div>
        </div>
        <div class="row">
          <div class="form-group col-md-6 col-xs-12">
            <label for="middlename">Middlename</label>
            <input id="middlename" name="middlename" type="text" class="form-control" placeholder="Middlename" required value="{{$patient->user->people->middlename}}">
          </div>
        </div>
        <div class="row mt15">
          
          <div class="col-md-6 xdisplay_inputx form-group has-feedback">
            <label for="birthdate">Birthdate</label>
              <input name="birthdate" type="text" class="form-control has-feedback-left date-picker" id="single_cal4" placeholder="Birthdate" aria-describedby=""  value="{{date('m/d/Y', strtotime($patient->user->people->birthdate))}}">
              <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
          </div>
          <div class="form-group col-md-6 col-xs-12">
            <label for="birthplace">Birthplace</label>
            <input id="birthplace" name="birthplace" type="text" class="form-control" placeholder="Birthplace" required value="{{$patient->user->people->birthplace}}">
          </div>
        </div>

        <div class="row mt15">
          <div class="form-group col-md-6 col-xs-12">
            <label for="gender">Gender</label>
            <select name="gender" id="gender" class="form-control">
              <option disabled selected>Select Gender</option>
              <option value="male" {{$patient->user->people->gender == 'male' ? 'selected': ''}}>Male</option>
              <option value="female"  {{$patient->user->people->gender == 'female' ? 'selected': ''}}>Female</option>
            </select>
          </div>
          <div class="form-group col-md-6 col-xs-12">
            <label for="civil_status">Civil Status</label>
            <select name="civil_status" id="civil_status" class="form-control">
              <option disabled selected>Select Civil Status</option>
              <option value="single" {{$patient->user->people->civil_status == 'single' ? 'selected': ''}}>Single</option>
              <option value="married" {{$patient->user->people->civil_status == 'married' ? 'selected': ''}}>Married</option>
              <option value="widowed" {{$patient->user->people->civil_status == 'widowed' ? 'selected': ''}}>Widowed</option>
              <option value="divorced" {{$patient->user->people->civil_status == 'divorced' ? 'selected': ''}}>Divorced</option>
            </select>
          </div>
        </div>

        <div class="row mt15">
          <div class="form-group col-md-6 col-xs-12">
            <label for="nationality">Nationality</label>
            <input id="nationality" name="nationality" type="text" class="form-control" placeholder="Nationality" required value="{{$patient->user->people->nationality}}">
          </div>
        </div>


        <h5 class="mt25">Contact Information</h5>
        
        <div class="row">
          <div class="form-group col-md-6 col-xs-12">
            <label for="mobile">Mobile No.</label>
            <input id="mobile" name="mobile" type="number" class="form-control" placeholder="Mobile Number" required value="{{$patient->user->people->contact->mobile}}">
          </div>
          <div class="form-group col-md-6 col-xs-12">
            <label for="tel">Landline No.</label>
            <input id="tel" name="tel" type="text" class="form-control" placeholder="Landline Number" required value="{{$patient->user->people->contact->telephone}}">
          </div>
        </div>


        <h5 class="mt25">Address Information</h5>
        
        <div class="row">
          <div class="form-group col-md-6 col-xs-12">
            <label for="street">Street</label>
            <input id="street" name="street" type="text" class="form-control" placeholder="Street" required value="{{$patient->user->people->address->street}}">
          </div>
        </div>
        <div class="row">
          <div class="form-group col-md-6 col-xs-12">
            <label for="city">City</label>
            <input id="city" name="city" type="text" class="form-control" placeholder="City" required value="{{$patient->user->people->address->city}}">
          </div>
          <div class="form-group col-md-6 col-xs-12">
            <label for="province">Province</label>
            <input id="province" name="province" type="text" class="form-control" placeholder="Province" required value="{{$patient->user->people->address->province}}">
          </div>
        </div>

        <!-- <h5 class="mt25">Credential Information</h5>
        
        <div class="row">
          <div class="form-group col-md-6 col-xs-12">
            <label for="type">User Type</label>
            <select name="type" id="type" class="form-control">
              <option value="patient" selected>Patient</option>
            </select>
          </div>
        </div>
        <div class="row">
          <div class="form-group col-md-6 col-xs-12">
            <label for="email">Email</label>
            <input id="email" name="email" type="email" class="form-control" placeholder="Email" required>
          </div>
        </div>
        <div class="row">
          <div class="form-group col-md-6 col-xs-12">
            <label for="password">Password</label>
            <input id="password" name="password" type="password" class="form-control" placeholder="Password" required>
          </div>
          <div class="form-group col-md-6 col-xs-12">
            <label for="confirm_password">Confirm Password</label>
            <input id="confirm_password" name="confirm_password" type="password" class="form-control" placeholder="Confirm Password" required>
          </div>     
        </div> -->
 

        <h5 class="mt25">Patient Information</h5>
        
        <div class="row">
          <div class="form-group col-md-6 col-xs-12">
            <label for="patient_id">Patient ID</label>
            <input id="patient_id" name="patient_id" type="text" class="form-control" placeholder="Patient ID" required value="{{$patient->PatientID}}" disabled>
          </div> 
          
          <div class="form-group col-md-6 col-xs-12">
            <label for="blood_type">Blood Type</label>
            <input id="blood_type" name="blood_type" type="text" class="form-control" placeholder="Blood Type" required  value="{{$patient->blood_type}}">
          </div>
          <div class="form-group col-md-6 col-xs-12 hidden">
            <label for="previous_doctor">Previous Doctor</label>
            <input id="previous_doctor" name="previous_doctor" type="text" class="form-control" placeholder="Previous Doctor" value="N/A">
          </div>
        </div>
        <div class="row"> 
          <div class="form-group col-md-12 col-xs-12">
            <label for="previous_docs">Previous Doctors</label>
          </div>
          <div class="doc_holder">
            @foreach($prev_docs as $pd)
              <div class="row">
                <div class="form-group col-md-5 col-xs-12">
                  <input id="previous_docs" name="previous_docs[]" type="text" class="form-control" placeholder="Previous Doctor" value="{{$pd->name}}">
                </div>
                <div class="form-group col-md-5 col-xs-10">
                  <input id="previous_docs" name="doc_hosp[]" type="text" class="form-control" placeholder="Institution" value="{{$pd->details}}">
                </div>
                <div class="btn btn-success btn-xs addPD"><i class="fa fa-plus"></i></div>
                <div class="btn btn-danger btn-xs removePD"><i class="fa fa-remove"></i></div>
              </div> 
            @endforeach
            @if(sizeof($prev_docs) == 0)             
              <div class="row">
                <div class="form-group col-md-5 col-xs-12">
                  <input id="previous_docs" name="previous_docs[]" type="text" class="form-control" placeholder="Previous Doctor">
                </div>
                <div class="form-group col-md-5 col-xs-10">
                  <input id="previous_docs" name="doc_hosp[]" type="text" class="form-control" placeholder="Institution">
                </div>
                <div class="btn btn-success btn-xs addPD"><i class="fa fa-plus"></i></div>
                <div class="btn btn-danger btn-xs removePD"><i class="fa fa-remove"></i></div>
              </div> 
            @endif
          </div>

        </div>

      <div style="width: 100%;">&nbsp;</div>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        {!! Form::submit('Submit', ['class' => 'btn submit-btn btn-primary btn-gradient pull-right']) !!}
      {!! Form::close() !!}
    </div>

  </div>
</div>


 <!-- Laravel Javascript Validation -->
 <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>

 {!! JsValidator::formRequest('App\Http\Requests\AddPatientRequest', '#edit-patients-form') !!}

<script type="text/javascript">
  $(function(){
      
      var old_pic = '{{$patient->icon}}';
      var formData = new FormData();
      $("#file_img").change(function(){ 
          formData.append('file', $('#file_img')[0].files[0]);
          formData.append('old_file', old_pic);
          formData.append('_token', $('[name="_token"]').val());
          formData.append('user_id', '{{$patient->user->id}}');
          formData.append('patient_ud', '{{$patient->user->id}}');
          $.ajax({
                type: 'POST',
                url: '/uploadPicture',
                data: formData,
                processData: false,
                contentType: false,
                success: function(data){
                  console.log(data);
                  if(data.success){
                    $('.img_preview img').attr('src', '/uploads/'+data.img);
                    $('.fileinput-new').hide();
                    $('.fileinput-exists').show();
                    old_pic = data.img; 
                  }else{ 

                  } 
                },
               beforeSend: function(){
                  $('.img_preview img').attr('src', '/img/spin.gif');
               }
            });
      });
      $('.date-picker').datepicker({
          autoclose: true,
          todayHighlight: true
        })
        //show datepicker when clicking on the icon
        .next().on(ace.click_event, function(){
          $(this).prev().focus();
        });
      $('select').chosen({allow_single_deselect:true}); 
      $('select').chosen({allow_single_deselect:true}); 
          //resize the chosen on window resize
      
          $(window)
          .off('resize.chosen')
          .on('resize.chosen', function() {
            $('select').each(function() {
               var $this = $(this);
               $this.next().css({'width': $this.parent().width()});
            })
          }).trigger('resize.chosen');
          //resize chosen on sidebar collapse/expand
          $(document).on('settings.ace.chosen', function(e, event_name, event_val) {
            if(event_name != 'sidebar_collapsed') return;
            $('select').each(function() {
               var $this = $(this);
               $this.next().css({'width': $this.parent().width()});
            })
          });

      
      $("#edit-patients-form").on('submit', function(e){
          e.preventDefault();
          var that = this;
          if($("#edit-patients-form").valid()){
            $(".submit-btn").addClass("disabled");
            $('button[type=submit], input[type=submit]').prop('disabled',true);
            $.ajax({
                type: 'PATCH',
                url: '/patients/'+{{$patient->id}},
                data: $('#edit-patients-form').serialize()+'&icon='+old_pic,
                dataType: 'json',
                success: function(data){
                  $('button[type=submit], input[type=submit]').prop('disabled',false);
                  $(".submit-btn").removeClass("disabled");
                }
            }).done(function(data) {
              if(data.success){
                $("#patients-table").DataTable().ajax.url( '/get-patients' ).load();
                notiff('Success!', data.msg,'success'); //title, msg, type
                $('#editmodal').modal('toggle');
                $('.ref_').click();
              }else{
                notiff('Error!', data.msg,'error'); //title, msg, type
               // $('#addmodal').modal('toggle');
                  $('button[type=submit], input[type=submit]').prop('disabled',false);
                  $(".submit-btn").removeClass("disabled");
              }
            }).error(function(data) {
                notiff('Error!', data.msg, 'warning'); //title, msg, type
                //$('#addmodal').modal('toggle');
                  $('button[type=submit], input[type=submit]').prop('disabled',false);
                  $(".submit-btn").removeClass("disabled");
            });
          }
        });
  
      $(document).off('click', '.upload-btn').on('click', '.upload-btn', function(e){
        e.preventDefault(); 
        $('#file_img').click();
      });  
      

    var docs = '  <div class="row">'+
                    '<div class="form-group col-md-5 col-xs-12">'+
                      '<input id="previous_docs" name="previous_docs[]" type="text" class="form-control" placeholder="Previous Doctor">'+
                    '</div>'+
                    '<div class="form-group col-md-5 col-xs-10">'+
                      '<input id="previous_docs" name="doc_hosp[]" type="text" class="form-control" placeholder="Institution">'+
                    '</div>'+
                    '<div class="btn btn-success btn-xs addPD"><i class="fa fa-plus"></i></div>'+
                    '<div class="btn btn-danger btn-xs removePD"><i class="fa fa-remove"></i></div>'+
                  '</div> ';
    
    $(document).off('click', '.addPD').on('click', '.addPD', function(e){
      e.preventDefault();
      $('.doc_holder').append(docs);
    });
    $(document).off('click', '.removePD').on('click', '.removePD', function(e){
      e.preventDefault();
      if($('[name="previous_docs[]"]').length > 1){
        $(this).parent().remove();
      }
    });
    
  });  
 </script>
<style type="text/css">
  .edit-pt-form h5{
      margin: 0px;
      margin-bottom: 10px;
      padding: 10px 5px 10px;
      border-bottom: 1px solid;
      background: #096bb1;
      color: #fff;
      font-weight: bold;
  } 
</style>