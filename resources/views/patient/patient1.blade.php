@include('not_supported')
<!-- top action nav -->
<div class="top_nav" style="min-height: 37px;margin-bottom: 0px;background: #ededed;    border-bottom: 0px;">
  <div class="nav_menu"  style="min-height: 37px;margin-bottom: 10px;background: #ededed;margin-top: -10px;    border-bottom: 0px;">
    <nav> 


      <div class="col-md-6 search_bar_">
      		<div class="btn-group action_group"> 
	          <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
	            <i class="fa fa-bars"></i>
	            <span class="sr-only">Toggle Dropdown</span>
	          </button>
	          <ul class="dropdown-menu dropdown-menu-left" role="menu">
	            <li>
	            	<a href="#" class="pull-right action_btns addPatientBtn"><h5>Add Patient <i class=" c-orange glyphicon glyphicon-remove-circle trans-rt-45"></i></h5></a>
	            </li>
	            <li>
	            	<a href="#" class="pull-right action_btns addTransBtn"><h5>Add Transaction <i class=" c-orange glyphicon glyphicon-remove-circle trans-rt-45"></i></h5></a>
	            </li> 
	            <li class="divider"></li>
	            <li>  
			          <li><a href="#">Active</a>
			          </li>
			          <li><a href="#">Inactive</a>
			          </li> 
	            </li>
	          </ul>
	        </div>
	      <ul class="nav navbar-nav navbar-left s_bar_">
	        <li class="">
	      		<input type="text" name="search" placeholder="Search..." id="patient_search">
	      		<i class="fa fa-search"></i>
	        </li>
	      </ul>

      </div>
      <div class="col-md-6 pull-right">
      	<a href="#" class="dropdown-toggle pull-right action_btns hide_md" data-toggle="dropdown" role="button" aria-expanded="false"><h5>Active <i class=" c-orange glyphicon glyphicon glyphicon-chevron-down"></i></h5></a>
	        <ul class="dropdown-menu" role="menu">
	          <li><a href="#" data-id="active" class="status_choice">Active</a>
	          </li>
	          <li><a href="#"  data-id="inactive"  class="status_choice">Inactive</a>
	          </li>
	        </ul>

      	<a href="#" class="pull-right action_btns hide_md addPatientBtn"><h5>Add Patient <i class=" c-orange glyphicon glyphicon-remove-circle trans-rt-45"></i></h5></a>
      	<a href="#" class="pull-right action_btns hide_md addTransBtn"><h5>Add Transaction <i class=" c-orange glyphicon glyphicon-remove-circle trans-rt-45"></i></h5></a>

      
        
      </div> 
    </nav>
  </div>
</div>
<!-- /top action nav -->

<div class="right_col p15 pt30" role="main">
	
	<div class="row p15 mt50 "> 
			<table class="table jambo_table theme-table theme-table-edit dt-responsive nowrap " id="patients-table" width="100%">
				<thead>
					<tr> 
						<th style="width: 100px;">Patient ID</th>
						<th>Name</th>
						<th>Age</th>
						<th>Gender</th>
						<th>Address</th>
						<th>Contact Info</th>
						<th>Status</th>
						<th>Edit </th>
					</tr>
				</thead>
				<tbody>
					
				</tbody>
			</table>  
	</div>

</div>

<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;" id="addmodal"></div>
<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;" id="editmodal"></div>

<script type="text/javascript">
	$(function(){

		$('#patients-table').DataTable({
		    bProcessing: true,
		    bServerSide: false,
		    sServerMethod: "GET",
			'ajax': '/get-patients',
			searching: true, 
			paging: true, 
			filtering:false, 
			bInfo: false,
			responsive: true,
			language:{
				"paginate": {
			        "next":       "<i class='fa fa-chevron-right'></i>",
			        "previous":   "<i class='fa fa-chevron-left'></i>"
			    }
			},
			"columns": [ 
	            {data: 'patientid', name: 'id', className: 'text-center'+($(window).width() < 768 ? ' col-xs-2':'  '), searchable: true, 	sortable: true},
	            {data: 'name', 	name: 'name', className: 'text-left'+($(window).width() < 768 ? ' col-xs-10':' col-md-3'), 	searchable: true,	sortable: true},
	            {data: 'age', 	name: 'age', className: 'text-center '+($(window).width() < 768 ? ' col-md-12':' '), 	searchable: true,	sortable: true},
	            {data: 'gender', 	name: 'gender', className: 'text-center '+($(window).width() < 768 ? ' col-md-12':' '), 	searchable: true,	sortable: true},
	            {data: 'address', 	name: 'address', className: 'text-left '+($(window).width() < 768 ? ' col-md-12':' col-md-3 '), 	searchable: true,	sortable: true},
	            {data: 'contact', 	name: 'contact', className: ' text-left '+($(window).width() < 768 ? ' col-md-12':' col-md-2'), 	searchable: true,	sortable: true},
	            {data: 'status', 	name: 'status', className: ' text-center '+($(window).width() < 768 ? ' col-md-12':' '), 	searchable: true,	sortable: true},
	            {data: 'actions', 	name: 'actions', className: ' text-center '+($(window).width() < 768 ? ' col-md-12':' '), 	searchable: false,	sortable: false},
	        ]
		});

		$(".addPatientBtn").click(function(x){  
			var that = this;
			$("#addmodal").modal();
			$.ajax({
				url: '/patients/create',					
				success: function(data) {
					$("#addmodal").html(data);
				}
			});	
		});

		var p_table = $('#patients-table').DataTable(); 
		$('#patient_search').keyup(function(){ 
		    p_table.search($(this).val()).draw();
		})

		$(document).on('click', "#patients-table > tbody > tr > td", function(x){ 
			if(!$(this).is( "#patients-table > tbody > tr > td:last-child" ) && !$(this).is( "#patients-table > tbody > tr > td:first-child" )){
				loadPage('/patients/'+$(this).parent().children()[0].innerText);
			}
			//console.log($(this).children()[0].innerText);
			///window.open('http://stackoverflow.com/', '_blank');
		});

		$(document).off('click', ".editBtn").on('click', ".editBtn", function(x){ 
			var that = this;
			$("#editmodal").modal();
			$.ajax({
				url: '/patients/'+that.dataset.id+'/edit',					
				success: function(data) {
					$("#editmodal").html(data);
				}
			});	
		});

		$(".status_choice").on('click', function(){
			alert(this.dataset.id);
		})

	});
	
</script>
<style type="text/css">
	tbody > tr:hover{
		cursor: pointer;
	}
	.dataTables_filter{
	  display: none;
	}
	.search_bar_{
		display: block;
	}
	.ppi_{
		display: none;
	}
	@media (max-width: 768px){
 
		.p15{
			padding: 5px !important;
			padding-left: 5px !important;
		    margin-right: 0px;
		    margin-left: 0px;
		}
		.s_bar_{
			float: right;
		    margin-right: 5px;
		    margin-top: 0px;
		}
		.search_bar_ {
			width: 100%;
		} 
		.ppi_{
			display: block;
		}
	}
</style>