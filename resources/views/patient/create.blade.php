<div class="modal-dialog modal-lg add-pt-form">
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button>
      <h4 class="modal-title">Add Patient Form</h4>
    </div>

    <form action="{{url('/image-upload')}}"  class="hidden" id="upload-profile-picture-form"  >
        {{ csrf_field() }} 
          <input type="file" name="file" id="file_img">
    </form> 

    {!! Form::open(array('url' => url('/patients'),'enctype' => 'multipart/form-data', 'method' => 'POST', 'id' => 'add-patients-form', 'files'=> true)) !!}
    <div class="modal-body">

      <h5>Profile Picture</h5>
        
      <div class="profile_img profile_img__ text-center p10"> 
        
                        <div class="row">
                          <div class="col-md-12 text-center img_preview">
                            <img src="{{asset('dst/images/user.png')}}" class=" avatar-view"  height="150" width="150" id="prev_img" style="box-shadow: 0px 0px 0px 1px #808080; " alt="Avatar" title=" ">
                            <div class="">
                              <label> 
                                <label for="files" class="btn btn-primary fileinput-new" style="margin-top: 5px;"><i class="fa fa-upload"></i> Upload Image</label>
                                <label for="files" class="btn btn-info fileinput-exists" style="margin-top: 5px; display: none;"><i class="fa fa-upload"></i> Change Image</label>
                                <input id="files" style="visibility:hidden;" type="file" name="icon" > 
                              </label>
                            </div>
                          </div> 
                        </div> 
      </div>  
      <h5>Personal Information</h5>
       
        <div class="row">
          <div class="form-group col-md-6 col-xs-12">
            <label for="firstname">Firstname</label>
            <input id="firstname" name="firstname" type="text" class="form-control" placeholder="Firstname" >
          </div>
          <div class="form-group col-md-6 col-xs-12">
            <label for="lastname">Lastname</label>
            <input id="lastname" name="lastname" type="text" class="form-control" placeholder="Lastname" >
          </div>
        </div>
        <div class="row">
          <div class="form-group col-md-6 col-xs-12">
            <label for="middlename">Middlename</label>
            <input id="middlename" name="middlename" type="text" class="form-control" placeholder="Middlename" >
          </div>
        </div>
        <div class="row mt15">
          
          <div class="col-md-6 xdisplay_inputx form-group has-feedback">
            <label for="birthdate">Birthdate</label>
              <input name="birthdate" type="text" class="form-control has-feedback-left date-picker" id="single_cal4" placeholder="Birthdate" aria-describedby="">
              <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
          </div>
          <div class="form-group col-md-6 col-xs-12">
            <label for="birthplace">Birthplace</label>
            <input id="birthplace" name="birthplace" type="text" class="form-control" placeholder="Birthplace" >
          </div>
        </div>

        <div class="row mt15">
          <div class="form-group col-md-6 col-xs-12">
            <label for="gender">Gender</label>
            <select name="gender" id="gender" class="form-control">
              <option disabled selected>Select Gender</option>
              <option value="male">Male</option>
              <option value="female">Female</option>
            </select>
          </div>
          <div class="form-group col-md-6 col-xs-12">
            <label for="civil_status">Civil Status</label>
            <select name="civil_status" id="civil_status" class="form-control">
              <option disabled selected>Select Civil Status</option>
              <option value="single">Single</option>
              <option value="married">Married</option>
              <option value="widowed">Widowed</option>
              <option value="divorced">Divorced</option>
            </select>
          </div>
        </div>

        <div class="row mt15">
          <div class="form-group col-md-6 col-xs-12">
            <label for="nationality">Nationality</label>
            <input id="nationality" name="nationality" type="text" class="form-control" placeholder="Nationality" >
          </div>
        </div>


        <h5 class="mt25">Contact Information</h5>
        
        <div class="row">
          <div class="form-group col-md-6 col-xs-12">
            <label for="mobile">Mobile No.</label>
            <input id="mobile" name="mobile" type="number" class="form-control" placeholder="Mobile Number" >
          </div>
          <div class="form-group col-md-6 col-xs-12">
            <label for="tel">Landline No.</label>
            <input id="tel" name="tel" type="text" class="form-control" placeholder="Landline Number" >
          </div>
        </div>
        <div class="row">
          <div class="form-group col-md-6 col-xs-12">
            <label for="email">Email</label>
            <input id="email" name="email" type="email" class="form-control" placeholder="Email" >
          </div>
        </div>  


        <h5 class="mt25">Address Information</h5>
        
        <div class="row">
          <div class="form-group col-md-6 col-xs-12">
            <label for="province">Province</label>
            <input id="province" name="province" type="text" class="form-control" placeholder="Province" >
          </div>
          <div class="form-group col-md-6 col-xs-12">
            <label for="city">City</label>
            <input id="city" name="city" type="text" class="form-control" placeholder="City" >
          </div>
        </div>
        <div class="row">
          <div class="form-group col-md-6 col-xs-12">
            <label for="street">Street</label>
            <input id="street" name="street" type="text" class="form-control" placeholder="Street" >
          </div>
        </div>

        <input type="hidden" name="type" value="patient">
       <!--  <h5 class="mt25">Credential Information</h5>
        
        <div class="row">
          <div class="form-group col-md-6 col-xs-12">
            <label for="type">User Type</label>
            <select name="type" id="type" class="form-control">
              <option value="patient" selected>Patient</option>
            </select>
          </div>
        </div>
        <div class="row">
          <div class="form-group col-md-6 col-xs-12">
            <label for="password">Password</label>
            <input id="password" name="password" type="password" class="form-control" placeholder="Password" >
          </div>
          <div class="form-group col-md-6 col-xs-12">
            <label for="confirm_password">Confirm Password</label>
            <input id="confirm_password" name="confirm_password" type="password" class="form-control" placeholder="Confirm Password" >
          </div>     
        </div> -->

        <h5 class="mt25">Patient Information</h5>
        
        <div class="row">
          <div class="form-group col-md-6 col-xs-12">
            <label for="patient_id">Patient ID</label>
            <input id="patient_id" name="patient_id" type="text" class="form-control" placeholder="Patient ID" >
          </div> 
          
          <div class="form-group col-md-6 col-xs-12">
            <label for="blood_type">Blood Type</label>
            <input id="blood_type" name="blood_type" type="text" class="form-control" placeholder="Blood Type" >
          </div>
          <div class="form-group col-md-6 col-xs-12 hidden">
            <label for="previous_doctor">Previous Doctor</label>
            <input id="previous_doctor" name="previous_doctor" type="text" class="form-control" placeholder="Previous Doctor" value="N/A">
          </div>
        </div>
        <div class="row"> 
          <div class="form-group col-md-12 col-xs-12">
            <label for="previous_docs">Previous Doctors</label>
          </div>
          <div class="doc_holder">
            <div class="row">
              <div class="form-group col-md-5 col-xs-12">
                <input id="previous_docs" name="previous_docs[]" type="text" class="form-control" placeholder="Previous Doctor">
              </div>
              <div class="form-group col-md-5 col-xs-10">
                <input id="previous_docs" name="doc_hosp[]" type="text" class="form-control" placeholder="Institution">
              </div>
              <div class="btn btn-success btn-xs addPD"><i class="fa fa-plus"></i></div>
              <div class="btn btn-danger btn-xs removePD"><i class="fa fa-remove"></i></div>
            </div> 
          </div>

        </div>


      <div style="width: 100%;">&nbsp;</div>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        {!! Form::submit('Submit', ['class' => 'btn submit-btn btn-primary btn-gradient pull-right']) !!}
      {!! Form::close() !!}
    </div>

  </div>
</div>


 <!-- Laravel Javascript Validation -->
 <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>

 {!! JsValidator::formRequest('App\Http\Requests\AddPatientRequest', '#add-patients-form') !!}

<script type="text/javascript">
  $(function(){
      

    document.getElementById('files').onchange = function (evt) {
        var tgt = evt.target || window.event.srcElement,
            files = tgt.files;

        // FileReader support
        if (FileReader && files && files.length) {
            var fr = new FileReader();
            $('.img_preview img').attr('src', '/img/spin.gif');
            fr.onload = function () {
                document.getElementById('prev_img').src = fr.result;
              $('.fileinput-new').hide();
              $('.fileinput-exists').show();

            }
            fr.readAsDataURL(files[0]);
        }

        // Not supported
        else {
            // fallback -- perhaps submit the input to an iframe and temporarily store
            // them on the server until the user's session ends.
        }
    }
      

     /* var old_pic = '';
      var formData = new FormData();
      $("#file_img").change(function(){ 
          formData.append('file', $('#file_img')[0].files[0]);
          formData.append('old_file', old_pic);
          formData.append('_token', $('[name="_token"]').val());
          $.ajax({
                type: 'POST',
                url: '/uploadPicture',
                data: formData,
                processData: false,
                contentType: false,
                success: function(data){
                  console.log(data);
                  if(data.success){
                    $('.img_preview img').attr('src', '/uploads/'+data.img);
                    $('.fileinput-new').hide();
                    $('.fileinput-exists').show();
                    old_pic = data.img; 
                  }else{ 

                  } 
                }
            });
      });*/
      
      $('.date-picker').datepicker({
          autoclose: true,
          todayHighlight: true
        })
        //show datepicker when clicking on the icon
        .next().on(ace.click_event, function(){
          $(this).prev().focus();
        });
      $('select').chosen({allow_single_deselect:true}); 
      $('select').chosen({allow_single_deselect:true}); 
          //resize the chosen on window resize
      
          $(window)
          .off('resize.chosen')
          .on('resize.chosen', function() {
            $('select').each(function() {
               var $this = $(this);
               $this.next().css({'width': $this.parent().width()});
            })
          }).trigger('resize.chosen');
          //resize chosen on sidebar collapse/expand
          $(document).on('settings.ace.chosen', function(e, event_name, event_val) {
            if(event_name != 'sidebar_collapsed') return;
            $('select').each(function() {
               var $this = $(this);
               $this.next().css({'width': $this.parent().width()});
            })
          });
      $(document).off('submit', '#add-patients-form').on('submit', '#add-patients-form', function(e){
          e.preventDefault();
          var that = this;
         var formData2 = new FormData($("form#add-patients-form")[0]);
          if($("#add-patients-form").valid()){
            $(".submit-btn").addClass("disabled");
            $('button[type=submit], input[type=submit]').prop('disabled',true);
            $.ajax({
                type: 'POST',
                url: '/patients',
                /*data: $('#add-patients-form').serialize()+'&icon='+old_pic,
                dataType: 'json',*/
                
                data: formData2,
                processData: false,
                contentType: false,
                success: function(data){
                  $('button[type=submit], input[type=submit]').prop('disabled',false);
                  $(".submit-btn").removeClass("disabled");
                }
            }).done(function(data) {
              if(data.success){
                $("#patients-table").DataTable().ajax.url( '/get-patients' ).load();
                notiff('Success!', data.msg,'success'); //title, msg, type
                $('#addmodal').modal('toggle');
              }else{
                notiff('Error!', data.msg,'error'); //title, msg, type
               // $('#addmodal').modal('toggle');
                  $('button[type=submit], input[type=submit]').prop('disabled',false);
                  $(".submit-btn").removeClass("disabled");
              }
            }).error(function(data) {
                notiff('Error!', data.msg, 'warning'); //title, msg, type
                //$('#addmodal').modal('toggle');
                  $('button[type=submit], input[type=submit]').prop('disabled',false);
                  $(".submit-btn").removeClass("disabled");
            });
          }
        });
 
      $.ajax({
          type: 'GET',
          url: '/get-last-patient-id',
          dataType: 'json',
          success: function(data){
            console.log(data);
            
            var str = ''+data.patients[data.patients.length-1].PatientID;
            var val =  str.split('-')[0]+'-'+(parseInt(str.split('-')[1])+1);
             $("#patient_id").val(val);
          }
      });

      $(document).off('click', '.upload-btn').on('click', '.upload-btn', function(e){
        e.preventDefault(); 
        $('#file_img').click();
      });  


    var docs = '  <div class="row">'+
                    '<div class="form-group col-md-5 col-xs-12">'+
                      '<input id="previous_docs" name="previous_docs[]" type="text" class="form-control" placeholder="Previous Doctor">'+
                    '</div>'+
                    '<div class="form-group col-md-5 col-xs-10">'+
                      '<input id="previous_docs" name="doc_hosp[]" type="text" class="form-control" placeholder="Institution">'+
                    '</div>'+
                    '<div class="btn btn-success btn-xs addPD"><i class="fa fa-plus"></i></div>'+
                    '<div class="btn btn-danger btn-xs removePD"><i class="fa fa-remove"></i></div>'+
                  '</div> ';
    
    $(document).off('click', '.addPD').on('click', '.addPD', function(e){
      e.preventDefault();
      $('.doc_holder').append(docs);
    });
    $(document).off('click', '.removePD').on('click', '.removePD', function(e){
      e.preventDefault();
      if($('[name="previous_docs[]"]').length > 1){
        $(this).parent().remove();
      }
    });
    
  });  
 </script>
<style type="text/css">
  .add-pt-form h5{
      margin: 0px;
      margin-bottom: 10px;
      padding: 10px 5px 10px;
      border-bottom: 1px solid;
      background: #096bb1;
      color: #fff;
      font-weight: bold;
  } 
</style>