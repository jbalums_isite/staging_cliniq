@extends('new_template.app')
 
@section('content') 
<div class="breadcrumbs ace-save-state" id="breadcrumbs">
	<ul class="breadcrumb">
		<li> 
			<a href="/">Home</a>
		</li> 
		<li> 
			Patient
		</li> 
	</ul><!-- /.breadcrumb -->

	
	<ul class="breadcrumb pull-right action_buttons">
		<li> 
			<a href="#" class="addTransBtn">
				<i class="fa fa-plus"></i>
				Add Transactions
			</a> 
		</li>  
		<li> 
			<a href="javascript:void(0)" class="addPatientBtn">
				<i class="fa fa-plus"></i>
				Add Patient
			</a> 
		</li> 
		<li class="hidden"> 
			<a href="javascript:void(0)">
				<i class="fa fa-angle-down"></i>
				Active
			</a> 
		</li> 
	</ul><!-- /.breadcrumb -->

</div>


<div class="page-content"> 
	<div class="row">
		<div class="col-xs-12">

			<table class="table table-striped table-bordered dt-responsive nowrap" id="patients-table" width="100%">
				<thead>
					<tr> 
						<!-- <th><input type="checkbox" class="form-field-checkbox" name="select_all" value="1" id="example-select-all"></th> -->
						<th style="width: 100px;">Patient ID</th>
						<th>Name</th>
						<th>Age</th>
						<th>Gender</th>
						<th>Address</th>
						<th>Contact Info</th>
						<th>Status</th>
						<th>&nbsp;</th>
					</tr>
				</thead>
				<tbody>
					
				</tbody>
			</table>  

		</div><!-- /.col -->
	</div><!-- /.row -->
</div><!-- /.page-content -->
  <!-- /page content -->

<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;" id="addmodal"></div>
<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;" id="editmodal"></div>


<style type="text/css">
	tbody > tr:hover{
		cursor: pointer;
	} 
	.search_bar_{
		display: block;
	}
	.ppi_{
		display: none;
	}
	@media (max-width: 768px){
 
		.p15{
			padding: 5px !important;
			padding-left: 5px !important;
		    margin-right: 0px;
		    margin-left: 0px;
		}
		.s_bar_{
			float: right;
		    margin-right: 5px;
		    margin-top: 0px;
		}
		.search_bar_ {
			width: 100%;
		} 
		.ppi_{
			display: block;
		}
	}
</style>

@endsection 

@section('script_content')
	
<script type="text/javascript">
	$(function(){

		var table = $('#patients-table').DataTable({
		    bProcessing: true,
		    bServerSide: false,
		    sServerMethod: "GET",
			'ajax': '/get-patients',
			searching: true, 
			paging: true, 
			filtering:false, 
			bInfo: true,
			responsive: true,
			language:{
				"paginate": {
			        "next":       "<i class='fa fa-chevron-right'></i>",
			        "previous":   "<i class='fa fa-chevron-left'></i>"
			    }
			},
			"columns": [ 
/*				{searchable:false},*/
	            {data: 'patientid', name: 'id', className: 'text-center', searchable: true, 	sortable: true},
	            {data: 'name', 	name: 'name', className: 'text-left', 	searchable: true,	sortable: true},
	            {data: 'age', 	name: 'age', className: 'text-center  col-md-1', 	searchable: true,	sortable: true},
	            {data: 'gender', 	name: 'gender', className: 'text-center ', 	searchable: true,	sortable: true},
	            {data: 'address', 	name: 'address', className: 'text-left  col-md-4', 	searchable: true,	sortable: true},
	            {data: 'contact', 	name: 'contact', className: ' text-left  col-md-1', 	searchable: true,	sortable: true},
	            {data: 'status', 	name: 'status', className: ' text-center  col-md-1', 	searchable: true,	sortable: true},
	            {data: 'actions', 	name: 'actions', className: ' text-center ', 	searchable: false,	sortable: false},
	        ],
	         /*'columnDefs': [{
		         'targets': 0,
		         'searchable': false,
		         'orderable': false,
		         'className': 'dt-body-center',
		         'render': function (data, type, full, meta){
		             return '<input type="checkbox" name="id[]" class="form-field-checkbox" value="' + $('<div/>').text(data).html() + '">';
		         }
		      }],*/
      		'order': [[1, 'asc']]
		});

	/*	// Handle click on "Select all" control
	    $('#example-select-all').on('click', function(){
	      // Get all rows with search applied
	      var rows = table.rows({ 'search': 'applied' }).nodes();
	      // Check/uncheck checkboxes for all rows in the table
	      $('input[type="checkbox"]', rows).prop('checked', this.checked);
	   });

	   // Handle click on checkbox to set state of "Select all" control
		$('#patients-table tbody').on('change', 'input[type="checkbox"]', function(){
	      // If checkbox is not checked
	      if(!this.checked){
	         var el = $('#example-select-all').get(0);
	         // If "Select all" control is checked and has 'indeterminate' property
	         if(el && el.checked && ('indeterminate' in el)){
	            // Set visual state of "Select all" control
	            // as 'indeterminate'
	            el.indeterminate = true;
	         }
	      }
	   });
*/
		//initiate dataTables plugin
		
		$(".addPatientBtn").click(function(x){  
			x.preventDefault();
			var that = this;
			$("#addmodal").modal({
                backdrop: 'static',
                keyboard: false
            });
			$.ajax({
				url: '/patients/create',					
				success: function(data) {
					$("#addmodal").html(data);
				}
			});	
		});

		var p_table = $('#patients-table').DataTable(); 
		$('#patient_search').keyup(function(){ 
		    p_table.search($(this).val()).draw();
		})

		$(document).off('click', "#patients-table > tbody > tr > td").on('click', "#patients-table > tbody > tr > td", function(x){  
			if(!$(this).is( "#patients-table > tbody > tr > td:last-child" ) && !$(this).is( "#patients-table > tbody > tr > td:first-child" )){
				loadPage('/patients/'+$(this).parent().children()[0].innerText);
			} 
		});

		$(document).off('click', ".edit-patient-btn").on('click', ".edit-patient-btn", function(x){ 
			var that = this;
			x.preventDefault();
			$("#editmodal").modal();
			$.ajax({
				url: '/patients/'+that.dataset.id+'/edit',					
				success: function(data) {
					$("#editmodal").html(data);
				}
			});	
		});

		
		 $(document).off('click', '.delete-patient-btn').on('click', '.delete-patient-btn', function(x){
            var that = this;
			x.preventDefault();
            bootbox.confirm({
              title: "Confirm Delete Patient?",
              className: "del-bootbox",
              message: "Are you sure you want to delete record?",
              buttons: {
                  confirm: {
                      label: 'Yes',
                      className: 'btn-success'
                  },
                  cancel: {
                      label: 'No',
                      className: 'btn-danger'
                  }
              },
              callback: function (result) {
                 if(result){
                  var token = '{{csrf_token()}}'; 
                  $.ajax({
                  url:'/patients/'+that.dataset.id,
                  type: 'post',
                  data: {_method: 'delete', _token :token},
                  success:function(data){
                	$("#patients-table").DataTable().ajax.url( '/get-patients' ).load();
                	notiff('Success!', data.msg,'success'); //title, msg, type
                  }
                  }); 
                 }
              }
          });
      });
		$(".status_choice").on('click', function(){
			alert(this.dataset.id);
		})

		setTimeout(resize_action_buttons_height, 100);
        function resize_action_buttons_height(){  
			$('.action_buttons').css('height',$('#breadcrumbs').height()+"px");
			$('.action_buttons a').css('height',$('#breadcrumbs').height()+"px");
        }
	});
	function loadPage(ajaxURL){
		if(ajaxURL != null){
				$.ajax({
				    // Adds a new body class, preps the content container, and adds a css animation
					beforeSend: function() {
					  $('#content_wrapper').addClass('animated animated-shortest fadeOut');
					  NProgress.start();
					},
					url: ajaxURL,
					cache:false, 
					success: function(data) {
					   $('.main-content-inner').html(data);					   
					},
					// On Complete reInit checkbox plugin and remove css animations
					complete: function() {
        				NProgress.done();
					}	  
				});
			}
	}	
</script> 

@endsection