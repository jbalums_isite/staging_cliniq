<div class="breadcrumbs ace-save-state" id="breadcrumbs">
	<ul class="breadcrumb">
		<li> 
			<a href="/">Home</a>
		</li> 
		<li> 
			<a href="/patients">Patient</a>
		</li> 
		<li> 
		</li> 
			<a href="javascript:void(0)" onclick="refreshPatient();" class="ref_">Patient Profile</a>
	</ul><!-- /.breadcrumb -->

	
	<ul class="breadcrumb pull-right action_buttons">
		<li> 
			<a href="/patients" class="back_btn">
				<i class="fa fa-angle-left"></i>
				Back
			</a> 
		</li>  
	</ul><!-- /.breadcrumb -->

</div>

<style type="text/css">
	
  .downmenu-patient{

      width: 100%;
      background: #008608;
      margin-top: -12px;
  }

  .downmenu-patient a,
  .downmenu-patient li:hover a{
    color: #fff !important;
    background: transparent !important;
  }

  .downmenu-patient li{
    transition: .1s ease-in-out  !important;
    background: transparent;
  }
  .downmenu-patient li:hover{
    background: #007007 !important;
  }
</style>
<div class="page-content"> 
	<div class="row">
		<div class="col-xs-12">
			<div class="col-md-4">

				<div class="panel panel-default patient_personal_info">
					

					<div class="btn-group table-dropdown" style="float: right;top: 5px;"> 
						<button data-toggle="dropdown" class="btn btn-white btn-xs no-border setting_btn dropdown-toggle" style="top: -5px;right: -1px;">
							<i class="ace-icon fa fa-cog bigger-120 blue"></i>
						</button>	
                        <ul class="dropdown-menu dropdown-caret dropdown-menu-right">
                            <li>
                                <a href="#" class="edit-patient-profile-btn edit-ptnt-btn " data-id="{{$patient->id}}">Edit Patient Profile</a>
                            </li>  
                            <li>
                                <a href="#" class="refreshData " data-id="{{$patient->id}}">Refresh data</a>
                            </li>  
                        </ul>
                    </div>
					<div class="panel-heading ppt1" style="padding-top: 2px;">
						<div class="col-md-4 patient_profile"> 
			                  @if($patient->user->people->gender == 'male' && sizeof($patient->icon) == 0)                
			                    <img src="/dst/images/user.png" alt="Avatar" title=" ">
			                  @elseif($patient->user->people->gender == 'female' && sizeof($patient->icon) == 0)
			                    <img src="/dst/images/picture.jpg" alt="Avatar" title=" ">
			                  @elseif(sizeof($patient->icon) > 0)
			                    <img src="/uploads/{{$patient->icon}}" alt="Avatar" title=" ">
			                  @endif 
						</div>
						<div class="col-md-8 patient_profile_name">
							<span>PATIENT ID: <b>{{$patient->PatientID}}</b></span>
							<h3>{{ucfirst($patient->user->people->firstname)}} {{substr(ucfirst($patient->user->people->middlename), 0, 1)}}{{strlen($patient->user->people->middlename) > 0 ? '.':''}}</h3>
							<h1>{{ucfirst($patient->user->people->lastname)}} </h1>
						</div>
					</div>
 
					<div class="panel-body">
						<div class="col-md-12 p0">
							<span>
								<small><b>CONTACT NUMBER</b></small>
								<b class="contact_no">{{$patient->user->people->contact->mobile}}</b>
							</span>
							<span>
								<small><b>HOME ADDRESS</b></small>
								<b>{{ucfirst($patient->user->people->address->street)}}, 
								{{ucfirst($patient->user->people->address->city)}},
								{{ucfirst($patient->user->people->address->province)}}
								</b>
							</span>
							<table style="margin-top: 15px;float: left;">
								<tr>
									<td><small><b>DATE OF BIRTH</b></small></td>
									<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
									<td><b>{{date('M d, Y', strtotime($patient->user->people->birthdate))}}</b></td>
								</tr>
								<tr>
									<td><small><b>AGE</b></small></td>
									<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
									<td><b>
										<?php
										    $dob=$patient->user->people->birthdate;
										    $diff = (date('Y') - date('Y',strtotime($dob)));
										    echo $diff;
										?>
									</b></td>
								</tr>
								<tr>
									<td><small><b>STATUS</b></small></td>
									<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
									<td><b>{{ucfirst($patient->user->people->civil_status)}}</b></td>
								</tr>
								<tr>
									<td><small><b>NATIONALITY</b></small></td>
									<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
									<td><b>{{ucfirst($patient->user->people->nationality)}}</b></td>
								</tr>
								<tr>
									<td><small><b>GENDER</b></small></td>
									<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
									<td><b>{{ucfirst($patient->user->people->gender)}}</b></td>
								</tr>
							</table>
						</div>
					</div> 
				</div>

				<div class="panel panel-default patient_personal_info2">
					<div class="panel-heading">
						<h4 class="panel-title">
							<b>PREVIOUS DOCTORS</b> 
							<div class="btn-group table-dropdown" style="float: right;right: -16px;">
								<button  data-toggle="dropdown" class="btn btn-white btn-xs no-border setting_btn sb2_btn dropdown-toggle">
									<i class="ace-icon fa fa-cog bigger-120 blue"></i>
								</button>	 

	                            <ul class="dropdown-menu dropdown-caret dropdown-menu-right">
	                                <li>
                                		<a href="#" class="edit-patient-profile-btn edit-ptnt-btn " data-id="{{$patient->id}}">Edit</a>
	                                </li>   
	                            </ul>
	                        </div>
						</h4>

					</div>
						<div class="panel-body">
							
						<div class="col-md-12 p0"> 
							@php
								$prev_docs = \App\PreviousDoctors::where('patient_id', $patient->id)->get();
							@endphp
							@foreach($prev_docs as $pd) 
								<span>
									<small><b>{{$pd->name}}</b></small>
									<b>{{$pd->details}}</b> 
								</span>
							@endforeach
							@if(sizeof($prev_docs) == 0) 
								<span>
									<small><i>No records found</i></small>
								</span>
							@endif
						</div>

					</div> 
				</div>

			</div>
			<div class="col-md-8 profile_right_side">
				<div class="col-md-12" style="float: none;">
					<h1 class="patient_name"><b>{{ucfirst($patient->user->people->firstname)}}'s</b> Medical Record						
						<button class="name_buttons"><i class="fa fa-mail-forward"></i> FORWARD</button>
						<button class="name_buttons"><i class="fa fa-print"></i> PRINT</button>
					</h1>
					<!-- <button class="add_med_rec_btn btn-green-gradient" data-toggle="modal" data-target="#myModal">ADD MEDICAL RECORD </button> -->
					<div class="btn-group pull-right ">
												<button data-toggle="dropdown" class="add_med_rec_btn btn-green-gradient">
													ADD MEDICAL RECORD &nbsp;
													<i class="ace-icon fa fa-angle-down icon-on-right"></i>
												</button>

												<ul class="dropdown-menu dropdown-success dropdown-menu-right downmenu-patient" style="    margin-top: -10px;">
													<li><a href="javascript:void(0);" class=" addAllergyBtn " data-id="{{$patient->id}}" >Allergy  Record </a></li>
													<li><a href="javascript:void(0);" class="addmedhistorybtn " data-id="{{$patient->id}}" >Medical History</a></li>
													<li><a href="javascript:void(0);" class="addresultbtn "  data-type="laboratory" data-id="{{$patient->id}}" >Lab  Results</a></li>
													<li><a href="javascript:void(0);" class="addresultbtn "  data-type="test" data-id="{{$patient->id}}" >Test  Results</a></li>
													<li><a href="javascript:void(0);" class="addvitalsbtn " data-id="{{$patient->id}}" >Vital  Signs</a></li>
													<li><a href="javascript:void(0);" class="addmedsbtn " data-id="{{$patient->id}}" >Medications</a></li>
													<li><a href="javascript:void(0);" class="addNotesBtn " data-id="{{$patient->id}}" >Notes</a></li>
 
												</ul>
											</div><!-- /.btn-group -->

				</div>
				<?php 
					$allergies 		= \App\Allergies::where('patient_id', $patient->id)->get(); 
					$notes 	   		= \App\Notes::where('patient_id', $patient->id)->get(); 
					$medications 	= \App\Medications::where('patient_id', $patient->id)->get();  
					$vitals 		= \App\Vitals::where('patient_id', $patient->id)->get();   
					$hist_mhs		= \App\MedicalHistories::where('patient_id', $patient->id)->where('history_type', 'medical_history')->get();
					$hist_fams 		= \App\MedicalHistories::where('patient_id', $patient->id)->where('history_type', 'family_history')->get();
					$hist_sgs	 	= \App\MedicalHistories::where('patient_id', $patient->id)->where('history_type', 'surgical_history')->get();
					$hist_scs	 	= \App\MedicalHistories::where('patient_id', $patient->id)->where('history_type', 'social_history')->get();
					$lab_res	 	= \App\Results::where('patient_id', $patient->id)->where('result_type', 'laboratory_result')->get();
					$test_res	 	= \App\Results::where('patient_id', $patient->id)->where('result_type', 'test_result')->get();
				?>
				<div id="accordion" class="accordion-style1 panel-group medical_records">
					<div class="panel panel-default">
						<div class="btn-group table-dropdown drp_pnl">
                            <button data-toggle="dropdown" class="btn-xs btn-for-table dropdown-toggle">
                                
                                <span class="ace-icon fa fa-ellipsis-h icon-on-center"></span>
                            </button>

                            <ul class="dropdown-menu dropdown-caret dropdown-menu-right">
                                <li>
                                    <a href="#" class="edit-mf-btn edit-allery-btn" data-element="allergies_list">Edit</a>
                                </li> 
                                <li>
                                    <a href="#" class="del-mf-btn"  data-element="allergies_list">Delete</a>
                                </li> 
                            </ul>
                        </div>
						<div class="panel-heading">
							<h4 class="panel-title">
								<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false">
									<i class="bigger-110 ace-icon fa fa-caret-right" data-icon-hide="ace-icon fa fa-caret-down" data-icon-show="ace-icon fa fa-caret-right"></i>
									&nbsp;Allergies
								</a>  
							</h4>
						</div>

						<div class="panel-collapse collapse" id="collapseOne" aria-expanded="false" style="height: 0px;">
							<div class="panel-body">
								<ul class="inner_text_body allergies_list">
								@php 	
									$actr = 0;
								@endphp
								@foreach($allergies as $allergy)
									@php
										$actr++;
									@endphp
									<li>
										<div class="full-width_">
											<div class="btn btn-xs btn-danger pull-right del-btn_" style="display: none;" data-id="{{$allergy->id}}" data-table="allergies" data-record="ALLERGY"><i class="fa fa-remove"></i></div>
											<div class="btn btn-xs btn-primary pull-right edit-btn_" style="display: none;" data-id="{{$allergy->id}}" data-table="allergies" data-record="ALLERGY"><i class="fa fa-edit"></i></div>
											<b  data-type="text" class="x-edit_" data-pk="{{$allergy->id}}" data-url="/update_profile_data" data-title="Allergy Name" data-table="allergies" data-field="name">{{$allergy->name}}</b>
										</div>
										<div class="full-width_">
											<span  data-type="text" class="x-edit_" data-pk="{{$allergy->id}}" data-url="/update_profile_data" data-title="Allergy Name" data-table="allergies" data-field="description">
												{{$allergy->description}}
											</span>											
										</div> 
									</li>
								@endforeach 
								@if($actr==0)
									<li><div class="full-width_"><i class="text-muted">No Records found.</i></div></li>
								@endif
								</ul>
							</div>
						</div>
					</div>



					<div class="panel panel-default">
						<div class="btn-group table-dropdown drp_pnl">
                            <button data-toggle="dropdown" class="btn-xs btn-for-table dropdown-toggle">
                                
                                <span class="ace-icon fa fa-ellipsis-h icon-on-center"></span>
                            </button>

                            <ul class="dropdown-menu dropdown-caret dropdown-menu-right">
                                <li>
                                    <a href="#" class="edit-mf-btn edit-allery-btn" data-element="medical_histories_">Edit</a>
                                </li> 
                                <li>
                                    <a href="#" class="del-mf-btn"  data-element="medical_histories_">Delete</a>
                                </li> 
                            </ul>
                        </div>
						<div class="panel-heading">
							<h4 class="panel-title">
								<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false">
									<i class="ace-icon fa fa-caret-right bigger-110" data-icon-hide="ace-icon fa fa-caret-down" data-icon-show="ace-icon fa fa-caret-right"></i>
									&nbsp;Medical Histories
								</a>
							</h4>
						</div>

						<div class="panel-collapse collapse" id="collapseTwo" aria-expanded="false">
							<div class="panel-body"> 

								<div class="tabbable">
									<ul class="nav nav-tabs padding-12 tab-color-blue background-blue" id="myTab4">
										<li class="active">
											<a data-toggle="tab" href="#mh" aria-expanded="true">Medical History</a>
										</li> 
										<li class="">
											<a data-toggle="tab" href="#sgh" aria-expanded="false">Surgical History </a>
										</li> 
										<li class="">
											<a data-toggle="tab" href="#sch" aria-expanded="false">Social History </a>
										</li>
										<li class="">
											<a data-toggle="tab" href="#fh" aria-expanded="false">Family History </a>
										</li>
									</ul>

									<div class="tab-content medical_histories_">
										<div id="mh" class="tab-pane active">
											@php 
												$mhCtr = 0;
											@endphp
											@foreach($hist_mhs as $hist_mh) 
												@php 
													$mhCtr++;
													$imgs_ = \App\Uploads::where('patient_id', $hist_mh->patient_id)
					                                ->where('record_type', $hist_mh->history_type)
					                                ->where('record_id', $hist_mh->id)->get();
												@endphp
  
												<p>

													<div class="full-width_">
														<div class="btn btn-xs btn-danger pull-right del-btn_" style="display: none;" data-id="{{$hist_mh->id}}" data-table="medical_histories" data-type="{{$hist_mh->history_type}}"  data-record="Medical History"><i class="fa fa-remove"></i></div>
														<div class="btn btn-xs btn-primary pull-right edit-btn_" style="display: none;" data-id="{{$hist_mh->id}}" data-table="medical_histories" data-type="{{$hist_mh->history_type}}" ><i class="fa fa-edit"></i></div> 
													</div>
													<b class="">{{$hist_mh->name}}</b><i class="pull-right">{{date('F d, Y', strtotime($hist_mh->date))}}</i>
													<p>{{$hist_mh->description}}</p>	

													<ul class="ace-thumbnails clearfix">
														@foreach($imgs_ as $img)

															<li>
																<a href="javascript:" data-rel="colorbox" class="cboxElement viewUploadedImage" data-type="{{$hist_mh->history_type}}" data-id="{{$hist_mh->id}}">
																	<img width="100" height="100" alt="100x100" src="{{asset('uploads/'.$img->image_name)}}">
																	<div class="text">
																		<div class="inner">Click To Preview</div>
																	</div>
																</a>
															</li>
														@endforeach 
															<li>
																<a href="javascript:" data-rel="colorbox" class="cboxElement uploadImage" data-type="{{$hist_mhs[0]->history_type}}" data-id="{{$hist_mhs[0]->id}}">
																	<img width="100" height="100" alt="100x100" src="{{asset('img/img_holder.png')}}">
																	<div class="text">
																		<div class="inner">Click to Add Picture</div>
																	</div>
																</a>
															</li>
				 
													</ul>	 
												</p>
											@endforeach
											@if($mhCtr==0)
												<div class="full-width_"><i class="text-muted">No Records found.</i></div>
											@endif
										</div>

										<div id="sgh" class="tab-pane">
											@php 
												$mhCtr = 0;
											@endphp
											@foreach($hist_sgs as $hist_sg) 
												@php 
													$mhCtr++;
													$imgs_ = \App\Uploads::where('patient_id', $hist_sg->patient_id)
					                                ->where('record_type', $hist_sg->history_type)
					                                ->where('record_id', $hist_sg->id)->get();
												@endphp
 

												<p>
													<div class="full-width_">
														<div class="btn btn-xs btn-danger pull-right del-btn_" style="display: none;" data-id="{{$hist_sg->id}}" data-table="medical_histories" data-type="{{$hist_sg->history_type}}" data-record="Surgical History"><i class="fa fa-remove"></i></div>
														<div class="btn btn-xs btn-primary pull-right edit-btn_" style="display: none;" data-id="{{$hist_sg->id}}" data-table="medical_histories" data-type="{{$hist_sg->history_type}}"><i class="fa fa-edit"></i></div> 
													</div>
													<b class="">{{$hist_sg->name}}</b><i class="pull-right">{{date('F d, Y', strtotime($hist_sg->date))}}</i>
													<p>{{$hist_sg->description}}</p>	

													<ul class="ace-thumbnails clearfix">
														@foreach($imgs_ as $img)

															<li>
																<a href="javascript:" data-rel="colorbox" class="cboxElement viewUploadedImage" data-type="{{$hist_sg->history_type}}" data-id="{{$hist_sg->id}}">
																	<img width="100" height="100" alt="100x100" src="{{asset('uploads/'.$img->image_name)}}">
																	<div class="text">
																		<div class="inner">
																			Click To Preview<br><br>
																			<span class="btn btn-danger btn-xs" title="click to delete picture" data-type="{{$hist_sg->history_type}}" data-id="{{$hist_sg->id}}"><i class="fa fa-trash"></i></span>
																		</div>
																	</div>

																</a>
															</li>
														@endforeach 
															<li>
																<a href="javascript:" data-rel="colorbox" class="cboxElement uploadImage" data-type="{{$hist_sgs[0]->history_type}}" data-id="{{$hist_sgs[0]->id}}">
																	<img width="100" height="100" alt="100x100" src="{{asset('img/img_holder.png')}}">
																	<div class="text">
																		<div class="inner">Click to Add Picture</div>
																	</div>
																</a>
															</li>
				 
													</ul>	 
												</p>
											@endforeach
											@if($mhCtr==0)
												<div class="full-width_"><i class="text-muted">No Records found.</i></div>
											@endif
										</div>

										<div id="sch" class="tab-pane">
											@php 
												$mhCtr = 0;
											@endphp
											@foreach($hist_scs as $hist_mh)
												@php 
													$mhCtr++;
													$imgs_ = \App\Uploads::where('patient_id', $hist_mh->patient_id)
					                                ->where('record_type', $hist_mh->history_type)
					                                ->where('record_id', $hist_mh->id)->get();
												@endphp
 

												<p>
													<div class="full-width_">
														<div class="btn btn-xs btn-danger pull-right del-btn_" style="display: none;" data-id="{{$hist_mh->id}}" data-table="medical_histories" data-type="{{$hist_mh->history_type}}" data-record="Social History"><i class="fa fa-remove"></i></div>
														<div class="btn btn-xs btn-primary pull-right edit-btn_" style="display: none;" data-id="{{$hist_mh->id}}" data-table="medical_histories" data-type="{{$hist_mh->history_type}}"><i class="fa fa-edit"></i></div> 
													</div>
													<b class="">{{$hist_mh->name}}</b><i class="pull-right">{{date('F d, Y', strtotime($hist_mh->date))}}</i>
													<p>{{$hist_mh->description}}</p>	

													<ul class="ace-thumbnails clearfix">
														@foreach($imgs_ as $img)

															<li>
																<a href="javascript:" data-rel="colorbox" class="cboxElement viewUploadedImage" data-type="{{$hist_mh->history_type}}" data-id="{{$hist_mh->id}}">
																	<img width="100" height="100" alt="100x100" src="{{asset('uploads/'.$img->image_name)}}">
																	<div class="text">
																		<div class="inner">Click To Preview</div>
																	</div>
																</a>
															</li>
														@endforeach 
															<li>
																<a href="javascript:" data-rel="colorbox" class="cboxElement uploadImage" data-type="{{$hist_scs[0]->history_type}}" data-id="{{$hist_scs[0]->id}}">
																	<img width="100" height="100" alt="100x100" src="{{asset('img/img_holder.png')}}">
																	<div class="text">
																		<div class="inner">Click to Add Picture</div>
																	</div>
																</a>
															</li>
				 
													</ul>	 
												</p>
											@endforeach
											@if($mhCtr==0)
												<div class="full-width_"><i class="text-muted">No Records found.</i></div>
											@endif
										</div>
 
										<div id="fh" class="tab-pane">
											@php 
												$mhCtr = 0;
											@endphp
											@foreach($hist_fams as $hist_mh)
												@php 
													$mhCtr++;
													$imgs_ = \App\Uploads::where('patient_id', $hist_mh->patient_id)
					                                ->where('record_type', $hist_mh->history_type)
					                                ->where('record_id', $hist_mh->id)->get();
												@endphp
 

												<p>
													<div class="full-width_">
														<div class="btn btn-xs btn-danger pull-right del-btn_" style="display: none;" data-id="{{$hist_mh->id}}" data-table="medical_histories" data-type="{{$hist_mh->history_type}}"  data-record="Family History"><i class="fa fa-remove"></i></div>
														<div class="btn btn-xs btn-primary pull-right edit-btn_" style="display: none;" data-id="{{$hist_mh->id}}" data-table="medical_histories" data-type="{{$hist_mh->history_type}}"><i class="fa fa-edit"></i></div> 
													</div>
													<b class="">{{$hist_mh->name}}</b><i class="pull-right">{{date('F d, Y', strtotime($hist_mh->date))}}</i>
													<p>{{$hist_mh->description}}</p>	

													<ul class="ace-thumbnails clearfix">
														@foreach($imgs_ as $img)

															<li>
																<a href="javascript:" data-rel="colorbox" class="cboxElement viewUploadedImage" data-type="{{$hist_mh->history_type}}" data-id="{{$hist_mh->id}}">
																	<img width="100" height="100" alt="100x100" src="{{asset('uploads/'.$img->image_name)}}">
																	<div class="text">
																		<div class="inner">Click To Preview</div>
																	</div>
																</a>
															</li>
														@endforeach 
															<li>
																<a href="javascript:" data-rel="colorbox" class="cboxElement uploadImage" data-type="{{$hist_fams[0]->history_type}}" data-id="{{$hist_fams[0]->id}}">
																	<img width="100" height="100" alt="100x100" src="{{asset('img/img_holder.png')}}">
																	<div class="text">
																		<div class="inner">Click to Add Picture</div>
																	</div>
																</a>
															</li>
				 
													</ul>	 
												</p>
											@endforeach
											@if($mhCtr==0)
												<div class="full-width_"><i class="text-muted">No Records found.</i></div>
											@endif
										</div>
 
									</div>
								</div>

							</div>
						</div>
					</div>

					

					<div class="panel panel-default">
						<div class="btn-group table-dropdown drp_pnl">
                            <button data-toggle="dropdown" class="btn-xs btn-for-table dropdown-toggle">
                                
                                <span class="ace-icon fa fa-ellipsis-h icon-on-center"></span>
                            </button>

                            <ul class="dropdown-menu dropdown-caret dropdown-menu-right"> 
                                <li>
                                    <a href="#" class="edit-mf-btn edit-allery-btn" data-element="lab_res_list">Edit</a>
                                </li> 
                                <li>
                                    <a href="#" class="del-mf-btn"  data-element="lab_res_list">Delete</a>
                                </li>
                            </ul>
                        </div>
						<div class="panel-heading">
							<h4 class="panel-title">
								<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse3" aria-expanded="false">
									<i class="ace-icon fa fa-caret-right bigger-110" data-icon-hide="ace-icon fa fa-caret-down" data-icon-show="ace-icon fa fa-caret-right"></i>
									&nbsp;Laboratory Results
								</a>
							</h4>
						</div>

						<div class="panel-collapse collapse" id="collapse3" aria-expanded="false">
							<div class="panel-body lab_res_list"> 
								@php 
									$mhCtr = 0;
								@endphp
								@foreach($lab_res as $res)
									@php 
										$mhCtr++;
										$imgs_ = \App\Uploads::where('patient_id', $res->patient_id)
		                                ->where('record_type', $res->result_type)
		                                ->where('record_id', $res->id)->get();
									@endphp


									<p>

										<div class="full-width_">
											<div class="btn btn-xs btn-danger pull-right del-btn_" style="display: none;" data-id="{{$res->id}}" data-table="results" data-type="{{$res->result_type}}"  data-record="Laboratory Result"><i class="fa fa-remove"></i></div>
											<div class="btn btn-xs btn-primary pull-right edit-btn_" style="display: none;" data-id="{{$res->id}}" data-table="results" data-type="{{$res->result_type}}"><i class="fa fa-edit"></i></div> 
										</div>
										<b class="">{{$res->name}}</b><i class="pull-right">{{date('F d, Y', strtotime($res->date))}}</i>
										<p>{{$res->description}}</p>	

										<ul class="ace-thumbnails clearfix">
											@foreach($imgs_ as $img)

												<li>
													<a href="javascript:" data-rel="colorbox" class="cboxElement viewUploadedImage" data-type="{{$res->result_type}}" data-id="{{$res->id}}">
														<img width="100" height="100" alt="100x100" src="{{asset('uploads/'.$img->image_name)}}">
														<div class="text">
															<div class="inner">Click To Preview</div>
														</div>
													</a>
												</li>
											@endforeach 
												<li>
													<a href="javascript:" data-rel="colorbox" class="cboxElement uploadImage" data-type="{{$lab_res[0]->result_type}}" data-id="{{$lab_res[0]->id}}">
														<img width="100" height="100" alt="100x100" src="{{asset('img/img_holder.png')}}">
														<div class="text">
															<div class="inner">Click to Add Picture</div>
														</div>
													</a>
												</li>
	 
										</ul>	 
									</p>
								@endforeach
								@if($mhCtr==0)
									<div class="full-width_"><i class="text-muted">No Records found.</i></div>
								@endif
							</div>
						</div>
					</div>

					

					<div class="panel panel-default">
						<div class="btn-group table-dropdown drp_pnl">
                            <button data-toggle="dropdown" class="btn-xs btn-for-table dropdown-toggle">
                                
                                <span class="ace-icon fa fa-ellipsis-h icon-on-center"></span>
                            </button>

                            <ul class="dropdown-menu dropdown-caret dropdown-menu-right"> 
                                <li>
                                    <a href="#" class="edit-mf-btn edit-allery-btn" data-element="test_res_list">Edit</a>
                                </li> 
                                <li>
                                    <a href="#" class="del-mf-btn"  data-element="test_res_list">Delete</a>
                                </li>  
                            </ul>
                        </div>
						<div class="panel-heading">
							<h4 class="panel-title">
								<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse4" aria-expanded="false">
									<i class="ace-icon fa fa-caret-right bigger-110" data-icon-hide="ace-icon fa fa-caret-down" data-icon-show="ace-icon fa fa-caret-right"></i>
									&nbsp;Test Results
								</a>
							</h4>
						</div>

						<div class="panel-collapse collapse" id="collapse4" aria-expanded="false">
							<div class="panel-body">
								<ul class="inner_text_body test_res_list">
									@php 
										$mhCtr = 0;
									@endphp
									@foreach($test_res as $res) 
										@php 
											$mhCtr++;
											$imgs_ = \App\Uploads::where('patient_id', $res->patient_id)
			                                ->where('record_type', $res->result_type)
			                                ->where('record_id', $res->id)->get();
										@endphp


										<p>
											<div class="full-width_">
												<div class="btn btn-xs btn-danger pull-right del-btn_" style="display: none;" data-id="{{$res->id}}" data-table="results" data-type="{{$res->result_type}}"  data-record="Laboratory Result"><i class="fa fa-remove"></i></div>
												<div class="btn btn-xs btn-primary pull-right edit-btn_" style="display: none;" data-id="{{$res->id}}" data-table="results" data-type="{{$res->result_type}}"><i class="fa fa-edit"></i></div> 
											</div>
											<b class="">{{$res->name}}</b><i class="pull-right">{{date('F d, Y', strtotime($res->date))}}</i>
											<p>{{$res->description}}</p>	

											<ul class="ace-thumbnails clearfix">
												@foreach($imgs_ as $img)

													<li style="width: 104px;">
														<a href="javascript:" data-rel="colorbox" class="cboxElement viewUploadedImage" data-type="{{$res->result_type}}" data-id="{{$res->id}}">
															<img width="100" height="100" alt="100x100" src="{{asset('uploads/'.$img->image_name)}}">
															<div class="text">
																<div class="inner">Click To Preview</div>
															</div>
														</a>
													</li>
												@endforeach 
													<li style="width: 104px;">
														<a href="javascript:" data-rel="colorbox" class="cboxElement uploadImage" data-type="{{$test_res[0]->result_type}}" data-id="{{$test_res[0]->id}}">
															<img width="100" height="100" alt="100x100" src="{{asset('img/img_holder.png')}}">
															<div class="text">
																<div class="inner">Click to Add Picture</div>
															</div>
														</a>
													</li>
		 
											</ul>	 
										</p>
									@endforeach
									@if($mhCtr==0)
										<div class="full-width_"><i class="text-muted">No Records found.</i></div>
									@endif
							</div>
						</div>
					</div>

					

					<div class="panel panel-default">
						<div class="btn-group table-dropdown drp_pnl">
                            <button data-toggle="dropdown" class="btn-xs btn-for-table dropdown-toggle">
                                
                                <span class="ace-icon fa fa-ellipsis-h icon-on-center"></span>
                            </button>

                            <ul class="dropdown-menu dropdown-caret dropdown-menu-right">
                                <li>
                                    <a href="#" class=" edit-mf-btn"  data-element="vitals_list" >Edit</a>
                                </li> 
                                <li>
                                    <a href="#" class=" del-mf-btn"  data-element="vitals_list">Delete</a>
                                </li> 
                            </ul>
                        </div>
						<div class="panel-heading">
							<h4 class="panel-title">
								<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse5" aria-expanded="false">
									<i class="ace-icon fa fa-caret-right bigger-110" data-icon-hide="ace-icon fa fa-caret-down" data-icon-show="ace-icon fa fa-caret-right"></i>
									&nbsp;Vitals
								</a>
							</h4>
						</div>

						<div class="panel-collapse collapse" id="collapse5" aria-expanded="false">
							<div class="panel-body">
								<ul class="inner_text_body vitals_list" style="float: left; width: 100%;">
								@foreach($vitals as $vital)
									<li style="padding-bottom: 10px;border-bottom: 1px solid #eee;float: left;width: 225px;padding-right: 15px;border-right: 1px solid #eee;padding-left: 5px;">
										<div class="full-width_">
											<div class="btn btn-xs btn-danger pull-right del-btn_" style="display: none;" data-id="{{$vital->id}}" data-table="vitals" data-record="Note"><i class="fa fa-remove"></i></div>
											<div class="btn btn-xs btn-primary pull-right edit-btn_" style="display: none;" data-id="{{$vital->id}}" data-table="vitals" data-record="Note"><i class="fa fa-edit"></i></div>
											<b class="color-b">Date: {{date('F d, Y', strtotime($vital->date))}}</b>
										</div>
										<div class="full-width_"> 
											<table class="  table-hover " style="margin-top: 5px;"> 
												<tbody>
													<tr>
														<td class="text-left"><b>Weight</b></td>
														<td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
														<td>{{$vital->weight}}</td>
													</tr>
													<tr>
														<td class="text-left"><b>Temperature</b></td>
														<td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
														<td>{{$vital->temperature}}</td>
													</tr>
													<tr>
														<td class="text-left"><b>Pulse</b></td>
														<td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
														<td>{{$vital->pulse}}</td>
													</tr>
													<tr>
														<td class="text-left"><b>BP</b></td>
														<td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
														<td>{{$vital->blood_pressure}}</td>
													</tr>
													<tr>
														<td class="text-left"><b>RR</b></td>
														<td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
														<td>{{$vital->respiration}}</td>
													</tr>
													<tr>
														<td class="text-left"><b>Pain</b></td>
														<td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
														<td>{{$vital->pain}}</td>
													</tr>
												</tbody>
											</table>					
										</div> 
									</li>
								@endforeach 
									@if(sizeof($vitals)==0)
										<li><div class="full-width_"><i class="text-muted">No Records found.</i></div></li>
									@endif
								</ul>
							</div>
						</div>
					</div>

					

					<div class="panel panel-default">
						<div class="btn-group table-dropdown drp_pnl">
                            <button data-toggle="dropdown" class="btn-xs btn-for-table dropdown-toggle">
                                
                                <span class="ace-icon fa fa-ellipsis-h icon-on-center"></span>
                            </button>

                            <ul class="dropdown-menu dropdown-caret dropdown-menu-right">
                                <li>
                                    <a href="#" class="edit-mf-btn edit-allery-btn" data-element="medications_list">Edit</a>
                                </li> 
                                <li>
                                    <a href="#" class="del-mf-btn"  data-element="medications_list">Delete</a>
                                </li> 
                            </ul>
                        </div>
						<div class="panel-heading">
							<h4 class="panel-title">
								<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse6" aria-expanded="false">
									<i class="ace-icon fa fa-caret-right bigger-110" data-icon-hide="ace-icon fa fa-caret-down" data-icon-show="ace-icon fa fa-caret-right"></i>
									&nbsp;Medications
								</a>
							</h4>
						</div>

						<div class="panel-collapse collapse" id="collapse6" aria-expanded="false">
							<div class="panel-body">
								<ul class="inner_text_body medications_list">
								@foreach($medications as $medication)
									<li>

										<div class="full-width_">
											<div class="btn btn-xs btn-danger pull-right del-btn_" style="display: none;" data-id="{{$medication->id}}" data-table="medications" data-record="Medication"><i class="fa fa-remove"></i></div>
											<div class="btn btn-xs btn-primary pull-right edit-btn_" style="display: none;" data-id="{{$medication->id}}" data-table="medications" data-record="Medication"><i class="fa fa-edit"></i></div>
											<b class="color-b">Date: {{date('F d, Y', strtotime($medication->date))}}</b>
										</div>
										<div class="full-width_">
											<table class="table table-hover table-bordered table-striped">
												<tbody>
													@foreach($medication->descriptions as $description)
													<tr>
														<td class="col-md-4">{{$description->medicine->name}}</td>
														<td class="col-md-8">{{$description->description}}</td>
													</tr>
													@endforeach

												</tbody>
											</table>							
										</div>  
									</li> 
								@endforeach
									@if(sizeof($medications)==0)
										<li><div class="full-width_"><i class="text-muted">No Records found.</i></div></li>
									@endif
								</ul>
							</div>
						</div>
					</div>

					

					<div class="panel panel-default">
						<div class="btn-group table-dropdown drp_pnl">
                            <button data-toggle="dropdown" class="btn-xs btn-for-table dropdown-toggle">
                                
                                <span class="ace-icon fa fa-ellipsis-h icon-on-center"></span>
                            </button>

                            <ul class="dropdown-menu dropdown-caret dropdown-menu-right">
                                <li>
                                    <a href="#" class="edit-mf-btn edit-allery-btn" data-element="notes_list">Edit</a>
                                </li> 
                                <li>
                                    <a href="#" class="del-mf-btn"  data-element="notes_list">Delete</a>
                                </li> 
                            </ul>
                        </div>
						<div class="panel-heading">
							<h4 class="panel-title">
								<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse7" aria-expanded="false">
									<i class="ace-icon fa fa-caret-right bigger-110" data-icon-hide="ace-icon fa fa-caret-down" data-icon-show="ace-icon fa fa-caret-right"></i>
									&nbsp;Notes
								</a>
							</h4>
						</div>

						<div class="panel-collapse collapse" id="collapse7" aria-expanded="false">
							<div class="panel-body">
								<ul class="inner_text_body notes_list">
								@foreach($notes as $note)
									<li>
										<div class="full-width_">
											<div class="btn btn-xs btn-danger pull-right del-btn_" style="display: none;" data-id="{{$note->id}}" data-table="notes" data-record="Note"><i class="fa fa-remove"></i></div>
											<div class="btn btn-xs btn-primary pull-right edit-btn_" style="display: none;" data-id="{{$note->id}}" data-table="notes" data-record="Note"><i class="fa fa-edit"></i></div>
											<b>{{date('F d, Y', strtotime($note->date))}}</b>
										</div>
										<div class="full-width_">
											<span>
												{{$note->notes}}
											</span>											
										</div> 
									</li>
								@endforeach 

									@if(sizeof($notes)==0)
										<li><div class="full-width_"><i class="text-muted">No Records found.</i></div></li>
									@endif
								</ul>
							</div>
						</div>
					</div>

				</div>



			</div>

		</div><!-- /.col -->
	</div><!-- /.row -->
</div><!-- /.page-content -->
  <!-- /page content -->
<!-- Modal -->
<input type="hidden" id="patient_id" value="{{$patient->id}}">
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Select record type to add</h4>
      </div>
      <div class="modal-body text-center	"> 
			<a href="#" class="btn btn-app btn-primary no-radius btn-app fs15 addAllergyBtn fs15" data-id="{{$patient->id}}" style="line-height: 1.2em;">
				<i class="ace-icon fa fa-plus-square bigger-230"></i>
				Allergy <br> Record 
			</a>
			<a href="#" class="btn btn-app btn-primary no-radius btn-app addmedhistorybtn fs15" data-id="{{$patient->id}}" style="line-height: 1.2em;">
				<i class="ace-icon fa fa-plus-square bigger-230"></i>
				Medical <br>History 
			</a>
			<a href="#" class="btn btn-app btn-primary no-radius btn-app addresultbtn fs15"  data-type="laboratory" data-id="{{$patient->id}}" style="line-height: 1.2em;">
				<i class="ace-icon fa fa-plus-square bigger-230"></i>
				Lab  <br>Results
			</a>
			<a href="#" class="btn btn-app btn-primary no-radius btn-app addresultbtn fs15"  data-type="test" data-id="{{$patient->id}}" style="line-height: 1.2em;">
				<i class="ace-icon fa fa-plus-square bigger-230"></i>
				Test  <br>Results
			</a>
			<a href="#" class="btn btn-app btn-primary no-radius btn-app addvitalsbtn fs15" data-id="{{$patient->id}}" style="line-height: 1.2em;">
				<i class="ace-icon fa fa-plus-square bigger-230"></i>
				Vital  <br>Signs
			</a>
			<a href="#" class="btn btn-app btn-primary no-radius btn-app addmedsbtn fs15" data-id="{{$patient->id}}" style="line-height: 1.2em;">
				<i class="ace-icon fa fa-plus-square bigger-230"></i>
				Medications
			</a>
			<a href="#" class="btn btn-app btn-primary no-radius btn-app addNotesBtn fs15" data-id="{{$patient->id}}" style="line-height: 1.2em;">
				<i class="ace-icon fa fa-plus-square bigger-230"></i>
				Notes
			</a>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<div class="modal fade addmodal" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;" id="addallergymodal"></div>
<div class="modal fade addmodal" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;" id="addhistmodal"></div>
<div class="modal fade addmodal" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;" id="addresmodal"></div>
<div class="modal fade addmodal" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;" id="addmedsmodal"></div>
<div class="modal fade addmodal" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;" id="addvitalmodal"></div>
<div class="modal fade addmodal" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;" id="addnotesmodal"></div>


<div class="modal fade editmodal" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;" id="editallergymodal"></div>
<div class="modal fade editmodal" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;" id="edithistmodal"></div>
<div class="modal fade editmodal" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;" id="editresmodal"></div>
<div class="modal fade editmodal" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;" id="editmedsmodal"></div>
<div class="modal fade editmodal" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;" id="editvitalmodal"></div>
<div class="modal fade editmodal" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;" id="editnotesmodal"></div>


<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;" id="uploadFormModal"></div>
<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;" id="viewuploadFormModal"></div>


<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;" id="editmodal"></div>

<script type="text/javascript">

	$(function(){

	/*$.fn.editable.defaults.mode = 'inline'; 
	$.fn.editable.defaults.params = function (params) {
        params._token = $("meta[name=csrf-token]").attr("content");
        return params;
    };*/
    /*
    $('.x-edit_').editable({
        url: '/update_profile_data',
        validate: function(value) {
            if($.trim(value) == '') return 'This value is required.';
        },
        type: 'text', 
        title: 'Enter Freight Value',
        params: function(params) { 
            var data = {};
            data['id'] = params.pk; 
            data['value'] = params.value; 
            data['field'] = $(this).attr("data-field"); 
            data['table'] = $(this).attr("data-table"); 
            data['_token'] = $("meta[name=csrf-token]").attr("content");
            return data;
        },
        ajaxOptions: {
            dataType: 'json'
        },
        success: function(response, newValue) {

        }
    });*/
/*
    $('.edit-mf-btn').click(function(x){
    	x.preventDefault();
    	var elem = this.dataset.element; 

    	if($(this)[0].innerText == 'Edit'){
    		$(this)[0].innerText = "Cancel Edit";
    		$('.'+elem+' .x-edit_').editable({
		        url: '/update_profile_data',
		        validate: function(value) {
		            if($.trim(value) == '') return 'This value is required.';
		        },
		        type: 'text', 
		        title: 'Enter Freight Value',
		        params: function(params) { 
		            var data = {};
		            data['id'] = params.pk; 
		            data['value'] = params.value; 
		            data['field'] = $(this).attr("data-field"); 
		            data['table'] = $(this).attr("data-table"); 
		            data['_token'] = $("meta[name=csrf-token]").attr("content");
		            return data;
		        },
		        ajaxOptions: {
		            dataType: 'json'
		        },
		        success: function(response, newValue) {

		        }
		    });
    	}else{
    		$(this)[0].innerText = "Edit";
    		$('.'+elem+' .x-edit_').editable('destroy');
    	}
    	
    });*/


    $(document).off('click', '.uploadImage').on('click', '.uploadImage', function(x){
      if($(".dz-clickable").length > 0){
        Dropzone.forElement(".dropzone").destroy();                                  
      } 
      x.preventDefault();  
      var that = this;
      $("#uploadFormModal").modal();
      $.ajax({
        url: '/upload-form',  
        data: 'patient_id='+$("#patient_id").val()+'&record_type='+that.dataset.type+'&record_id='+that.dataset.id,   
        success: function(data) {
          $("#uploadFormModal").html(data);
        }
      }); 
    });
    $(document).off('click', '.viewUploadedImage').on('click', '.viewUploadedImage', function(x){
      x.preventDefault();  
      var that = this;
      $("#viewuploadFormModal").modal();
      $.ajax({
        url: '/view-images',  
        data: 'patient_id='+$("#patient_id").val()+'&record_type='+that.dataset.type+'&record_id='+that.dataset.id,        
        success: function(data) {
          $("#viewuploadFormModal").html(data);
        }
      }); 
    });
    
    $('.del-mf-btn').click(function(x){
    	x.preventDefault();
    	var elem = this.dataset.element; 

    	if($(this)[0].innerText == 'Delete'){
    		$(this)[0].innerText = "Cancel Delete";
    		$('.'+elem+' .del-btn_').show();
    	}else{
    		$(this)[0].innerText = "Delete";
    		$('.'+elem+' .del-btn_').hide()
    	}
    	
    });
    $('.edit-mf-btn').click(function(x){
    	x.preventDefault();
    	var elem = this.dataset.element; 

    	if($(this)[0].innerText == 'Edit'){
    		$(this)[0].innerText = "Cancel Edit";
    		$('.'+elem+' .edit-btn_').show();
    	}else{
    		$(this)[0].innerText = "Edit";
    		$('.'+elem+' .edit-btn_').hide()
    	}
    	
    });

    $(document).off('click', '.del-btn_').on('click', '.del-btn_', function(x){
            var that = this; 
			x.preventDefault();
            bootbox.confirm({
              title: "Confirm Delete "+that.dataset.record+"",
              className: "del-bootbox",
              message: "Are you sure you want to delete record?",
              buttons: {
                  confirm: {
                      label: 'Yes',
                      className: 'btn-success'
                  },
                  cancel: {
                      label: 'No',
                      className: 'btn-danger'
                  }
              },
              callback: function (result) {
                 if(result){
                  var token = '{{csrf_token()}}'; 
                  $.ajax({
                  url:'/delete_profile_data',
                  type: 'post',
                  data: {_token :token, table: that.dataset.table, id:that.dataset.id},
                  success:function(data){ 
                  	if(data.success){
                  		if(that.dataset.table == 'medical_histories' || that.dataset.table == 'results'){
                  			 refreshPatient();
                  		}else{
                  			$(that).parent().parent().remove();                  			
                  		}
                		notiff('Success!', data.msg,'success');                 		
                  	}
                  }
                  }); 
                 }
              }
          });
      });

    $(document).off('click', '.edit-btn_').on('click', '.edit-btn_', function(x){
        var that = this;
		x.preventDefault();
		var table = that.dataset.table; 
		if(table=='allergies'){ 
	      $("#editallergymodal").modal();
	      $("#editallergymodal").html('');
	      $.ajax({
	        url: '/allergies/'+that.dataset.id+'/edit',  
	        data: 'patient_id='+$("#patient_id").val(),       
	        success: function(data) {
	          $("#editallergymodal").html(data);
	        }
	      }); 
		}else if(table == 'notes'){
	      $("#editnotesmodal").modal();
	      $("#editnotesmodal").html('');
	      $.ajax({
	        url: '/notes/'+that.dataset.id+'/edit',  
	        data: 'patient_id='+$("#patient_id").val(),       
	        success: function(data) {
	          $("#editnotesmodal").html(data);
	        }
	      }); 
		}else if(table == 'vitals'){
	      $("#editvitalmodal").modal();
	      $("#editvitalmodal").html('');
	      $.ajax({
	        url: '/vitals/'+that.dataset.id+'/edit',  
	        data: 'patient_id='+$("#patient_id").val(),       
	        success: function(data) {
	          $("#editvitalmodal").html(data);
	        }
	      }); 
		}else if(table == 'medications'){

	      $("#editmedsmodal").modal();
	      $("#editmedsmodal").html('');
	      $.ajax({
	        url: '/medications/'+that.dataset.id+'/edit',  
	        data: 'patient_id='+$("#patient_id").val(),       
	        success: function(data) {
	          $("#editmedsmodal").html(data);
	        }
	      }); 
		}else if(table == 'medical_histories'){

	      $("#edithistmodal").modal();
	      $("#edithistmodal").html('');
	      $.ajax({
	        url: '/medicalhistories/'+that.dataset.id+'/edit',  
	        data: 'patient_id='+$("#patient_id").val(),       
	        success: function(data) {
	          $("#edithistmodal").html(data);
	        }
	      }); 
		}else if(table == 'results'){

	      $("#editresmodal").modal();
	      $("#editresmodal").html('');
	      $.ajax({
	        url: '/results/'+that.dataset.id+'/edit',  
        	data: 'patient_id='+$("#patient_id").val()+'&type='+that.dataset.type,      
	        success: function(data) {
	          $("#editresmodal").html(data);
	        }
	      }); 
		}
	});
	
	setTimeout(resize_action_buttons_height, 100);
    function resize_action_buttons_height(){  
		$('.action_buttons').css('height',$('#breadcrumbs').height()+"px");
		$('.action_buttons a').css('height',$('#breadcrumbs').height()+"px");
    }


	$(document).off('click', ".edit-ptnt-btn").on('click', ".edit-ptnt-btn", function(x){ 
			var that = this;
			x.preventDefault();
			$("#editmodal").modal();
			$.ajax({
				url: '/patients/{{$patient->id}}/edit',					
				success: function(data) {
					$("#editmodal").html(data);
				}
			});	
		});

        $('.back_btn').click(function(){
        	$('#patients_menu').click();
        })
		
		   $(".addAllergyBtn").click(function(x){  
		      x.preventDefault();
		      var that = this;
		      $("#addallergymodal").modal();
		      $.ajax({
		        url: '/allergies/create',  
		        data: 'patient_id='+$("#patient_id").val(),        
		        success: function(data) {
		          $("#addallergymodal").html(data);
		        }
		      }); 
		    });

		    $(".addmedhistorybtn").click(function(x){
		      x.preventDefault();  
		      var that = this;
		      $("#addhistmodal").modal();
		      $.ajax({
		        url: '/medicalhistories/create',  
		        data: 'patient_id='+$("#patient_id").val(),        
		        success: function(data) {
		          $("#addhistmodal").html(data);
		        }
		      }); 
		    });

		    $(".addresultbtn").click(function(x){ 
		      x.preventDefault(); 
		      var that = this;
		      $("#addresmodal").modal();
		      $.ajax({
		        url: '/results/create',  
		        data: 'patient_id='+$("#patient_id").val()+'&type='+that.dataset.type,        
		        success: function(data) {
		          $("#addresmodal").html(data);
		        }
		      }); 
		    });

		    $(".addmedsbtn").click(function(x){
		      x.preventDefault();  
		      var that = this;
		      $("#addmedsmodal").modal();
		      $.ajax({
		        url: '/medications/create',  
		        data: 'patient_id='+$("#patient_id").val(),        
		        success: function(data) {
		          $("#addmedsmodal").html(data);
		        }
		      }); 
		    });

		    $(".addvitalsbtn").click(function(x){
		      x.preventDefault();  
		      var that = this;
		      $("#addvitalmodal").modal();
		      $.ajax({
		        url: '/vitals/create',  
		        data: 'patient_id='+$("#patient_id").val(),        
		        success: function(data) {
		          $("#addvitalmodal").html(data);
		        }
		      }); 
		    });


		    $(".addNotesBtn").click(function(x){
		      x.preventDefault();  
		      var that = this;
		      $("#addnotesmodal").modal();
		      $.ajax({
		        url: '/notes/create',  
		        data: 'patient_id='+$("#patient_id").val(),        
		        success: function(data) {
		          $("#addnotesmodal").html(data);
		        }
		      }); 
		    });
		    $(document).off('click', '.refreshData').on('click', '.refreshData', function(x){
		    	x.preventDefault();
		    	refreshPatient();
		    }) 
	});

	function refreshData(element, table, type = null){

      $.ajax({
      	type:'GET',
        url: '/getAllRecords/'+$("#patient_id").val()+'/'+table,      
        success: function(data) { 
        	console.log(data);
        	var str = '';
			var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
        	for(var i = 0 ; i < data.records.length ; i++){
        		if(table=='allergies'){

        		str+= 	'<li>'+
							'<div class="full-width_">'+
								'<div class="btn btn-xs btn-danger pull-right del-btn_" style="display: none;" data-id="'+data.records[i].id+'" data-table="'+data.table+'" data-record="ALLERGY"><i class="fa fa-remove"></i></div>'+
								'<div class="btn btn-xs btn-primary pull-right edit-btn_" style="display: none;" data-id="'+data.records[i].id+'" data-table="'+data.table+'" data-record="ALLERGY"><i class="fa fa-edit"></i></div>'+
								'<b>'+data.records[i].name+'</b>'+
							'</div>'+
							'<div class="full-width_">'+
								'<span>'+data.records[i].description+'</span>'+											
							'</div> '+
						'</li>';
        		}else if(table == 'notes'){
        			var d_ = new Date(data.records[i].date);

        			str+= 	'<li>'+
							'<div class="full-width_">'+
								'<div class="btn btn-xs btn-danger pull-right del-btn_" style="display: none;" data-id="'+data.records[i].id+'" data-table="'+data.table+'" data-record="ALLERGY"><i class="fa fa-remove"></i></div>'+
								'<div class="btn btn-xs btn-primary pull-right edit-btn_" style="display: none;" data-id="'+data.records[i].id+'" data-table="'+data.table+'" data-record="ALLERGY"><i class="fa fa-edit"></i></div>'+
								'<b>'+months[d_.getMonth()]+' '+d_.getDate()+', '+d_.getFullYear()+'</b>'+
							'</div>'+
							'<div class="full-width_">'+
								'<span>'+data.records[i].notes+'</span>'+											
							'</div> '+
						'</li>';
        		}
        	}
        	$(element).html(str);
        }
      }); 
	}

	function refreshPatient(){
		setTimeout(function(){ loadPage('/patients/{{$patient->PatientID}}');  }, 1000); 
	}

	function loadPage(ajaxURL){ 
		if(ajaxURL != null){
				$.ajax({
				    // Adds a new body class, preps the content container, and adds a css animation
					beforeSend: function() {
					  $('#content_wrapper').addClass('animated animated-shortest fadeOut');
					  NProgress.start();
					},
					url: ajaxURL,
					cache:false, 
					success: function(data) {
					   $('.main-content-inner').html(data);					   
					},
					// On Complete reInit checkbox plugin and remove css animations
					complete: function() {
        				NProgress.done();
					}	  
				});
			}
	}	

	function getVitals(element){

      	$.ajax({
	      	type:'GET',
	        url: '/getAllVitals/'+$("#patient_id").val(),      
	        success: function(data) {  
	        	var str = '';
				var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
	        	for(var i = 0 ; i < data.records.length ; i++){
        			var d_ = new Date(data.records[i].date);
	        		str+='<li style="padding-bottom: 10px;border-bottom: 1px solid #eee;float: left;width: 225px;padding-right: 15px;border-right: 1px solid #eee;padding-left: 5px;">'+
										'<div class="full-width_">'+
										'<div class="btn btn-xs btn-danger pull-right del-btn_" style="display: none;" data-id="'+data.records[i].id+'" data-table="vitals" data-record="Note"><i class="fa fa-remove"></i></div>'+
										'<div class="btn btn-xs btn-primary pull-right edit-btn_" style="display: none;" data-id="'+data.records[i].id+'" data-table="vitals" data-record="Note"><i class="fa fa-edit"></i></div>'+
								'<b class="color-b">Date: '+months[d_.getMonth()]+' '+d_.getDate()+', '+d_.getFullYear()+'</b>'+
										'</div>'+
										'<div class="full-width_"> '+
											'<table class="  table-hover " style="margin-top: 5px;"> '+
												'<tbody>'+
													'<tr>'+
														'<td class="text-left"><b>Weight</b></td>'+
														'<td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>'+
														'<td>'+data.records[i].weight+'</td>'+
													'</tr>'+
													'<tr>'+
														'<td class="text-left"><b>Temperature</b></td>'+
														'<td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>'+ 
														'<td>'+data.records[i].temperature+'</td>'+
													'</tr>'+
													'<tr>'+
														'<td class="text-left"><b>Pulse</b></td>'+
														'<td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>'+ 
														'<td>'+data.records[i].pulse+'</td>'+
													'</tr>'+
													'<tr>'+
														'<td class="text-left"><b>BP</b></td>'+
														'<td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>'+ 
														'<td>'+data.records[i].blood_pressure+'</td>'+
													'</tr>'+
													'<tr>'+
														'<td class="text-left"><b>RR</b></td>'+
														'<td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>'+ 
														'<td>'+data.records[i].respiration+'</td>'+
													'</tr>'+
													'<tr>'+
														'<td class="text-left"><b>Pain</b></td>'+
														'<td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>'+ 
														'<td>'+data.records[i].pain+'</td>'+
													'</tr>'+
												'</tbody>'+
											'</table>'+					
										'</div>'+
									'</li>';
	        	}
        		$(element).html(str);
	        }
    	});

	} 
	function getMedications(element){
      	$.ajax({
	      	type:'GET',
	        url: '/getAllMeds/'+$("#patient_id").val(),      
	        success: function(data) {   
	        	var str = '';
				var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
	        	for(var i = 0 ; i < data.records.length ; i++){

        			var d_ = new Date(data.records[i].med[0].date);

        			var  str2 = '';
				 	for(var l = 0 ; l < data.records[i].med.desc.length ; l++){
				 		
											str2+=	'<tr>'+
													'<td class="col-md-4">'+data.records[i].med.desc[l].name+'</td>'+
													'<td class="col-md-8">'+data.records[i].med.desc[l].description+'</td>'+
												'</tr>'; 
				 	}

		        	 str+='<li>'+
								'<div class="full-width_">'+
									'<div class="btn btn-xs btn-danger pull-right del-btn_" style="display: none;" data-id="'+data.records[i].med[0].id+'" data-table="medications" data-record="Medication"><i class="fa fa-remove"></i></div>'+
									'<div class="btn btn-xs btn-primary pull-right edit-btn_" style="display: none;" data-id="'+data.records[i].med[0].id+'" data-table="medications" data-record="Medication"><i class="fa fa-edit"></i></div>'+ 
								'<b class="color-b">Date:'+months[d_.getMonth()]+' '+d_.getDate()+', '+d_.getFullYear()+'</b>'+
								'</div>'+
								'<div class="full-width_">'+
									'<table class="table table-hover table-bordered table-striped">'+
										'<tbody>'+str2+'</tbody>'+
									'</table>'+							
								'</div>'+ 
							'</li> ';

	        	}

        		$(element).html(str);
	        }
    	});

	}
	function getTestResults(id, element){
	}
	function getLabResutls(id, element){
      	$.ajax({
	      	type:'GET',
	        url: '/getAllLabRes/'+$("#patient_id").val(),      
	        success: function(data) {  
	        	var str = '';
				var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
	        	for(var i = 0 ; i < data.records.length ; i++){
	        	}
	        }
    	});
    }
	function getMedResults(id, element){}
	function getMedHist(id, element){

      	$.ajax({
	      	type:'GET',
	        url: '/getAllMedHists/'+$("#patient_id").val(),      
	        success: function(data) {  
	        	var str = '';
				var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
	        	for(var i = 0 ; i < data.records.length ; i++){
	        	}
	        }
    	});

	}
</script>
<style type="text/css">

	.main-container:before {
	    display: block;
	    content: "";
	    position: absolute;
	    z-index: -2;
	    width: 100%;
	    max-width: inherit;
	    bottom: 0;
	    top: 0;
	    background-color: #e6e7e9;
	}
	.page-content {
	    background-color: #e6e7e9;
	    padding-top: 25px;
	}
</style>