 @php
							                                  $activities = Spatie\Activitylog\Models\Activity::orderBy('id', 'desc')->get();
							                                @endphp
							                                @foreach($activities as $activity)
															<div class="profile-activity clearfix">
																<div>
																	 @if($activity->description == 'login') 
																	 <img src="{{asset('uploads/'.$activity->user->icon)}}" class="pull-left thumbicon fa  btn-info no-hover">
																	
								                                      @elseif($activity->description == 'create')
																	<i class="pull-left thumbicon fa fa-check-square-o btn-success no-hover"></i>
								                                      @elseif($activity->description == 'update')
																	<i class="pull-left thumbicon fa fa-edit btn-primary no-hover"></i>
								                                      @elseif($activity->description == 'delete') 
																	<i class="pull-left thumbicon fa fa-trash-o btn-danger no-hover"></i>
								                                      @endif
																	<a class="user" href="{{url('users/'.$activity->user->id)}}" target="_blank"> {{$activity->user->people->firstname." ".$activity->user->people->lastname}} </a> 
																	
							                                      @if($activity->description == 'login') 
							                                      logged in.
							                                      @elseif($activity->description == 'create')
							                                      <b>added</b> new {{$activity->properties['table']}} record. 
							                                      @elseif($activity->description == 'update')
							                                      <b>updated</b> {{$activity->properties['table']}} record.
							                                      @elseif($activity->description == 'delete') 
							                                      <b>deleted</b> {{$activity->properties['table']}} record.
							                                      @endif
																	<div class="time">
																		<i class="ace-icon fa fa-clock-o bigger-110"></i>
																		{{$activity->created_at->diffForHumans()}}
																	</div>
																</div> 
															</div>
							                                @endforeach