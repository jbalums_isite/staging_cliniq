@extends('includes.admin-page.app')
 
@section('content') 

<!-- top action nav -->
<div class="top_nav" style="min-height: 37px;margin-bottom: 0px;background: #ededed;    border-bottom: 0px;">
  <div class="nav_menu"  style="min-height: 37px;margin-bottom: 10px;background: #ededed;margin-top: -10px;    border-bottom: 0px;">
    <nav> 

      <div class="col-md-6 pull-left"> 
        <h2 style="float:left;">&nbsp;&nbsp;&nbsp; <i class="fa fa-home"></i> Home</h2>     
      </div>
      <div class="col-md-6 pull-right"> 
      	
        <a href="#" class="pull-right action_btns addTransBtn"><h5>Add Transaction <i class=" c-orange glyphicon glyphicon-remove-circle trans-rt-45"></i></h5></a>
      </div>
    </nav>
  </div>
</div>
<!-- /top action nav -->

<div class="right_col p15 pt30" role="main">
	
	<div class="row p15 mt50">

    <div class="container pt40 dashboard_menu_ ">   
      <ul>
        <li href="{{ url('/'.\App\ClinicAccounts::find(Auth::user()->clinic_id)->prefix) }}" class="dashboard_menu active" disabled><i class="fa fa-home"></i> <br>Home</li>
        <li href="/patients" class="dashboard_menu"><i class="fa fa-info"></i> <br>Patient Information</li> 
        <li href="/inventories" class="dashboard_menu"><i class="fa fa-cubes"></i> <br>Inventory</li> 
        <li href="/schedules" class="dashboard_menu"><i class="fa fa-calendar"></i> <br>Schedules</li> 
        <li href="/services" class="dashboard_menu"><i class="fa fa-money"></i> <br>Services</li> 
        <li href="/medicines" class="dashboard_menu"><i class="fa fa-cubes"></i> <br>Medicines</li> 
        <li href="/daily_sales_reports" class="dashboard_menu"><i class="fa fa-file-text-o"></i> <br>Reports</li> 
        <li href="/referral_form" class="dashboard_menu"><i class="fa fa-file-text"></i> <br>Referral Form</li> 
        <li href="/request_form" class="dashboard_menu"><i class="fa fa-file-text"></i> <br>Request Form</li> 
        <li href="/custom_form" class="dashboard_menu"><i class="fa fa-file-text"></i> <br>Custom Form</li> 
      </ul>
    </div>

	</div>
 
</div> 
@endsection