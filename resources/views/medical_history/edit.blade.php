<div class="modal-dialog modal-md">
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button>
      <h4 class="modal-title">Edit History Form</h4>
    </div>
    {!! Form::open(array('url' => url('/medicalhistories/'.$medical_history->id), 'method' => 'PATCH', 'id' => 'edit-medicalhistories-form')) !!}
    <div class="modal-body"> 
      <input type="hidden" name="patient_id" value="{{ $patient_id }}">
      <input type="hidden" name="old_type" id="old_type" value="{{substr($medical_history->history_type, 0, -8)}}">
      <div class="form-group mb10">
        <label for="name">History Type</label>
        <select name="type" id="type" class="form-control">
          <option disabled selected>SELECT  HISTORY TYPE</option>
          <option value="medical" {{  substr($medical_history->history_type, 0, -8) == 'medical' ? 'selected' : '' }}>Medical History</option>
          <option value="surgical" {{  substr($medical_history->history_type, 0, -8) == 'surgical' ? 'selected' : '' }}>Surgical History</option>
          <option value="social" {{  substr($medical_history->history_type, 0, -8) == 'social' ? 'selected' : '' }}>Social History</option>
          <option value="family" {{  substr($medical_history->history_type, 0, -8) == 'family' ? 'selected' : '' }}>Family History</option>
        </select> 
      </div>
      <div class="form-group mb10">
        <label for="date">Date</label>
        <input id="date" name="date" type="text" class="form-control" id="single_cal4" placeholder="Date" value="{{ $medical_history->date }}" required>
      </div>
      <div class="form-group mb10">
        <label for="name">Name</label>
        <input id="name" name="name" type="text" class="form-control" placeholder="Name" required value="{{ $medical_history->name }}">
      </div>
      
      <div class="form-group mb10">
        <label for="description">Description</label>
        <textarea rows="3" class="form-control" name="description" id="description" placeholder="Description">{{ $medical_history->description }}</textarea>
      </div>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        {!! Form::submit('Submit', ['class' => 'btn submit-btn btn-primary btn-gradient pull-right']) !!}
      {!! Form::close() !!}
    </div>

  </div>
</div>


 <!-- Laravel Javascript Validation -->
 <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>

 {!! JsValidator::formRequest('App\Http\Requests\AddMedicalhistoriesRequest', '#edit-medicalhistories-form') !!}

<script type="text/javascript">
  $(function(){ 
      $('[name="date"]').datepicker({
        format: "yyyy-mm-dd",
        autoclose: true, 
      }).on('changeDate', function(selected){
       
      });   
      $("#edit-medicalhistories-form").on('submit', function(e){
          e.preventDefault();
          var that = this;
          if($("#edit-medicalhistories-form").valid()){
            $(".submit-btn").addClass("disabled");
            $('button[type=submit], input[type=submit]').prop('disabled',true);
            $.ajax({
                type: 'PATCH',
                url: '/medicalhistories/'+{{$medical_history->id}},
                data: $('#edit-medicalhistories-form').serialize(),
                dataType: 'json',
                success: function(data){
                  $('button[type=submit], input[type=submit]').prop('disabled',false);
                  $(".submit-btn").removeClass("disabled");
                }
            }).done(function(data) {
              if(data.success){
               // $("#"+$("#old_type").val()+"-history-table").DataTable().ajax.url( '/get-hist/'+$("[name='patient_id']").val()+'/'+$("#old_type").val()+'_history' ).load();
                //$("#"+$("#type").val()+"-history-table").DataTable().ajax.url( '/get-hist/'+$("[name='patient_id']").val()+'/'+$("#type").val()+'_history' ).load();
                notiff('Success!', data.msg,'success'); //title, msg, type
                $('#edithistmodal').modal('toggle');
                refreshPatient();
              }else{
                notiff('Error!', data.msg,'error'); //title, msg, type
               // $('#addallergymodal').modal('toggle');
              }
            }).error(function(data) {
                notiff('Error!', data.msg, 'warning'); //title, msg, type
                //$('#addallergymodal').modal('toggle');
            });
          }
        });
      
  });  
 </script>