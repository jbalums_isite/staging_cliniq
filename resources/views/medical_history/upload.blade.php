<div class="modal-dialog modal-md">
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button>
      <h4 class="modal-title">Add Image</h4>
    </div>
    <div class="modal-body"> 
      <form action="{{url('image-upload')}}" class="dropzone">
        {{ csrf_field() }} 
        <input type="hidden" name="patient_id" value="{{$patient_id}}">
        <input type="hidden" name="record_type" value="{{$record_type}}">
        <input type="hidden" name="record_id" value="{{$record_id}}"> 
      </form> 
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> 
    </div>

  </div>
</div>


 <!-- Laravel Javascript Validation -->
 <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>

 {!! JsValidator::formRequest('App\Http\Requests\AddManufacturerRequest', '#add-img-form') !!}

<script type="text/javascript">
  $(function(){  
      $(".dropzone").dropzone({
        clickable: true,
        maxFilesize: 4,
        init: function () {
          this.on("success", function (file, response) {
              console.log("sucesso");
              var str = '{{$record_type}}';
              if(str.split('_')[1] == 'history'){
            //    $("#"+str.split('_')[0]+"-history-table").DataTable().ajax.url( '/get-hist/{{$patient_id}}/'+str ).load();
              }else{
             //   $("#"+str.split('_')[0]+"-result-table").DataTable().ajax.url( '/get-result/{{$patient_id}}/'+str ).load();
              }
          });
        }
      }); 

      /*$("#add-img-form").on('submit', function(e){
          e.preventDefault();
          var that = this;
          if($("#add-img-form").valid()){
            $(".submit-btn").addClass("disabled");
            $('button[type=submit], input[type=submit]').prop('disabled',true);
            $.ajax({
                type: 'POST',
                enctype: 'multipart/form-data',
                url: '/image-upload',
                data:new FormData($("#add-img-form")[0]),
                dataType: 'json',

      async:false,
      type:'post',
      processData: false,
      contentType: false,
                success: function(data){
                  $('button[type=submit], input[type=submit]').prop('disabled',false);
                  $(".submit-btn").removeClass("disabled");
                }
            }).done(function(data) {
              if(data.success){
                $("#imgs-table").DataTable().ajax.url( '/get-imgs' ).load();
                notiff('Success!', data.msg,'success'); //title, msg, type
                $('#addmodal').modal('toggle');
              }else{
                notiff('Error!', data.msg,'error'); //title, msg, type
                $('#addmodal').modal('toggle');
              }
            }).error(function(data) {
                notiff('Error!', data.msg, 'warning'); //title, msg, type
                $('#addmodal').modal('toggle');
            });
          }
        });*/
  });  
 </script>