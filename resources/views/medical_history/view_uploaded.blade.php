<style type="text/css">
  .gal-img{
    width: 150px;
  } 
  #lightgallery > li{
    margin-bottom: 15px !important;
  } 
  .lg-image{
    background-color: #000000;
  }
  .lg-img-wrap{
    background: #000;
  }
</style> 
<div class="modal-dialog modal-md" style="margin-top: 70px; opacity: 0;">
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button>
      <h4 class="modal-title">View Uploaded</h4>
    </div>
    <div class="modal-body"> 
      

        <div class="demo-gallery">
            <ul id="lightgallery" class="list-unstyled row">
                @foreach($images as $image)
                <li class="col-xs-6 col-sm-3 col-md-4"  data-src="uploads/{{$image->image_name}}">
                    <a href="">
                        <img class="img-responsive gal-img" src="uploads/{{$image->image_name}}">
                    </a>
                </li>
                @endforeach
            </ul>
        </div> 
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal" id="close_mdl">Close</button> 
    </div>

  </div>
</div>

<script type="text/javascript">
  $(document).ready(function(){
      $('#lightgallery').lightGallery();
      $(".gal-img").click();
      
      $(document).on('click', ".lg-close", function(){
        $("#close_mdl").click();
        
      });
  });
</script>
