@include('not_supported')
<!-- top action nav -->
<div class="top_nav" style="height: 37px;margin-bottom: 0px;background: #ededed;    border-bottom: 0px;">
  <div class="nav_menu"  style="height: 37px;margin-bottom: 10px;background: #ededed;margin-top: -10px;    border-bottom: 0px;">
    <nav> 

      <div class="col-md-6">
      	<ul class="nav navbar-nav navbar-left">
	        <li class="">
	      		 <h2>&nbsp;&nbsp;&nbsp;Referral Form</h2>
	        </li>
	      </ul>
      </div>
      <div class="col-md-6 pull-right"> 
      	<a href="#" class="btn btn-primary pull-right  save_btn"><i class="fa fa-save"></i> Save Form</a>
      </div>
    </nav>
  </div>
</div>
<!-- /top action nav -->

<div class="right_col p15 pt30" role="main">
	
	<div class="row p15 mt50">
		@php
			$form = \App\Forms::where('type', 'referral')->orderBy('created_at', 'DESC')->get()->first();
		@endphp

		<div class="summernote"></div>
	</div>

{{ csrf_field() }}
</div>

<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;" id="addmodal"></div>
<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;" id="editmodal"></div>

<link rel="stylesheet" type="text/css" href="/summernote/summernote.css">
<script type="text/javascript" src="/summernote/summernote.js"></script> 
<script type="text/javascript" src="/summernote/summernote-ext-print.js"></script>
<script type="text/javascript">
	$(function(){
			var str = '';
			@if($form)
				str = '{{$form->body}}';
			@endif
			function decodeHTMLEntities(text) {
			    var entities = [
			        ['amp', '&'],
			        ['apos', '\''],
			        ['#x27', '\''],
			        ['#x2F', '/'],
			        ['#39', '\''],
			        ['#47', '/'],
			        ['lt', '<'],
			        ['gt', '>'],
			        ['nbsp', ' '],
			        ['quot', '"']
			    ];

			    for (var i = 0, max = entities.length; i < max; ++i) 
			        text = text.replace(new RegExp('&'+entities[i][0]+';', 'g'), entities[i][1]);

			    return text;
			} 
 			$('.summernote').summernote({
 				height: 700,
 				toolbar: [ 
 					['style', ['bold', 'italic', 'underline', 'clear']],
				    ['font', ['strikethrough', 'superscript', 'subscript']],
				    ['fontsize', ['fontsize']],
				    ['color', ['color']],
				    ['para', ['ul', 'ol', 'paragraph']],
				    ['height', ['height']],
			        ['misc', ['print']]
			    ], 
 			});
 			var s = decodeURIComponent(str);
 			$(".note-editable").html(decodeHTMLEntities(s));

 			$('.save_btn').click(function(){
 				 $.ajax({
		                type: 'POST',
		                url: '/forms',
		                data: 'type=referral&body='+encodeURIComponent($(".note-editable").html())+'&_token='+$("[name='_token']").val(),
		                dataType: 'json',
		                success: function(data){
		                  $('button[type=submit], input[type=submit]').prop('disabled',false);
		                  $(".submit-btn").removeClass("disabled");
		                }
		            });
 			});	
	});
	
</script>
 
<style type="text/css">
	tbody > tr:hover{
		cursor: pointer;
	} 
	.search_bar_{
		display: block;
	}
	.ppi_{
		display: none;
	}
	@media (max-width: 768px){
 
		.p15{
			padding: 5px !important;
			padding-left: 5px !important;
		    margin-right: 0px;
		    margin-left: 0px;
		}
		.s_bar_{
			float: right;
		    margin-right: 5px;
		    margin-top: 0px;
		}
		.search_bar_ {
			width: 100%;
		} 
		.ppi_{
			display: block;
		}
	}
</style>