@extends('new_template.app')
 
@section('content') 
<div class="breadcrumbs ace-save-state" id="breadcrumbs">
	<ul class="breadcrumb">
		<li> 
			<a href="/">Home</a>
		</li> 
		<li> 
			Custom Form
		</li> 
	</ul><!-- /.breadcrumb -->

	
	<ul class="breadcrumb pull-right action_buttons">
		<li> 
			<a href="#" class="addTransBtn">
				<i class="fa fa-plus"></i>
				Add Transactions
			</a> 
		</li>  
		<li>  
			<a href="javascript:void(0)" class="save_btn FormBtn">
				<i class="fa fa-save"></i>
				Save Form
			</a> 
		</li>  
	</ul><!-- /.breadcrumb -->

</div>


<div class="page-content"> 
	<div class="row">
		<div class="col-xs-12">

				
			@php
				$form = \App\Forms::where('type', 'custom')->orderBy('created_at', 'DESC')->get()->first();
			@endphp

			<div class="summernote"></div>

		</div><!-- /.col -->
	</div><!-- /.row -->
</div><!-- /.page-content -->
  <!-- /page content -->
{{ csrf_field() }}


 
<style type="text/css">
	tbody > tr:hover{
		cursor: pointer;
	} 
	.search_bar_{
		display: block;
	}
	.ppi_{
		display: none;
	}
	@media (max-width: 768px){
 
		.p15{
			padding: 5px !important;
			padding-left: 5px !important;
		    margin-right: 0px;
		    margin-left: 0px;
		}
		.s_bar_{
			float: right;
		    margin-right: 5px;
		    margin-top: 0px;
		}
		.search_bar_ {
			width: 100%;
		} 
		.ppi_{
			display: block;
		}
	}
</style>
@endsection

@section('script_content')

<link rel="stylesheet" type="text/css" href="/summernote/summernote.css">
<script type="text/javascript" src="/summernote/summernote.js"></script> 
<script type="text/javascript" src="/summernote/summernote-ext-print.js"></script>
<script type="text/javascript">
	$(function(){
			var str = '';
			@if($form)
				str = '{{$form->body}}';
			@endif
			function decodeHTMLEntities(text) {
			    var entities = [
			        ['amp', '&'],
			        ['apos', '\''],
			        ['#x27', '\''],
			        ['#x2F', '/'],
			        ['#39', '\''],
			        ['#47', '/'],
			        ['lt', '<'],
			        ['gt', '>'],
			        ['nbsp', ' '],
			        ['quot', '"']
			    ];

			    for (var i = 0, max = entities.length; i < max; ++i) 
			        text = text.replace(new RegExp('&'+entities[i][0]+';', 'g'), entities[i][1]);

			    return text;
			} 
 			$('.summernote').summernote({
 				height: 700,
 				toolbar: [ 
 					['style', ['bold', 'italic', 'underline', 'clear']],
				    ['font', ['strikethrough', 'superscript', 'subscript']],
				    ['fontsize', ['fontsize']],
				    ['color', ['color']],
				    ['para', ['ul', 'ol', 'paragraph']],
				    ['height', ['height']],
			        ['misc', ['print']]
			    ], 
 			});
 			var s = decodeURIComponent(str);
 			$(".note-editable").html(decodeHTMLEntities(s));

 			$('.save_btn').click(function(){
 				 $.ajax({
		                type: 'POST',
		                url: '/forms',
		                data: 'type=custom&body='+encodeURIComponent($(".note-editable").html())+'&_token='+$("[name='_token']").val(),
		                dataType: 'json',
		                success: function(data){
		                	if(data.success){
			                	notiff('Success!', data.msg,'success'); //title, msg, type
		                	}
		                  $('button[type=submit], input[type=submit]').prop('disabled',false);
		                  $(".submit-btn").removeClass("disabled");
		                }
		            });
 			});	
 			
		setTimeout(resize_action_buttons_height, 100);
        function resize_action_buttons_height(){  
			$('.action_buttons').css('height',$('#breadcrumbs').height()+"px");
			$('.action_buttons a').css('height',$('#breadcrumbs').height()+"px");
        }
	});
	
</script>
 @endsection