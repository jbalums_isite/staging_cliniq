<div class="modal-dialog modal-md">
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button>
      <h4 class="modal-title">Add {{ucfirst($type)}} Results Form</h4>
    </div>
    {!! Form::open(array('url' => url('/results'), 'method' => 'POST', 'id' => 'add-results-form')) !!}
    <div class="modal-body">
      <input type="hidden" name="patient_id" value="{{ $patient_id }}"> 
      <input type="hidden" name="type" value="{{$type.'_result'}}">
      <div class="form-group mb10">
        <label for="date">Date</label>
        <input id="date" name="date" type="text" class="form-control" id="single_cal4" placeholder="Date" value="{{ date('Y-m-d') }}" required>
      </div>
      <div class="form-group mb10">
        <label for="name">Name</label>
        <input id="name" name="name" type="text" class="form-control" placeholder="Name" required>
      </div>
      <div class="form-group mb10">
        <label for="description">Description</label>
        <textarea rows="3" class="form-control" name="description" id="description" placeholder="Description"></textarea>
      </div>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        {!! Form::submit('Submit', ['class' => 'btn submit-btn btn-primary btn-gradient pull-right']) !!}
      {!! Form::close() !!}
    </div>

  </div>
</div>


 <!-- Laravel Javascript Validation -->
 <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>

 {!! JsValidator::formRequest('App\Http\Requests\AddResultsRequest', '#add-results-form') !!}

<script type="text/javascript">
  $(function(){   
      $('[name="date"]').datepicker({
        format: "yyyy-mm-dd",
        autoclose: true, 
      }).on('changeDate', function(selected){
       
      }); 
      $("#add-results-form").on('submit', function(e){
          e.preventDefault();
          var that = this;
          if($("#add-results-form").valid()){
            $(".submit-btn").addClass("disabled");
            $('button[type=submit], input[type=submit]').prop('disabled',true);
            $.ajax({
                type: 'POST',
                url: '/results',
                data: $('#add-results-form').serialize(),
                dataType: 'json',
                success: function(data){
                  $('button[type=submit], input[type=submit]').prop('disabled',false);
                  $(".submit-btn").removeClass("disabled");
                }
            }).done(function(data) {
              if(data.success){
                $("#{{$type}}-result-table").DataTable().ajax.url( '/get-result/'+$("[name='patient_id']").val()+'/{{$type}}_result' ).load();
                notiff('Success!', data.msg,'success'); //title, msg, type
                $('#addresmodal').modal('toggle');
              }else{
                notiff('Error!', data.msg,'error'); //title, msg, type
               // $('#addallergymodal').modal('toggle');
              }
            }).error(function(data) {
                notiff('Error!', data.msg, 'warning'); //title, msg, type
                //$('#addallergymodal').modal('toggle');
            });
          }
        });
      
  });  
 </script>