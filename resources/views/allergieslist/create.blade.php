<div class="modal-dialog modal-md">
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button>
      <h4 class="modal-title">Add Allergy Form</h4>
    </div>
    {!! Form::open(array('url' => url('/allergieslist'), 'method' => 'POST', 'id' => 'add-allergieslist-form')) !!}
    <div class="modal-body"> 
      <div class="form-group mb10">
        <label for="name">Name</label>
        <input id="name" name="name" type="text" class="form-control" placeholder="Name" required>
      </div>
      <div class="form-group mb10">
        <label for="description">Description</label>
        <textarea rows="3" class="form-control" name="description" id="description" placeholder="Description"></textarea>
      </div>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        {!! Form::submit('Submit', ['class' => 'btn submit-btn btn-primary btn-gradient pull-right']) !!}
      {!! Form::close() !!}
    </div>

  </div>
</div>


 <!-- Laravel Javascript Validation -->
 <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>

 {!! JsValidator::formRequest('App\Http\Requests\AddAllergiesListRequest', '#add-allergieslist-form') !!}

<script type="text/javascript">
  $(function(){
      $("#add-allergieslist-form").on('submit', function(e){
          e.preventDefault();
          var that = this;
          if($("#add-allergieslist-form").valid()){
            $(".submit-btn").addClass("disabled");
            $('button[type=submit], input[type=submit]').prop('disabled',true);
            $.ajax({
                type: 'POST',
                url: '/allergieslist',
                data: $('#add-allergieslist-form').serialize(),
                dataType: 'json',
                success: function(data){
                  $('button[type=submit], input[type=submit]').prop('disabled',false);
                  $(".submit-btn").removeClass("disabled");
                }
            }).done(function(data) {
              if(data.success){
                $("#allergy-list-table").DataTable().ajax.url( '/get_allergieslist/').load();
                notiff('Success!', data.msg,'success'); //title, msg, type
                $('#addmodal').modal('toggle');
              }else{
                notiff('Error!', data.msg,'error'); //title, msg, type
               // $('#addalmodal').modal('toggle');
              }
            }).error(function(data) {
                notiff('Error!', data.msg, 'warning'); //title, msg, type
                //$('#addalmodal').modal('toggle');
            });
          }
        });
  });  
 </script>