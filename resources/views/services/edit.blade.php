<div class="modal-dialog modal-md">
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button>
      <h4 class="modal-title">Add Service Form</h4>
    </div>
    {!! Form::open(array('url' => url('/services/'.$service->id), 'method' => 'PATCH', 'id' => 'add-services-form')) !!}
    <div class="modal-body">
      <div class="form-group">
        <label for="name">Service Name</label>
        <input id="name" name="name" type="text" class="form-control" placeholder="Name" required value="{{$service->name}}">
      </div>
      <div class="form-group">
        <label for="description">Description</label>
        <textarea id="description" name="description" class="form-control" placeholder="Description" rows="3" required>{{$service->description}}</textarea>
      </div>
      <div class="form-group">
        <label for="price">Service Charge</label>
        <input id="price" name="price" type="number" min="1" class="form-control" placeholder="Charge" required  value="{{$service->price}}">
      </div>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        {!! Form::submit('Submit', ['class' => 'btn submit-btn btn-primary btn-gradient pull-right']) !!}
      {!! Form::close() !!}
    </div>

  </div>
</div>



 <!-- Laravel Javascript Validation -->
 <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>

 {!! JsValidator::formRequest('App\Http\Requests\AddServicesRequest', '#add-services-form') !!}

<script type="text/javascript">
  $(function(){
      $("#add-services-form").on('submit', function(e){
          e.preventDefault();
          var that = this;
          if($("#add-services-form").valid()){
            $(".submit-btn").addClass("disabled");
            $('button[type=submit], input[type=submit]').prop('disabled',true);
            $.ajax({
                type: 'PATCH',
                url: '/services/'+{{$service->id}},
                data: $('#add-services-form').serialize(),
                dataType: 'json',
                success: function(data){
                  $('button[type=submit], input[type=submit]').prop('disabled',false);
                  $(".submit-btn").removeClass("disabled");
                }
            }).done(function(data) {
              if(data.success){
                $("#services-table").DataTable().ajax.url( '/get-services' ).load();
                notiff('Success!', data.msg,'success'); //title, msg, type
                $('#editmodal').modal('toggle');
              }else{
                notiff('Error!', data.msg,'error'); //title, msg, type
                $('#editmodal').modal('toggle');
              }
            }).error(function(data) {
                notiff('Error!', data.msg, 'warning'); //title, msg, type
                $('#editmodal').modal('toggle');
            });
          }
        });
    $("#price").focusout(function(){
      $(this).val(parseFloat($(this).val()).toFixed(2));
    })
  });  
 </script>