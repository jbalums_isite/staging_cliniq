@extends('new_template.app')
 
@section('content') 
<div class="breadcrumbs ace-save-state" id="	 ">
	<ul class="breadcrumb"> 
		<li> 
			<a href="/">Home</a>
		</li> 
		<li> 
			Settings
		</li> 
	</ul><!-- /.breadcrumb -->
 
</div>



<div class="page-content" style="padding-top: 50px;"> 
	<div class="row"> 
	<div class="col-md-12"> 
		<div class="col-md-5" style="padding: 25px; box-shadow: 0px 0px 2px 0px #a5a5a5;">
			<div class="row"> 
				<h3 style="margin-top: 0px;">User Settings: <b>{{ucfirst($user->people->title)." ".ucfirst($user->people->firstname).' '.ucfirst($user->people->lastname)}}</b><br><small>User Type: <b>{{ucfirst($user->type)}}</b></small></h3>
			</div>
			<div class="row">
				
						@if(isset($success))
							@if($success)
							<div class="alert alert-block alert-success">
								<button type="button" class="close" data-dismiss="alert">
									<i class="ace-icon fa fa-times"></i>
								</button>

								<p>
									<strong>
										<i class="ace-icon fa fa-check"></i>
										Success!
									</strong>
									Settings successfully updated.
								</p> 
							</div>
							@else
							<div class="alert alert-block alert-success">
								<button type="button" class="close" data-dismiss="alert">
									<i class="ace-icon fa fa-times"></i>
								</button>

								<p>
									<strong>
										<i class="ace-icon fa fa-check"></i>
										Error!
									</strong>
									An error occured while updating Settings.
								</p> 
							</div>
							@endif
						@endif
			</div>
		    {!! Form::open(array('url' => url('/settings/'.$user->setting->id), 'method' => 'PATCH', 'id' => 'add-results-form')) !!}
 			<div class="row"> 
				<table class="table table-hover border-less">
					<tr>
						<th class="col-md-8">Dashboard</th>
						<td> 
							<label>
								<input name="dasboard" class="ace ace-switch ace-switch-7" type="checkbox" {{$user->setting->dasboard ? 'checked':''}}>
								<span class="lbl"></span>
							</label>
						</td>
					</tr>
					<tr>
						<th class="col-md-8">Patient</th>
						<td> 
							<label>
								<input name="patient" class="ace ace-switch ace-switch-7"  type="checkbox" {{$user->setting->patient ? 'checked':''}}>
								<span class="lbl"></span>
							</label>
						</td>
					</tr>
					<tr>
						<th class="col-md-8">medicine</th>
						<td> 
							<label>
								<input name="medicine" class="ace ace-switch ace-switch-7"  type="checkbox" {{$user->setting->medicine ? 'checked':''}}>
								<span class="lbl"></span>
							</label>
						</td>
					</tr>

					<tr>
						<th class="col-md-8">services</th>
						<td> 
							<label>
								<input name="services" class="ace ace-switch ace-switch-7"  type="checkbox" {{$user->setting->services ? 'checked':''}}>
								<span class="lbl"></span>
							</label>
						</td>
					</tr>

					<tr>
						<th class="col-md-8">view products</th>
						<td> 
							<label>
								<input name="view_products" class="ace ace-switch ace-switch-7"  type="checkbox" {{$user->setting->view_products ? 'checked':''}}>
								<span class="lbl"></span>
							</label>
						</td>
					</tr>

					<tr>
						<th class="col-md-8">stocks</th>
						<td> 
							<label>
								<input name="stocks" class="ace ace-switch ace-switch-7"  type="checkbox" {{$user->setting->stocks ? 'checked':''}}>
								<span class="lbl"></span>
							</label>
						</td>
					</tr> 

					<tr>
						<th class="col-md-8">manufacturers</th>
						<td> 
							<label>
								<input name="manufacturers" class="ace ace-switch ace-switch-7"  type="checkbox" {{$user->setting->manufacturers ? 'checked':''}}>
								<span class="lbl"></span>
							</label>
						</td>
					</tr>

					<tr>
						<th class="col-md-8">brands</th>
						<td> 
							<label>
								<input name="brands" class="ace ace-switch ace-switch-7"  type="checkbox" {{$user->setting->brands ? 'checked':''}}>
								<span class="lbl"></span>
							</label>
						</td>
					</tr>

					<tr>
						<th class="col-md-8">schedules</th>
						<td> 
							<label>
								<input name="schedules" class="ace ace-switch ace-switch-7"  type="checkbox" {{$user->setting->schedules ? 'checked':''}}>
								<span class="lbl"></span>
							</label>
						</td>
					</tr>

					<tr>
						<th class="col-md-8">referral form</th>
						<td> 
							<label>
								<input name="referral_form" class="ace ace-switch ace-switch-7"  type="checkbox" {{$user->setting->referral_form ? 'checked':''}}>
								<span class="lbl"></span>
							</label>
						</td>
					</tr>

					<tr>
						<th class="col-md-8">request form</th>
						<td> 
							<label>
								<input name="request_form" class="ace ace-switch ace-switch-7"  type="checkbox" {{$user->setting->request_form ? 'checked':''}}>
								<span class="lbl"></span>
							</label>
						</td>
					</tr>


					<tr>
						<th class="col-md-8">custom form</th>
						<td> 
							<label>
								<input name="custom_form" class="ace ace-switch ace-switch-7"  type="checkbox" {{$user->setting->custom_form ? 'checked':''}}>
								<span class="lbl"></span>
							</label>
						</td>
					</tr>


					<tr>
						<th class="col-md-8">dialy reports</th>
						<td> 
							<label>
								<input name="dialy_reports" class="ace ace-switch ace-switch-7"  type="checkbox" {{$user->setting->dialy_reports ? 'checked':''}}>
								<span class="lbl"></span>
							</label>
						</td>
					</tr>


					<tr>
						<th class="col-md-8">monthly reports</th>
						<td> 
							<label>
								<input name="monthly_reports" class="ace ace-switch ace-switch-7"  type="checkbox" {{$user->setting->monthly_reports ? 'checked':''}}>
								<span class="lbl"></span>
							</label>
						</td>
					</tr>

					<tr>
						<th class="col-md-8">quarterly reports</th>
						<td> 
							<label>
								<input name="quarterly_reports" class="ace ace-switch ace-switch-7"  type="checkbox" {{$user->setting->quarterly_reports ? 'checked':''}}>
								<span class="lbl"></span>
							</label>
						</td>
					</tr>

					
					
					<tr>
						<th class="col-md-8">annual reports</th>
						<td> 
							<label>
								<input name="annual_reports" class="ace ace-switch ace-switch-7"  type="checkbox" {{$user->setting->annual_reports ? 'checked':''}}>
								<span class="lbl"></span>
							</label>
						</td>
					</tr>

					
					
					<tr>
						<th class="col-md-8">users</th>
						<td> 
							<label>
								<input name="users" class="ace ace-switch ace-switch-7"  type="checkbox" {{$user->setting->users ? 'checked':''}}>
								<span class="lbl"></span>
							</label>
						</td>
					</tr>

					
					
					<tr>
						<th class="col-md-8">allergies</th>
						<td> 
							<label>
								<input name="allergies" class="ace ace-switch ace-switch-7"  type="checkbox" {{$user->setting->allergies ? 'checked':''}}>
								<span class="lbl"></span>
							</label>
						</td>
					</tr>
 					
					<tr>
						<td colspan="2" class="text-center">
						<hr> 
					        {!! Form::submit('Update Settings', ['class' => 'btn submit-btn btn-primary btn-gradient pull-right']) !!}
						</td>
					</tr>
				</table>   

 			</div>
		      {!! Form::close() !!}
		</div><!-- /.col -->
	</div><!-- /.row -->
	</div><!-- /.row -->
</div><!-- /.page-content -->
  <!-- /page content -->
  <style type="text/css">
  	.border-less th, 
  	.border-less td{
  		border: 0px !important;
  		vertical-align: middle !important;
  		padding-top: 5px !important;
  		padding-bottom: 5px !important;
  	}
  	th{
  		text-transform: capitalize;
  	}
  </style>
@endsection 

@section('script_content')
<script type="text/javascript">	
   window.history.pushState('',document.title,(window.location.href).split("?")[0]);
</script>
@endsection 