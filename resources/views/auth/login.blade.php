<!DOCTYPE html>
<html>
<head>
	<meta name="keywords" content="Cliniq" /> 

	<title>Login</title>

	<link href="/tmpl/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
	<meta name="keywords" content="Cliniq" />
	<link rel="shortcut icon" href="{{asset('tmpl/images/cliniq-logo.png')}}">
	
	<link rel="stylesheet" type="text/css" href="{{asset('tmpl/fonts/font-awesome-4.7.0/css/font-awesome.css')}}">

	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700|Poiret+One|Mitr" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="{{asset('css/login.css')}}">
</head>
<body>
	<div class="col-md-12 main">
		<div class="col-md-7 pull-left left">
			<center class="text-center center_text">
				<span>WELCOME TO</span>
				<b>CLINIQ</b>
				<span class="slogan">Simply Patients Management</span>
			</center>
			
			<center class="text-center bottom_text">
				<div class="col-md-12 social-media">
					<ul>
						<li><a href="#"><i class="fa fa-facebook"></i></a></li>
						<li><a href="#"><i class="fa fa-twitter"></i></a></li>
						<li><a href="#"><i class="fa fa-instagram"></i></a></li>
					</ul>
				</div>

				<footer>
					Copyright © 2017 iSiteSolution Consultancy.<br>
					All rights reserved.
				</footer>
			</center>
		</div>

		<div class="col-md-5 pull-right right">
			
			<center class="text-center center_text">
				<img src="{{asset('/tmpl/images/stet.png')}}">

				<form class="form-horizontal login-form" role="form" method="POST" action="{{ url('/login') }}">
                        {{ csrf_field() }}


                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">     
                        <input type="email" class="form-control" name="email" placeholder="Email" required="" /> 
                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">   
                        <input type="password" class="form-control" name="password" placeholder="Password"  required="" />
                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>


                    <button type="submit" class="btn btn-block btn-sky-blue">LOGIN</button>
                    
                    <br>
                    <h4 class="sky-blue text-center p5"><a href="#">Forgot password?</a></h4>
                </form>
			</center>
		</div>
	</div>

	

</body>
</html>