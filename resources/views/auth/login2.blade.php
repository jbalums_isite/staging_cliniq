@extends('includes.app')

@section('content')

<style type="text/css">
    .footer{

    margin-top: 35px;
    }
</style>
<div class="full-page">
    
    <div class="login-grids col-md-4 col-md-offset-4 col-xs-10 col-xs-offset-1">
            
            <div class="login-right">
                <div class="col-md-12  text-center">
                    <a href="/"><img id="lg_img" src="/tmpl/images/cliniq-logo.png"></a>
                </div>
                <input type="hidden" value="" id="">
                <h3 class="c-gray  text-center" style="text-transform: uppercase;">{{ $clinic->clinic_name }}</h3>
                <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
                        {{ csrf_field() }}


                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">    
                        <div class="inner-addon left-addon">
                            <i class="glyphicon glyphicon-user"></i>
                            <input type="email" class="form-control" name="email" value="Email" required="" />
                        </div>
                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">  
                        <div class="inner-addon left-addon">
                            <i class="glyphicon glyphicon-lock"></i>
                            <input type="password" class="form-control" name="password" value="Password"  required="" />
                        </div>
                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>

                    <button type="submit" class="btn btn-block btn-sky-blue">
                    SIGN IN
                    </button>
                    
                    <br>
                    <h4 class="sky-blue text-center p5"><a href="#">Forgot password?</a></h4>
                </form>
            </div>
            
    </div>
</div>
<script type="text/javascript">
    
    history.pushState({}, null, '{{  \App\ClinicAccounts::find($clinic->id)->prefix }}');

    $(window).resize(function(){
        $('.full-page').css('height', $(window).height());

            if($(window).height() < 585){
                $('#lg_img').addClass('pt25');
            }
    });
    $(window).ready(function(){
        $('.full-page').css('height', $(window).height());

            if($(window).height() < 585){
                $('#lg_img').addClass('pt25');
            }
    });
    if($(window).width() < 716){
        
    }else{

    }
    $(function(){
        $('[name="email"]').on('focus', function(){ 
            var val_ = ""+$(this).val();
            if(val_ == 'Email'){
                $(this).val(""); 
            }else if(val_ == ''){
                $(this).val("Email"); 
            }
        });
        $('[name="email"]').blur(function(){ 
            var val_ = ""+$(this).val();
            if(val_ == 'Email'){
                $(this).val(""); 
            }else if(val_ == ''){
                $(this).val("Email"); 
            }
        });
        $('[name="password"]').on('focus', function(){ 
            var val_ = ""+$(this).val();
            if(val_ == 'Password'){
                $(this).val(""); 
            }else if(val_ == ''){
                $(this).val("Password"); 
            }
        });
        $('[name="password"]').blur(function(){ 
            var val_ = ""+$(this).val();
            if(val_ == 'Password'){
                $(this).val(""); 
            }else if(val_ == ''){
                $(this).val("Password"); 
            }
        });


    })
</script>
    <style type="text/css">

        @media (min-width: 995px) {
            .footer_{
                position: absolute;
                bottom: 0px;
            }
        }
        .pt25{
            padding-top: 25px !important;
        }
    </style>
@endsection
