<div class="modal-dialog modal-md">
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button>
      <h4 class="modal-title">Add Stock Form</h4>
    </div>
    {!! Form::open(array('url' => url('/inventories'), 'method' => 'POST', 'id' => 'add-inventory-form')) !!}
    <div class="modal-body"> 
      <div class="form-group mb10">
        <label for="date">Date</label>
        <input id="date" name="date" type="date" class="form-control" id="single_cal4" placeholder="Date" value="{{ date('Y-m-d') }}" required>
      </div>
      <div class="form-group mb10 mt10">
        <label>Stock &nbsp;<a href="#" class="pull-right  addnewrow "  data-type="test" style=""><i class=" c-orange glyphicon glyphicon-remove-circle trans-rt-45"></i></a></label>
        <table class="table table-bordered" id="stocks_table">
          <thead>
            <th class="col-md-7">Product</th>
            <th class="col-md-4">Quantity</th>
            <th></th>
          </thead>
          <tbody>
          </tbody>
        </table>
      </div>
      
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        {!! Form::submit('Submit', ['class' => 'btn submit-btn btn-primary btn-gradient pull-right']) !!}
      {!! Form::close() !!}
    </div>

  </div>
</div>


 <!-- Laravel Javascript Validation -->
 <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>

 {!! JsValidator::formRequest('App\Http\Requests\AddStockRequest', '#add-inventory-form') !!}

<script type="text/javascript">
  $(function(){
       
     var str ='<tr>'+
                '<td>'+
                  '{!! Form::select("product_id[]", $products, null,["id"=>"products","class"=> "form-control", "placeholder"=>"Select Product"]) !!}'+
                '</td>'+
                '<td>'+
                  '<input type="number" min="1" name="qty[]" placeholder="Quantity" class="form-control">'+
                '</td>'+
                '<td>'+
                  '<span class="badge delrow"><i class="fa fa-trash"></i></span>'+
                '</td>'+
              '</tr>';
      $("#stocks_table tbody").append(str);
      $("#products").selectize({
          create: false,
          sortField: 'text'
      });
      $("#add-inventory-form").on('submit', function(e){
          e.preventDefault();
          var that = this;
          if($("#add-inventory-form").valid()){
            $(".submit-btn").addClass("disabled");
            $('button[type=submit], input[type=submit]').prop('disabled',true);
            $.ajax({
                type: 'POST',
                url: '/inventories',
                data: $('#add-inventory-form').serialize(),
                dataType: 'json',
                success: function(data){
                  $('button[type=submit], input[type=submit]').prop('disabled',false);
                  $(".submit-btn").removeClass("disabled");
                }
            }).done(function(data) {
              if(data.success){
                $("#inventories-table").DataTable().ajax.url('/get-inventories').load();
                $("#inventory-histories-table").DataTable().ajax.url('/get-inventory-histories').load();
                notiff('Success!', data.msg,'success'); //title, msg, type
                $('#addmodal').modal('toggle');
              }else{
                notiff('Error!', data.msg,'error'); //title, msg, type
                //$('#addstockmodal').modal('toggle');
              }
            }).error(function(data) {
                notiff('Error!', data.msg, 'warning'); //title, msg, type
                //$('#addstockmodal').modal('toggle');
            });
          }
        });
      $(".addnewrow").click(function(x){
        x.preventDefault();
        $("#stocks_table tbody").append(str);
        $("[name='product_id[]']").last().selectize({
            create: false,
            sortField: 'text'
        });
      }); 

      $(document).on('click', '.delrow', function(x){
        x.preventDefault();
        if($("#stocks_table  tbody > tr").length > 1){
          $(this).parent().parent().remove();
        }
      })
  });  
 </script>
 <style type="text/css">
   #stocks_table .badge{ 
    padding: 10px;
    border-radius: 50%;
    background: transparent;
    color: red;
    margin-top: 0px;
    font-size: 15px;
    transition: .2s ease-in-out;
    background: rgba(255, 186, 186, 0.25);
   }
   #stocks_table .badge:hover{
    cursor: pointer;
    background: red;
    color:white;
   }
 </style>