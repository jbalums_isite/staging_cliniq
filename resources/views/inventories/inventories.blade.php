@extends('new_template.app')
 
@section('content') 
<div class="breadcrumbs ace-save-state" id="breadcrumbs">
	<ul class="breadcrumb">
		<li> 
			<a href="/">Home</a>
		</li> 
		<li> 
			Products
		</li> 
		<li> 
			Stocks
		</li> 
	</ul><!-- /.breadcrumb -->

	
	<ul class="breadcrumb pull-right action_buttons">
		<li> 
			<a href="#" class="addTransBtn">
				<i class="fa fa-plus"></i>
				Add Transactions
			</a> 
		</li>  
		<li> 
			<a href="javascript:void(0)" class="addStocksBtn">
				<i class="fa fa-plus"></i>
				Add Stocks
			</a> 
		</li>  
	</ul><!-- /.breadcrumb -->

</div>


<div class="page-content"> 
	<div class="row">
		<div class="col-xs-12">

			<table class="table jambo_table theme-table theme-table-edit dt-responsive nowrap " id="stocks-table" width="100%">
				<thead>
					<tr>  
						<th>#</th>
						<th>Product</th>
						<th>Qty. Left</th>
						<th>Beginning Qty.</th>
						<th>Date</th>
					</tr>
				</thead>
				<tbody>
					
				</tbody>
			</table>  

		</div><!-- /.col -->
	</div><!-- /.row -->

	<div class="row">
		<div class="col-xs-12"> 
			<h3 style="margin: 0px;padding-top: 10px;padding-bottom: 10px;box-shadow: 0px -1px 0px 0px #eee, 0px 1px 0px 0px #eee;">Stock History</h3>
			<table class="table jambo_table theme-table theme-table-edit dt-responsive nowrap " id="inventory-histories-table" width="100%">
				<thead>
					<tr> 
						<th>#</th>
						<th>Product</th>
						<th>Quantity</th>
						<th>Remarks</th>
						<th>Date</th>
					</tr>
				</thead>
				<tbody>
					
				</tbody>
			</table>  

		</div><!-- /.col -->
	</div><!-- /.row -->
</div><!-- /.page-content -->
  <!-- /page content -->

<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;" id="addmodal"></div>
<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;" id="editmodal"></div>
<style type="text/css">
	tbody > tr:hover{
		cursor: pointer;
	} 
	.search_bar_{
		display: block;
	}
	.ppi_{
		display: none;
	}
	@media (max-width: 768px){
 
		.p15{
			padding: 5px !important;
			padding-left: 5px !important;
		    margin-right: 0px;
		    margin-left: 0px;
		}
		.s_bar_{
			float: right;
		    margin-right: 5px;
		    margin-top: 0px;
		}
		.search_bar_ {
			width: 100%;
		} 
		.ppi_{
			display: block;
		}
	}
</style>
@endsection 

@section('script_content')
<script type="text/javascript">
	$(function(){

		var table = $('#stocks-table').DataTable({
		    bProcessing: true,
		    bServerSide: false,
		    sServerMethod: "GET",
			'ajax': '/get-inventories',
			searching: true, 
			paging: true, 
			filtering:false, 
			bInfo: true,
			responsive: true,
			language:{
				"paginate": {
			        "next":       "<i class='fa fa-chevron-right'></i>",
			        "previous":   "<i class='fa fa-chevron-left'></i>"
			    }
			},
			"columns": [  
	            {data: 'row', name: 'row', className: 'hidden col-md-1 text-center', searchable: false, 	sortable: true},
	            {data: 'product', 	name: 'product', className: ' text-left', 	searchable: true,	sortable: true},
	            {data: 'ending', 	name: 'ending', className: 'col-md-2 text-left text-center', 	searchable: true,	sortable: true},
	            {data: 'beginning', 	name: 'beginning', className: 'col-md-2 text-left text-center', 	searchable: true,	sortable: true},
	            {data: 'date', 	name: 'date', className: 'col-md-2 text-left', 	searchable: true,	sortable: true},
	        ], 
      		'order': [[1, 'asc']]
		});

	
			$('#inventory-histories-table').DataTable({
		    bProcessing: true,
		    bServerSide: false,
		    sServerMethod: "GET",
			'ajax': '/get-inventory-histories',
			searching: false, 
			paging: true, 
			filtering:false, 
			responsive: true,
			bInfo: true,
			language:{
				"paginate": {
			        "next":       "<i class='fa fa-chevron-right'></i>",
			        "previous":   "<i class='fa fa-chevron-left'></i>"
			    }
			},
        	"order": [[ 4, "desc" ]],
			"columns": [
	            {data: 'row', name: 'row', className: 'col-md-1 text-center hidden', searchable: false, 	sortable: true},
	            {data: 'product', 	name: 'product', className: ' text-left', 	searchable: true,	sortable: true},
	            {data: 'qty', 	name: 'qt', className: ' text-left', 	searchable: true,	sortable: true},
	            {data: 'remarks', 	name: 'remarks', className: ' text-left', 	searchable: true,	sortable: true},
	            {data: 'date', 	name: 'date', className: ' text-left', 	searchable: true,	sortable: true},
	        ]
		});
		//initiate dataTables plugin
		
		$(".addStocksBtn").click(function(x){  
			x.preventDefault();
			var that = this;
			$("#addmodal").modal();
			$.ajax({
				url: '/inventories/create',					
				success: function(data) {
					$("#addmodal").html(data);
				}
			});	
		});

		var p_table = $('#stocks-table').DataTable(); 
		$('#patient_search').keyup(function(){ 
		    p_table.search($(this).val()).draw();
		})
 
		$(document).off('click', ".edit-patient-btn").on('click', ".edit-patient-btn", function(x){ 
			var that = this;
			x.preventDefault();
			$("#editmodal").modal();
			$.ajax({
				url: '/inventories/'+that.dataset.id+'/edit',					
				success: function(data) {
					$("#editmodal").html(data);
				}
			});	
		});

		
		 $(document).off('click', '.delete-patient-btn').on('click', '.delete-patient-btn', function(x){
            var that = this;
			x.preventDefault();
            bootbox.confirm({
              title: "Confirm Delete Stocks?",
              className: "del-bootbox",
              message: "Are you sure you want to delete record?",
              buttons: {
                  confirm: {
                      label: 'Yes',
                      className: 'btn-success'
                  },
                  cancel: {
                      label: 'No',
                      className: 'btn-danger'
                  }
              },
              callback: function (result) {
                 if(result){
                  var token = '{{csrf_token()}}'; 
                  $.ajax({
                  url:'/stocks/'+that.dataset.id,
                  type: 'post',
                  data: {_method: 'delete', _token :token},
                  success:function(data){
                	$("#stocks-table").DataTable().ajax.url( '/get-stocks' ).load();
                	notiff('Success!', data.msg,'success'); //title, msg, type
                  }
                  }); 
                 }
              }
          });
      });
		$(".status_choice").on('click', function(){
			alert(this.dataset.id);
		})

		setTimeout(resize_action_buttons_height, 100);
        function resize_action_buttons_height(){  
			$('.action_buttons').css('height',$('#breadcrumbs').height()+"px");
			$('.action_buttons a').css('height',$('#breadcrumbs').height()+"px");
        }
	});
	
</script>

@endsection