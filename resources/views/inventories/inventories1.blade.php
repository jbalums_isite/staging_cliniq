@include('not_supported')
<!-- top action nav -->
<div class="top_nav" style="height: 37px;margin-bottom: 0px;background: #ededed;    border-bottom: 0px;">
  <div class="nav_menu"  style="height: 37px;margin-bottom: 10px;background: #ededed;margin-top: -10px;    border-bottom: 0px;">
    <nav> 

    <!--   <ul class="nav navbar-nav navbar-left hidden">
        <li class="">
      		<input type="text" name="search" placeholder="Search...">
      		<i class="fa fa-search"></i>
        </li>
      </ul>
      <div class="col-xs-4 pull-right">
      	<a href="#" class="dropdown-toggle pull-right action_btns hidden" data-toggle="dropdown" role="button" aria-expanded="false"><h5>Active <i class=" c-orange glyphicon glyphicon glyphicon-chevron-down"></i></h5></a>
	        <ul class="dropdown-menu" role="menu">
	          <li><a href="#">Active</a>
	          </li>
	          <li><a href="#">Inactive</a>
	          </li>
	        </ul>
      	<a href="#" class="pull-right action_btns addStockBtn"><h5>Add Stock <i class=" c-orange glyphicon glyphicon-remove-circle trans-rt-45"></i></h5></a>
      	<a href="#" class="pull-right action_btns addTransBtn"><h5>Add Transaction <i class=" c-orange glyphicon glyphicon-remove-circle trans-rt-45"></i></h5></a>
      </div>
 -->

      <div class="col-md-6 search_bar_">
      		<div class="btn-group action_group"> 
	          <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
	            <i class="fa fa-bars"></i>
	            <span class="sr-only">Toggle Dropdown</span>
	          </button>
	          <ul class="dropdown-menu dropdown-menu-left" role="menu">
	            <li> 
      				<a href="#" class="pull-right action_btns addStockBtn"><h5>Add Stock <i class=" c-orange glyphicon glyphicon-remove-circle trans-rt-45"></i></h5></a>
	            </li>
	            <li>
	            	<a href="#" class="pull-right action_btns addTransBtn"><h5>Add Transaction <i class=" c-orange glyphicon glyphicon-remove-circle trans-rt-45"></i></h5></a>
	            </li>  
	          </ul>
	        </div>
	      <ul class="nav navbar-nav navbar-left s_bar_">
	        <li  class="hidden">
	      		<input type="text" name="search" placeholder="Search..." id="medicines_search">
	      		<i class="fa fa-search"></i>
	        </li>
	      </ul>

      </div>

      <div class="col-md-6 pull-right"> 
      	<a href="#" class="pull-right action_btns addStockBtn hide_md"><h5>Add Stock <i class=" c-orange glyphicon glyphicon-remove-circle trans-rt-45"></i></h5></a>
      	<a href="#" class="pull-right action_btns addTransBtn hide_md"><h5>Add Transaction <i class=" c-orange glyphicon glyphicon-remove-circle trans-rt-45"></i></h5></a>
      </div>


    </nav>
  </div>
</div>
<!-- /top action nav -->
<div class="right_col p15 pt30" role="main">
	
	<div class="row p15 mt50  ">
		<h3>Stocks 
			<div class="col-md-3 col-sm-4 col-xs-8 pull-right ">
				<input type="date" name="date_1" class="form-control pull-right" value="{{date('Y-m-d')}}">
			</div>
		</h3>
		<div class=" ">
			<table class="table jambo_table theme-table dt-responsive nowrap" id="inventories-table" width="100%">
				<thead>
					<tr>
						<th>#</th>
						<th>Product</th>
						<th>Qty. Left</th>
						<th>Beginning Qty.</th>
						<th>Date</th>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		</div>
	</div>

	<div class="row p15 mt20  ">
		<h3>Stocks History</h3>
		<div class=" ">
			<table class="table jambo_table theme-table  dt-responsive nowrap" id="inventory-histories-table" width="100%">
				<thead>
					<tr>
						<th>#</th>
						<th>Product</th>
						<th>Quantity</th>
						<th>Remarks</th>
						<th>Date</th>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		</div>
	</div>

</div>

<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;" id="addstockmodal"></div>

<script type="text/javascript">
	$(function(){

		$('#inventories-table').DataTable({
		    bProcessing: true,
		    bServerSide: false,
		    sServerMethod: "GET",
			'ajax': '/get-inventories',
			searching: false, 
			paging: true, 
			filtering:false, 
			responsive: true,
			bInfo: false,
			language:{
				"paginate": {
			        "next":       "<i class='fa fa-chevron-right'></i>",
			        "previous":   "<i class='fa fa-chevron-left'></i>"
			    }
			},
        	"order": [[ 4, "desc" ]],
			"columns": [
	            {data: 'row', name: 'row', className: 'hidden col-md-1 text-center', searchable: false, 	sortable: true},
	            {data: 'product', 	name: 'product', className: ' text-left', 	searchable: true,	sortable: true},
	            {data: 'ending', 	name: 'ending', className: 'col-md-2 text-left text-center', 	searchable: true,	sortable: true},
	            {data: 'beginning', 	name: 'beginning', className: 'col-md-2 text-left text-center', 	searchable: true,	sortable: true},
	            {data: 'date', 	name: 'date', className: 'col-md-2 text-left', 	searchable: true,	sortable: true},
	        ]
		});
		
		$('#inventory-histories-table').DataTable({
		    bProcessing: true,
		    bServerSide: false,
		    sServerMethod: "GET",
			'ajax': '/get-inventory-histories',
			searching: false, 
			paging: true, 
			filtering:false, 
			responsive: true,
			bInfo: false,
			language:{
				"paginate": {
			        "next":       "<i class='fa fa-chevron-right'></i>",
			        "previous":   "<i class='fa fa-chevron-left'></i>"
			    }
			},
        	"order": [[ 4, "desc" ]],
			"columns": [
	            {data: 'row', name: 'row', className: 'col-md-1 text-center', searchable: false, 	sortable: true},
	            {data: 'product', 	name: 'product', className: ' text-left', 	searchable: true,	sortable: true},
	            {data: 'qty', 	name: 'qt', className: ' text-left', 	searchable: true,	sortable: true},
	            {data: 'remarks', 	name: 'remarks', className: ' text-left', 	searchable: true,	sortable: true},
	            {data: 'date', 	name: 'date', className: ' text-left', 	searchable: true,	sortable: true},
	        ]
		});

		$(".addStockBtn").click(function(x){ 
			var that = this;
			$("#addstockmodal").modal();
			$.ajax({
				url: '/inventories/create',					
				success: function(data) {
					$("#addstockmodal").html(data);
				}
			});	
		});
		
	});
	
</script>

<style type="text/css">
	
	tbody > tr:hover{
		cursor: pointer;
	}
	.dataTables_filter{
	  display: none;
	}
	.search_bar_{
		display: block;
	}
	.ppi_{
		display: none;
	}
	@media (max-width: 768px){
 
		.p15{
			padding: 5px !important;
			padding-left: 5px !important;
		    margin-right: 0px;
		    margin-left: 0px;
		}
		.s_bar_{
			float: right;
		    margin-right: 5px;
		    margin-top: 0px;
		}
		.search_bar_ {
			width: 100%;
		} 
		.ppi_{
			display: block;
		}
	}
</style>