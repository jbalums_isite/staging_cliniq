<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="pragma" content="no-cache" />
    <link rel="shortcut icon" href="/tmpl/images/cliniq-logo.png">

    <title>Cliniq | Admin</title>

    <!-- Bootstrap -->
    {!! Html::style(url('dst/vendors/bootstrap/dist/css/bootstrap.min.css')) !!}
    <!-- Font Awesome -->
    {!! Html::style(url('dst/vendors/font-awesome/css/font-awesome.min.css')) !!}
    <!-- NProgress -->
    {!! Html::style(url('dst/vendors/nprogress/nprogress.css')) !!}

    <!-- Custom Theme Style -->
    {!! Html::style(url('dst/build/css/custom.css')) !!} 
    <style type="text/css">
      .wizard_horizontal ul.wizard_steps li a.done:before, .wizard_horizontal ul.wizard_steps li a.done .step_no {
          background: #00b3ff;
          color: #fff;
      }
      h5{
        background: #516699;
        color: #fff;
        padding: 10px;
      }
      .x_title h2 { 
          width: 100%;
      }
      .form-group{
        padding-left: 25px;
        padding-right: 25px;
      }

      .form-control-feedback.left {
          border-right: 1px solid #ccc;
          left: 25px;
      }
      .actionBar {
          width: 100%;
          border-top: 1px solid #ddd;
          padding: 10px 5px;
          text-align: right;
          margin-top: 10px;
          float: left;
      }
    </style>
  </head>

  <body class="nav-md" style="background: #34495e;">
    
        <!-- page content -->
        <div class="col-md-12 col-sm-12 col-xs-12" role="main" style="    width: 100% !important;">
          <div class="">

            <div class="row" style="padding-top: 15px;">

              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2> <i class="fa fa-cogs"></i> Clinic Account Set-up</h2> 
                    <div class="clearfix"></div>
                  </div>  
                  <div class="x_content">

                    <div id="wizard" class="form_wizard wizard_horizontal">
                      <ul class="wizard_steps" style="padding-left: 0px;">
                        <li>
                          <a href="#step-1">
                            <span class="step_no">1</span>
                            <span class="step_descr">
                                Step 1<br />
                                <small>Clinic Information</small>
                            </span>
                          </a>
                        </li>
                        <li>
                          <a href="#step-2">
                            <span class="step_no">2</span>
                            <span class="step_descr">
                                Step 2<br />
                                <small>Personal Information</small>
                            </span>
                          </a>
                        </li>
                        <li>
                          <a href="#step-3">
                            <span class="step_no">3</span>
                            <span class="step_descr">
                                Step 3<br />
                                <small>Address Information</small>
                            </span>
                          </a>
                        </li>
                        <li>
                          <a href="#step-4">
                            <span class="step_no">4</span>
                            <span class="step_descr">
                                Step 4<br />
                                <small>Credentials</small>
                            </span>
                          </a>
                        </li>
                      </ul>
                      <div id="step-1">
                          
                           
                          <form class="form-horizontal form-label-left col-md-10 col-xs-12 col-md-offset-1 " id="form_1_" method="post">
                            
                            <h5>Clinic Information</h5>
                            {{ csrf_field() }}
                            <div class="col-md-12 ">
                              <div class="row">
                                <div class="form-group col-md-12 col-xs-12">
                                  <label for="clinic_name">Clinic Name</label>
                                  <input id="clinic_name" name="clinic_name" type="text" class="form-control" placeholder="Clinic Name" required>
                                </div>
                              </div>
                              
                              <div class="row">
                                <div class="form-group col-md-12 col-xs-12">
                                  <label for="clinic_type">Clinic Type</label>
                                  <input id="clinic_type" name="clinic_type" type="text" class="form-control" placeholder="Clinic Type" required>
                                </div>
                              </div>
                              
                              <div class="row">
                                <div class="form-group col-md-12 col-xs-12">
                                  <label for="prefix">Clinic Prefix</label>
                                  <input id="prefix" name="prefix" type="text" class="form-control" placeholder="Clinic Prefix" required>
                                </div>
                              </div>

                            </div>

                          </form>

                      </div>
                      <div id="step-2"> 

                        <form class="form-horizontal form-label-left col-md-10 col-md-offset-1" id="form_2_">

                          <div class="col-md-12" >

                            <h5>Personal Information</h5>
                            <div class="row">
                              <div class="form-group col-md-6 col-xs-12">
                                <label for="title">Title</label>
                                <input id="title" name="title" type="text" class="form-control" placeholder="Title" required>
                              </div>
                            </div>
                            <div class="row">
                              <div class="form-group col-md-6 col-xs-12">
                                <label for="firstname">Firstname</label>
                                <input id="firstname" name="firstname" type="text" class="form-control" placeholder="Firstname" required>
                              </div>
                              <div class="form-group col-md-6 col-xs-12">
                                <label for="lastname">Lastname</label>
                                <input id="lastname" name="lastname" type="text" class="form-control" placeholder="Lastname" required>
                              </div>
                            </div>
                            <div class="row">
                              <div class="form-group col-md-6 col-xs-12">
                                <label for="middlename">Middlename</label>
                                <input id="middlename" name="middlename" type="text" class="form-control" placeholder="Middlename" required>
                              </div>
                            </div>
                            <div class="row mt15">
                              
                              <div class="col-md-6 xdisplay_inputx form-group has-feedback">
                                <label for="birthdate">Birthdate</label>
                                  <input name="birthdate" type="text" class="form-control has-feedback-left" id="single_cal4" placeholder="Birthdate" aria-describedby="">
                                  <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                              </div>
                              <div class="form-group col-md-6 col-xs-12">
                                <label for="birthplace">Birthplace</label>
                                <input id="birthplace" name="birthplace" type="text" class="form-control" placeholder="Birthplace" required>
                              </div>
                            </div>

                            <div class="row mt15">
                              <div class="form-group col-md-6 col-xs-12">
                                <label for="gender">Gender</label>
                                <select name="gender" id="gender" class="form-control">
                                  <option disabled selected>Select Gender</option>
                                  <option value="male">Male</option>
                                  <option value="female">Female</option>
                                </select>
                              </div>
                              <div class="form-group col-md-6 col-xs-12">
                                <label for="civil_status">Civil Status</label>
                                <select name="civil_status" id="civil_status" class="form-control">
                                  <option disabled selected>Select Civil Status</option>
                                  <option value="single">Single</option>
                                  <option value="married">Married</option>
                                  <option value="widowed">Widowed</option>
                                  <option value="divorced">Divorced</option>
                                </select>
                              </div>
                            </div>

                            <div class="row mt15">
                              <div class="form-group col-md-6 col-xs-12">
                                <label for="nationality">Nationality</label>
                                <input id="nationality" name="nationality" type="text" class="form-control" placeholder="Nationality" required>
                              </div>
                            </div>


                           


                          </div>
                        </form>
                        <div style="width: 100%;">&nbsp;</div>
                      </div>
                      <div id="step-3">                          
                          <form class="form-horizontal form-label-left col-md-10 col-md-offset-1" id="form_3_">
                            
                            <div class="col-md-12 ">
                              <h5>Contact Information</h5>
                              <div class="row">
                                <div class="form-group col-md-6 col-xs-12">
                                  <label for="mobile">Mobile No.</label>
                                  <input id="mobile" name="mobile" type="number" class="form-control" placeholder="Mobile Number" required>
                                </div>
                                <div class="form-group col-md-6 col-xs-12">
                                  <label for="tel">Landline No.</label>
                                  <input id="tel" name="tel" type="text" class="form-control" placeholder="Landline Number" required>
                                </div>
                              </div> 
                            </div>
                            

                              <div class="col-md-12 ">
                              <h5>Address Information</h5>
                                <div class="row">
                                  <div class="form-group col-md-6 col-xs-12">
                                    <label for="province">Province</label>
                                    <input id="province" name="province" type="text" class="form-control" placeholder="Province" required>
                                  </div>
                                  <div class="form-group col-md-6 col-xs-12">
                                    <label for="city">City</label>
                                    <input id="city" name="city" type="text" class="form-control" placeholder="City" required>
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="form-group col-md-6 col-xs-12">
                                    <label for="street">Street</label>
                                    <input id="street" name="street" type="text" class="form-control" placeholder="Street" required>
                                  </div>
                                </div>
                              </div>
                          </form>
                      </div>
                      <div id="step-4">   
                             
                          <form class="form-horizontal form-label-left col-md-10 col-md-offset-1" id="form_4_">


                            <div class="alert alert-success scss" role="alert" style="display: none;"><b>Well done!</b> Your Good to go in <b><i id="time">00:05</i></b>s</div> 


                            <div class="col-md-12 ">
                              <h5 class="StepTitle">Credential Information</h5>
                      
                              <input type="hidden" name="type" value="doctor">
                                <div class="row">
                                  <div class="form-group col-md-6 col-xs-12">
                                    <label for="email">Email</label>
                                    <input id="email" name="email" type="email" class="form-control" placeholder="Email" required>
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="form-group col-md-6 col-xs-12">
                                    <label for="password">Password</label>
                                    <input id="password" name="password" type="password" class="form-control" placeholder="Password" required>
                                  </div>
                                  <div class="form-group col-md-6 col-xs-12">
                                    <label for="confirm_password">Confirm Password</label>
                                    <input id="confirm_password" name="confirm_password" type="password" class="form-control" placeholder="Confirm Password" required>
                                  </div>     
                                </div>
                            </div>
                          </form>
                      </div>

                    </div>
                    <!-- End SmartWizard Content -->


                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
    </div>

    <!-- bootstrap-daterangepicker -->
    {!! Html::style(url('dst/vendors/bootstrap-daterangepicker/daterangepicker.css')) !!}
    <!-- jQuery -->
    {!! Html::script(url('dst/vendors/jquery/dist/jquery.min.js')) !!}
    <!-- Bootstrap -->
    {!! Html::script(url('dst/vendors/bootstrap/dist/js/bootstrap.min.js')) !!}
    <!-- FastClick -->
    {!! Html::script(url('dst/vendors/fastclick/lib/fastclick.js')) !!}
    <!-- NProgress -->
    {!! Html::script(url('dst/vendors/nprogress/nprogress.js')) !!}
    <!-- jQuery Smart Wizard -->
    {!! Html::script(url('dst/vendors/jQuery-Smart-Wizard/js/jquery.smartWizard.js')) !!} 
    <!-- Custom Theme Scripts -->
    {!! Html::script(url('dst/build/js/custom.js')) !!}

    <!-- bootstrap-daterangepicker -->
    {!! Html::script(url('dst/vendors/moment/min/moment.min.js')) !!}
    {!! Html::script(url('dst/vendors/bootstrap-daterangepicker/daterangepicker.js')) !!}

    <!-- Laravel Javascript Validation -->
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>

    {!! JsValidator::formRequest('App\Http\Requests\AccountSetUpRequest', '#form_1_') !!}
    {!! JsValidator::formRequest('App\Http\Requests\AccountSetUpRequest', '#form_2_') !!}
    {!! JsValidator::formRequest('App\Http\Requests\AccountSetUpRequest', '#form_3_') !!}
    {!! JsValidator::formRequest('App\Http\Requests\AccountSetUpRequest', '#form_4_') !!}
     

    <!-- jQuery Smart Wizard -->
    <script>
      $(document).ready(function() {
        $(".buttonNext").on('click', function(e){
          e.preventDefault();
          return;
        });
        var form_val_ = "";
        $('#wizard').smartWizard({ 
          onLeaveStep:leaveAStepCallback,
          onFinish:onFinishCallback
        });

         function leaveAStepCallback(obj, context){ 
              return validateSteps(context.fromStep); // return false to stay on step and true to continue navigation 
          }

          function onFinishCallback(objs, context){
              if(validateAllSteps()){
                 $.ajax({
                      type: 'POST',
                      url: '/set-up',
                      data: $("#form_1_").serialize()+'&'+$("#form_2_").serialize()+'&'+$("#form_3_").serialize()+'&'+$("#form_4_").serialize(),
                      dataType: 'json',
                      success: function(data){
                        $('.scss').fadeIn();
                        var fiveMinutes = 5,
                            display = $('#time');
                        startTimer(fiveMinutes, display);
                        var url_ = "{{url('/')}}";
                        setTimeout(function() { 
                          window.location = url_+"/"+$("[name='prefix']").val(); 
                        }, 5000);
                        
                      }
                  });
                 // alert("All Good! Ready to go, this is for test only...");
              }
          }

          // Your Step validation logic
          function validateSteps(stepnumber){
              var isStepValid = true;
              // validate step 1
              if(stepnumber == 1){
                  return $("#form_1_").valid();
              }else if(stepnumber == 2){
                  return $("#form_2_").valid();
              }else if(stepnumber == 3){
                  return $("#form_3_").valid();
              }else if(stepnumber == 4){
                  return $("#form_4_").valid();
              } 
          }
          function validateAllSteps(){
              var isStepValid = false;
              if($("#form_1_").valid() && $("#form_2_").valid() && $("#form_3_").valid() && $("#form_4_").valid()){
                isStepValid = true;
              }
              // all step validation logic     
              return isStepValid;
          }         
        $('#wizard_verticle').smartWizard({
          transitionEffect: 'slide'
        });
        $('.buttonNext').addClass('btn btn-success');
        $('.buttonPrevious').addClass('btn btn-primary');
        $('.buttonFinish').addClass('btn btn-default');

        $('#single_cal4').daterangepicker({
          singleDatePicker: true,
          singleClasses: "picker_4"
        }, function(start, end, label) {
          
        });

      });


      function startTimer(duration, display) {
        var timer = duration, minutes, seconds;
        setInterval(function () {
            minutes = parseInt(timer / 60, 10)
            seconds = parseInt(timer % 60, 10);

            minutes = minutes < 10 ? "0" + minutes : minutes;
            seconds = seconds < 10 ? "0" + seconds : seconds;

            display.text(minutes + ":" + seconds);

            if (--timer < 0) {
                timer = duration;
            }
        }, 1000); 
      }
 

    </script>
 
  </body>
</html>