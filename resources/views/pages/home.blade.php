@extends('includes.app')

@section('content')

    <div class="header  ">
        <div class="container"> 
            <div class="header_right header_left">
                <ul>
                    <li>(63) 38 501 7603</li>
                    <li>info@isitesolution.com</li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <!-- //header -->
    <div class="header-bottom "> 
                <div class="col-xs-6 logo-area">
                    <img src="tmpl/images/logo_nw.png">
                </div>
                <div class="col-xs-6 logo-area2"> 
                    <img src="tmpl/images/iSiteSolution Consultancy HD Logo.png">
                </div> 
        </div>

<!-- banner -->
<div class="banner">

                <script>
                        // You can also use "$(window).load(function() {"
                        $(function () {
                         // Slideshow 4
                        $("#slider3").responsiveSlides({
                            auto: true,
                            pager: true,
                            nav: false,
                            speed: 500,
                            timeout: 5000,
                            namespace: "callbacks",
                            before: function () {
                        $('.events').append("<li>before event fired.</li>");
                        },
                        after: function () {
                            $('.events').append("<li>after event fired.</li>");
                            }
                            });
                        });
                </script>
        <div  id="top" class="callbacks_container">
            <ul class="rslides" id="slider3">
                <li>
                    <div class="banner1">
                        <div class="container features_">
                            <div class="banner-info">
                                <h3 class="upper_h"><span>FEATURES</span></h3>
                                <h3 class="lower_h">Patient <span>Information System</span></h3>
                                <p>In this section Secretary will input the patient information such as
                                    (Patient’s general information, Allergies, Medical Histories, Laboratories, Test Result, and
                                    Medication. The Doctor can also add notes for the patient on this section.</p> 
                            </div>
                        </div>
                    </div>
                </li> 
                <li>
                    <div class="banner2">
                        <div class="container features_">
                            <div class="banner-info">
                                <h3 class="upper_h"><span>FEATURES</span></h3>
                                <h3 class="lower_h">Inventory <span> System</span></h3>
                                <p>This module is the complete list of products the clinic selling. The stocks
                                    monitoring and each details.</p> 
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="banner1">
                        <div class="container features_">
                            <div class="banner-info">
                                <h3 class="upper_h"><span>FEATURES</span></h3>
                                <h3 class="lower_h">Scheduling <span> System</span></h3>
                                <p> Calendar view for Doctor’s appointment on patient’s schedule and
                                    reservation. It may consultation or any clinic services offered.</p> 
                            </div>
                        </div>
                    </div>
                </li> 
                <li>
                    <div class="banner2">
                        <div class="container features_">
                            <div class="banner-info">
                                <h3 class="upper_h"><span>FEATURES</span></h3>
                                <h3 class="lower_h">Billing <span> System</span></h3>
                                <p> Process bills in its transaction and product sold</p> 
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="banner1">
                        <div class="container features_">
                            <div class="banner-info">
                                <h3 class="upper_h"><span>FEATURES</span></h3>
                                <h3 class="lower_h">Reports <span></span></h3>
                                <p> This will show the summary report for services and products transaction inside the
                                    clinic. Summary Report for Daily, Weekly, Monthly, and Annual clinic transaction. And will only
                                    be access by the Doctor.</p> 
                            </div>
                        </div>
                    </div>
                </li> 
                <li>
                    <div class="banner2">
                        <div class="container features_">
                            <div class="banner-info">
                                <h3 class="upper_h"><span>FEATURES</span></h3>
                                <h3 class="lower_h">Doctor’s <span>Form</span></h3>
                                <p>This will be access by both secretary and Doctor for referral purposes,
doctor’s request purposes and other doctor’s documents issue. </p> 
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
        <div class="clearfix"></div>
</div>
<div class="our-clients-side">
    OUR CLIENT
</div>
<div class="our-clients-side-list">
    <div class="col-md-12">
        <a href="{{url('/demo')}}" >
            <div class="col-sm-4" >
                <img src="tmpl/images/female_doctor.png" href="{{url('/demo')}}" height="120px" width="120px">
                <a href="{{url('/login/demo')}}">Demo Clinic</a>
            </div>
        </a>
        <a href="#" target="">
            <div class="col-sm-4" >
                <img src="tmpl/images/male_doctor.jpg" href="{{url('/demo')}}" height="120px" width="120px">
                <a href="">Demo Clinic 2</a>
            </div>
        </a>
        <a href="" target="">
            <div class="col-sm-4" >
                <img src="tmpl/images/female_doctor.png" href="{{url('/demo')}}" height="120px" width="120px">
                <a href="">Demo Clinic 3</a>
            </div>
        </a>
    </div>
</div>
<div class="contact-us-side ">
    CONTACT US
</div>
<div class="contact-us-side-form text-center ">
    <h3 class="text-left">CONTACT US <i class="fa fa-envelope fa-fa2x" style="position: relative;
    right: -40%;"></i></h3>
    <form>
        <p>Fill in the form below to send us a message</p>
        <div style="width: 85%;">
            <div class="form-group">
                <input type="text" class="form-control" name="name" placeholder="Full Name (Required)">
            </div>
        </div>
        <div style="width: 85%;">
            <div class="form-group">
                <input type="email" class="form-control" name="email" placeholder="Email (Required)">
            </div>
        </div>
        <div style="width: 85%;">
            <div class="form-group">
                <input type="text" class="form-control" name="subject" placeholder="Subject (Required)">
            </div>
        </div>
        <div style="width: 85%;">
            <div class="form-group">
                <textarea class="form-control" name="subject" placeholder="Message" rows="4"></textarea>
            </div>
        </div>
        <div style="width: 85%;">
            <button class="btn btn-success btn-block">Send Message</button>
        </div>
    </form>
</div>
<!-- //banner -->

    <!-- services 
    <div class="services" id="our-clients">
        <div class="container">
            <h3 class="c-blue text-center oc">Our Clients</h3>
            
            <div class="col-md-2 our-clients text-center wow bounceInDown animated c-black" data-wow-duration="1s" data-wow-delay="0s" style="visibility: visible; animation-duration: 1.5s; animation-delay: 0s; animation-name: bounceInUp;">
                <a href="/login/1" target="_blank">
                    <img src="/dst/images/user.png">
                    <h5 class="c-blue">Dr. John Doe</h5>
                </a>
            </div>-->
            <!-- <div class="col-md-2 our-clients text-center">
                <a href="/mejia">
                    <img src="/tmpl/images/mejia.png">
                    <h5 class="c-blue">Dr. Athena Mejia</h5>
                </a>
            </div>
            <div class="col-md-2 our-clients text-center">
                <a href="/mejia">
                    <img src="/tmpl/images/mejia.png">
                    <h5 class="c-blue">Dr. Athena Mejia</h5>
                </a>
            </div>
            <div class="col-md-2 our-clients text-center">
                <a href="/mejia">
                    <img src="/tmpl/images/mejia.png">
                    <h5 class="c-blue">Dr. Athena Mejia</h5>
                </a>
            </div>
            <div class="col-md-2 our-clients text-center">
                <a href="/mejia">
                    <img src="/tmpl/images/mejia.png">
                    <h5 class="c-blue">Dr. Athena Mejia</h5>
                </a>
            </div>
            <div class="col-md-2 our-clients text-center">
                <a href="/mejia">
                    <img src="/tmpl/images/mejia.png">
                    <h5 class="c-blue">Dr. Athena Mejia</h5>
                </a>
            </div> 
        </div>
    </div>-->
    <div class="container hidden" id="contact-us">
            <div id="book" class="col-lg-8 col-md-offset-2 content_middle wow flipInY" data-wow-duration="1.5s" data-wow-delay="0.3s">
                <h3>Contact Us</h3>
                <form action="#" method="post">
                    <input type="text" name="Name" placeholder="Your Name (Required)" required>
                    <input type="text" name="Email" placeholder="Email Address (Required)" required>
                    <input type="text" name="Subject" placeholder="Subject (Required)" required>
                    <textarea rows="4" class="form-control">Message</textarea>
                    <br>
                    <button type="submit" class="btn btn-primary btn-block">Submit</button>
                </form>
            </div>
        </div>
    <!-- //services -->

    <style type="text/css">
                
        @media (max-width: 715px) {
        /*    .contact-us-side, .our-clients-side{
                display: none;
            }*/
        }

        .our-clients-side-list{
            overflow: auto;
        }
    </style>
@endsection
