<div class="modal-dialog modal-md">
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button>
      <h4 class="modal-title">Edit Medication Form</h4>
    </div>
    {!! Form::open(array('url' => url('/medications/'.$medication->id), 'method' => 'PATCH', 'id' => 'add-medication-form')) !!}
    <div class="modal-body">
      <input type="hidden" name="patient_id" value="{{ $patient_id }}"> 
      <div class="form-group mb10">
        <label for="date">Date</label>
        <input id="date" name="date" type="text" class="form-control" id="single_cal4" placeholder="Date" value="{{ $medication->date }}" required>
      </div>
      <div class="form-group mb10 mt10">
        <label>Medication &nbsp;<a href="#" class="pull-right  addnewrow"  data-type="test"><i class=" c-orange glyphicon glyphicon-remove-circle trans-rt-45"></i></a></label>
        <table class="table table-bordered" id="medicines_table">
          <thead>
            <th class="col-md-5">Medicine</th>
            <th class="col-md-7">Description</th>
            <th></th>
          </thead>
          <tbody>
            @php 
              $meds_descs = \App\MedicationDescriptions::where('medication_id', $medication->id)->get();
            @endphp
            @foreach($meds_descs as $meds)

              <tr>
                <td>
                  {!! Form::select("medicine_id[]", $medicines, $meds->medicine->id,["id"=>"medicines","class"=> "form-control medications_select", "placeholder"=>"Select Medicine"]) !!}
                </td>
                <td> 
                  <textarea class="form-control" name="description[]" rows="3" placeholder="Description">{{$meds->description}}</textarea>
                </td>
                <td>
                  <span class="badge badge_ delrow"><i class="fa fa-trash"></i></span>
                </td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
      
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        {!! Form::submit('Submit', ['class' => 'btn submit-btn btn-primary btn-gradient pull-right']) !!}
      {!! Form::close() !!}
    </div>

  </div>
</div>


 <!-- Laravel Javascript Validation -->
 <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>

 {!! JsValidator::formRequest('App\Http\Requests\AddMedicationRequest', '#add-medication-form') !!}

<script type="text/javascript">
  $(function(){
       
      $('[name="date"]').datepicker({
        format: "yyyy-mm-dd",
        autoclose: true, 
      }).on('changeDate', function(selected){
       
      }); 
     var str ='<tr>'+
                '<td>'+
                  '{!! Form::select("medicine_id[]", $medicines, null,["id"=>"medicines","class"=> "form-control medications_select", "placeholder"=>"Select Medicine"]) !!}'+
                '</td>'+
                '<td>'+
                  '<textarea class="form-control" name="description[]" rows="3" placeholder="Description"></textarea>'+
                '</td>'+
                '<td>'+
                  '<span class="badge badge_ delrow"><i class="fa fa-trash"></i></span>'+
                '</td>'+
              '</tr>';
      //$("#medicines_table tbody").append(str);
      $("#add-medication-form").on('submit', function(e){
          e.preventDefault();
          var that = this;
          if($("#add-medication-form").valid()){
            $(".submit-btn").addClass("disabled");
            $('button[type=submit], input[type=submit]').prop('disabled',true);
            $.ajax({
                type: 'PATCH',
                url: '/medications/'+{{$medication->id}},
                data: $('#add-medication-form').serialize(),
                dataType: 'json',
                success: function(data){
                  $('button[type=submit], input[type=submit]').prop('disabled',false);
                  $(".submit-btn").removeClass("disabled");
                }
            }).done(function(data) {
              if(data.success){
                
                //$("#medications-table").DataTable().ajax.url( '/get-meds/'+$("[name=patient_id]").val() ).load();
                notiff('Success!', data.msg,'success'); //title, msg, type
                $('#editmedsmodal').modal('toggle');
                getMedications('.medications_list');
              }else{
                notiff('Error!', data.msg,'error'); //title, msg, type
               // $('#addmedsmodal').modal('toggle');
              }
            }).error(function(data) {
                notiff('Error!', data.msg, 'warning'); //title, msg, type
               // $('#addmedsmodal').modal('toggle');
            });
          }
        });
      $(".addnewrow").click(function(x){
        x.preventDefault();
        $("#medicines_table tbody").append(str);
      }); 

      $(document).on('click', '.delrow', function(x){
        x.preventDefault();
        if($("#medicines_table  tbody > tr").length > 1){
          $(this).parent().parent().remove();
        }
      });
      $(document).on('change', '.medications_select', function(){
      //$(".medications_select").on('change', function(){
            var that = this; 
            $.ajax({
                type: 'get',
                url: '/get-meds-desc',
                data:  'id='+$(that).val(),
                dataType: 'json',
                success: function(data){
                  $(that).parent().next().children().html(data.data.description)
                }
            });
      });
  });  
 </script>
 <style type="text/css">
   .badge_{
    /*  position: absolute;
      right: 1px;*/
      padding: 7px;
  /*    border-radius: 50%;*/
 /*     background: transparent;*/
      color: red;
      margin-top: 0px;
      font-size: 15px;
      transition: .2s ease-in-out;
      background: rgba(255, 186, 186, 0.25);
   }
   .badge_:hover{
    cursor: pointer;
    background: red;
    color:white;
   }
 </style>