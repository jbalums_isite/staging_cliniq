<!DOCTYPE html>
<html>
    <head>
        <title>Page Not Found!</title>

        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                color: #B0BEC5;
                display: table;
                font-weight: 100;
                font-family: 'Lato', sans-serif;
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 72px;
                margin-bottom: 40px;
			    color: black;
			    font-weight: bold;
            }
            .btn{    text-decoration: none;
    color: #ffffff;
    font-size: 20px;
    font-weight: bold;
    padding: 15px;
    background: rgb(0, 121, 21);
    border-radius: 5px;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="content">
                <div class="title">404 Page Not Found!</div>
                <a href="/" class="btn">Go back to dashboard</a>
            </div>
        </div>
    </body>
</html>
