@include('not_supported')
<!-- top action nav -->
<div class="top_nav" style="min-height: 37px;margin-bottom: 0px;background: #ededed;    border-bottom: 0px;">
  <div class="nav_menu"  style="min-height: 37px;margin-bottom: 10px;background: #ededed;margin-top: -10px;    border-bottom: 0px;">
    <nav> 

  <!--     <div class="col-md-6">
      	<ul class="nav navbar-nav navbar-left">
	        <li class="">
	      		<input type="text" name="search" placeholder="Search..." id="manufacturers_search">
	      		<i class="fa fa-search"></i>
	        </li>
	      </ul>
      </div>
      <div class="col-md-6 pull-right">
      	<a href="#" class="dropdown-toggle pull-right action_btns hidden" data-toggle="dropdown" role="button" aria-expanded="false"><h5>Active <i class=" c-orange glyphicon glyphicon glyphicon-chevron-down"></i></h5></a>
	        <ul class="dropdown-menu" role="menu">
	          <li><a href="#">Active</a>
	          </li>
	          <li><a href="#">Inactive</a>
	          </li>
	        </ul>
      	<a href="#" class="pull-right action_btns addBtn"><h5>Add Manufacturer <i class=" c-orange glyphicon glyphicon-remove-circle trans-rt-45"></i></h5></a>
      	<a href="#" class="pull-right action_btns addTransBtn"><h5>Add Transaction <i class=" c-orange glyphicon glyphicon-remove-circle trans-rt-45"></i></h5></a>
      </div>
 -->

      <div class="col-md-6 search_bar_">
      		<div class="btn-group action_group"> 
	          <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
	            <i class="fa fa-bars"></i>
	            <span class="sr-only">Toggle Dropdown</span>
	          </button>
	          <ul class="dropdown-menu dropdown-menu-left" role="menu">
	            <li> 
      				<a href="#" class="pull-right action_btns addBtn"><h5>Add Manufacturer <i class=" c-orange glyphicon glyphicon-remove-circle trans-rt-45"></i></h5></a>
	            </li>
	            <li>
	            	<a href="#" class="pull-right action_btns addTransBtn"><h5>Add Transaction <i class=" c-orange glyphicon glyphicon-remove-circle trans-rt-45"></i></h5></a>
	            </li>  
	          </ul>
	        </div>
	      <ul class="nav navbar-nav navbar-left s_bar_">
	        <li class="">
	      		<input type="text" name="search" placeholder="Search Manufacturer..." id="manufacturers_search">
	      		<i class="fa fa-search"></i>
	        </li>
	      </ul>

      </div>

      <div class="col-md-6 pull-right"> 
      	<a href="#" class="pull-right action_btns addBtn hide_md"><h5>Add Manufacturer <i class=" c-orange glyphicon glyphicon-remove-circle trans-rt-45"></i></h5></a>
      	<a href="#" class="pull-right action_btns addTransBtn hide_md"><h5>Add Transaction <i class=" c-orange glyphicon glyphicon-remove-circle trans-rt-45"></i></h5></a>
      </div>



    </nav>
  </div>
</div>
<!-- /top action nav -->

<div class="right_col p15 pt30" role="main">
	
	<div class="row p15 mt50  "> 
			<table class="table jambo_table theme-table theme-table-edit  dt-responsive nowrap" id="manufacturers-table" width="100%">
				<thead>
					<tr>
						<th>#</th>
						<th>Name</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table> 
	</div>

</div>

<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;" id="addmodal"></div>
<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;" id="editmodal"></div>

<script type="text/javascript">
	$(function(){

		$('#manufacturers-table').DataTable({
		    bProcessing: true,
		    bServerSide: false,
		    sServerMethod: "GET",
			'ajax': '/get-manufacturers',
			searching: true, 
			paging: true, 
			filtering:true, 
			bInfo: false,
			language:{
				"paginate": {
			        "next":       "<i class='fa fa-chevron-right'></i>",
			        "previous":   "<i class='fa fa-chevron-left'></i>"
			    }
			},
			"columns": [
	            {data: 'id', name: 'id', className: 'col-md-1 text-center', searchable: false, 	sortable: true},
	            {data: 'name', 	name: 'name', className: 'col-md-11 text-left', 	searchable: true,	sortable: true},
	            {data: 'actions', 	name: 'actions', className: ' text-left', 	searchable: false,	sortable: false},
	        ]
		});

		$(".addBtn").click(function(x){ 
			var that = this;
			$("#addmodal").modal();
			$.ajax({
				url: '/manufacturers/create',					
				success: function(data) {
					$("#addmodal").html(data);
				}
			});	
		});
		$(document).off('click', ".editBtn").on('click', ".editBtn", function(x){ 
			var that = this;
			$("#editmodal").modal();
			$.ajax({
				url: '/manufacturers/'+that.dataset.id+'/edit',					
				success: function(data) {
					$("#editmodal").html(data);
				}
			});	
		});
		
		var p_table = $('#manufacturers-table').DataTable(); 
		$('#manufacturers_search').keyup(function(){ 
		    p_table.search($(this).val()).draw();
		})
	});
	
</script>
<style type="text/css">
	tbody > tr:hover{
		cursor: pointer;
	}
	.dataTables_filter{
	  display: none;
	}
	.search_bar_{
		display: block;
	}
	.ppi_{
		display: none;
	}
	@media (max-width: 768px){
 
		.p15{
			padding: 5px !important;
			padding-left: 5px !important;
		    margin-right: 0px;
		    margin-left: 0px;
		}
		.s_bar_{
			float: right;
		    margin-right: 5px;
		    margin-top: 0px;
		}
		.search_bar_ {
			width: 100%;
		} 
		.ppi_{
			display: block;
		}
	}
</style>