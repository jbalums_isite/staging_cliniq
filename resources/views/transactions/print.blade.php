@extends('includes.admin-page.app')

@section('content')

<!-- top action nav -->
<div class="top_nav" style="height: 0px;margin-bottom: 0px;background: #ededed;    border-bottom: 0px;">
  <div class="nav_menu"  style="height: 37px;margin-bottom: 10px;background: #ededed;margin-top: -10px;    border-bottom: 0px;">
    <nav> 

      <ul class="nav navbar-nav navbar-left">
        <li class="hidden">
      		<input type="text" name="search" placeholder="Search..." id="patient_search">
      		<i class="fa fa-search"></i>
        </li>
      </ul>
      <div class="col-xs-4 pull-right">
      </div>
    </nav>
  </div>
</div>
<!-- /top action nav -->

<div class="right_col p15 pt30" role="main">
	
    <a href="#" class="btn btn-primary pull-right print_btn"><i class="fa fa-print"></i> Print Invoice</a>
	<div class="row p15 printable">
		<div class="row  ">
	        <div class="col-sm-4 " > 
	          From&nbsp;
	          <address>
	          	  @php 	
	          	  	$clinic = \App\ClinicAccounts::where('id', Auth::user()->clinic_id)->get();
	          	  	$user = \App\Users::where('id', Auth::user()->id)->get();
                  @endphp
                  <strong>{{$clinic[0]->clinic_name}}</strong> 
                  <br> {{ $user[0]->email }}
                  <br> {{ chunk_split($user[0]->people->contact->mobile, 4, ' ') }}
              </address>
	        </div>
	        <!-- /.col -->
	        <div class="col-sm-4 ">
	          To&nbsp;
	          <address>
	          		@php 
	          			if($master[0]->appointment_id == 0){
	          				$n = $master[0]->customer_name; 
	          				$c_ = '';
	          				$e_ = '';
	          			}else{
	          				$n = $master[0]->appointment->patient->user->people->firstname." ".$master[0]->appointment->patient->user->people->lastname;
	          				$c_ = $master[0]->appointment->patient->user->people->contact->mobile;
	          				$e_ = $master[0]->appointment->patient->user->email;
	          			}
	          			
	          		@endphp
                  <strong>{{ $n }}</strong> 
                  <br> {{$e_}}
                  <br> {{chunk_split($c_, 4, ' ')}}
              </address>
	        </div>
	        <!-- /.col -->
	        <div class="col-sm-4  text-right pull-right">
	          <br><br> 
	          <br> 
	          <b>Payment Due:</b> {{date('m/d/Y', strtotime($master[0]->created_at))}}
	        </div>
	        <!-- /.col -->
	      </div>
		<table class="table jambo_table  " id="transactions-table">
			<thead>
				<tr> 
					<th class="col-md-8">Description</th> 
					<th class="col-md-2 text-right">Unit Cost</th> 
					<th class="col-md-1 text-right">Qty</th>
					<th class="col-md-2 text-right">Amount</th>
				</tr>
			</thead>
			<tbody>
				@php 
					$sub_total = 0.00;
				@endphp
				@foreach($details as $detail)
					@php 
						$sub_total += $detail->amount;
					@endphp
					<tr> 
						<td>
							<b>{{$detail->product->name}}</b><br>
							<small>{{$detail->product->description}}</small>
						</td>
						<td>{{number_format($detail->amount/$detail->qty,2)}}</td>
						<td>{{number_format($detail->qty,2)}}</td>
						<td>{{number_format($detail->amount,2)}}</td>
					</tr>
				@endforeach 
			</tbody>
			<tfoot>
				<tr>
					<th></th>
					<th colspan="2">Subtotal: </th>				
					<th>{{number_format($sub_total,2)}}</th>
				</tr>
			</tfoot>
		</table>
		<div class="col-xs-offset-8 col-xs-4">
  		<b>Amount Due:</b> {{date('m/d/Y', strtotime($master[0]->created_at))}}
          <div class="table-responsive">
            <table class="table total_table">
              <tbody>
                <tr>
                  <th>Subtotal:</th>
                  <td>{{number_format($sub_total,2)}}</td>
                </tr>
                <tr>
                  <th>Discount{{ $master[0]->percentage == 0 ? ':':'('.$master[0]->discount.'%):' }}</th>
                  <td>{{ $master[0]->percentage == 0 ? '-'.number_format($master[0]->discount,2):'-'.number_format(($sub_total*($master[0]->discount/100)) , 2) }}</td>
                </tr> 
                <tr>
                  <th>Total:</th>
                  <td>{{ number_format($sub_total-($sub_total*($master[0]->discount/100)), 2) }}</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
	</div>

</div>


<style type="text/css">
	h3{
		line-height: 1.1em;
		margin: 0px;
	}
	th:nth-child(2),
	th:nth-child(3),
	th:nth-child(4),
	tbody td:nth-child(2),
	tbody td:nth-child(3),
	tbody td:nth-child(4){
		text-align: right;
	}
	thead {
	    background: rgba(255, 255, 255, 0.94);
	    color: #165388;
	    border-top: 4px solid #202a31;
	}
	.main_container{
		background: #fff;
	}

	.total_table tbody td:nth-child(2){
		text-align: right;
	}

	tbody td:nth-child(3){
		width: 100px;
	}
	.nav_menu{
		display: none;
	}
	.left_col{
		display: none !important;
	}
	.right_col{
		float: left;
		width: 100%;
		margin-left: 0px;
	}
	.nav-md .container.body .right_col{
		margin-left: 0px;
	}

	.printable{
		width: 8.5in;
	    background: white;
	    margin: 0 auto;
	    padding-top: 0px;
	}
	@media print{
		.col-sm-4{
			width: 25%;
		}
		
	}
</style>

<style type="text/css" media="print">
	
	.printable{
		margin-top: 0px !important;
		padding-top: 0px !important;
	}
</style> 
@endsection


@section('script_content')

<script type="text/javascript" src="/js/printThis.js"></script>
<script type="text/javascript">
	$(function(){
		$(".print_btn").click(function(){ 
			$(".printable").printThis({
			    debug: false,               
			    importCSS: true,           
			    importStyle: true,         
			    printContainer: true,        
			    pageTitle: "",              
			    removeInline: false,      
			    printDelay: 200,          
			    header: null,        
			    footer: null,             
			    base: true,              
			    formValues: true   ,      
			    canvas: false  ,            
			}); 
		}); 
		window.onafterprint = function(){
			window.close();
			window.top.close();
			console.log('asd');
		}
		
		$(".print_btn").click();
	});
 
	$(window).unload(function(){
	   $('#addtransmodal').modal('toggle');
	});
</script>
@endsection