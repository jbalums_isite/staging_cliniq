@include('not_supported')
<!-- top action nav -->
<div class="top_nav" style="height: 37px;margin-bottom: 0px;background: #ededed;    border-bottom: 0px;">
  <div class="nav_menu"  style="height: 37px;margin-bottom: 10px;background: #ededed;margin-top: -10px;    border-bottom: 0px;">
    <nav> 

      <ul class="nav navbar-nav navbar-left">
        <li class="">
      		<input type="text" name="search" placeholder="Search..." id="patient_search">
      		<i class="fa fa-search"></i>
        </li>
      </ul>
      <div class="col-xs-4 pull-right"> 
      	<a href="#" class="pull-right action_btns addTransBtn"><h5>Add Transaction <i class=" c-orange glyphicon glyphicon-remove-circle trans-rt-45"></i></h5></a>
      </div>
    </nav>
  </div>
</div>
<!-- /top action nav -->

<div class="right_col p15 pt30" role="main">
	
	<div class="row p15 mt50">
		<table class="table jambo_table theme-table theme-table-edit " id="transactions-table">
			<thead>
				<tr>
					<th>#</th>
					<th>Transaction Date</th>
					<th>Patient</th>
					<th>Doctor</th> 
					<th></th>
				</tr>
			</thead>
			<tbody>
				
			</tbody>
		</table>
	</div>

</div>

<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;" id="addmodal"></div>
<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;" id="editmodal"></div>

<script type="text/javascript">
	$(function(){

		$('#transactions-table').DataTable({
		    bProcessing: true,
		    bServerSide: false,
		    sServerMethod: "GET",
			'ajax': '/get-transactions',
			searching: true, 
			paging: true, 
			filtering:false, 
			bInfo: false,
			language:{
				"paginate": {
			        "next":       "<i class='fa fa-chevron-right'></i>",
			        "previous":   "<i class='fa fa-chevron-left'></i>"
			    }
			},
			"columns": [
	          //  {data: 'id', name: 'id', className: 'hidden', searchable: false, 	sortable: true},  
	            {data: 'row', 	name: 'row', className: 'text-left', 	searchable: true,	sortable: true},
	            {data: 'date', 	name: 'date', className: 'col-md-3 text-left', 	searchable: true,	sortable: true}, 
	            {data: 'patient', 	name: 'patient', className: 'col-md-4 text-left', 	searchable: true,	sortable: true}, 
	            {data: 'doctor', 	name: 'doctor', className: 'col-md-3 text-left', 	searchable: true,	sortable: true}, 
	           // {data: 'total', 	name: 'total', className: 'col-md-1 text-left', 	searchable: true,	sortable: true}, 
	            {data: 'actions', 	name: 'actions', className: ' text-center', 	searchable: false,	sortable: false},
	        ]
		});

		$(".addTransBtn").click(function(x){  
			var that = this;
			$("#addmodal").modal();
			$.ajax({
				url: '/transactions/create',					
				success: function(data) {
					$("#addmodal").html(data);
				}
			});	
		});

		var p_table = $('#patients-table').DataTable(); 
		$('#patient_search').keyup(function(){ 
		    p_table.search($(this).val()).draw();
		})

		$(document).on('click', "#patients-table > tbody > tr > td", function(x){ 
			if(!$(this).is( "#patients-table > tbody > tr > td:last-child" )){
				loadPage('/patients/'+$(this).parent().children()[0].innerText);
			}
			//console.log($(this).children()[0].innerText);
			///window.open('http://stackoverflow.com/', '_blank');
		});

		$(document).off('click', ".editBtn").on('click', ".editBtn", function(x){ 
			var that = this;
			$("#editmodal").modal();
			$.ajax({
				url: '/patients/'+that.dataset.id+'/edit',					
				success: function(data) {
					$("#editmodal").html(data);
				}
			});	
		});
	});
	
</script>
<style type="text/css">
	tbody > tr:hover{
		cursor: pointer;
	}
	.dataTables_filter{
	  display: none;
	}
	table{
		width: 100% !important;
	}
</style>