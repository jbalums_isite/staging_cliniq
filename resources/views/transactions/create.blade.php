<div class="modal-dialog modal-lg transaction-form">
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button>
      <h4 class="modal-title">Add Transaction Form</h4>
    </div>
    {!! Form::open(array('url' => url('/transactions'), 'method' => 'POST', 'id' => 'add-trans-form')) !!}
    <div class="modal-body">
      <div class="row"  id="patient_fields1">
        <div class="form-group col-md-5 col-xs-12">
          <label for="name">Patient</label> 
          <select class="form-control" id="appointment_select" name="appointment_id"> 
            <option></option>
            @foreach($appointments as $appointment)
              <option value="{{$appointment->id}}">{{ ucfirst($appointment->patient->user->people->firstname)." ".
                      ucfirst(substr($appointment->patient->user->people->middlename, 0, 1)).". ".
                      ucfirst($appointment->patient->user->people->lastname) }}</option>
            @endforeach            
          </select>
        </div>
        <div class="form-group col-md-4 col-xs-12">
          <label for="name">Date and Time</label>
          <input id="date" type="text" class="form-control" placeholder="Date and Time" required readonly>
        </div> 
      </div>
      <div class="row"   id="patient_fields2">
        <div class="form-group col-md-5  col-xs-12">
          <label for="name">Doctor</label>
          <input id="doctor" type="text" class="form-control" placeholder="Doctor" readonly>
        </div>
      </div>
      <div class="row">
        <div class="form-group col-md-5  col-xs-12 hidden">
          <label for="name">Customer</label>
<!--           <input id="customer" type="text" class="form-control" placeholder="Customer Name" name="customer_name"> -->
          @php
            $patients = \App\Patients::all();
          @endphp
          <select  id="customer" type="text" class="form-control" placeholder="Customer Name" name="customer_name">
            <option></option>
            @foreach($patients as $patient)
              <option value='{{ ucfirst($patient->user->people->firstname)." ".
                      ucfirst(substr($patient->user->people->middlename, 0, 1)).". ".
                      ucfirst($patient->user->people->lastname) }}'>{{ ucfirst($patient->user->people->firstname)." ".
                      ucfirst(substr($patient->user->people->middlename, 0, 1)).". ".
                      ucfirst($patient->user->people->lastname) }}</option>
            @endforeach
          </select>
        </div>
        <div class="form-group col-md-4  col-xs-12 pull-right text-right">
          <label for="name">&nbsp;</label><br>
          <label><input type="checkbox" name="" id="checkbx_customer"> Check if walk-in customer.</label>
        </div>
      </div>
      <div class="row hidden">
        @php 
          $clinic = \App\ClinicAccounts::where('id', Auth::user()->clinic_id)->get();
          $user = \App\Users::where('id', Auth::user()->id)->get();
          $people = \App\People::where('id', Auth::user()->people_id)->get()->first();  
        @endphp 
        <div class="col-md-12 col-sm-12 col-xs-12">
          <h5><b>Doctor's Notes:</b></h5>
          <div class="summernote"> 
          <h3 style="text-align: center; line-height: 1;">{{$clinic[0]->clinic_name}}</h3>
          <div style="text-align: center;"> {{$people->address->city.", ".$people->address->province}}</div><br><br>
          <b>Patient: </b> <u id="customer_name_ul"><i >Patient name here...</i></u>
          <br>
          <b>Date: </b><u>{{date('m/d/Y')}}</u><br> 
          <b>Age: </b> <u><i> Age here...</i></u>
          <br> 
          <b>Sex: </b><u>Male/Female</u>

          <br><br><br><br><br><br><br><br>
          <b>Sig. </b><br><br>
          <p style="line-height: 3;text-align: right;"><b><u>________________________</u>&nbsp;MD. &nbsp; &nbsp; &nbsp;</b></p>
          <p style="line-height: 3;text-align: right;"><b><u>________________________</u> Lic. No.</b></p>
          </div>
        </div> 

      </div>
      <h4 for="name">Rendered Products/Services
        <a href="#" class="action_btns addRProductBtn" data-toggle="tooltip" data-placement="top" title="Add Product Field"><i class=" c-orange glyphicon glyphicon-remove-circle trans-rt-45"></i></a>
        <a href="#" class="action_btns addRServiceBtn" data-toggle="tooltip" data-placement="top" title="Add Service Field"><i class=" c-blue glyphicon glyphicon-remove-circle trans-rt-45"></i></a>
      </h4>
      <div class="table-responsive">
        <table class="table  " id="trans-tbl">
          <thead>
            <tr>
              <th>#</th>
              <th class="col-md-5 text-center">Item/Service</th>
              <th class="col-md-2 text-center">Charge/Price</th>
              <th class="col-md-2 text-center">Qty</th>
              <th class="col-md-2 text-center" >Amount</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            
          </tbody>
          <tfoot> 
            <tr>
              <th colspan="4" class="text-right"><b>Sub Total: </b></th>
              <th><b class="c-red text-right form-control " id="grand_total"></b></th>
            </tr>
            <tr>
              <th colspan="4" class="text-right"><b>Discount: </b></th>
              <th>
                <input type="text" name="discount" class="form-control text-right" value="0">
                <input type="hidden" name="is_percentage" value="0">
              </th>
            </tr>
            <tr class=""> 
              <th colspan="4" class="text-right"><b>Grand Total: </b></th>
              <th> 
                <input id="grand_total2" type="text" name="total_payable" class="c-red text-right form-control " readonly>
              </th>
            </tr> 
            <tr>
              <th colspan="4" class="text-right"><b>Payment: </b></th>
              <th><input type="number" name="paid_amount" class="text-right form-control"> </th>
            </tr>
            <tr>
              <th colspan="4" class="text-right"><b>Change: </b></th>
              <th>
                <input type="text" name="change" readonly class="c-green text-right form-control" value="0">
              </th>
            </tr>
          </tfoot>
      </table>
      </div>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Close</button>
      {!! Form::submit('Pay', ['class' => 'btn submit-btn btn-primary btn-gradient pull-right']) !!}
      <a href="{{url('/print/1')}}" target="blank" class="btn btn-primary pull-right print-btn  hidden"  > <i class="fa fa-print"></i> Pay &amp; Print</a>
      {!! Form::close() !!}
    </div>

  </div>
</div>


 <!-- Laravel Javascript Validation -->
 <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>

<link rel="stylesheet" type="text/css" href="/summernote/summernote.css">
<script type="text/javascript" src="/summernote/summernote.js"></script> 
<script type="text/javascript" src="/summernote/summernote-ext-print.js"></script>
 {!! JsValidator::formRequest('App\Http\Requests\AddTransRequest', '#add-trans-form') !!}

<script type="text/javascript">
  $(function(){
      $("#appointment_select").selectize({
          create: false,
          sortField: 'text',
          placeholder: 'Select Patient With Appointment',
      });
      $("#customer").selectize({
          create: true,
          sortField: 'text',
          placeholder: 'Select/Input Walkin Customer',
      });
      /*$('[name="record_id[]"]').selectize({
          create: false,
          sortField: 'text',
          placeholder: 'Select Service',
      });*/
      $('.summernote').summernote({
        height: 200,
        toolbar: [ 
          ['style', ['bold', 'italic', 'underline', 'clear']], 
            ['fontsize', ['fontsize']],  
            ['para', ['ul', 'ol', 'paragraph']],
            ['height', ['height']],
              ['misc', ['print']]
          ], 
      });
      var ctr = 1;
     
      $('button[type=submit], input[type=submit]').prop('disabled',true);
          
      
      $(".addRProductBtn").click(function(){
         var p = '<tr>'+
              '<td>'+(ctr++)+'</td>'+
              '<td>'+
                '<select class="form-control" name="record_id[]"><option></option>'+ 
                  '@foreach($products as $product)'+
                    '<option value="{{$product->id}}">{{$product->name}}</option>'+
                  '@endforeach'+
                '</select>'+
                '<input type="hidden" name="record_type[]" value="product">'+
              '</td>'+
              '<td> '+
                '<input type="text" class="form-control text-right" readonly>'+
              '</td>'+
              '<td>'+
                '<input type="text" name="qty[]" class="form-control text-center">'+
              '</td>'+
              '<td>'+
                '<input type="text" name="amount[]" class="form-control text-right">'+
              '</td>'+
              '<td>'+
                '<a href="#" class="del-row-btn"><i class="fa fa-trash c-red"></i></a>'+
              '</td>'+
            '</tr>';

          $('#trans-tbl tbody').append(p);

        $('[name="record_id[]"]').last().selectize({
            create: false,
            sortField: 'text', 
            placeholder: 'Select Product',
        });
          //$("[name='record_id[]']").last().select2();
      });

      $(".addRServiceBtn").click(function(){
         var s = '<tr>'+
                  '<td>'+(ctr++)+'</td>'+
                  '<td>'+
                    '<select class="form-control" name="record_id[]"><option></option>'+ 
                      '@foreach($services as $service)'+
                        '<option value="{{$service->id}}">{{$service->name}}</option>'+
                      '@endforeach'+
                    '</select>'+
                    '<input type="hidden" name="record_type[]" value="service">'+
                  '</td>'+
                  '<td> '+
                    '<input type="text" class="form-control text-right" readonly>'+
                  '</td>'+
                  '<td>'+
                    '<input type="text" name="qty[]" class="form-control text-center" readonly>'+
                  '</td>'+
                  '<td>'+
                    '<input type="text" name="amount[]" class="form-control text-right">'+
                  '</td>'+
                  '<td>'+
                    '<a href="#" class="del-row-btn"><i class="fa fa-trash c-red"></i></a>'+
                  '</td>'+
                '</tr>';

          $('#trans-tbl tbody').append(s);
        $('[name="record_id[]"]').last().selectize({
            create: false,
            sortField: 'text',
            placeholder: 'Select Service',
        });
      });

      $(document).ready(function(){$('[data-toggle="tooltip"]').tooltip({container:"body"})});

      $("#add-trans-form").on('submit', function(e){
          e.preventDefault();
          var that = this;
          if($("#add-trans-form").valid()){
            $(".submit-btn").addClass("disabled");
            $('button[type=submit], input[type=submit]').prop('disabled',true);
            $.ajax({
                type: 'POST',
                url: '/transactions',
                data: $('#add-trans-form').serialize(),
                dataType: 'json',
                success: function(data){
                  $('button[type=submit], input[type=submit]').prop('disabled',false);
                  $(".submit-btn").removeClass("disabled");
                }
            }).done(function(data) {
              if(data.success){
                var print_url_ ="{{ url('/print') }}";
                var x = (window.screen.width/6);
                var win_ = window.open(print_url_+'/'+data.master_id,'_blank', 'width=850, height=600, top=20, left='+x);
                $("#transactions-table").DataTable().ajax.url( '/get-transactions' ).load();
                notiff('Success!', data.msg,'success'); //title, msg, type 
                $('#addtransmodal').modal('toggle');
              }else{
                notiff('Error!', data.msg,'error'); //title, msg, type
                //$('#addmodal').modal('toggle');
              }
            }).error(function(data) {
                notiff('Error!', data.msg, 'warning'); //title, msg, type
               // $('#addmodal').modal('toggle');
            });
          }
        });

      $('#appointment_select').on('change', function(){
        var that = this;
        $.ajax({
                type: 'GET',
                url: '/get-appointment-details/'+$(that).val(),
                dataType: 'json',
                success: function(data){
                  console.log(data);
                  $("#date").val(data.date);
                  $("#doctor").val(data.doctor);
                }
            })
      });
      $(document).off('change', "[name='record_id[]']").on('change', "[name='record_id[]']", function(){
      //$("[name='record_id[]']").change(function(){
        console.log($(this).next().val());
        var that = this;
        $(that).parent().find("input[name='record_type[]']")
        $.ajax({
            type: 'GET',
            url: '/get-record-details/'+$(that).val()+'/'+$(that).parent().find("input[name='record_type[]']").val(),
            dataType: 'json',
            success: function(data){
              if($(that).next().val() == 'product'){
                $(that).parent().next().children()[0].value =  parseFloat(data.price).toFixed(2);
                $(that).parent().next().next().children()[0].value = 1;
                $("[name='qty[]']").change();
                compute_grand_total();
              }else{
                $(that).parent().next().children()[0].value = parseFloat(data.price).toFixed(2);
                $(that).parent().next().next().children()[0].value = 1;
                $(that).parent().next().next().next().children()[0].value =  parseFloat(data.price).toFixed(2);
                $("[name='qty[]']").change();
                compute_grand_total()
              }
            }
        });
      });
      
      $("[name='discount']").on('change', function(){
        if($(this).val() == ''){
          $(this).val(0);
        }
        compute_grand_total();
      });
      $("[name='paid_amount']").on('change', function(){  
        if($(this).val() == ''){
          $('button[type=submit], input[type=submit]').prop('disabled',true);
        }
        compute_change();
      });


      $(document).off('change', "[name='qty[]']").on('change', "[name='qty[]']", function(){
      //$("[name='qty[]']").on('change', function(){
        var price = parseFloat($(this).parent().prev().children()[0].value);
        var qty = parseFloat($(this).val());
        $(this).parent().next().children()[0].value = (price * qty).toFixed(2);
        compute_grand_total();
      });

      $(document).off('click', '.del-row-btn').on('click', '.del-row-btn', function(){
      //$(".del-row-btn").click(function(){
        $(this).parent().parent().remove();
        compute_grand_total();
        ctr--;
      })



      function compute_grand_total(){
        var g_total = 0.0;
        $("[name='amount[]']").each(function(){
          g_total += parseFloat($(this).val());
        });
        $("#grand_total").html(g_total.toFixed(2));
        compute_grand_total2()
      }

      function compute_grand_total2(){

        var g1 = parseFloat($("#grand_total").html());
        var last_total = 0.00;
        var disc_val = ''+$("[name='discount']").val();
        
        if(disc_val.search('%') != -1){ 
          var d = disc_val.replace('%','');
          last_total = g1 - ((parseFloat(d)/100) * g1);
          $("[name='is_percentage']").val(1);
        }else{
          last_total = g1 - parseFloat(disc_val);          
          $("[name='is_percentage']").val(0);
        }
        
        $("#grand_total2").val(parseFloat(last_total).toFixed(2));
      }

      function compute_change(){
        var payment = parseFloat($("[name='paid_amount']").val());
        var g_total = parseFloat($("#grand_total2").val());
        console.log(payment > g_total);
        if(payment >= g_total){
          $("[name='change']").val((payment-g_total).toFixed(2));
          $('button[type=submit], input[type=submit]').prop('disabled',false);
        }else{
            $('button[type=submit], input[type=submit]').prop('disabled',true);
        }
      }

      $("#checkbx_customer").on('click', function(){
        if($(this)[0].checked){
          $("#customer").parent().fadeIn(300).removeClass('hidden');
          $("#patient_fields1").slideUp(100);
          $("#patient_fields2").slideUp(100);
        }else{
          $("#customer").parent().fadeOut(300);
          $("#patient_fields1").slideDown(100);
          $("#patient_fields2").slideDown(100);
        }
      })

      $("#customer").on('change', function(){
        $('#customer_name_ul').html($(this).val());
      });
  });  
 </script>

 <style type="text/css">
   .transaction-form .del-row-btn{
    float: left;
        padding-top: 3px;
   }
   .transaction-form .action_btns {
      margin-left: 5px;
  }
  .transaction-form .form-control{
      height: 25px;
      padding: 0px 5px;
    margin: 0px;
  }
  .transaction-form #trans-tbl tfoot > tr > th{
    border-right: 0px !important;
    border-left: 0px !important;
  } 
  .modal-lg{
    width: 90%;
  }
  @media (max-width: 768px){

    .transaction-form{
      width: 95%;
    } 
  }
  .selectize-dropdown-content {
    padding: 5px 0;
    background: #fff;
    box-shadow: 0px 0px 0px 1px;
}
.selectize-dropdown, .selectize-dropdown.form-control{
  background: transparent;
  border: 0px;
  }
 </style>}
