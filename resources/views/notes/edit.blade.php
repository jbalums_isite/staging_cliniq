<div class="modal-dialog modal-md">
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button>
      <h4 class="modal-title">Add Notes Form</h4>
    </div>
    {!! Form::open(array('url' => url('/notes/'.$notes->id), 'method' => 'PATCH', 'id' => 'edit-notes-form')) !!}
    <div class="modal-body">
      <input type="hidden" name="patient_id" value="{{ $patient_id }}">
      <div class="form-group mb10">
        <label for="single_cal42">Date</label>
        <input id="single_cal42" name="date" type="text" class="form-control" placeholder="Date" value="{{ date('Y-m-d', strtotime($notes->date)) }}" required>
      </div> 
      <div class="form-group mb10">
        <label for="notes">Notes</label>
        <textarea rows="3" class="form-control" name="notes" id="notes" placeholder="Notes">{{$notes->notes}}</textarea>
      </div>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        {!! Form::submit('Submit', ['class' => 'btn submit-btn btn-primary btn-gradient pull-right']) !!}
      {!! Form::close() !!}
    </div>

  </div>
</div>


 <!-- Laravel Javascript Validation -->
 <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>

 {!! JsValidator::formRequest('App\Http\Requests\AddNotesRequest', '#edit-notes-form') !!}

<script type="text/javascript">
  $(function(){
      
      $('[name="date"]').datepicker({
        format: "yyyy-mm-dd",
        autoclose: true, 
      }).on('changeDate', function(selected){
       
      }); 
      $("#edit-notes-form").on('submit', function(e){
          e.preventDefault();
          var that = this;
          if($("#edit-notes-form").valid()){
            $(".submit-btn").addClass("disabled");
            $('button[type=submit], input[type=submit]').prop('disabled',true);
            $.ajax({
                type: 'PATCH',
                url: '/notes/'+{{$notes->id}},
                data: $('#edit-notes-form').serialize(),
                dataType: 'json',
                success: function(data){
                  $('button[type=submit], input[type=submit]').prop('disabled',false);
                  $(".submit-btn").removeClass("disabled");
                }
            }).done(function(data) {
              if(data.success){
                //$("#notes-table").DataTable().ajax.url( '/get-notes/'+$("[name='patient_id']").val() ).load();
                
                refreshData('.notes_list', 'notes');
                notiff('Success!', data.msg,'success'); //title, msg, type
                $('#editnotesmodal').modal('toggle');
              }else{
                notiff('Error!', data.msg,'error'); //title, msg, type
               // $('#addallergymodal').modal('toggle');
              }
            }).error(function(data) {
                notiff('Error!', data.msg, 'warning'); //title, msg, type
                //$('#addallergymodal').modal('toggle');
            });
          }
        });
  });  
 </script>