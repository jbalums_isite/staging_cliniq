@extends('new_template.app')
 
@section('content') 
<div class="breadcrumbs ace-save-state" id="breadcrumbs">
	<ul class="breadcrumb">
		<li> 
			<a href="/">Home</a>
		</li> 
		<li> 
			Daily Reports
		</li> 
	</ul><!-- /.breadcrumb -->

	
	<ul class="breadcrumb pull-right action_buttons">
		<li> 
      <a href="#" class="addTransBtn">
				<i class="fa fa-plus"></i>
				Add Transactions
			</a> 
		</li>   
    <li> 
      <span><input type="text" id="from" placeholder="From" form-control" data-date-format="yyyy" name="from" class="form-control" value="{{date('Y-m-d')}}"></span>
    </li>  
    <li> 
      <span><input type="text" id="to" placeholder="To" form-control" data-date-format="yyyy" name="to" class="form-control" value="{{date('Y-m-d')}}"></span>
    </li>  
	</ul><!-- /.breadcrumb -->

</div>


<div class="page-content"> 
	<div class="row table-responsive">
		<div class="col-xs-12 col-md-12">
			<h3 style="margin: 0px;padding-top: 10px;padding-bottom: 10px;box-shadow: 0px -1px 0px 0px #eee, 0px 1px 0px 0px #eee;">Daily Sales Reports</h3>
			<table class="table jambo_table " id="sales-report-table">
		      <thead>
		        <tr>
		          <th>#</th>
		          <th>Date</th>
		          <th>Patient</th>
		          <th>Item</th>
		          <th>Price</th>
		          <th>Qty</th>
		          <th>Discount</th>
		          <th>Total</th>
		        </tr>
		      </thead>
		      <tbody>
		      </tbody>
		      <tfoot>
		        <tr>
		          <th></th>
		          <th></th>
		          <th></th>
		          <th></th>
		          <th></th>
		          <th></th>
		          <th><b class="pull-right">Grand Total:</b></th>
		          <th></th>
		        </tr>
		      </tfoot>
		    </table>

		</div><!-- /.col -->
	</div><!-- /.row -->

</div><!-- /.page-content -->
  <!-- /page content -->

<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;" id="addmodal"></div>
<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;" id="editmodal"></div>
<style type="text/css">
	tbody > tr:hover{
		cursor: pointer;
	} 
	.search_bar_{
		display: block;
	}
	.ppi_{
		display: none;
	}
	@media (max-width: 768px){
 
		.p15{
			padding: 5px !important;
			padding-left: 5px !important;
		    margin-right: 0px;
		    margin-left: 0px;
		}
		.s_bar_{
			float: right;
		    margin-right: 5px;
		    margin-top: 0px;
		}
		.search_bar_ {
			width: 100%;
		} 
		.ppi_{
			display: block;
		}
	} 
  table.dataTable thead th, table.dataTable tfoot th,
  table.dataTable tbody th, table.dataTable tbody td{
    padding: 3px;
    font-size: 12px !important;
  }
  @media (max-width: 768px){
    #year{
         margin-top: 8px;
    }
  }
</style>

<style type="text/css" media="print">
  td{
    border: 1px solid #eee;
  }
  tfoot th:nth-child(2), td:nth-child(3), th:nth-child(3){
    text-align: right !important;
  } 
  table.dataTable tbody th, table.dataTable tbody td{
  	padding: 2px !important;
  }
</style>
@endsection 

@section('script_content')

    {!! Html::style(url('datatable/jquery.dataTables.min.css')) !!}
    {!! Html::style(url('datatable/buttons.dataTables.min.css')) !!}

    {!! Html::script(url('datatable/jquery.dataTables.min.js')) !!}
    {!! Html::script(url('datatable/dataTables.buttons.min.js')) !!}
    {!! Html::script(url('datatable/jszip.min.js')) !!}
    {!! Html::script(url('datatable/pdfmake.min.js')) !!}
    {!! Html::script(url('datatable/vfs_fonts.js')) !!}
    {!! Html::script(url('datatable/buttons.html5.min.js')) !!}
    {!! Html::script(url('datatable/buttons.print.min.js')) !!}
    {!! Html::script(url('datatable/accounting.js')) !!}

    {!! Html::script(url('dst/vendors/moment/min/moment.min.js')) !!}
<script type="text/javascript">
  $(function(){  
  
   $('#from').datepicker({
        format: "yyyy-mm-d",
        autoclose: true, 
    }).on('changeDate', function(selected){
        startDate =  $("#from").val();
        $('#to').datepicker('setStartDate', startDate);
        $("#sales-report-table").DataTable().ajax.url( '{{url("/get-sales-report")}}/'+$("#from").val()+'/'+$("#to").val() ).load();
    }); 
      
    $('#to').datepicker({
        format: "yyyy-mm-d",
        autoclose: true, 
    }).on('changeDate', function(selected){
        startDate =  $("#from").val();
        $('#to').datepicker('setStartDate', startDate);
        $("#sales-report-table").DataTable().ajax.url( '{{url("/get-sales-report")}}/'+$("#from").val()+'/'+$("#to").val() ).load();
    }); 

    var start2 = moment();
    var end2 = moment();
    $('#sales-report-table').DataTable({
        bProcessing: true,
        bServerSide: false,
        sServerMethod: "GET",
      'ajax': '{{url("/get-sales-report")}}/'+start2.format('YYYY-M-D')+'/'+end2.format('YYYY-M-D'),
      searching: false, 
      paging: true, 
      filtering:false, 
      bDestroy: true,
      bInfo: false,
      language:{
        "paginate": {
              "next":       "<i class='fa fa-chevron-right'></i>",
              "previous":   "<i class='fa fa-chevron-left'></i>"
          }
      },
      "columns": [
              {data: 'row', name: 'row', className: ' text-center', searchable: false, sortable: true},
              {data: 'date', name: 'date', className: 'col-md-1 text-left', searchable: true, sortable: true}, 
              {data: 'patient', name: 'patient', className: 'col-md-3 text-left', searchable: true, sortable: true}, 
              {data: 'item', name: 'item', className: 'col-md-3 text-left', searchable: true, sortable: true}, 
              {data: 'price', name: 'price', className: 'col-md-2 text-right', searchable: true, sortable: true}, 
              {data: 'qty', name: 'qty', className: 'col-md-1 text-center', searchable: true, sortable: true}, 
              {data: 'discount', name: 'discount', className: 'col-md-1 text-right', searchable: true, sortable: true}, 
              {data: 'total', name: 'total', className: 'col-md-2 text-right', searchable: true, sortable: true}, 
          ],
      dom: 'Bfrtip',
      buttons: [
        
                 {
                    extend: 'print',
                    title: 'Sales Report', 
                    footer:true
                },
                 {
                    extend: 'copyHtml5',
                    title: 'Sales Report ', 
                    footer:true
                },
                 {
                    extend: 'excelHtml5',
                    title: 'Sales Report ', 
                    footer:true
                },
                 {
                    extend: 'csvHtml5',
                    title: 'Sales Report ', 
                    footer:true
                },
                 {
                    extend: 'pdfHtml5',
                    title: 'Sales Report ', 
                    footer:true
                }
      ],

      "footerCallback": function( tfoot, data, start, end, display ) {
          var api = this.api(), data;
          var t= 0;
          for(var x = 0; x < data.length; x++){  
            t+= parseFloat(data[x].total.split(',').join('')); 
          }
                $( api.column( 7 ).footer() ).html(accounting.formatMoney(t, ''));
        }
    });
      var d = new Date();
     /* $('#birthday').val((d.getMonth()+1)+'/'+d.getDate()+"/"+d.getFullYear()); 
      $('#birthday').daterangepicker({ 
        singleDatePicker: true,
        calender_style: "picker_4", 
        locale: {
          format: 'MM/DD/YYYY'
        }
      },function(start, end, label) { 
        $("#sales-report-table").DataTable().ajax.url( '{{url("/get-sales-report")}}/'+start.format('YYYY-M-D')+'/'+start.format('YYYY-M-D')).load();
      });*/
      
 });
  
    setTimeout(resize_action_buttons_height, 100);
        function resize_action_buttons_height(){  
          $('.action_buttons').css('height',$('#breadcrumbs').height()+"px");
          $('.action_buttons a').css('height',$('#breadcrumbs').height()+"px");
          $('.action_buttons span').css('height',$('#breadcrumbs').height()+"px");
          $('.action_buttons span').css('float',"left");
          $('.action_buttons span').css('padding-top',"3px");
        }
</script>
@endsection