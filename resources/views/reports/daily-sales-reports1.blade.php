@include('not_supported')
<!-- top action nav -->
<div class="top_nav" style="min-height: 37px;margin-bottom: 0px;background: #ededed;    border-bottom: 0px;">
  <div class="nav_menu"  style="min-height: 37px;margin-bottom: 10px;background: #ededed;margin-top: -10px;    border-bottom: 0px;">
    <nav> 

        <div class="col-md-6 col-sm-6">
          <ul class="nav navbar-nav navbar-left">
            <li class="">
              <h4>&nbsp;&nbsp;<i class="fa fa-calendar"></i> Sales Report</h4>
              <input type="text" name="search" placeholder="Search..." class="hidden">
              <i class="fa fa-search hidden"></i>
            </li>
          </ul>
        </div>
      <div class="col-md-6 col-sm-6  p10 text-right">   

        <input id="birthday" name="date" class="date-picker form-control pull-right" required="required" type="text" value="{{date('m-d-Y')}}">

   
      </div>
    </nav>
  </div>
</div>
<!-- /top action nav -->



<div class="right_col p15 pt30" role="main">
  
  <div class="row p15 mt50 table-responsive">
    <table class="table jambo_table " id="sales-report-table">
      <thead>
        <tr>
          <th>#</th>
          <th>Date</th>
          <th>Patient</th>
          <th>Item</th>
          <th>Price</th>
          <th>Qty</th>
          <th>Discount</th>
          <th>Total</th>
        </tr>
      </thead>
      <tbody>
      </tbody>
      <tfoot>
        <tr>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th><b class="pull-right">Grand Total:</b></th>
          <th></th>
        </tr>
      </tfoot>
    </table>
  </div>

</div>

    {!! Html::style(url('datatable/jquery.dataTables.min.css')) !!}
    {!! Html::style(url('datatable/buttons.dataTables.min.css')) !!}

    {!! Html::script(url('datatable/jquery.dataTables.min.js')) !!}
    {!! Html::script(url('datatable/dataTables.buttons.min.js')) !!}
    {!! Html::script(url('datatable/jszip.min.js')) !!}
    {!! Html::script(url('datatable/pdfmake.min.js')) !!}
    {!! Html::script(url('datatable/vfs_fonts.js')) !!}
    {!! Html::script(url('datatable/buttons.html5.min.js')) !!}
    {!! Html::script(url('datatable/buttons.print.min.js')) !!}
    {!! Html::script(url('datatable/accounting.js')) !!}

    {!! Html::script(url('dst/vendors/moment/min/moment.min.js')) !!}
<script type="text/javascript">
  $(function(){ 
        var start2 = moment();
        var end2 = moment();
    $('#sales-report-table').DataTable({
        bProcessing: true,
        bServerSide: false,
        sServerMethod: "GET",
      'ajax': '{{url("/get-sales-report")}}/'+start2.format('YYYY-M-D')+'/'+end2.format('YYYY-M-D'),
      searching: false, 
      paging: true, 
      filtering:false, 
      bDestroy: true,
      bInfo: false,
      language:{
        "paginate": {
              "next":       "<i class='fa fa-chevron-right'></i>",
              "previous":   "<i class='fa fa-chevron-left'></i>"
          }
      },
      "columns": [
              {data: 'row', name: 'row', className: ' text-center', searchable: false, sortable: true},
              {data: 'date', name: 'date', className: 'col-md-1 text-left', searchable: true, sortable: true}, 
              {data: 'patient', name: 'patient', className: 'col-md-3 text-left', searchable: true, sortable: true}, 
              {data: 'item', name: 'item', className: 'col-md-3 text-left', searchable: true, sortable: true}, 
              {data: 'price', name: 'price', className: 'col-md-2 text-right', searchable: true, sortable: true}, 
              {data: 'qty', name: 'qty', className: 'col-md-1 text-center', searchable: true, sortable: true}, 
              {data: 'discount', name: 'discount', className: 'col-md-1 text-right', searchable: true, sortable: true}, 
              {data: 'total', name: 'total', className: 'col-md-2 text-right', searchable: true, sortable: true}, 
          ],
      dom: 'Bfrtip',
      buttons: [
        
                 {
                    extend: 'print',
                    title: 'Sales Report', 
                    footer:true
                },
                 {
                    extend: 'copyHtml5',
                    title: 'Sales Report ', 
                    footer:true
                },
                 {
                    extend: 'excelHtml5',
                    title: 'Sales Report ', 
                    footer:true
                },
                 {
                    extend: 'csvHtml5',
                    title: 'Sales Report ', 
                    footer:true
                },
                 {
                    extend: 'pdfHtml5',
                    title: 'Sales Report ', 
                    footer:true
                }
      ],

      "footerCallback": function( tfoot, data, start, end, display ) {
          var api = this.api(), data;
          var t= 0;
          for(var x = 0; x < data.length; x++){  
            t+= parseFloat(data[x].total.split(',').join('')); 
          }
                $( api.column( 7 ).footer() ).html(accounting.formatMoney(t, ''));
        }
    });
      var d = new Date();
      $('#birthday').val((d.getMonth()+1)+'/'+d.getDate()+"/"+d.getFullYear()); 
      $('#birthday').daterangepicker({ 
        singleDatePicker: true,
        calender_style: "picker_4", 
        locale: {
          format: 'MM/DD/YYYY'
        }
      },function(start, end, label) { 
        $("#sales-report-table").DataTable().ajax.url( '{{url("/get-sales-report")}}/'+start.format('YYYY-M-D')+'/'+start.format('YYYY-M-D')).load();
      });
 });
  
</script>

<style type="text/css">
  table.dataTable thead th, table.dataTable tfoot th,
  table.dataTable tbody th, table.dataTable tbody td{
    padding: 3px;
    font-size: 12px !important;
  }
    #birthday{
      width: 40% !important;
    }
  @media (max-width: 1090px){  
     
    #birthday{
      width: 70% !important;
      float: right;
    } 

  }

  @media (max-width: 768px){
    #birthday{
      width: 100% !important;
    }
  }
</style>

<style type="text/css" media="print">
  td{
    border: 1px solid #eee;
  }
  td:nth-child(6), th:nth-child(6){
    text-align: center !important;
  }
  td:nth-child(5),td:nth-child(8), tfoot th:nth-child(7), tfoot th:nth-child(8){
    text-align: right !important;
  }
  td:nth-child(7){
    text-align: center !important;
  }
  @media (max-width: 768px){

  }
</style>