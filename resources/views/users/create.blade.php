<div class="modal-dialog modal-lg add-user-form">
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button>
      <h4 class="modal-title">Add user Form</h4>
    </div>

    <form action="{{url('/image-upload')}}"  class="hidden" id="upload-profile-picture-form"  >
        {{ csrf_field() }} 
          <input type="file" name="file" id="file_img">
    </form> 

    {!! Form::open(array('url' => url('/users'), 'method' => 'POST', 'id' => 'add-users-form')) !!}
    <div class="modal-body">

      <h5>Profile Picture</h5>
        
      <div class="profile_img profile_img__ text-center p10">
        <div class="col-md-12 img_preview"><img class=" avatar-view" src="/dst/images/user.png" alt="Avatar" title=" "></div> 
        <div class="btn btn-success upload-btn fileinput-new"> <i class="fa fa-upload"></i> Upload</div>
        <div class="btn btn-info upload-btn fileinput-exists" style="display: none;"> <i class="fa fa-refresh"></i> Change</div>
        
      </div>

      <h5>Personal Information</h5>
      
        <div class="row">
          <div class="form-group col-md-6  col-xs-12">
            <label for="title">Title</label>
            <input id="title" name="title" type="text" class="form-control" placeholder="Title" required>
          </div>
        </div>
        <div class="row">
          <div class="form-group col-md-6  col-xs-12">
            <label for="firstname">Firstname</label>
            <input id="firstname" name="firstname" type="text" class="form-control" placeholder="Firstname" required>
          </div>
          <div class="form-group col-md-6  col-xs-12">
            <label for="lastname">Lastname</label>
            <input id="lastname" name="lastname" type="text" class="form-control" placeholder="Lastname" required>
          </div>
        </div>
        <div class="row">
          <div class="form-group col-md-6  col-xs-12">
            <label for="middlename">Middlename</label>
            <input id="middlename" name="middlename" type="text" class="form-control" placeholder="Middlename" required>
          </div>
        </div>
        <div class="row mt15">
          
          <div class="col-md-6 xdisplay_inputx form-group has-feedback">
            <label for="birthdate">Birthdate</label>
              <input name="birthdate" type="text" class="form-control has-feedback-left date-picker" id="single_cal4" placeholder="Birthdate" aria-describedby="">
              <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
          </div>
          <div class="form-group col-md-6  col-xs-12">
            <label for="birthplace">Birthplace</label>
            <input id="birthplace" name="birthplace" type="text" class="form-control"  placeholder="Birthplace" required>
          </div>
        </div>

        <div class="row mt15">
          <div class="form-group col-md-6  col-xs-12">
            <label for="gender">Gender</label>
            <select name="gender" id="gender" class="form-control">
              <option disabled selected>Select Gender</option>
              <option value="male">Male</option>
              <option value="female">Female</option>
            </select>
          </div>
          <div class="form-group col-md-6  col-xs-12">
            <label for="civil_status">Civil Status</label>
            <select name="civil_status" id="civil_status" class="form-control">
              <option disabled selected>Select Civil Status</option>
              <option value="single">Single</option>
              <option value="married">Married</option>
              <option value="widowed">Widowed</option>
              <option value="divorced">Divorced</option>
            </select>
          </div>
        </div>

        <div class="row mt15">
          <div class="form-group col-md-6  col-xs-12">
            <label for="nationality">Nationality</label>
            <input id="nationality" name="nationality" type="text" class="form-control" placeholder="Nationality" required>
          </div>
        </div>


        <h5 class="mt25">Contact Information</h5>
        
        <div class="row">
          <div class="form-group col-md-6  col-xs-12">
            <label for="mobile">Mobile No.</label>
            <input id="mobile" name="mobile" type="number" class="form-control" placeholder="Mobile Number" required>
          </div>
          <div class="form-group col-md-6  col-xs-12">
            <label for="tel">Landline No.</label>
            <input id="tel" name="tel" type="text" class="form-control" placeholder="Landline Number" required>
          </div>
        </div>


        <h5 class="mt25">Address Information</h5>
        
        <div class="row">
          <div class="form-group col-md-6  col-xs-12">
            <label for="province">Province</label>
            <input id="province" name="province" type="text" class="form-control" placeholder="Province" required>
          </div>
          <div class="form-group col-md-6  col-xs-12">
            <label for="city">City</label>
            <input id="city" name="city" type="text" class="form-control" placeholder="City" required>
          </div>
        </div>
        <div class="row">
          <div class="form-group col-md-6  col-xs-12">
            <label for="street">Street</label>
            <input id="street" name="street" type="text" class="form-control" placeholder="Street" required>
          </div>
        </div>

        <h5 class="mt25">Credential Information</h5>
        
        <div class="row">
          <div class="form-group col-md-6  col-xs-12">
            <label for="type">User Type</label>
            <select name="type" id="type" class="form-control">
              <option disabled selected>Select User Type</option>
              <option value="doctor">Doctor</option>
              <option value="secretary">Secretary</option>
              <option value="patient">Patient</option>
            </select>
          </div>
        </div>
        <div class="row">
          <div class="form-group col-md-6  col-xs-12">
            <label for="email">Email</label>
            <input id="email" name="email" type="email" class="form-control" placeholder="Email" required>
          </div>
        </div>
        <div class="row">
          <div class="form-group col-md-6  col-xs-12">
            <label for="password">Password</label>
            <input id="password" name="password" type="password" class="form-control" placeholder="Password" required>
          </div>
          <div class="form-group col-md-6  col-xs-12">
            <label for="confirm_password">Confirm Password</label>
            <input id="confirm_password" name="confirm_password" type="password" class="form-control" placeholder="Confirm Password" required>
          </div>     
        </div>


      <div style="width: 100%;">&nbsp;</div>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        {!! Form::submit('Submit', ['class' => 'btn submit-btn btn-primary btn-gradient pull-right']) !!}
      {!! Form::close() !!}
    </div>

  </div>
</div>


 <!-- Laravel Javascript Validation -->
 <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>

 {!! JsValidator::formRequest('App\Http\Requests\AddUserRequest', '#add-users-form') !!}

<script type="text/javascript">
  $(function(){
      
      var old_pic = '';
      var formData = new FormData();
      $("#file_img").change(function(){ 
          formData.append('file', $('#file_img')[0].files[0]);
          formData.append('old_file', old_pic);
          formData.append('_token', $('[name="_token"]').val());
          $.ajax({
                type: 'POST',
                url: '/uploadPicture',
                data: formData,
                processData: false,
                contentType: false,
                success: function(data){
                  console.log(data);
                  if(data.success){
                    $('.img_preview img').attr('src', '/uploads/'+data.img);
                    $('.fileinput-new').hide();
                    $('.fileinput-exists').show();
                    old_pic = data.img; 
                  }else{ 

                  } 
                },
               beforeSend: function(){
                  $('.img_preview img').attr('src', '/img/spin.gif');
               }
            });
      });
      

      $(document).off('click', '.upload-btn').on('click', '.upload-btn', function(e){
        e.preventDefault(); 
        $('#file_img').click();
      });  

     $('.date-picker').datepicker({
          autoclose: true,
          todayHighlight: true
        })
        //show datepicker when clicking on the icon
        .next().on(ace.click_event, function(){
          $(this).prev().focus();
        });
      $("#add-users-form").on('submit', function(e){
          e.preventDefault();
          var that = this;
          if($("#add-users-form").valid()){
            $(".submit-btn").addClass("disabled");
            $('button[type=submit], input[type=submit]').prop('disabled',true);
            $.ajax({
                type: 'POST',
                url: '/users',
                data: $('#add-users-form').serialize()+'&icon='+old_pic,
                dataType: 'json',
                success: function(data){
                  $('button[type=submit], input[type=submit]').prop('disabled',false);
                  $(".submit-btn").removeClass("disabled");
                }
            }).done(function(data) {
              if(data.success){
                $("#users-table").DataTable().ajax.url( '/get-users' ).load();
                notiff('Success!', data.msg,'success'); //title, msg, type
                $('#addmodal').modal('toggle');
              }else{
                notiff('Error!', data.msg,'error'); //title, msg, type
                $('#addmodal').modal('toggle');
              }
            }).error(function(data) {
                notiff('Error!', data.msg, 'warning'); //title, msg, type
                $('#addmodal').modal('toggle');
            });
          }
        });
  });  
 </script>
<style type="text/css">
  .add-user-form h5{
      margin: 0px;
      margin-bottom: 10px;
      padding: 10px 5px 10px;
      border-bottom: 1px solid;
      background: #096bb1;
      color: #fff;
      font-weight: bold;
  } 
</style>