@extends('new_template.app')
 
@section('content') 
<div class="breadcrumbs ace-save-state" id="breadcrumbs">
	<ul class="breadcrumb"> 
		<li> 
			<a href="/">Home</a>
		</li> 
		<li> 
			Users
		</li> 
	</ul><!-- /.breadcrumb -->

	
	<ul class="breadcrumb pull-right action_buttons hidden">
		<li> 
			<a href="#" class="addTransBtn">
				<i class="fa fa-plus"></i>
				Add Transactions
			</a> 
		</li>  
		<li> 
			<a href="javascript:void(0)" class="addUsersBtn">
				<i class="fa fa-plus"></i>
				Add Users
			</a> 
		</li>  
	</ul><!-- /.breadcrumb -->

</div>


<div class="page-content" style="padding-top: 50px;"> 
	<div class="row">
		<div class="col-md-12"> 
										

		</div> 
	</div>
	<div class="row">
		<div class="col-xs-12">
 
		    <form action="{{url('/image-upload')}}"  class="hidden" id="upload-profile-picture-form"  >
		        {{ csrf_field() }} 
		          <input type="file" name="file" id="file_img"> 
		    </form> 
			<div id="home" class="tab-pane in active">
				<div class="row">
					<div class="col-md-12">
						@if(isset($success_update_profile))
							<div class="alert alert-block alert-success">
								<button type="button" class="close" data-dismiss="alert">
									<i class="ace-icon fa fa-times"></i>
								</button>

								<p>
									<strong>
										<i class="ace-icon fa fa-check"></i>
										Success!
									</strong>
									Your account successfully updated.
								</p> 
							</div>
						@endif
					</div> 
				</div>
				<div class="row">
					<div class="col-xs-12 col-sm-5 col-md-3 center"> 
						<h3 class="blue">
										&nbsp;&nbsp;
										<span class="middle">
										{{$user->people->firstname.' '.
										substr($user->people->middlename, 0,1).'. '.
										$user->people->lastname
										}}
										</span> 
									</h3> 
					  <div class="profile_img profile_img__ text-center p10"> 
				        <div class="col-md-12 img_preview">

				            @if($user->people->gender == 'male' && sizeof($user->icon) == 0)     
				              <img class=" avatar-view" src="{{asset('/dst/images/user.png')}}" alt="Avatar" title=" ">
				            @elseif($user->people->gender == 'female' && sizeof($user->icon) == 0) 
				              <img class=" avatar-view" src="{{asset('/dst/images/picture.jpg')}}" alt="Avatar" title=" ">
				            @elseif(sizeof($user->icon) > 0) 
				              <img class=" avatar-view" src="{{asset('uploads/'.$user->icon)}}" alt="Avatar" title=" ">
				            @endif
				        </div> 
				        <div class="btn btn-success upload-btn fileinput-new"> <i class="fa fa-upload"></i> Upload</div>
				        <div class="btn btn-info upload-btn fileinput-exists" style="display: none;"> <i class="fa fa-refresh"></i> Change</div>
				      </div>
				      
					</div><!-- /.col -->

					<div class="col-xs-12 col-sm-6 col-md-6">
						<div class="tabbable">
							<ul class="nav nav-tabs" id="myTab">
								<li class="active">
									<a data-toggle="tab" href="#profile_1">
										<i class="green ace-icon fa fa-user bigger-120"></i>
										Profile
									</a>
								</li>

								<li>
									<a data-toggle="tab" href="#messages">
										<i class="red ace-icon fa fa-lock bigger-120"></i>
										Change Password 
									</a>
								</li>

							</ul>

							<div class="tab-content">
								<div id="profile_1" class="tab-pane fade in active">
									
			    					{!! Form::open(array('url' => url('/users/'.$user->id), 'method' => 'PATCH', 'id' => 'edit-users-form')) !!}
									<div class="profile-user-info text-left">

								        <input type="hidden" name="people_id" value="{{$user->people->id}}">
								        <input type="hidden" name="address_id" value="{{$user->people->address->id}}">
								        <input type="hidden" name="contact_id" value="{{$user->people->contact->id}}">
								        <input type="hidden" name="user_id" value="{{$user->id}}">
								        <input type="hidden" name="type" value="{{$user->type}}">
								        <input type="hidden" name="from_profile" value="true">
										<div class="profile-info-row {{Auth::user()->type == 'doctor' ? '':'hidden'}}">
											<div class="profile-info-name"> Title: </div> 
											<div class="profile-info-value"><div class="form-group">
									            <input id="title" name="title" type="text" class="form-control" placeholder="Title" required value="{{$user->people->title}}">
											</div></div>
										</div>
										<div class="profile-info-row">
											<div class="profile-info-name"> Firstname: </div> 
											<div class="profile-info-value"><div class="form-group">
												<input type="text" name="firstname" class="form-control" value="{{$user->people->firstname}}">
											</div></div>
										</div>

										<div class="profile-info-row">
											<div class="profile-info-name"> Middlename: </div> 
											<div class="profile-info-value"><div class="form-group"> 
												<input type="text" name="middlename" class="form-control" value="{{$user->people->middlename}}">
											</div></div>
										</div>

										<div class="profile-info-row">
											<div class="profile-info-name"> Lastname: </div>

											<div class="profile-info-value"><div class="form-group"> 
												<input type="text" name="lastname" class="form-control" value="{{$user->people->lastname}}">
											</div></div>
										</div>

										<div class="profile-info-row">
											<div class="profile-info-name"> Date of Birth: </div>

											<div class="profile-info-value"><div class="form-group">
											<!-- 		<span>{{date('F d, Y', strtotime($user->people->birthdate))}}</span> --> 
								              <input name="birthdate" type="text" class="form-control has-feedback-left date-picker" id="single_cal4" placeholder="Birthdate" aria-describedby="" value="{{date('m/d/Y', strtotime($user->people->birthdate))}}"> 
											</div></div>
										</div>
										<div class="profile-info-row">
											<div class="profile-info-name"> Birthplace: </div>

											<div class="profile-info-value"><div class="form-group"> 
												<input type="text" name="birthplace" class="form-control" value="{{$user->people->birthplace}}">
											</div></div>
										</div>
										<div class="profile-info-row">
											<div class="profile-info-name"> Nationality: </div>

											<div class="profile-info-value"><div class="form-group"> 
												<input type="text" name="nationality" class="form-control" value="{{$user->people->nationality}}">
											</div></div>
										</div>
										<div class="profile-info-row">
											<div class="profile-info-name"> Gender: </div>

											<div class="profile-info-value"><div class="form-group"> 
									            <select name="gender" id="gender" class="form-control">
									              <option disabled selected>Select Gender</option>
									              <option value="male" {{$user->people->gender == 'male' ? 'selected':''}}>Male</option>
									              <option value="female" {{$user->people->gender == 'female' ? 'selected':''}}>Female</option>
									            </select>
											</div></div>
										</div>


			 

										<div class="profile-info-row">
											&nbsp;
										</div>

										<div class="profile-info-row">
											<div class="profile-info-name" style="    vertical-align: top;padding-top: 10px;"> Address: </div>

											<div class="profile-info-value">
												<div class="form-group"><b>Street</b>
									            <input id="street" name="street" type="text" class="form-control" placeholder="Street" required value="{{$user->people->address->street}}"></div>
												<div class="form-group"><b>City</b>
									            <input id="street" name="city" type="text" class="form-control" placeholder="City" required value="{{$user->people->address->city}}"></div>
												<div class="form-group"><b>Province</b>
									            <input id="street" name="province" type="text" class="form-control" placeholder="Province" required value="{{$user->people->address->province}}"></div>
											</div>
										</div>

										<div class="profile-info-row">
											<div class="profile-info-name"> Status: </div>

											<div class="profile-info-value"><div class="form-group">
									            <select name="civil_status" id="civil_status" class="form-control">
									              <option disabled selected>Select Civil Status</option>
									              <option value="single"  {{$user->people->civil_status == 'single' ? 'selected':''}}>Single</option>
									              <option value="married"  {{$user->people->civil_status == 'married' ? 'selected':''}}>Married</option>
									              <option value="widowed"  {{$user->people->civil_status == 'widowed' ? 'selected':''}}>Widowed</option>
									              <option value="divorced"  {{$user->people->civil_status == 'divorced' ? 'selected':''}}>Divorced</option>
									            </select>
											</div></div>
										</div>

										<div class="profile-info-row">
											&nbsp;
										</div>


										<div class="profile-info-row">
											<div class="profile-info-name"> Contact No.: </div>

											<div class="profile-info-value"><div class="form-group">
			            						<input id="mobile" name="mobile" type="number" class="form-control" placeholder="Mobile Number" required value="{{$user->people->contact->mobile}}">
											</div></div>
										</div>
										<div class="profile-info-row">
											<div class="profile-info-name"> Tel No.: </div>

											<div class="profile-info-value"><div class="form-group">
												
			            						<input id="tel" name="tel" type="text" class="form-control" placeholder="Landline Number" required value="{{$user->people->contact->telephone}}">
											</div></div>
										</div>
			 
										<div class="profile-info-row">
											&nbsp;
										</div>

										<div class="profile-info-row">
											<div class="profile-info-name"> Email Address: </div>

											<div class="profile-info-value"><div class="form-group">
			            						<input id="email" name="email" type="email" class="form-control" placeholder="Email" required value="{{$user->email}}">
											</div></div>
										</div>
										<div class="profile-info-row">
											<div class="profile-info-name"> &nbsp;</div>
											<div class="profile-info-value"><div class="form-group">
			        					{!! Form::submit('Update Profile', ['class' => 'btn submit-btn btn-success btn-gradient pull-right']) !!}
											</div></div>
			        					</div>
									</div>

			      					{!! Form::close() !!}
								</div>

								<div id="messages" class="tab-pane fade">
									<div class="row">
										<div class="col-md-12">
											{!! Form::open(array('url' => url('/changepassword/'.$user->id), 'method' => 'PATCH', 'id' => 'edit-pass-form')) !!}
									<h4 style="margin-bottom: 0px;margin-top: 30px; float: left;"><b>Change Password</b></h4>
									<div class="row">
									<hr class="col-md-10">
										<div class="col-md-12">
											
										<div class="alert alert-block alert-success pass-success" style="display: none;">
											<button type="button" class="close" data-dismiss="alert">
												<i class="ace-icon fa fa-times"></i>
											</button>

											<p>
												<strong>
													<i class="ace-icon fa fa-check"></i>
													Success!
												</strong>
												Password successfully changed.
											</p> 
										</div>
										</div>
									</div>
							        <div class="row">
							          <div class="form-group col-md-6">
							            <label for="old_password">Old Password</label>
							            <input id="old_password" name="old_password" type="password" class="form-control" placeholder="Password" required>
							          </div>     
							        </div>
							        <div class="row">
							          <div class="form-group col-md-6">
							            <label for="new_password">New Password</label>
							            <input id="new_password" name="new_password" type="password" class="form-control" placeholder="New Password" required>
							          </div>
							          <div class="form-group col-md-6">
							            <label for="confirm_new_password">Confirm New Password</label>
							            <input id="confirm_new_password" name="confirm_password" type="password" class="form-control" placeholder="Confirm New Password" required>
							          </div>     
							        </div>
							        <div class="row">
								    	<div class="col-md-12">
								    		{!! Form::submit('Change Password', ['class' => 'btn submit-btn btn-success btn-gradient pull-right']) !!}
							        	
								    	</div> 
							        </div>
								    {!! Form::close() !!}
										</div> 
									</div>
								</div>
							</div>
						</div>  
					</div><!-- /.col -->
				</div><!-- /.row -->

				<div class="space-20"></div>
 
			</div><!-- /#home -->


		</div><!-- /.col -->
	</div><!-- /.row -->
</div><!-- /.page-content -->
  <!-- /page content -->

<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;" id="addmodal"></div>
<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;" id="editmodal"></div>

<style type="text/css">
	tbody > tr:hover{
		cursor: pointer;
	} 
	.search_bar_{
		display: block;
	}
	.ppi_{
		display: none;
	}
	@media (max-width: 768px){
 
		.p15{
			padding: 5px !important;
			padding-left: 5px !important;
		    margin-right: 0px;
		    margin-left: 0px;
		}
		.s_bar_{
			float: right;
		    margin-right: 5px;
		    margin-top: 0px;
		}
		.search_bar_ {
			width: 100%;
		} 
		.ppi_{
			display: block;
		}
	}
</style>
@endsection 

@section('script_content')


 <!-- Laravel Javascript Validation -->
 <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
 {!! JsValidator::formRequest('App\Http\Requests\EditUserRequest', '#edit-users-form') !!}
 {!! JsValidator::formRequest('App\Http\Requests\ChangePasswordRequest', '#edit-pass-form') !!} 
<script type="text/javascript">
	$(function(){
		

     
     $('.date-picker').datepicker({
          autoclose: true,
          todayHighlight: true
        })
        //show datepicker when clicking on the icon
        .next().on(ace.click_event, function(){
          $(this).prev().focus();
        });

		setTimeout(resize_action_buttons_height, 100);
        function resize_action_buttons_height(){  
			$('.action_buttons').css('height',$('#breadcrumbs').height()+"px");
			$('.action_buttons a').css('height',$('#breadcrumbs').height()+"px");
        }
	});
	
      var old_pic = '{{$user->icon}}';
      var formData = new FormData();
      $("#file_img").change(function(){ 
          formData.append('file', $('#file_img')[0].files[0]);
          formData.append('old_file', old_pic);
          formData.append('_token', $('[name="_token"]').val());
          formData.append('user_id', $('[name="user_id"]').val());
          $.ajax({
                type: 'POST',
                url: '/uploadPicture',
                data: formData,
                processData: false,
                contentType: false,
                async: false,
                success: function(data){
                  console.log(data);
                  if(data.success){
                    $('.img_preview img').attr('src', '/uploads/'+data.img);
                    $('.nav-user-photo').attr('src', '/uploads/'+data.img);
                    $('.fileinput-new').hide();
                    $('.fileinput-exists').show();
                    old_pic = data.img; 
                  }else{ 

                  } 
                },
			   beforeSend: function(){
                    $('.img_preview img').attr('src', '/img/spin.gif');
			   } 
            });
      });
      
      $(document).off('click', '.upload-btn').on('click', '.upload-btn', function(e){
        e.preventDefault(); 
        $('#file_img').click();
      }); 
     $('.date-picker').datepicker({
          autoclose: true,
          todayHighlight: true
        })
        //show datepicker when clicking on the icon
        .next().on(ace.click_event, function(){
          $(this).prev().focus();
        });
      $("#edit-users-form").on('submit', function(e){ 
          //e.preventDefault();
          var that = this;
          if($("#edit-users-form").valid()){
            $(".submit-btn").addClass("disabled");
            $('button[type=submit], input[type=submit]').prop('disabled',true);
            $.ajax({
                type: 'PATCH',
                url: '{{url("/users")}}/{{$user->id}}',
                data: $('#edit-users-form').serialize()+'&icon='+old_pic,
                dataType: 'json',
                success: function(data){
                  $('button[type=submit], input[type=submit]').prop('disabled',false);
                  $(".submit-btn").removeClass("disabled");
                }
            }).done(function(data) {
              if(data.success){
                $("#users-table").DataTable().ajax.url( '/get-users' ).load();
                notiff('Success!', data.msg,'success'); //title, msg, type 
              }else{
                notiff('Error!', data.msg,'error'); //title, msg, type 
              }
            }).error(function(data) {
                notiff('Error!', data.msg, 'warning'); //title, msg, type
                 
            });
          }
        });

      $("#edit-pass-form").on('submit', function(e){ 
          e.preventDefault();
           window.history.pushState('',document.title,(window.location.href).split("?")[0]);
          var that = this;
          if($("#edit-pass-form").valid()){
            $(".submit-btn").addClass("disabled");
            $('button[type=submit], input[type=submit]').prop('disabled',true);
            $.ajax({
                type: 'PATCH',
                url: '{{url("/changepassword")}}/{{$user->id}}',
                data: $('#edit-pass-form').serialize(),
                dataType: 'json',
                success: function(data){
                  $('button[type=submit], input[type=submit]').prop('disabled',false);
                  $(".submit-btn").removeClass("disabled");
                }
            }).done(function(data) {
              if(data.success){ 
                //notiff('Success!', data.msg,'success'); //title, msg, type
                $('.pass-success').fadeIn();
               //setTimeout(function(){ window.location.reload(); }, 2000);

              }else{
                notiff('Error!', data.msg,'error'); //title, msg, type 
              }
            }).error(function(data) {
                notiff('Error!', data.msg, 'warning'); //title, msg, type 
            });
          }
        });
           window.history.pushState('',document.title,(window.location.href).split("?")[0]);
</script>
@endsection