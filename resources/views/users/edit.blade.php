<div class="modal-dialog modal-lg edit-user-form">
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button>
      <h4 class="modal-title">Edit user Form</h4>
    </div>


    <form action="{{url('/image-upload')}}"  class="hidden" id="upload-profile-picture-form"  >
        {{ csrf_field() }} 
          <input type="file" name="file" id="file_img">
    </form> 
    {!! Form::open(array('url' => url('/users/'.$user->id), 'method' => 'PATCH', 'id' => 'edit-users-form')) !!}
    <div class="modal-body">


      <h5>Profile Picture</h5>
        
      <div class="profile_img profile_img__ text-center p10"> 
        <div class="col-md-12 img_preview">

            @if($user->people->gender == 'male' && sizeof($user->icon) == 0)     
              <img class=" avatar-view" src="{{asset('/dst/images/user.png')}}" alt="Avatar" title=" ">
            @elseif($user->people->gender == 'female' && sizeof($user->icon) == 0) 
              <img class=" avatar-view" src="{{asset('/dst/images/picture.jpg')}}" alt="Avatar" title=" ">
            @elseif(sizeof($user->icon) > 0) 
              <img class=" avatar-view" src="{{asset('uploads/'.$user->icon)}}" alt="Avatar" title=" ">
            @endif
        </div> 
        <div class="btn btn-success upload-btn fileinput-new"> <i class="fa fa-upload"></i> Upload</div>
        <div class="btn btn-info upload-btn fileinput-exists" style="display: none;"> <i class="fa fa-refresh"></i> Change</div>
      </div>
      
      <h5>Personal Information</h5> 
        <input type="hidden" name="people_id" value="{{$user->people->id}}">
        <input type="hidden" name="address_id" value="{{$user->people->address->id}}">
        <input type="hidden" name="contact_id" value="{{$user->people->contact->id}}">
        <input type="hidden" name="user_id" value="{{$user->id}}">
        <div class="row">
          <div class="form-group col-md-6">
            <label for="title">Title</label>
            <input id="title" name="title" type="text" class="form-control" placeholder="Title" required value="{{$user->people->title}}">
          </div>
        </div>
        <div class="row">
          <div class="form-group col-md-6">
            <label for="firstname">Firstname</label>
            <input id="firstname" name="firstname" type="text" class="form-control" placeholder="Firstname" required value="{{$user->people->firstname}}">
          </div>
          <div class="form-group col-md-6">
            <label for="lastname">Lastname</label>
            <input id="lastname" name="lastname" type="text" class="form-control" placeholder="Lastname" required value="{{$user->people->lastname}}">
          </div>
        </div>
        <div class="row">
          <div class="form-group col-md-6">
            <label for="middlename">Middlename</label>
            <input id="middlename" name="middlename" type="text" class="form-control" placeholder="Middlename" required value="{{$user->people->middlename}}">
          </div>
        </div>
        <div class="row mt15">
          
          <div class="col-md-6 xdisplay_inputx form-group has-feedback">
            <label for="birthdate">Birthdate</label>
              <input name="birthdate" type="text" class="form-control has-feedback-left date-picker" id="single_cal4" placeholder="Birthdate" aria-describedby="" value="{{date('m/d/Y', strtotime($user->people->birthdate))}}">
              <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
          </div>
          <div class="form-group col-md-6">
            <label for="birthplace">Birthplace</label>
            <input id="birthplace" name="birthplace" type="text" class="form-control" placeholder="Birthplace" required value="{{$user->people->birthplace}}">
          </div>
        </div>

        <div class="row mt15">
          <div class="form-group col-md-6">
            <label for="gender">Gender</label>
            <select name="gender" id="gender" class="form-control">
              <option disabled selected>Select Gender</option>
              <option value="male" {{$user->people->gender == 'male' ? 'selected':''}}>Male</option>
              <option value="female" {{$user->people->gender == 'female' ? 'selected':''}}>Female</option>
            </select>
          </div>
          <div class="form-group col-md-6">
            <label for="civil_status">Civil Status</label>
            <select name="civil_status" id="civil_status" class="form-control">
              <option disabled selected>Select Civil Status</option>
              <option value="single"  {{$user->people->civil_status == 'single' ? 'selected':''}}>Single</option>
              <option value="married"  {{$user->people->civil_status == 'married' ? 'selected':''}}>Married</option>
              <option value="widowed"  {{$user->people->civil_status == 'widowed' ? 'selected':''}}>Widowed</option>
              <option value="divorced"  {{$user->people->civil_status == 'divorced' ? 'selected':''}}>Divorced</option>
            </select>
          </div>
        </div>

        <div class="row mt15">
          <div class="form-group col-md-6">
            <label for="nationality">Nationality</label>
            <input id="nationality" name="nationality" type="text" class="form-control" placeholder="Nationality" required value="{{$user->people->nationality}}">
          </div>
        </div>


        <h5 class="mt25">Contact Information</h5> 
        <div class="row">
          <div class="form-group col-md-6">
            <label for="mobile">Mobile No.</label>
            <input id="mobile" name="mobile" type="number" class="form-control" placeholder="Mobile Number" required value="{{$user->people->contact->mobile}}">
          </div>
          <div class="form-group col-md-6">
            <label for="tel">Landline No.</label>
            <input id="tel" name="tel" type="text" class="form-control" placeholder="Landline Number" required value="{{$user->people->contact->telephone}}">
          </div>
        </div>


        <h5 class="mt25">Address Information</h5> 
        <div class="row">
          <div class="form-group col-md-6">
            <label for="street">Street</label>
            <input id="street" name="street" type="text" class="form-control" placeholder="Street" required value="{{$user->people->address->street}}">
          </div>
        </div>
        <div class="row">
          <div class="form-group col-md-6">
            <label for="city">City</label>
            <input id="city" name="city" type="text" class="form-control" placeholder="City" required value="{{$user->people->address->city}}">
          </div>
          <div class="form-group col-md-6">
            <label for="province">Province</label>
            <input id="province" name="province" type="text" class="form-control" placeholder="Province" required value="{{$user->people->address->province}}">
          </div>
        </div>

        <h5 class="mt25">Credential Information</h5> 
        <div class="row">
          <div class="form-group col-md-6">
            <label for="type">User Type</label>
            <select name="type" id="type" class="form-control">
              <option disabled>Select User Type</option>
              <option value="doctor" {{$user->type == 'doctor' ? 'selected':''}}>Doctor</option>
              <option value="secretary" {{$user->type == 'secretary' ? 'selected':''}}>Secretary</option>
              <option value="patient" {{$user->type == 'patient' ? 'selected':''}}>Patient</option>
            </select>
          </div>
        </div>
        <div class="row">
          <div class="form-group col-md-6">
            <label for="email">Email</label>
            <input id="email" name="email" type="email" class="form-control" placeholder="Email" required value="{{$user->email}}">
          </div>
        </div>
        <div class="row">
          <div class="form-group col-md-6">
            <label for="password">Old Password</label>
            <input id="password" name="password" type="password" class="form-control" placeholder="Password" required>
          </div>     
        </div>
        <div class="row">
          <div class="form-group col-md-6">
            <label for="new_password">New Password</label>
            <input id="new_password" name="new_password" type="password" class="form-control" placeholder="New Password" required>
          </div>
          <div class="form-group col-md-6">
            <label for="confirm_new_password">Confirm New Password</label>
            <input id="confirm_new_password" name="confirm_new_password" type="password" class="form-control" placeholder="Confirm New Password" required>
          </div>     
        </div>


      <div style="width: 100%;">&nbsp;</div>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        {!! Form::submit('Submit', ['class' => 'btn submit-btn btn-primary btn-gradient pull-right']) !!}
      {!! Form::close() !!}
    </div>

  </div>
</div>


 <!-- Laravel Javascript Validation -->
 <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>

 {!! JsValidator::formRequest('App\Http\Requests\EditUserRequest', '#edit-users-form') !!}

<script type="text/javascript">
  $(function(){
      
      var old_pic = '{{$user->icon}}';
      var formData = new FormData();
      $("#file_img").change(function(){ 
          formData.append('file', $('#file_img')[0].files[0]);
          formData.append('old_file', old_pic);
          formData.append('_token', $('[name="_token"]').val());
          formData.append('user_id', $('[name="user_id"]').val());
          $.ajax({
                type: 'POST',
                url: '/uploadPicture',
                data: formData,
                processData: false,
                contentType: false,
                success: function(data){
                  console.log(data);
                  if(data.success){
                    $('.img_preview img').attr('src', '/uploads/'+data.img);
                    $('.fileinput-new').hide();
                    $('.fileinput-exists').show();
                    old_pic = data.img; 
                  }else{ 

                  } 
                },
               beforeSend: function(){
                  $('.img_preview img').attr('src', '/img/spin.gif');
               }
            });
      });
      

      $(document).off('click', '.upload-btn').on('click', '.upload-btn', function(e){
        e.preventDefault(); 
        $('#file_img').click();
      }); 
     $('.date-picker').datepicker({
          autoclose: true,
          todayHighlight: true
        })
        //show datepicker when clicking on the icon
        .next().on(ace.click_event, function(){
          $(this).prev().focus();
        });
      $("#edit-users-form").on('submit', function(e){ 
          e.preventDefault();
          var that = this;
          if($("#edit-users-form").valid()){
            $(".submit-btn").addClass("disabled");
            $('button[type=submit], input[type=submit]').prop('disabled',true);
            $.ajax({
                type: 'PATCH',
                url: '{{url("/users")}}/{{$user->id}}',
                data: $('#edit-users-form').serialize()+'&icon='+old_pic,
                dataType: 'json',
                success: function(data){
                  $('button[type=submit], input[type=submit]').prop('disabled',false);
                  $(".submit-btn").removeClass("disabled");
                }
            }).done(function(data) {
              if(data.success){
                $("#users-table").DataTable().ajax.url( '/get-users' ).load();
                notiff('Success!', data.msg,'success'); //title, msg, type
                $('#editmodal').modal('toggle');
              }else{
                notiff('Error!', data.msg,'error'); //title, msg, type
                $('#editmodal').modal('toggle');
              }
            }).error(function(data) {
                notiff('Error!', data.msg, 'warning'); //title, msg, type
                $('#editmodal').modal('toggle');
            });
          }
        });
      $("#password").on('blur', function(){
         var that = this; 
         $.ajax({
            type: 'GET',
            url: '/check-password/'+{{$user->id}},
            data: 'password='+$(that).val(),
            dataType: 'json',
            success: function(data){
              if(data){
                $(that).removeClass('invalid');
                $(that).removeClass('valid');
                $(that).addClass('valid');
                $(that).parent().removeClass('has-error');
              }else{              
                $(that).parent().addClass('has-error');
                $(that).removeClass('invalid');
                $(that).removeClass('valid');   
                $(that).addClass('invalid');
              }
            }
          });
      });
  });  
 </script>


<style type="text/css">
  .edit-user-form h5{
      margin: 0px;
      margin-bottom: 10px;
      padding: 10px 5px 10px;
      border-bottom: 1px solid;
      background: #096bb1;
      color: #fff;
      font-weight: bold;
  } 
</style>