					
				</div>
			</div><!-- /.main-content -->

			<div class="footer hidden">
				<div class="footer-inner">
					<div class="footer-content">
						<span class="bigger-120">
							<span class="blue bolder"></span>
							&copy; 2013-2014
						</span>

						&nbsp; &nbsp;
						<span class="action-buttons">
							<a href="#">
								<i class="ace-icon fa fa-twitter-square light-blue bigger-150"></i>
							</a>

							<a href="#">
								<i class="ace-icon fa fa-facebook-square text-primary bigger-150"></i>
							</a>

							<a href="#">
								<i class="ace-icon fa fa-rss-square orange bigger-150"></i>
							</a>
						</span>
					</div>
				</div>
			</div>

			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
			</a>
		</div><!-- /.main-container -->

		<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;" id="addtransmodal"></div>
		<!-- basic scripts -->

		<!--[if !IE]> -->
		<script src="{{asset('admin/assets/js/jquery-2.1.4.min.js')}}"></script> 
		<!-- <![endif]-->

		<!--[if IE]>
		<script src="assets/js/jquery-1.11.3.min.js"></script>
		<![endif]-->
		<script type="text/javascript">
			if('ontouchstart' in document.documentElement) document.write("<script src='assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
		</script>
		<script src="{{asset('admin/assets/js/bootstrap.min.js')}}"></script>

		<!-- page specific plugin scripts -->
		<script src="{{asset('admin/assets/js/jquery.dataTables.min.js')}}"></script>
		<script src="{{asset('admin/assets/js/jquery.dataTables.bootstrap.min.js')}}"></script>
		<script src="{{asset('admin/assets/js/dataTables.buttons.min.js')}}"></script>
		<script src="{{asset('admin/assets/js/buttons.flash.min.js')}}"></script>
		<script src="{{asset('admin/assets/js/buttons.html5.min.js')}}"></script>
		<script src="{{asset('admin/assets/js/buttons.print.min.js')}}"></script>
		<script src="{{asset('admin/assets/js/buttons.colVis.min.js')}}"></script>
		<script src="{{asset('admin/assets/js/dataTables.select.min.js')}}"></script>

	    <script src="{{asset('dst/vendors/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
	    <script src="{{asset('dst/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js')}}"></script>

		<!-- ace scripts -->
		<script src="{{asset('admin/assets/js/ace-elements.min.js')}}"></script>
		<script src="{{asset('admin/assets/js/ace.min.js')}}"></script>
	<!-- 	<script src="{{asset('admin/assets/js/ajax.js')}}"></script> -->

	   	<script src="{{asset('dst/vendors/nprogress/nprogress.js')}}"></script>
	   	
		<script src="{{asset('admin/assets/js/bootstrap-datepicker.min.js')}}"></script>
		<script src="{{asset('admin/assets/js/jquery.gritter.min.js')}}"></script>
		<script src="{{asset('admin/assets/js/chosen.jquery.min.js')}}"></script>
		<script src="{{asset('admin/assets/js/bootbox.js')}}"></script>
		<script src="{{asset('js/selectize.js')}}"></script> 

<!-- 
		<link href="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet"/>
		<script src="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/js/bootstrap-editable.min.js"></script>
 -->
		<script src="{{asset('js/moment-with-locales.js')}}"></script>
		<script src="{{asset('js/bootstrap-datetimepicker.js')}}"></script>
		<script src="{{asset('dst/vendors/dropzone/dist/min/dropzone.min.js')}}"></script> 
		

    <script src="{{asset('dst/vendors/lightgallery/js/lightgallery.min.js')}}"></script>
    <script src="{{asset('dst/vendors/lightgallery/js/jquery.mousewheel.min.js')}}"></script>
    
    <script src="{{asset('dst/vendors/lightgallery/js/lightgallery.js')}}"></script>
    <script src="{{asset('dst/vendors/lightgallery/js/lg-fullscreen.js')}}"></script>
    <script src="{{asset('dst/vendors/lightgallery/js/lg-thumbnail.js')}}"></script>
    <script src="{{asset('dst/vendors/lightgallery/js/lg-video.js')}}"></script>
    <script src="{{asset('dst/vendors/lightgallery/js/lg-autoplay.js')}}"></script>
    <script src="{{asset('dst/vendors/lightgallery/js/lg-zoom.js')}}"></script>
    <script src="{{asset('dst/vendors/lightgallery/js/lg-hash.js')}}"></script>
    <script src="{{asset('dst/vendors/lightgallery/js/lg-pager.js')}}"></script>
		<script type="text/javascript">
			/*$(document).keydown(function(e) {
		        if (e.keyCode == 82 && e.ctrlKey) {
					e.preventDefault(); 
		        }
		    });*/

	        $(document).off('click', '.addTransBtn').on('click', '.addTransBtn', function(){ 
	            var that = this;
	            $("#addtransmodal").modal();
	            $.ajax({
	                url: '/transactions/create',                    
	                success: function(data) {
	                    $("#addtransmodal").html(data);
	                }
	            }); 

	        });
			function notiff(title, msg, type){
				
				$.gritter.add({ 
					title: title, 
					text: msg,
					time: '3000',
					class_name: 'gritter-'+type+' gritter-light'
				});
				return false;


			}
			$(function(){
				$('a[href="'+window.location.pathname+'"').parent().addClass('active');
				if($('a[href="'+window.location.pathname+'"').parent().hasClass('submenu_li')){
					$('a[href="'+window.location.pathname+'"').parent().parent().parent().addClass('open');
					$('a[href="'+window.location.pathname+'"').parent().parent().css('display', 'block');
				}
			});
		</script>