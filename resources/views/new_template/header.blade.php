	<div id="navbar" class="navbar navbar-default          ace-save-state">
			<div class="navbar-container ace-save-state" id="navbar-container">
				<button type="button" class="navbar-toggle menu-toggler pull-left" id="menu-toggler" data-target="#sidebar">
					<span class="sr-only">Toggle sidebar</span>

					<span class="icon-bar"></span>

					<span class="icon-bar"></span>

					<span class="icon-bar"></span>
				</button>

				<div class="navbar-header pull-left">
					<a href="{{url('/')}}" class="navbar-brand">
						<b>CLINIQ</b>
						<small>
							ACCESS PORTAL
						</small>
					</a>
				</div>

				<div class="navbar-buttons navbar-header pull-right" role="navigation">
					<ul class="nav ace-nav"> 

						<li class="notiff_dropdown dropdown-modal">
							<a data-toggle="dropdown" class="" href="#">
								<i class="ace-icon fa fa-calendar icon-animated-bell"></i>
								@php
									$appts = \App\Appointments::selectRaw('appointments.*, appointments.id as apt_id')
								                                ->join('patients', 'patients.id', '=', 'patient_id')
								                                ->where('appointments.datetime', '>', date('Y-m-d H:i:00'))    
								                                ->orderBy('datetime', 'ASC')->get();
								@endphp
								<span class="badge badge-important">{{sizeof($appts)}}</span>
							</a>

							<ul class="dropdown-menu-right dropdown-navbar navbar-pink dropdown-menu dropdown-caret dropdown-close">
								<li class="dropdown-header">
									<i class="ace-icon fa fa-exclamation-triangle"></i>
									{{sizeof($appts)}} Upcoming Appointments
								</li>

								<li class="dropdown-content">
									<ul class="dropdown-menu dropdown-navbar navbar-pink">  
										@foreach($appts as $appt)
											<li> 
												<a href="javascript:void(0);">
													<i class="btn btn-xs btn-primary fa fa-user"></i>
													{{$appt->patient->user->people->firstname.' '.$appt->patient->user->people->lastname}} <br> <b>{{date('F d, Y h:i A', strtotime($appt->datetime))}}</b>
												</a>
											</li>
										@endforeach
									</ul>
								</li>

								<li class="dropdown-footer">
									<a href="/schedules">
										Go to Schedules
										<i class="ace-icon fa fa-arrow-right"></i>
									</a>
								</li>
							</ul>
						</li>
 
						<li class="light-blue dropdown-modal">
							<a data-toggle="dropdown" href="#" class="dropdown-toggle">
								
								<span class="user-info"> 
									@php
										$mid = " ".substr(ucfirst(Auth::user()->people->middlename),0,1);
										if(strlen(Auth::user()->people->middlename) > 0){
											$mid.=". ";
										}else{
											$mid = ' ';
										}
									@endphp
									{{ucfirst(Auth::user()->people->title)." ".ucfirst(Auth::user()->people->firstname).$mid.ucfirst(Auth::user()->people->lastname)}}
								</span>
								@if(sizeof(Auth::user()->icon) > 0)									
									<img class="nav-user-photo" src="{{asset('uploads/'.Auth::user()->icon)}}" alt="Profile Pic" />
								@else
									<img class="nav-user-photo" src="{{asset('dst/images/user.png')}}" alt="Profile Pic" />
								@endif
								<i class="ace-icon fa fa-caret-down"></i>
							</a>

							<ul class="user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">  

 								@if(Auth::user()->type == 'doctor')
 								<li>
 									<a href="{{url('settings/'.Auth::user()->id)}}"><i class="ace-icon fa fa-cog"></i> Settings</a>
 								</li>
 								@endif
 								<li>
 									<a href="{{url('users/'.Auth::user()->id)}}"><i class="ace-icon fa fa-user"></i> Profile</a>
 								</li>
								<li>
									<a href="{{ url('/logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
										<i class="ace-icon fa fa-power-off"></i>
										Logout
									</a>
									<form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
								</li>  
							</ul>
						</li>
					</ul>
				</div>
			</div><!-- /.navbar-container -->
		</div>
