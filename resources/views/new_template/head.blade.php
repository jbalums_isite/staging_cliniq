<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title>Cliniq | Admin</title>

		<link rel="shortcut icon" href="{{asset('tmpl/images/cliniq-logo.png')}}">

		<meta name="description" content="" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />


		<meta name="csrf-token" content="{{ csrf_token() }}">
		
		
<!-- 		<link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}" />   -->
		<link rel="stylesheet" href="{{asset('css/dataTables.bootstrap.min.css')}}" />  
		<link rel="stylesheet" href="{{asset('css/responsive.bootstrap.min.css')}}" />  
		<!-- bootstrap & fontawesome -->
<!-- 		<link href="css/bootstrap.min.css" rel="stylesheet"> -->
		<link rel="stylesheet" href="{{asset('admin/assets/css/bootstrap.min.css')}}" /> 

		<!-- page specific plugin styles -->

		<!-- text fonts -->
		<link rel="stylesheet" href="{{asset('admin/assets/css/fonts.googleapis.com.css')}}" />

		<!-- ace styles -->
		<link rel="stylesheet" href="{{asset('admin/assets/css/ace.min.css')}}" class="ace-main-stylesheet" id="main-ace-style" />

		<!--[if lte IE 9]>
			<link rel="stylesheet" href="{{asset('admin/assets/css/ace-part2.min.css')}}" class="ace-main-stylesheet" />
		<![endif]-->
		<link rel="stylesheet" href="{{asset('admin/assets/css/ace-skins.min.css')}}" />
		<link rel="stylesheet" href="{{asset('admin/assets/css/ace-rtl.min.css')}}" />

		<!--[if lte IE 9]>
		  <link rel="stylesheet" href="{{asset('admin/assets/css/ace-ie.min.css')}}" />
		<![endif]-->

		<!-- inline styles related to this page -->

		<!-- ace settings handler -->
		<script src="{{asset('admin/assets/js/ace-extra.min.js')}}"></script>

		<!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries --> 
		<!--[if lte IE 8]>
		<script src="{{asset('admin/assets/js/html5shiv.min.js')}}"></script>
		<script src="{{asset('admin/assets/js/respond.min.js')}}"></script>
		<![endif]-->
		<link rel="stylesheet" type="text/css" href="{{asset('tmpl/fonts/font-awesome-4.7.0/css/font-awesome.css')}}">

	<!-- 	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet"> -->
		<link rel="stylesheet" type="text/css" href="{{asset('css/admin.css')}}">
		
	    <!-- NProgress -->
	   <link rel="stylesheet" type="text/css" href="{{asset('dst/vendors/nprogress/nprogress.css')}}">
	   
		<!-- page specific plugin styles -->
		<link rel="stylesheet" href="{{asset('admin/assets/css/jquery-ui.custom.min.css')}}" />
		<link rel="stylesheet" href="{{asset('admin/assets/css/chosen.min.css')}}" />
		<link rel="stylesheet" href="{{asset('admin/assets/css/bootstrap-datepicker3.min.css')}}" />
		<link rel="stylesheet" href="{{asset('admin/assets/css/bootstrap-timepicker.min.css')}}" />
		<link rel="stylesheet" href="{{asset('admin/assets/css/daterangepicker.min.css')}}" />
		<link rel="stylesheet" href="{{asset('admin/assets/css/bootstrap-datetimepicker.min.css')}}" />
		<link rel="stylesheet" href="{{asset('admin/assets/css/bootstrap-colorpicker.min.css')}}" />
		
		<link rel="stylesheet" href="{{asset('admin/assets/css/jquery.gritter.min.css')}}" />
		<link rel="stylesheet" href="{{asset('css/selectize.bootstrap3.css')}}" />  
		<link rel="stylesheet" href="{{asset('css/bootstrap-datetimepicker.css')}}" />  
	    
	    <!-- Dropzone.js -->
		<link rel="stylesheet" href="{{asset('dst/vendors/dropzone/dist/min/dropzone.min.css')}}" />   
		<link rel="stylesheet" href="{{asset('dst/vendors/lightgallery/css/lightgallery.css')}}" />   


	</head>

	<body class="no-skin">
	
