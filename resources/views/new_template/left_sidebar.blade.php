	<div class="main-container ace-save-state" id="main-container">
			<script type="text/javascript">
				try{ace.settings.loadState('main-container')}catch(e){}
			</script>

			<div id="sidebar" class="sidebar                  responsive                    ace-save-state">
				<script type="text/javascript">
					try{ace.settings.loadState('sidebar')}catch(e){}
				</script>

				
				<div class="breadcrumbs ace-save-state breadcrumb2" id="breadcrumbs"> 
 					<div class="sidebar-collapse" id="sidebar-collapse"> 
						<i id="sidebar-toggle-icon" class="ace-icon fa fa-angle-double-left ace-save-state" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
					</div>
				</div> 
				<ul class="nav nav-list left_sidebar">
					<li class="{{Auth::user()->setting[0]->dasboard ? '':' hidden'}}">
						<a href="{{url('')}}" class="non-ajax">
							<i class="menu-icon fa fa-tachometer"></i>
							<span class="menu-text"> Dashboard </span>
						</a> 
					</li> 
					<li class="{{Auth::user()->setting[0]->patient ? '':' hidden'}}">
						<a href="/patients" id="patients_menu">
							<i class="menu-icon fa fa-user"></i>
							<span class="menu-text"> Patient </span>
						</a> 
					</li> 
					<li class="{{Auth::user()->setting[0]->medicine ? '':' hidden'}}">
						<a href="/medicines">
							<i class="menu-icon fa fa-medkit"></i>
							<span class="menu-text"> Medicine </span>
						</a> 
					</li> 
					<li class="{{Auth::user()->setting[0]->services ? '':' hidden'}}">
						<a href="/services">
							<i class="menu-icon fa fa-stethoscope"></i>
							<span class="menu-text"> Services </span>
						</a> 
					</li> 
					@if(Auth::user()->setting[0]->view_products || Auth::user()->setting[0]->stocks || Auth::user()->setting[0]->manufacturers || Auth::user()->setting[0]->brands)
					<li class="">
						<a href="#" class="dropdown-toggle">
							<i class="menu-icon fa fa-cubes"></i>
							<span class="menu-text"> Products </span>

							<b class="arrow fa fa-sort-down"></b>
						</a>

						<b class="arrow"></b>

						<ul class="submenu">
							<li class=" submenu_li {{Auth::user()->setting[0]->view_products ? '':' hidden'}}">
								<a href="/products">
									<i class="menu-icon fa fa-caret-right"></i>
									View Products
								</a>
								<b class="arrow"></b>
							</li>
							<li class=" submenu_li {{Auth::user()->setting[0]->stocks ? '':' hidden'}}">
								<a href="/inventories">
									<i class="menu-icon fa fa-caret-right"></i>
									Stocks
								</a>
								<b class="arrow"></b>
							</li>
							<li class=" submenu_li {{Auth::user()->setting[0]->manufacturers ? '':' hidden'}}">
								<a href="/manufacturers">
									<i class="menu-icon fa fa-caret-right"></i>
									Manufacturers
								</a>
								<b class="arrow"></b>
							</li>
							<li class=" submenu_li {{Auth::user()->setting[0]->brands ? '':' hidden'}} ">
								<a href="/brands">
									<i class="menu-icon fa fa-caret-right"></i>
									Brands
								</a>
								<b class="arrow"></b>
							</li>
 
						</ul>
			   		</li> 
					@endif
					

					<li class="{{Auth::user()->setting[0]->schedules ? '':' hidden'}}">
						<a href="/schedules" id="sched-lnk">
							<i class="menu-icon fa fa-calendar"></i>
							<span class="menu-text"> Schedules </span>
						</a> 
					</li> 

					@if(Auth::user()->setting[0]->referral_form || Auth::user()->setting[0]->request_form || Auth::user()->setting[0]->custom_form)
					<li class="">
						<a href="#" class="dropdown-toggle">
							<i class="menu-icon fa fa-copy"></i>
							<span class="menu-text"> Forms </span>

							<b class="arrow fa fa-sort-down"></b>
						</a>

						<b class="arrow"></b>

						<ul class="submenu">
							<li class=" submenu_li {{Auth::user()->setting[0]->referral_form ? '':' hidden'}}">
								<a href="/referral_form">
									<i class="menu-icon fa fa-caret-right"></i>
									Referral Form
								</a> 
								<b class="arrow"></b>
							</li> 
							<li class=" submenu_li {{Auth::user()->setting[0]->request_form ? '':' hidden'}}">
								<a href="/request_form">
									<i class="menu-icon fa fa-caret-right"></i>
									Request Form
								</a> 
								<b class="arrow"></b>
							</li> 
							<li class=" submenu_li {{Auth::user()->setting[0]->custom_form ? '':' hidden'}}">
								<a href="/custom_form">
									<i class="menu-icon fa fa-caret-right"></i>
									Custom Form
								</a> 
								<b class="arrow"></b>
							</li> 
						</ul>
			   		</li>
			   		@endif
			   		

					@if(Auth::user()->setting[0]->dialy_reports || Auth::user()->setting[0]->monthly_reports || Auth::user()->setting[0]->quarterly_reports || Auth::user()->setting[0]->annual_reports)
					<li class=""  style="display:{{Auth::user()->type == 'secretary' ? 'none' : 'block'}};">
						<a href="#" class="dropdown-toggle">
							<i class="menu-icon fa fa-file-text-o"></i>
							<span class="menu-text"> Reports</span>

							<b class="arrow fa fa-sort-down"></b>
						</a>

						<b class="arrow"></b>

						<ul class="submenu">
							<li class=" submenu_li {{Auth::user()->setting[0]->dialy_reports ? '':' hidden'}}">
								<a href="/daily_sales_reports">
									<i class="menu-icon fa fa-caret-right"></i>
									Daily
								</a> 
								<b class="arrow"></b>
							</li> 
							<li class=" submenu_li {{Auth::user()->setting[0]->monthly_reports ? '':' hidden'}}">
								<a href="/monthly_sales_reports">
									<i class="menu-icon fa fa-caret-right"></i>
									Monthly
								</a> 
								<b class="arrow"></b>
							</li> 
							<li class=" submenu_li {{Auth::user()->setting[0]->quarterly_reports ? '':' hidden'}}">
								<a href="/quarterly_sales_reports">
									<i class="menu-icon fa fa-caret-right"></i>
									Quarterly
								</a> 
								<b class="arrow"></b>
							</li> 
							<li class=" submenu_li {{Auth::user()->setting[0]->annual_reports ? '':' hidden'}}">
								<a href="/annualy_sales_reports">
									<i class="menu-icon fa fa-caret-right"></i>
									Annually
								</a> 
								<b class="arrow"></b>
							</li> 
						</ul>
			   		</li>
			   		@endif


					@if(Auth::user()->setting[0]->users || Auth::user()->setting[0]->allergies)
					<li class="">
						<a href="#" class="dropdown-toggle">
							<i class="menu-icon fa fa-cogs"></i>
							<span class="menu-text"> Settings </span>

							<b class="arrow fa fa-sort-down"></b>
						</a>

						<b class="arrow"></b>

						<ul class="submenu">
							<li class=" submenu_li {{Auth::user()->setting[0]->users}} {{Auth::user()->setting[0]->users ? '':' hidden'}}">
								<a href="/users">
									<i class="menu-icon fa fa-caret-right"></i>
									Users
								</a> 
								<b class="arrow"></b>
							</li> 
							<li class=" submenu_li {{Auth::user()->setting[0]->allergies ? '':' hidden'}}">
								<a href="/allergieslist">
									<i class="menu-icon fa fa-caret-right"></i>
									Allergies
								</a> 
								<b class="arrow"></b>
							</li>  
						</ul>
			   		</li>
			   		@endif



				</ul><!-- /.nav-list -->

			</div>

			<div class="main-content">
				<div class="main-content-inner">
		
			 