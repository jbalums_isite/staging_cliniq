@extends('new_template.app')
 
@section('content') 
<div class="breadcrumbs ace-save-state" id="breadcrumbs">
	<ul class="breadcrumb">
		<li>
			<a href="/">Home</a>
		</li>
		<li> 
			Products
		</li> 
		<li> 
			Brands
		</li> 
	</ul><!-- /.breadcrumb -->

	
	<ul class="breadcrumb pull-right action_buttons">
		<li> 
			<a href="#" class="addTransBtn">
				<i class="fa fa-plus"></i>
				Add Transactions
			</a> 
		</li>  
		<li> 
			<a href="javascript:void(0)" class="addBrandsBtn">
				<i class="fa fa-plus"></i>
				Add Brands
			</a> 
		</li>  
	</ul><!-- /.breadcrumb -->

</div>


<div class="page-content"> 
	<div class="row">
		<div class="col-xs-12 col-md-7">

			<table class="table jambo_table theme-table theme-table-edit dt-responsive nowrap " id="brands-table" width="100%">
				<thead>
					<tr> 
			<!-- 			<th><input type="checkbox" class="form-field-checkbox" name="select_all" value="1" id="example-select-all"></th>
						 -->	
						<th>#</th>
						<th>Name</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					
				</tbody>
			</table>  

		</div><!-- /.col -->
	</div><!-- /.row -->
</div><!-- /.page-content -->
  <!-- /page content -->

<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;" id="addmodal"></div>
<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;" id="editmodal"></div>
<style type="text/css">
	tbody > tr:hover{
		cursor: pointer;
	} 
	.search_bar_{
		display: block;
	}
	.ppi_{
		display: none;
	}
	@media (max-width: 768px){
 
		.p15{
			padding: 5px !important;
			padding-left: 5px !important;
		    margin-right: 0px;
		    margin-left: 0px;
		}
		.s_bar_{
			float: right;
		    margin-right: 5px;
		    margin-top: 0px;
		}
		.search_bar_ {
			width: 100%;
		} 
		.ppi_{
			display: block;
		}
	}
</style>
@endsection 

@section('script_content')
<script type="text/javascript">
	$(function(){

		var table = $('#brands-table').DataTable({
		    bProcessing: true,
		    bServerSide: false,
		    sServerMethod: "GET",
			'ajax': '/get-brands',
			searching: true, 
			paging: true, 
			filtering:false, 
			bInfo: true,
			responsive: true,
			language:{
				"paginate": {
			        "next":       "<i class='fa fa-chevron-right'></i>",
			        "previous":   "<i class='fa fa-chevron-left'></i>"
			    }
			},
			"columns": [ 
/*				{className: 'text-center', searchable:false},*/
	            {data: 'id', name: 'id', className: 'text-center hidden', searchable: false, sortable: true},
	            {data: 'brand_name', name: 'brand_name', className: 'col-md-11 text-left', searchable: true,	sortable: true}, 	
	            {data: 'actions', name: 'actions', 	className: 'text-center', searchable: false, sortable: false},
	        ],
	        /* 'columnDefs': [{
		         'targets': 0,
		         'searchable': false,
		         'orderable': false,
		         'className': 'dt-body-center',
		         'render': function (data, type, full, meta){
		             return '<input type="checkbox" name="id[]" class="form-field-checkbox" value="' + $('<div/>').text(data).html() + '">';
		         }
		      }],*/
      		'order': [[1, 'asc']]
		});

		/*// Handle click on "Select all" control
	    $('#example-select-all').on('click', function(){
	      // Get all rows with search applied
	      var rows = table.rows({ 'search': 'applied' }).nodes();
	      // Check/uncheck checkboxes for all rows in the table
	      $('input[type="checkbox"]', rows).prop('checked', this.checked);
	   });

	   // Handle click on checkbox to set state of "Select all" control
		$('#brands-table tbody').on('change', 'input[type="checkbox"]', function(){
	      // If checkbox is not checked
	      if(!this.checked){
	         var el = $('#example-select-all').get(0);
	         // If "Select all" control is checked and has 'indeterminate' property
	         if(el && el.checked && ('indeterminate' in el)){
	            // Set visual state of "Select all" control
	            // as 'indeterminate'
	            el.indeterminate = true;
	         }
	      }
	   });*/

		//initiate dataTables plugin
		
		$(".addBrandsBtn").click(function(x){  
			x.preventDefault();
			var that = this;
			$("#addmodal").modal();
			$.ajax({
				url: '/brands/create',					
				success: function(data) {
					$("#addmodal").html(data);
				}
			});	
		});

		var p_table = $('#brands-table').DataTable(); 
		$('#patient_search').keyup(function(){ 
		    p_table.search($(this).val()).draw();
		})

		$(document).off('click', "#brands-table > tbody > tr > td").on('click', "#brands-table > tbody > tr > td", function(x){  
			if(!$(this).is( "#brands-table > tbody > tr > td:last-child" ) && !$(this).is( "#brands-table > tbody > tr > td:first-child" )){
				loadPage('/brands/'+$(this).parent().children()[1].innerText);
			} 
		});

		$(document).off('click', ".edit-patient-btn").on('click', ".edit-patient-btn", function(x){ 
			var that = this;
			x.preventDefault();
			$("#editmodal").modal();
			$.ajax({
				url: '/brands/'+that.dataset.id+'/edit',					
				success: function(data) {
					$("#editmodal").html(data);
				}
			});	
		});

		
		 $(document).off('click', '.delete-patient-btn').on('click', '.delete-patient-btn', function(x){
            var that = this;
			x.preventDefault();
            bootbox.confirm({
              title: "Confirm Delete Brands?",
              className: "del-bootbox",
              message: "Are you sure you want to delete record?",
              buttons: {
                  confirm: {
                      label: 'Yes',
                      className: 'btn-success'
                  },
                  cancel: {
                      label: 'No',
                      className: 'btn-danger'
                  }
              },
              callback: function (result) {
                 if(result){
                  var token = '{{csrf_token()}}'; 
                  $.ajax({
                  url:'/brands/'+that.dataset.id,
                  type: 'post',
                  data: {_method: 'delete', _token :token},
                  success:function(data){
                	$("#brands-table").DataTable().ajax.url( '/get-brands' ).load();
                	notiff('Success!', data.msg,'success'); //title, msg, type
                  }
                  }); 
                 }
              }
          });
      });
		$(".status_choice").on('click', function(){
			alert(this.dataset.id);
		})

		setTimeout(resize_action_buttons_height, 100);
        function resize_action_buttons_height(){  
			$('.action_buttons').css('height',$('#breadcrumbs').height()+"px");
			$('.action_buttons a').css('height',$('#breadcrumbs').height()+"px");
        }
	});
	
</script>
@endsection