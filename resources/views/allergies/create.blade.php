<div class="modal-dialog modal-md">
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button>
      <h4 class="modal-title">Add Allergy Form</h4>
    </div>
    {!! Form::open(array('url' => url('/allergies'), 'method' => 'POST', 'id' => 'add-allergies-form')) !!}
    <div class="modal-body">
      <input type="hidden" name="patient_id" value="{{ $patient_id }}">
      <div class="form-group mb10">
        <label for="brand_id">Select Allergy</label>
        <select class="form-control" id="allergy_id">
          <option disabled selected>SELECT ALLERGY</option>
          @foreach($allergies as $allergy)
            <option value="{{$allergy->description}}" data-id="{{$allergy->name}}">{{$allergy->name}}</option>
          @endforeach
        </select>
      </div>
      <div class="form-group mb10">
        <label for="name">Name</label>
        <input id="name" name="name" type="text" class="form-control" placeholder="Name" required>
      </div>
      <div class="form-group mb10">
        <label for="description">Description</label>
        <textarea rows="3" class="form-control" name="description" id="description" placeholder="Description"></textarea>
      </div>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        {!! Form::submit('Submit', ['class' => 'btn submit-btn btn-primary btn-gradient pull-right']) !!}
      {!! Form::close() !!}
    </div>

  </div>
</div>


 <!-- Laravel Javascript Validation -->
 <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>

 {!! JsValidator::formRequest('App\Http\Requests\AddAllergiesRequest', '#add-allergies-form') !!}

<script type="text/javascript">
  $(function(){
    
      $('select').chosen({allow_single_deselect:true}); 
      $("#add-allergies-form").on('submit', function(e){
          e.preventDefault();
          var that = this;
          if($("#add-allergies-form").valid()){
            $(".submit-btn").addClass("disabled");
            $('button[type=submit], input[type=submit]').prop('disabled',true);
            $.ajax({
                type: 'POST',
                url: '/allergies',
                data: $('#add-allergies-form').serialize(),
                dataType: 'json',
                success: function(data){
                  $('button[type=submit], input[type=submit]').prop('disabled',false);
                  $(".submit-btn").removeClass("disabled");
                }
            }).done(function(data) {
              if(data.success){                
                /*var str = '<li>'+
                              '<b>'+data.name+'</b>'+
                              '<p>'+data.description+'</p>'+
                            '</li>';
                $('.allergies_list').append(str);*/
                refreshData('.allergies_list', 'allergies');
                notiff('Success!', data.msg,'success'); //title, msg, type
                $('#addallergymodal').modal('toggle');
              }else{
                notiff('Error!', data.msg,'error'); //title, msg, type
               // $('#addallergymodal').modal('toggle');
              }
            }).error(function(data) {
                notiff('Error!', data.msg, 'warning'); //title, msg, type
                //$('#addallergymodal').modal('toggle');
            });
          }
        });
      $("#allergy_id").change(function(){
          var that = $("#allergy_id option:selected"); 
          $('#name').val(that[0].dataset.id);
          $('#description').val($(this).val());
                
      });
     
  });  
 </script>