<div class="modal-dialog modal-md">
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button>
      <h4 class="modal-title">Edit Allergy Form</h4>
    </div>
    {!! Form::open(array('url' => url('/allergies'.$allergy->id), 'method' => 'PATCH', 'id' => 'edit-allergies-form')) !!} 
    <div class="modal-body">
      <input type="hidden" name="patient_id" value="{{ $patient_id }}">
      <div class="form-group mb10">
        <label for="name">Name</label>
        <input id="name" name="name" type="text" class="form-control" placeholder="Name" required value="{{$allergy->name}}">
      </div>
      <div class="form-group mb10">
        <label for="description">Description</label>
        <textarea rows="3" class="form-control" name="description" id="description" placeholder="Description">{{$allergy->description}}</textarea>
      </div>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        {!! Form::submit('Submit', ['class' => 'btn submit-btn btn-primary btn-gradient pull-right']) !!}
      {!! Form::close() !!}
    </div>

  </div>
</div>


 <!-- Laravel Javascript Validation -->
 <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>

 {!! JsValidator::formRequest('App\Http\Requests\AddAllergiesRequest', '#edit-allergies-form') !!}

<script type="text/javascript">
  $(function(){
      $("#edit-allergies-form").on('submit', function(e){
          e.preventDefault();
          var that = this;
          if($("#edit-allergies-form").valid()){
            $(".submit-btn").addClass("disabled");
            $('button[type=submit], input[type=submit]').prop('disabled',true);
            $.ajax({
                type: 'PATCH',
                url: '/allergies/'+{{$allergy->id}},
                data: $('#edit-allergies-form').serialize(),
                dataType: 'json',
                success: function(data){
                  $('button[type=submit], input[type=submit]').prop('disabled',false);
                  $(".submit-btn").removeClass("disabled");
                }
            }).done(function(data) {
              if(data.success){
                //$("#allergies-table").DataTable().ajax.url( '/get-allergies/'+$("[name='patient_id']").val() ).load();
                
                refreshData('.allergies_list', 'allergies');
                notiff('Success!', data.msg,'success'); //title, msg, type
                $('#editallergymodal').modal('toggle');
              }else{
                notiff('Error!', data.msg,'error'); //title, msg, type
               // $('#addallergymodal').modal('toggle');
              }
            }).error(function(data) {
                notiff('Error!', data.msg, 'warning'); //title, msg, type
                //$('#addallergymodal').modal('toggle');
            });
          }
        });
  });  
 </script>