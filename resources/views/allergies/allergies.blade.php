<!-- top action nav -->
<div class="top_nav" style="height: 37px;margin-bottom: 0px;background: #ededed;    border-bottom: 0px;">
  <div class="nav_menu"  style="height: 37px;margin-bottom: 10px;background: #ededed;margin-top: -10px;    border-bottom: 0px;">
    <nav> 

      <ul class="nav navbar-nav navbar-left">
        <li class="">
      		<input type="text" name="search" placeholder="Search...">
      		<i class="fa fa-search"></i>
        </li>
      </ul>
      <div class="col-xs-4 pull-right">
      	<a href="#" class="dropdown-toggle pull-right action_btns hidden" data-toggle="dropdown" role="button" aria-expanded="false"><h5>Active <i class=" c-orange glyphicon glyphicon glyphicon-chevron-down"></i></h5></a>
	        <ul class="dropdown-menu" role="menu">
	          <li><a href="#">Active</a>
	          </li>
	          <li><a href="#">Inactive</a>
	          </li>
	        </ul>
      	<a href="#" class="pull-right action_btns addBtn"><h5>Add Brand <i class=" c-orange glyphicon glyphicon-remove-circle trans-rt-45"></i></h5></a>
      	<a href="#" class="pull-right action_btns addTransBtn"><h5>Add Transaction <i class=" c-orange glyphicon glyphicon-remove-circle trans-rt-45"></i></h5></a>
      </div>
    </nav>
  </div>
</div>
<!-- /top action nav -->

<div class="right_col p15 pt30" role="main">
	
	<div class="row p15 mt50 ">
		<div class="col-md-12">
			<table class="table jambo_table theme-table theme-table-edit" id="allergy-table">
				<thead>
					<tr>
						<th>#</th>
						<th>Name</th>
						<th>Description</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		</div>
	</div>

</div>

<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;" id="addmodal"></div>
<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;" id="editmodal"></div>

<script type="text/javascript">
	$(function(){

		$('#allergy-table').DataTable({
		    bProcessing: true,
		    bServerSide: false,
		    sServerMethod: "GET",
			'ajax': '/get-all-allergies',
			searching: false, 
			paging: true, 
			filtering:false, 
			bInfo: true,
			language:{
				"paginate": {
			        "next":       "<i class='fa fa-chevron-right'></i>",
			        "previous":   "<i class='fa fa-chevron-left'></i>"
			    }
			},
			"columns": [
	            {data: 'id', name: 'id', className: 'col-md-1 text-center', searchable: false, 	sortable: true},
	            {data: 'brand_name', 	name: 'brand_name', className: 'col-md-11 text-left', 	searchable: true,	sortable: true},
	            {data: 'actions', 	name: 'brand_name', className: ' text-left', 	searchable: true,	sortable: true},
	        ]
		});

		$(".addBtn").click(function(x){ 
			var that = this;
			$("#addmodal").modal();
			$.ajax({
				url: '/brands/create',					
				success: function(data) {
					$("#addmodal").html(data);
				}
			});	
		});
		
		$(document).off('click', ".editBtn").on('click', ".editBtn", function(x){ 
			var that = this;
			$("#editmodal").modal();
			$.ajax({
				url: '/brands/'+that.dataset.id+'/edit',					
				success: function(data) {
					$("#editmodal").html(data);
				}
			});	
		});

	});
	
</script>