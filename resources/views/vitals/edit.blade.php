<div class="modal-dialog modal-md">
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button>
      <h4 class="modal-title">Add Vital Form</h4>
    </div>
    {!! Form::open(array('url' => url('/vitals/'.$vitals->id), 'method' => 'PATCH', 'id' => 'edit-vitals-form')) !!}
    <div class="modal-body">
      <input type="hidden" name="patient_id" value="{{ $patient_id }}"> 
      <div class="form-group mb10">
        <label for="date">Date</label>
        <input id="date" name="date" type="date" class="form-control" id="single_cal4" placeholder="Date" value="{{ $vitals->date }}" required>
      </div>
      <div class="form-group mb10">
        <label for="weight">Weight</label>
        <input id="weight" name="weight" type="text" class="form-control" placeholder="Weight" required value="{{ $vitals->weight }}">
      </div>
      <div class="form-group mb10">
        <label for="temperature">Temperature</label>
        <input id="temperature" name="temperature" type="text" class="form-control" placeholder="Temperature" required value="{{ $vitals->temperature }}">
      </div>
      <div class="form-group mb10">
        <label for="pulse">Pulse</label>
        <input id="pulse" name="pulse" type="text" class="form-control" placeholder="Pulse" required value="{{ $vitals->pulse }}">
      </div>
      <div class="form-group mb10">
        <label for="blood_pressure">Blood Pressure</label>
        <input id="blood_pressure" name="blood_pressure" type="text" class="form-control" placeholder="Blood Pressure" required value="{{ $vitals->blood_pressure }}">
      </div>
      <div class="form-group mb10">
        <label for="respiration">Respiration</label>
        <input id="respiration" name="respiration" type="text" class="form-control" placeholder="Respiration" required value="{{ $vitals->respiration }}">
      </div>
      <div class="form-group mb10">
        <label for="pain">Pain</label>
        <input id="pain" name="pain" type="text" class="form-control" placeholder="Pain" required value="{{ $vitals->pain }}">
      </div>

    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        {!! Form::submit('Submit', ['class' => 'btn submit-btn btn-primary btn-gradient pull-right']) !!}
      {!! Form::close() !!}
    </div>

  </div>
</div>

 <!-- Laravel Javascript Validation -->
 <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>

 {!! JsValidator::formRequest('App\Http\Requests\AddVitalsRequest', '#edit-vitals-form') !!}

<script type="text/javascript">
  $(function(){
      $("#edit-vitals-form").on('submit', function(e){
          e.preventDefault();
          var that = this;
          if($("#edit-vitals-form").valid()){
            $(".submit-btn").addClass("disabled");
            $('button[type=submit], input[type=submit]').prop('disabled',true);
            $.ajax({
                type: 'PATCH',
                url: '/vitals/'+{{$vitals->id}},
                data: $('#edit-vitals-form').serialize(),
                dataType: 'json',
                success: function(data){
                  $('button[type=submit], input[type=submit]').prop('disabled',false);
                  $(".submit-btn").removeClass("disabled");
                }
            }).done(function(data) {
              if(data.success){
               // $("#vitals-table").DataTable().ajax.url( '/get-vitals/'+$("[name='patient_id']").val() ).load();
                getVitals('.vitals_list');
                notiff('Success!', data.msg,'success'); //title, msg, type
                $('#editvitalmodal').modal('toggle');
              }else{
                notiff('Error!', data.msg,'error'); //title, msg, type
               // $('#addallergymodal').modal('toggle');
              }
            }).error(function(data) {
                notiff('Error!', data.msg, 'warning'); //title, msg, type
                //$('#addallergymodal').modal('toggle');
            });
          }
        });
  });  
 </script>