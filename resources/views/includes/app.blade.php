<!DOCTYPE html>
<html>
<head>
<title>Cliniq</title>
<!-- for-mobile-apps -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Cliniq" />
<link rel="shortcut icon" href="/tmpl/images/cliniq-logo.png">
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
		function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- //for-mobile-apps -->
<link href="/tmpl/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link rel="stylesheet" href="/tmpl/css/jquery-ui.css" />
<link href="/tmpl/css/popuo-box.css" rel="stylesheet" type="text/css" media="all"/>
<link href="/tmpl/css/style.css" rel="stylesheet" type="text/css" media="all" />
<!-- js -->
<script type="text/javascript" src="/tmpl/js/jquery-2.1.4.min.js"></script>
<script type="text/javascript" src="/tmpl/js/numscroller-1.0.js"></script>

{!! Html::style(url('dst/vendors/font-awesome/css/font-awesome.min.css')) !!}

<!-- //js -->


<!-- fonts 
<link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Viga' rel='stylesheet' type='text/css'>-->

	<!-- start-smoth-scrolling -->
		<script type="text/javascript" src="/tmpl/js/move-top.js"></script>
		<script type="text/javascript" src="/tmpl/js/easing.js"></script>
		<script type="text/javascript">
			jQuery(document).ready(function($) {
				$(".scroll").click(function(event){		
					var link = $(this).attr('href');

					event.preventDefault();
					$('html,body').animate({scrollTop:$(""+link).offset().top},1000);
				});
			});
		</script>
	<!-- start-smoth-scrolling -->

<!--start-date-piker-->
	<script src="/tmpl/js/jquery-ui.js"></script>
		<script>
			$(function() {
				$( "#datepicker,#datepicker1" ).datepicker();
			});
		</script>
<!--/End-date-piker-->
	<script src="/tmpl/js/responsiveslides.min.js"></script>
			<!--animate-->
<link href="/tmpl/css/animate.css" rel="stylesheet" type="text/css" media="all">
<script src="/tmpl/js/wow.min.js"></script>
	<script>
		 new WOW().init();
	</script>
<!--//end-animate-->
<!-- <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet"> -->
</head>
<body>
<!-- header -->

 @yield('content')

<a href="javascript:" id="return-to-top"><i class="fa fa-arrow-up"></i></a>
<div class="footer_">
	<div class=" ">
		<div class="col-md-6"  >
			<p>Copyright &copy; 2017 iSiteSolution Consultancy. All rights reserved.</p>
		</div>
		<div class="col-md-6 text-right i_cons">
			<span><a href="#"><i class="fa fa-facebook"></i></a></span>
			<span><a href="#"><i class="fa fa-twitter"></i></a></span>
			<span><a href="#"><i class="fa fa-instagram"></i></a></span>
		</div>	
	</div>
</div>
<!-- <footer class=" footer col-md-12">
	<div class=" ">
		<div class="col-md-6"  >
			<p>Copyright &copy; 2017 iSiteSolution Consultancy. All rights reserved.</p>
		</div>
		<div class="col-md-6 text-right i_cons">
			<span><a href="#"><i class="fa fa-facebook"></i></a></span>
			<span><a href="#"><i class="fa fa-twitter"></i></a></span>
			<span><a href="#"><i class="fa fa-instagram"></i></a></span>
		</div>	
	</div>
</footer> -->
<!-- //login -->
<script type="text/javascript" src="/tmpl/js/bootstrap-3.1.1.min.js"></script>
<script type="text/javascript">
	// ===== Scroll to Top ==== 
	$(window).scroll(function() {
	    if ($(this).scrollTop() >= 50) {        // If page is scrolled more than 50px
	        $('#return-to-top').fadeIn(200);    // Fade in the arrow
	    } else {
	        $('#return-to-top').fadeOut(200);   // Else fade out the arrow
	    }
		
	});
	$('#return-to-top').click(function() {      // When arrow is clicked
	    $('body,html').animate({
	        scrollTop : 0                       // Scroll to top of body
	    }, 500);
	});

	$(".contact-us-side").click(function(){
      	
	    if($(window).width() < 716){
			var value = $('.contact-us-side-form').css('right') === '-55px' ? "-440px" : '-55px';
	      	var value2 = $('.contact-us-side').css('right') === '194px' ? "-55px" : '194px';
		      $('.contact-us-side-form').animate({
		          right: value
		      });
		      $('.contact-us-side').animate({
		          right: value2
		      }); 
	    	 
	    }else{
	    	var value = $('.contact-us-side-form').css('right') === '-55px' ? "-440px" : '-55px';
	      	var value2 = $('.contact-us-side').css('right') === '335px' ? "-55px" : '335px';
		      $('.contact-us-side-form').animate({
		          right: value
		      });
		      $('.contact-us-side').animate({
		          right: value2
		      }); 
	    }
	});
	$(".our-clients-side").click(function(){ 
      	
	    if($(window).width() < 769){
	   	
	    	var value = $('.our-clients-side-list').css('left') === '-640px' ? "0px" : '-640px'; 
	      	var value2 = $('.our-clients-side').css('left') === '169px' ? "-55px" : '169px'; 
		      $('.our-clients-side-list').animate({
		          left: value
		      }); 
		      $('.our-clients-side').animate({
		          left: value2
		      }); 
	    }else{
	    	var value = $('.our-clients-side-list').css('left') === '-640px' ? "0px" : '-640px'; 
	      	var value2 = $('.our-clients-side').css('left') === '590px' ? "-55px" : '590px'; 
		      $('.our-clients-side-list').animate({
		          left: value
		      }); 
		      $('.our-clients-side').animate({
		          left: value2
		      }); 
	   }

	});
	
		$('.banner1').css('min-height', $(window).height()-140+'px');
		$('.banner2').css('min-height', $(window).height()-140+'px');
		$(window).resize(function(){
			$('.banner1').css('min-height', $(window).height()-140+'px');
			$('.banner2').css('min-height', $(window).height()-140+'px');
			
		/*	if($(window).width() < 716){
			    var str = $(".footer_").css('min-height');
				
				var h_ = parseFloat(str.substring(0,2)); 
				$('.footer_').css('top', $(window).height()-h_+'px');
			}else{
			}

			if($(window).width() > 716){
				$('.footer_').css('bottom', '0px !important');			
			}*/
		});	
		$(document).ready(function(){
			if($(window).width() < 716){
			    var str = $(".footer_").css('min-height');
				
				var h_ = parseFloat(str.substring(0,2)); 
				$('.footer_').css('top', $(window).height()-h_+'px');
			}else{
			}

			if($(window).width() > 716){
				$('.footer_').css('bottom', '0px !important');			
			}
		});
</script>
<style type="text/css">
    @media (max-width: 995px) {
    	.footer{
    		text-align: center !important; 
    	}
    	.footer_{
    		text-align: center !important;
    	}
    	.i_cons{
    		text-align: center !important;     
    		padding-top: 10px;
    		padding-bottom: 10px;
    	}
    }

    @media (max-width: 1024px) {
    	.upper_h > span{
    		padding-bottom: 7px;
    	}
    }
    @media (max-width: 640px) {
    	.upper_h > span{
    		padding-bottom: 4px;
    	}
    }
    @media (max-width: 415px) {
    	.upper_h > span{
    		padding-bottom: 2px;
    	} 
    	.our-clients-side {
		    background: rgba(242, 108, 79, 0.5);
		}
		.contact-us-side-form {
		    background: rgba(0, 0, 0, 0.5);
		    z-index: 9999999;
		    position: fixed;
		    top: 105px;
		    padding: 20px;
		    font-weight: normal;
		    color: #ffffff;
		    text-align: left;
		    right: -440px;
		    width: 300px;
		    padding: 10px;
		    padding-left: 10px;
		    padding-right: 20px;
		}
		.contact-us-side-form p{
			    width: 85%;
		}

		.contact-us-side-form > form input{
		  padding-top: 5px;
		  margin-top: 0px;
		  padding-bottom: 5px;
		  margin-bottom: 3px;
		  height: 30px;
		}
		.contact-us-side-form .form-group{
			margin-bottom: 5px;
		}

    }

    @media (max-width: 768px) {
    	.our-clients-side-list{
    		width: 220px;
    	}
    	.our-clients-side-list .col-sm-4{
    		margin-bottom: 15px;
    	}
    }
    .footer_{
    	float: left;
	    width: 100%;
	    background: black;
	    position: absolute;
	    background-color: #108ec4;
	    min-height: 35px;
	    color: white;
	    padding-top: 7px;
    }
</style>
</body>
</html>

