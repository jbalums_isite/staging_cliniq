@include('includes.admin-page.head')
  @include('includes.admin-page.left_sidebar')
  @include('includes.admin-page.topbar')
  <!-- page content -->
  	<div class="pg_content">		
    	@yield('content')  	
  	</div>
  <!-- /page content -->
@include('includes.admin-page.scripts')

@yield('script_content')  	

@include('includes.admin-page.footer')
