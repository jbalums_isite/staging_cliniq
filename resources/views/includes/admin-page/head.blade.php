<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="pragma" content="no-cache" />
    <link rel="shortcut icon" href="/tmpl/images/cliniq-logo.png">

    <title>Cliniq | Admin</title>

    <!-- Bootstrap -->
    {!! Html::style(url('dst/vendors/bootstrap/dist/css/bootstrap.min.css')) !!}
    <!-- Font Awesome -->
    {!! Html::style(url('dst/vendors/font-awesome/css/font-awesome.min.css')) !!}
    <!-- NProgress -->
    {!! Html::style(url('dst/vendors/nprogress/nprogress.css')) !!}
    <!-- iCheck -->
    {!! Html::style(url('dst/vendors/iCheck/skins/flat/green.css')) !!}
    <!-- bootstrap-progressbar -->
    {!! Html::style(url('dst/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css')) !!}
    <!-- JQVMap -->
    {!! Html::style(url('dst/vendors/jqvmap/dist/jqvmap.min.css')) !!}
    <!-- bootstrap-daterangepicker -->
    {!! Html::style(url('dst/vendors/bootstrap-daterangepicker/daterangepicker.css')) !!}
    <!-- Dropzone.js -->
    {!! Html::style(url('dst/vendors/dropzone/dist/min/dropzone.min.css')) !!}

    {!! Html::style(url('dst/vendors/lightgallery/css/lightgallery.css')) !!}


    {!! Html::style(url('dst/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css')) !!}
    
    {!! Html::style(url('dst/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css')) !!}
    {!! Html::style(url('dst/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css')) !!}
    {!! Html::style(url('dst/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css')) !!} 
    {!! Html::style(url('datatable/responsive.dataTables.min.css')) !!}
    
    <!-- Custom Theme Style -->
    {!! Html::style(url('dst/build/css/custom.css')) !!}
    {!! Html::style(url('dst/css/cliniq_admin_custom.css')) !!}
    {!! Html::style(url('css/loader.css')) !!}
     
    <!-- PNotify -->
    {!! Html::style(url('dst/vendors/pnotify/dist/pnotify.css')) !!}
    {!! Html::style(url('dst/vendors/pnotify/dist/pnotify.buttons.css')) !!}
    {!! Html::style(url('dst/vendors/pnotify/dist/pnotify.nonblock.css')) !!}

    {!! Html::style(url('dst/vendors/select2/dist/css/select2.css')) !!}


<!--     {!! Html::style(url('jquery.dataTables.min.css')) !!} -->
    <!-- <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet"> -->
</head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">