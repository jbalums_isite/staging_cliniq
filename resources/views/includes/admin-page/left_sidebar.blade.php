<div class="col-md-3 left_col">
  <div class="left_col scroll-view">
    <div class="navbar nav_title" style="border: 0;">
      <a class="site_title" href="{{ url('/'.\App\ClinicAccounts::find(Auth::user()->clinic_id)->prefix) }}"><img src="/tmpl/images/logo-white.png"></a>
    </div>

    <div class="clearfix"></div>

    <!-- menu profile quick info -->
    <div class="profile clearfix">
      <div class="profile_pic  circle-pic"> 
          <img src="/dst/images/user.png" alt="..." class="img-circle profile_img"> 
      </div>
      <div class="profile_info">
        <span>Welcome,</span>
        <h2>
          {{ \App\People::find(Auth::user()->people_id)->title.' '.\App\People::find(Auth::user()->people_id)->firstname.' '.\App\People::find(Auth::user()->people_id)->lastname }}
        </h2>
      </div>
    </div>
    <!-- /menu profile quick info -->

    <!-- sidebar menu -->
    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
      <div class="menu_section active">
        <ul class="nav side-menu">
          <li><a><i class="fa fa-home"></i> Home <span class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu">
              <li><a href="/patients">Patient Records</a></li>
              <li class="hidden"><a class="addPatientBtn">Add New Patient</a></li>
            </ul>
          </li>
          <li><a><i class="fa fa-cubes"></i> Medicine <span class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu">
              <li><a href="/medicines">View Medicine</a></li>
              <li  class="hidden"><a class="addMedicinesBtn">Create Medicine</a></li>
            </ul>
          </li>
          <li><a><i class="fa fa-stethoscope"></i> Services <span class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu">
              <li><a href="/services">View Services</a></li>
              <li class="hidden"><a class="addServiceBtn">Create Services</a></li>
            </ul>
          </li>
          <li><a ><i class="fa fa-cubes"></i> Products <span class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu">
              <li><a href="/products">View Products</a></li>
              <li class="hidden"><a class="addProductBtn">Create Products</a></li> 
              <li><a href="/inventories">Stocks</a></li>
              <li><a href="/manufacturers">Manufacturers</a></li>
              <li><a href="/brands">Brands</a></li>
            </ul>
          </li>
          <li><a><i class="fa fa-calendar"></i> Schedules <span class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu">
              <li><a href="/schedules">View Schedule</a></li>
              <li class="hidden"><a>Create Schedule</a></li>
            </ul>
          </li>
          <li class="hidden"><a><i class="fa fa-folder-open-o"></i> Transactions <span class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu">
              <li><a href="/transactions">Check-out Patient</a></li>
            </ul>
          </li>
          <li><a><i class="fa fa-copy"></i> Forms<span class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu">
              <li><a href="/referral_form">Referral Form</a></li>
              <li><a href="/request_form">Request Form</a></li>
              <li><a href="/custom_form">Custom Form</a></li>
            </ul>
          </li>
          <li><a><i class="fa fa-file-text-o"></i> Reports <span class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu">
              <li><a href="/daily_sales_reports">Daily</a></li>
              <li><a href="/monthly_sales_reports">Monthly</a></li>
              <li><a href="/quarterly_sales_reports">Quarterly</a></li>
              <li><a href="/annualy_sales_reports">Annually</a></li>
            </ul>
          </li>
          <li><a><i class="fa fa-cog"></i> Settings <span class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu">
              <li><a href="/users">Users</a></li> 
              <li><a href="/allergieslist">Allergies</a></li>
              <li class="hidden"><a href="/transactions">Transactions</a></li>
            </ul>
          </li>
        </ul>
      </div>

    </div>
    <!-- /sidebar menu -->
      <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden">
              <a data-toggle="tooltip" data-placement="top" title="" data-original-title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="" data-original-title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="" data-original-title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="" data-original-title="Logout">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
            </div>
            <!-- /menu footer buttons -->
  </div>
</div>