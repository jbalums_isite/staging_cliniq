<!-- top navigation -->
<div class="top_nav">
  <div class="nav_menu">
    <nav>
      <div class="nav toggle">
        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
      </div>

      <ul class="nav navbar-nav navbar-right">
        <li class="">
          <a href="javascript:;" class="user-profile dropdown-toggle circle-pic" data-toggle="dropdown" aria-expanded="false">
            @if(Auth::user()->icon)
              <img src="{{asset('uploads/'.Auth::user()->icon)}}" alt=""> 
            @else
              <img src="/dst/images/user.png" alt=""> 
            @endif

            {{ \App\People::find(Auth::user()->people_id)->title.' '.\App\People::find(Auth::user()->people_id)->firstname.' '.\App\People::find(Auth::user()->people_id)->lastname }} 
            <span class=" fa fa-angle-down"></span>
          </a>
          <ul class="dropdown-menu dropdown-usermenu pull-right">
            <li><a href="{{ url('/logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">

            <i class="fa fa-sign-out pull-right"></i> Log Out</a>
             <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>

                                       
            </li>
          </ul>
        </li>
      </ul>
    </nav>
  </div>
</div>
<!-- /top navigation -->
