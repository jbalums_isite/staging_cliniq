   <!-- footer content -->
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    {!! Html::script(url('dst/vendors/jquery/dist/jquery.min.js')) !!}
    <!-- Bootstrap -->
    {!! Html::script(url('dst/vendors/bootstrap/dist/js/bootstrap.min.js')) !!}
    <!-- FastClick -->
    {!! Html::script(url('dst/vendors/fastclick/lib/fastclick.js')) !!}
    <!-- NProgress -->
    {!! Html::script(url('dst/vendors/nprogress/nprogress.js')) !!}
    <!-- NProgress -->
    {!! Html::script(url('dst/vendors/dropzone/dist/min/dropzone.min.js')) !!}
    <!-- Chart.js -->
    {!! Html::script(url('dst/vendors/Chart.js/dist/Chart.min.js')) !!}
    <!-- gauge.js -->
    {!! Html::script(url('dst/vendors/gauge.js/dist/gauge.min.js')) !!}
    <!-- bootstrap-progressbar -->
    {!! Html::script(url('dst/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js')) !!}
    <!-- iCheck -->
    {!! Html::script(url('dst/vendors/iCheck/icheck.min.js')) !!}
    <!-- Skycons -->
    {!! Html::script(url('dst/vendors/skycons/skycons.js')) !!}
    <!-- Flot -->
    {!! Html::script(url('dst/vendors/Flot/jquery.flot.js')) !!}
    {!! Html::script(url('dst/vendors/Flot/jquery.flot.pie.js')) !!}
    {!! Html::script(url('dst/vendors/Flot/jquery.flot.time.js')) !!}
    {!! Html::script(url('dst/vendors/Flot/jquery.flot.stack.js')) !!}
    {!! Html::script(url('dst/vendors/Flot/jquery.flot.resize.js')) !!}
    <!-- Flot plugins -->
    {!! Html::script(url('dst/vendors/flot.orderbars/js/jquery.flot.orderBars.js')) !!}
    {!! Html::script(url('dst/vendors/flot-spline/js/jquery.flot.spline.min.js')) !!}
    {!! Html::script(url('dst/vendors/flot.curvedlines/curvedLines.js')) !!}
    <!-- DateJS -->
    {!! Html::script(url('dst/vendors/DateJS/build/date.js')) !!}
    <!-- JQVMap -->
    {!! Html::script(url('dst/vendors/jqvmap/dist/jquery.vmap.js')) !!}
    {!! Html::script(url('dst/vendors/jqvmap/dist/maps/jquery.vmap.world.js')) !!}
    {!! Html::script(url('dst/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js')) !!}
    <!-- bootstrap-daterangepicker -->
    {!! Html::script(url('dst/vendors/moment/min/moment.min.js')) !!}
    {!! Html::script(url('dst/vendors/bootstrap-daterangepicker/daterangepicker.js')) !!}

    <!-- Custom Theme Scripts --> 
    {!! Html::script(url('dst/build/js/custom.js')) !!}

    <!-- Ajax Scripts -->
    {!! Html::script(url('/js/app/ajax.js')) !!}

    <!-- Datatable Script -->    
    {!! Html::script(url('dst/vendors/datatables.net/js/jquery.dataTables.min.js')) !!}
    {!! Html::script(url('dst/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js')) !!}
    {!! Html::script(url('dst/vendors/datatables.net-responsive/js/dataTables.responsive.min.js')) !!}
    {!! Html::script(url('dst/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js')) !!}


    <!-- PNotify -->
    {!! Html::script(url('dst/vendors/pnotify/dist/pnotify.js')) !!}       
    {!! Html::script(url('dst/vendors/pnotify/dist/pnotify.buttons.js')) !!}
    {!! Html::script(url('dst/vendors/pnotify/dist/pnotify.nonblock.js')) !!}
     
    {!! Html::script(url('dst/vendors/select2/dist/js/select2.js')) !!}



    {!! Html::script(url('dst/vendors/lightgallery/js/lightgallery.min.js')) !!}
    {!! Html::script(url('dst/vendors/lightgallery/js/jquery.mousewheel.min.js')) !!}
    
    {!! Html::script(url('dst/vendors/lightgallery/js/lightgallery.js')) !!}
    {!! Html::script(url('dst/vendors/lightgallery/js/lg-fullscreen.js')) !!}
    {!! Html::script(url('dst/vendors/lightgallery/js/lg-thumbnail.js')) !!}
    {!! Html::script(url('dst/vendors/lightgallery/js/lg-video.js')) !!}
    {!! Html::script(url('dst/vendors/lightgallery/js/lg-autoplay.js')) !!}
    {!! Html::script(url('dst/vendors/lightgallery/js/lg-zoom.js')) !!}
    {!! Html::script(url('dst/vendors/lightgallery/js/lg-hash.js')) !!}
    {!! Html::script(url('dst/vendors/lightgallery/js/lg-pager.js')) !!}
    <script type="text/javascript">
        @if(Request::segment(1) != 'print')
            history.pushState({}, null, '/{{  \App\ClinicAccounts::find(Auth::user()->clinic_id )->prefix }}');
        @endif 

        $(".child_menu > li").click(function(){
            $(".child_menu > li").removeClass('active');
            $(this).addClass('active');
        });
        $(document).off('click', '.addTransBtn').on('click', '.addTransBtn', function(){ 
            var that = this;
            $("#addtransmodal").modal();
            $.ajax({
                url: '/transactions/create',                    
                success: function(data) {
                    $("#addtransmodal").html(data);
                }
            }); 

        });

        $(document).off('click','.action_btns').on('click', '.action_btns', function(){
            
            if($(".left_col").width() == 70){
                $("#menu_toggle").click()
            }
    
        })
       /* $(function(){ 

            $('.left_col').css('height', ($('body').height() ));

        })*/
 
       /* $(".addTransBtn").click(function(x){  
            var that = this;
            $("#addtransmodal").modal();
            $.ajax({
                url: '/transactions/create',                    
                success: function(data) {
                    $("#addtransmodal").html(data);
                }
            }); 
        });
        $(function(){

        })*/
    </script>
