<div class="modal-dialog modal-md">
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button>
      <h4 class="modal-title">Add Product Form</h4>
    </div>
    {!! Form::open(array('url' => url('/products'), 'method' => 'POST', 'id' => 'add-products-form')) !!}
    <div class="modal-body">
      <div class="form-group">
        <label for="brand_id">Brand Name</label>
         {!! Form::select('brand_id', $brands, null,
            ['id'=>'brand_id','class'=> 'form-gui form-control', 'placeholder'=>'Select Brand']) !!}
      </div>
      <div class="form-group">
        <label for="name">Product Name</label>
        <input id="name" name="name" type="text" class="form-control" placeholder="Name" required>
      </div>
      <div class="form-group">
        <label for="description">Desc/SIG(Prescription)</label>
        <textarea id="description" name="description" class="form-control" placeholder="Prescription" rows="3" required></textarea>
      </div>
      <div class="form-group">
        <label for="manufacturer">Manufacturer</label>
         {!! Form::select('manufacturer_id', $manufacturers, null,
            ['id'=>'manufacturer','class'=> 'form-gui form-control', 'placeholder'=>'Select Manufacturer']) !!}
      </div>
      <div class="form-group">
        <label for="price">Price</label>
        <input id="price" name="price" type="number" min="1" class="form-control" placeholder="price" required>
      </div>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        {!! Form::submit('Submit', ['class' => 'btn submit-btn btn-primary btn-gradient pull-right']) !!}
      {!! Form::close() !!}
    </div>

  </div>
</div>


 <!-- Laravel Javascript Validation -->
 <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>

 {!! JsValidator::formRequest('App\Http\Requests\AddProductRequest', '#add-products-form') !!}

<script type="text/javascript">
  $(function(){
      $("#brand_id").selectize({
          create: false,
          sortField: 'text'
      });
      $("#manufacturer").selectize({
          create: false,
          sortField: 'text'
      });
      $("#add-products-form").on('submit', function(e){
          e.preventDefault();
          var that = this;
          if($("#add-products-form").valid()){
            $(".submit-btn").addClass("disabled");
            $('button[type=submit], input[type=submit]').prop('disabled',true);
            $.ajax({
                type: 'POST',
                url: '/products',
                data: $('#add-products-form').serialize(),
                dataType: 'json',
                success: function(data){
                  $('button[type=submit], input[type=submit]').prop('disabled',false);
                  $(".submit-btn").removeClass("disabled");
                }
            }).done(function(data) {
              if(data.success){
                  $("#products-table").DataTable().ajax.url( '/get-products' ).load();
                notiff('Success!', data.msg,'success'); //title, msg, type
                $('#addmodal').modal('toggle');
              }else{
                notiff('Error!', data.msg,'error'); //title, msg, type
                $('#addmodal').modal('toggle');
              }
            }).error(function(data) {
                notiff('Error!', data.msg, 'warning'); //title, msg, type
                $('#addmodal').modal('toggle');
            });
          }
        });
    $("#price").focusout(function(){
      $(this).val(parseFloat($(this).val()).toFixed(2));
    })
  });  
 </script>