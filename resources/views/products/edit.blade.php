<div class="modal-dialog modal-md">
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button>
      <h4 class="modal-title">Edit Product Form</h4>
    </div>
    {!! Form::open(array('url' => url('/products'.$product->id), 'method' => 'PATCH', 'id' => 'edit-products-form')) !!}
    <div class="modal-body">
      <div class="form-group">
        <label for="brand_id">Brand Name</label>
         {!! Form::select('brand_id', $brands, $product->brand_id,
            ['id'=>'brand_id','class'=> 'form-gui form-control', 'placeholder'=>'Select Brand']) !!}
      </div>
      <div class="form-group">
        <label for="name">Product Name</label>
        <input id="name" name="name" type="text" class="form-control" placeholder="Name" required value="{{$product->name}}">
      </div>
      <div class="form-group">
        <label for="description">Desc/SIG(Prescription)</label>
        <textarea id="description" name="description" class="form-control" placeholder="Prescription" rows="3" required>{{$product->description}}</textarea>
      </div>
      <div class="form-group">
        <label for="manufacturer">Manufacturer</label>
         {!! Form::select('manufacturer_id', $manufacturers, $product->manufacturer_id,
            ['id'=>'manufacturer','class'=> 'form-gui form-control', 'placeholder'=>'Select Manufacturer']) !!}
      </div>
      <div class="form-group">
        <label for="price">Price</label>
        <input id="price" name="price" type="number" min="1" class="form-control" placeholder="price" required value="{{$product->price}}">
      </div>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        {!! Form::submit('Submit', ['class' => 'btn submit-btn btn-primary btn-gradient pull-right']) !!}
      {!! Form::close() !!}
    </div>

  </div>
</div>


 <!-- Laravel Javascript Validation -->
 <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>

 {!! JsValidator::formRequest('App\Http\Requests\AddProductRequest', '#edit-products-form') !!}

<script type="text/javascript">
  $(function(){
    $("#brand_id").selectize({
          create: false,
          sortField: 'text'
      });
      $("#manufacturer").selectize({
          create: false,
          sortField: 'text'
      });
      $("#edit-products-form").on('submit', function(e){
          e.preventDefault();
          var that = this;
          if($("#edit-products-form").valid()){
            $(".submit-btn").addClass("disabled");
            $('button[type=submit], input[type=submit]').prop('disabled',true);
            $.ajax({
                type: 'PATCH',
                url: '/products/'+{{$product->id}},
                data: $('#edit-products-form').serialize(),
                dataType: 'json',
                success: function(data){
                  $('button[type=submit], input[type=submit]').prop('disabled',false);
                  $(".submit-btn").removeClass("disabled");
                }
            }).done(function(data) {
              if(data.success){
                $("#products-table").DataTable().ajax.url( '/get-products' ).load();
                notiff('Success!', data.msg,'success'); //title, msg, type
                $('#editmodal').modal('toggle');
              }else{
                notiff('Error!', data.msg,'error'); //title, msg, type
                $('#editmodal').modal('toggle');
              }
            }).error(function(data) {
                notiff('Error!', data.msg, 'warning'); //title, msg, type
                $('#editmodal').modal('toggle');
            });
          }
        });
    $("#price").focusout(function(){
      $(this).val(parseFloat($(this).val()).toFixed(2));
    })
  });  
 </script>