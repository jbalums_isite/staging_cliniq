<!-- top action nav -->
<div class="top_nav" style="height: 37px;margin-bottom: 0px;background: #ededed;    border-bottom: 0px;">
  <div class="nav_menu"  style="height: 37px;margin-bottom: 10px;background: #ededed;margin-top: -10px;    border-bottom: 0px;">
    <nav> 

      <ul class="nav navbar-nav navbar-left hidden">
        <li class="">
      		<input type="text" name="search" placeholder="Search...">
      		<i class="fa fa-search"></i>
        </li>
      </ul>
      <div class="col-md-6 pull-right">
      	<a href="#" class="dropdown-toggle pull-right action_btns hidden" data-toggle="dropdown" role="button" aria-expanded="false"><h5>Active <i class=" c-orange glyphicon glyphicon glyphicon-chevron-down"></i></h5></a>
	        <ul class="dropdown-menu" role="menu">
	          <li><a href="#">Active</a>
	          </li>
	          <li><a href="#">Inactive</a>
	          </li>
	        </ul>

      	<a href="#" class="pull-right action_btns addStocksBtn"><h5>Add Stocks <i class=" c-orange glyphicon glyphicon-remove-circle trans-rt-45"></i></h5></a>
      </div>
    </nav>
  </div>
</div>
<!-- /top action nav -->

<div class="right_col p15 pt30" role="main">
	
	<div class="row p15 mt50">
		<table class="table jambo_table theme-table " id="stocks-table">
			<thead>
				<tr>
					<th>#</th>
					<th>Name</th>
					<th>Description</th>
					<th>Manufacturer</th>
					<th>Price</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>1</td>
					<td>Biogesic</td>
					<td>500mg Paracetamol</td>
					<td>Manfacturer</td>
					<td>5.00</td>
				</tr>
			</tbody>
		</table>
	</div>

</div>

<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;" id="addmodal"></div>

<script type="text/javascript">
	$(function(){

		$('#s-table').DataTable({
		    bProcessing: true,
		    bServerSide: false,
		    sServerMethod: "GET",
			'ajax': '/get-products',
			searching: false, 
			paging: true, 
			filtering:false, 
			bInfo: true,
			language:{
				"paginate": {
			        "next":       "<i class='fa fa-chevron-right'></i>",
			        "previous":   "<i class='fa fa-chevron-left'></i>"
			    }
			},
			"columns": [
	            {data: 'id', name: 'id', className: ' text-center', searchable: false, sortable: true},
	            {data: 'name', name: 'name', className: ' text-left', searchable: true,	sortable: true},
	            {data: 'description', name: 'description', 	className: ' text-left', searchable: true, 	sortable: true},
	            {data: 'manufacturer', name: 'manufacturer', 	className: ' text-left', searchable: true, 	sortable: true},
	            {data: 'price', name: 'price', 	className: 'text-right', searchable: true, sortable: true},
	        ]
		});

		$(".addProductBtn").click(function(x){ 
			var that = this;
			$("#addmodal").modal();
			$.ajax({
				url: '/products/create',					
				success: function(data) {
					$("#addmodal").html(data);
				}
			});	
		});
		
	});
	
</script>